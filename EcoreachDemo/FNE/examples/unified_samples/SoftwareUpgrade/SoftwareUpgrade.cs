// <copyright file="SoftwareUpgrade.cs" company="Flexera Software LLC">
//     Copyright (c) 2011-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using DemoUtilities;
using FlxDotNetClient;
using IdentityData;

/****************************************************************************
    SoftwareUpgrade.cs

    This example program allows you to:
    1. Process the license file of your choice.
    2. Check for product updates.
    3. Determine whether the product update is for a licensed feature.
    4. If a license exists, download the product update.
*****************************************************************************/

namespace SoftwareUpgrade
{
    public static class SoftwareUpgrade
    {
        private static readonly string emptyIdentity =
@"License or product update code requires client identity data,
which you create with pubidutil and printbin -CS.
See the User Guide for more information.";

        private static string bufferFile = null;

        private static string connectProductId = "{77777777-7777-7777-7777-777777777777}";
        private static string connectProductName = "MyProduct";
        private static string connectServerUrl = "http://updates.flexnetoperations.com";

        const string connectProductVersion = "1.0";
        const string connectProductLanguage = "1033";
        const string connectPlatform = null;

        private static ILicensing licensing;
        private static IConnect   connect;

        private static bool ValidateCommandLineArgs(string[] args)
        {
            bool invalidSpec = false;
            bool unknownArg  = false;
            int  lastArgIndex = args.Length - 1;
            string currentArg = null;
            for (int i = 0; i < args.Length && !invalidSpec && !unknownArg; i++)
            {
                currentArg = args[i].ToLowerInvariant();
                switch (currentArg)
                {
                    case "-h":
                    case "-help":
                        Usage(Path.GetFileName(Environment.GetCommandLineArgs()[0]));
                        return false;
                    case "-licensefile":
                        if (!(invalidSpec = (i == lastArgIndex)))
                        {
                            bufferFile = args[++i];
                        }
                        break;
                    case "-server":
                        if (!(invalidSpec = (i == lastArgIndex)))
                        {
                            connectServerUrl = args[++i];
                        }
                        break;
                    case "-productid":
                        if (!(invalidSpec = (i == lastArgIndex)))
                        {
                            connectProductId = args[++i].Trim();
                            try
                            {
                                Guid guid = new Guid(connectProductId);
                            }
                            catch
                            {
                                Util.DisplayErrorMessage(String.Format("Invalid product code specified: {0}", connectProductId));
                                return false;
                            }
                        }
                        break;
                    case "-productname":
                        if (!(invalidSpec = (i == lastArgIndex)))
                        {
                            connectProductName = args[++i];
                        }
                        break;
                    default:
                        unknownArg = true;
                        break;
                }
            }
            if (unknownArg)
            {
                Util.DisplayErrorMessage(String.Format("unknown option: {0}", currentArg));
                Usage(Path.GetFileName(Environment.GetCommandLineArgs()[0]));
                return false;
            }
            else if (invalidSpec)
            {
                Util.DisplayErrorMessage(String.Format("invalid specification for option: {0}", currentArg));
                Usage(Path.GetFileName(Environment.GetCommandLineArgs()[0]));
                return false;
            }
            return true;
        }

        public static void Main(string[] args)
        {
            if (!ValidateCommandLineArgs(args))
            {
                return;
            }

            if (IdentityClient.IdentityData == null || IdentityClient.IdentityData.Length == 0)
            {
                Console.WriteLine(emptyIdentity);
                return;
            }

            try
            {
                string strPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + Path.DirectorySeparatorChar;
			    // Initialize ILicensing interface with identity data using the Windows common document 
                // respository as the trusted storage location and the hard-coded string hostid "1234567890".
                using (licensing = LicensingFactory.GetLicensing(
                          IdentityClient.IdentityData,
                          strPath,
                          "1234567890"))
                {
                    // note: the order in which license sources are added determines the
                    // order in which features are considered for license acquisition
                    // first create buffer license source if specified
                    if (!String.IsNullOrEmpty(bufferFile))
                    {
                        licensing.LicenseManager.AddBufferLicenseSource(bufferFile, bufferFile);
                    }
                    // next add trusted storage and trial licenses
                    licensing.LicenseManager.AddTrustedStorageLicenseSource();
                    licensing.LicenseManager.AddTrialLicenseSource();
                    // if specified, check numer of features loaded from the buffer license file
                    if (String.IsNullOrEmpty(bufferFile))
                    {
                        Util.DisplayInfoMessage("No license file specified.");
                    }
                    else
                    {
                        Util.DisplayInfoMessage(String.Format("Number of features loaded from license file: {0}",
                            licensing.LicenseManager.GetFeatureCollection(bufferFile).Count));
                    }
                    Util.DisplayInfoMessage(String.Format("Number of features loaded from trusted storage: {0}",
                        licensing.LicenseManager.GetFeatureCollection(LicenseSourceOption.TrustedStorage).Count));
                    Util.DisplayInfoMessage(String.Format("Number of features loaded from trials: {0}",
                        licensing.LicenseManager.GetFeatureCollection(LicenseSourceOption.Trials).Count));

                    using (connect = ConnectFactory.GetConnect(IdentityClient.IdentityData))
                    {
                        IProduct product = connect.GetProduct(connectProductId);
                        if (product != null)
                        {
                            Util.DisplayMessage("--------------------------------------------------------------------");
                            Util.DisplayMessage("Newly registered product");
                        }
                        else
                        {
                            product = connect.RegisterProduct(connectProductId, connectProductVersion, connectProductLanguage, connectPlatform);
                            Util.DisplayMessage("--------------------------------------------------------------------");
                            Util.DisplayMessage("Existing registered product");
                        }
                        product.NotificationServer = connectServerUrl;
                        Util.DisplayMessage(String.Format("  Product code        {0}", product.Code));
                        Util.DisplayMessage(String.Format("  Product version     {0}", product.Version));
                        Util.DisplayMessage(String.Format("  Product language    {0}", product.Language));
                        Util.DisplayMessage(String.Format("  Product platform    {0}", product.Platform));
                        Util.DisplayMessage(String.Format("  Notification server {0}", product.NotificationServer));
                        Util.DisplayMessage("--------------------------------------------------------------------");

                        INotificationUpdate update = CheckForUpdates(product);
                        // If an udpate exists for this product
                        if (update != null)
                        {
                            // Acquire and return a license
                            ILicense license = licensing.LicenseManager.Acquire(connectProductName, 1);
                            Util.DisplayInfoMessage(String.Format("Acquired {0} license", connectProductName));
                            license.ReturnLicense();
                            update.Download(null, null, null);
                            Util.DisplayInfoMessage(String.Format("Update downloaded to : {0}", update.DataLocation));
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                HandleException(exc);
            }
        }

        private static INotificationUpdate CheckForUpdates(IProduct product)
        {
            INotificationUpdate update = null;
            string notificationType;
            Util.DisplayInfoMessage("Checking for product updates");
            List<INotification> notifications = product.NotificationCollectionCreate(true);
            Util.DisplayInfoMessage(String.Format("Number of product notifications received: {0}", notifications.Count));
            for (int i = 0; i < notifications.Count; i++)
            {
                INotification notification = notifications[i];
                if (notification is INotificationUpdate)
                {
                    notificationType = "Update";
                }
                else if (notification is INotificationMessage)
                {
                    notificationType = "Message";
                }
                else
                {
                    notificationType = "Unknown";
                }

                Util.DisplayInfoMessage(String.Format("Notification item {0}: type: {1}: product: {2}",
                    i + 1, notificationType, notification.ProductName));
                if (update == null && (notification is INotificationUpdate) && notification.ProductName.Equals(connectProductName))
                {
                    update = notification as INotificationUpdate;
                }
            }
            if (update == null)
            {
                Util.DisplayErrorMessage(String.Format("No update notification item for product {0} found", connectProductName));
            }
            return update;
        }

        private static void HandleException(Exception exc)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(String.Format("{0} encountered: ", exc.GetType()));
            if (exc is FlxException)
            {
                builder.Append(((FlxException)exc).ToString());
            }
            else
            {
                builder.Append(exc.Message);
            }
            Util.DisplayErrorMessage(builder.ToString());
        }

        private static void Usage(string applicationName)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(String.Empty);
            builder.AppendLine(String.Format("{0} [-licensefile binary_license_file]", applicationName));
            builder.AppendLine(String.Format("{0} [-server url]", applicationName));
            builder.AppendLine(String.Format("{0} [-productid product_id]", applicationName));
            builder.AppendLine(String.Format("{0} [-productname product_name]", applicationName));

            builder.AppendLine(String.Empty);
            builder.AppendLine("Selects appropriate available product updates");
            builder.AppendLine("based on product name and available licenses.");
            builder.AppendLine("where:");
	        builder.AppendLine("-licensefile Name of a binary buffer license file to be used");
            builder.AppendLine("             as a license source along with trusted storage.");
            builder.AppendLine("-server      Url of the FlexNet Connect notification server.");
            builder.AppendLine("             Default is 'http://updates.flexnetoperations.com'.");
            builder.AppendLine("-productid   FlexNet Connect id of product to upgrade.");
            builder.AppendLine("             Default is {77777777-7777-7777-7777-777777777777}.");
            builder.AppendLine("-productname FlexNet Connect name of product to upgrade.");
            builder.AppendLine("             Default is 'MyProduct'.");

            Util.DisplayMessage(builder.ToString(), "USAGE");
        }
    }
}