// <copyright file="Lmflex.cs" company="Flexera Software LLC">
//     Copyright (c) 2011-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using FlxDotNetClient;
using IdentityData;
using DemoUtilities;

/****************************************************************************
    Lmflex.cs

    This example program illustrates the general sequence of functions
    required to acquire "f1" and "f2" features from a legacy
    certificate license source read from a file.

*****************************************************************************/

namespace Lmflex
{
    public static class Lmflex
    {
        private static readonly string defaultFile = "Using default certificate license file {0}";
        private static string legacyCertificateFile = "legacy.lic";

        private static ILicensing licensing;

        private static bool ValidateCommandLineArgs(string[] args)
        {
            bool validCommand = false;
            if (args.Length == 0)
            {
                Util.DisplayInfoMessage(String.Format(defaultFile, legacyCertificateFile));
                validCommand = true;
            }
            else if (args.Length == 1)
            {
                string argument = args[0].ToLowerInvariant();
                if (argument != "-h" && argument != "-help")
                {
                    validCommand = true;
                    legacyCertificateFile = args[0];
                }
            }
            return validCommand;
        }

        public static void Main(string[] args)
        {
            if (!ValidateCommandLineArgs(args))
            {
                Usage(Path.GetFileName(Environment.GetCommandLineArgs()[0]));
                return;
            }

            try
            {
                string strPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + Path.DirectorySeparatorChar;
                // initialize ILicensing interface with identity data using the Windows common document 
                // respository as the trusted storage location and the hard-coded string hostid "1234567890".
                using (licensing = LicensingFactory.GetLicensing(
                          IdentityClient.IdentityData,
                          strPath,
                          "1234567890"))
                {
                    // add legacy certificate license source
                    licensing.LicenseManager.AddCertificateLicenseSource(legacyCertificateFile, "CertificateFile");

                    // acquire 1 "f1" license
                    AcquireReturn("f1", "1.0");

                    // acquire 1 "f2" license
                    AcquireReturn("f2", "1.0");
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private static void AcquireReturn(string requestedFeature, string requestedVersion)
        {
            // acquire license
            string currentFeature = requestedFeature;
            try
            {
                ILicense acquiredLicense = licensing.LicenseManager.Acquire(requestedFeature, requestedVersion);
                try
                {
                    currentFeature = acquiredLicense.Name;
                    Util.DisplayInfoMessage(String.Format("License acquisition for feature " + currentFeature + " version " + requestedVersion + " successful."));
                }
                finally
                {
                    acquiredLicense.ReturnLicense();
                    Util.DisplayInfoMessage(String.Format("License for feature " + currentFeature + " version " + requestedVersion + " successfully returned."));
                }
            }
            catch (Exception exc)
            {
                HandleException(exc);
            }
        }

        private static void HandleException(Exception exc)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(String.Format("{0} encountered: ", exc.GetType()));
            PublicLicensingException flxException = exc as PublicLicensingException;
            if (flxException != null)
            {
                switch (flxException.ErrorCode)
                {
                    case ErrorCode.FLXERR_SHORT_CODE_LICENSE_EXPIRED:
                        builder.Append(String.Format("{0}: {1}", "Short code license expired", flxException));
                        break;
                    case ErrorCode.FLXERR_FEATURE_NOT_FOUND:
                        //builder.Append(String.Format("{0} {1}: {2}", "Feature not found", currentFeature, flxException));
                        break;
                    default:
                        builder.Append(flxException.ToString());
                        break;
                }
            }
            else
            {
                builder.Append(exc.Message);
            }
            Util.DisplayErrorMessage(builder.ToString());
        }
        private static void Usage(string applicationName)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(String.Empty);
            builder.AppendLine(String.Format("{0} [legacy_certificate_license_file]", applicationName));
            builder.AppendLine("Attempts to acquire 'f1' and 'f2' features from a signed ");
            builder.AppendLine("legacy certificate license file.");
            builder.AppendLine(String.Empty);
            builder.AppendLine(String.Format("If unset, default legacy_certificate_license_file is {0}.", legacyCertificateFile));

            Util.DisplayMessage(builder.ToString(), "USAGE");
        }
    }
}
