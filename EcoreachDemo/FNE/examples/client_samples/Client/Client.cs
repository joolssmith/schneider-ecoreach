// <copyright file="Client.cs" company="Flexera Software LLC">
//     Copyright (c) 2011-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using FlxDotNetClient;
using IdentityData;
using DemoUtilities;

/****************************************************************************
    Client.cs

    This example program enables you to:
    1. Process the license file of your choice.
    2. Acquire licenses from a combination of buffer, trusted storage, and
       trial license sources.
*****************************************************************************/


namespace Client
{
    public static class Client
    {
        private static readonly string emptyIdentity =
@"License-enabled code requires client identity data,
which you create with pubidutil and printbin -CS.
See the User Guide for more information.";

        private static readonly List<LicenseInfo> licenses = new List<LicenseInfo>();

        private const string version = "1.0";
        private static ILicensing licensing;

        private static string inputFile;
        private static byte[] inputFileData;

        public static void Main(string[] args)
        {
            if (!ValidateCommandLineArgs(args))
            {
                Usage(Path.GetFileName(Environment.GetCommandLineArgs()[0]));
                return;
            }

            if (IdentityClient.IdentityData == null || IdentityClient.IdentityData.Length == 0)
            {
                Console.WriteLine(emptyIdentity);
                return;
            }

            try
            {
                string strPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + Path.DirectorySeparatorChar;
			    // Initialize ILicensing interface with identity data using the Windows common document 
                // respository as the trusted storage location and the hard-coded string hostid "1234567890".
                using (licensing = LicensingFactory.GetLicensing(
                          IdentityClient.IdentityData,
                          strPath,
                          "1234567890"))
                {               
                    Init();
                    DisplayFeatureCounts();
                    AcquireLicenses();
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private static void Init()
        {
            /*
              note: the order in which license sources are added to the license source
              collection determines the order in which features are considered for
              license acquisition
            */
            if (!String.IsNullOrEmpty(inputFile))
            {
                Util.DisplayInfoMessage(String.Format("Reading data from {0}", inputFile));
                inputFileData = Util.ReadData(inputFile); 
                licensing.LicenseManager.AddBufferLicenseSource(inputFileData, "buffer1");
            }
            licensing.LicenseManager.AddTrustedStorageLicenseSource();
            licensing.LicenseManager.AddTrialLicenseSource();


            // Acquire 1 "survey" license
            licenses.Add(new LicenseInfo("survey", version, 1));

            // Acquire 1 "highres" license
            licenses.Add(new LicenseInfo("highres", version, 1));

            // Acquire 1 "lowres" license
            licenses.Add(new LicenseInfo("lowres", version, 1));

            // Acquire 1 "download" license
            licenses.Add(new LicenseInfo("download", version, 1));

            // Acquire 1 "upload" license
            licenses.Add(new LicenseInfo("upload", version, 1));

            // Acquire 1 "updates" license
            licenses.Add(new LicenseInfo("updates", version, 1));

            // Acquire 1 "special" license
            licenses.Add(new LicenseInfo("special", version, 1));

            // Acquire 100 "sdchannel" licenses
            licenses.Add(new LicenseInfo("sdchannel", version, 100));

            // Acquire 10 "hdchannel" licenses
            licenses.Add(new LicenseInfo("hdchannel", version, 10));
        }

        private static void HandleException(Exception exc)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(String.Format("{0} encountered: ", exc.GetType()));
            FlxException flxException = exc as FlxException;
            if (flxException != null)
            {
                builder.Append(flxException.ToString());
            }
            else
            {
                builder.Append(exc.Message);
            }
            Util.DisplayErrorMessage(builder.ToString());
        }

        private static string ExpirationDateToString(ILicense license)
        {
            return license.IsPerpetual ? "permanent" : (license.Expiration.HasValue ? license.Expiration.ToString() : "0");
        }

        private static void AcquireLicenses()
        {
            /* acquire the various licenses */
            foreach (LicenseInfo licenseInfo in licenses)
            {
                try
                {
                    ILicense license = licensing.LicenseManager.Acquire(licenseInfo.Name, licenseInfo.Version, licenseInfo.Count);
                    try
                    {
                        /* display license details */
                        Util.DisplayInfoMessage(
                            String.Format(
                                "Acquired: name={0}, version={1}, count={2}, expiration={3}",
                                license.Name,
                                license.Version,
                                license.Count,
                                ExpirationDateToString(license)));

                        license.ReturnLicense();
                    }
                    finally
                    {
                        license.ReturnLicense();
                    }
                }
                catch (Exception ex)
                {
                    HandleAcquireLicensesException(licenseInfo, ex);
                }
            }
        }

        private static void HandleAcquireLicensesException(LicenseInfo licenseInfo, Exception exc)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(String.Format("{0} encountered: ", exc.GetType()));
            PublicLicensingException licensingException = exc as PublicLicensingException;
            if (licensingException != null)
            {
                switch (licensingException.ErrorCode)
                {
                    case ErrorCode.FLXERR_FEATURE_NOT_FOUND:
                    case ErrorCode.FLXERR_FEATURE_EXPIRED:
                        builder.AppendFormat("Acquiring {0} license : {1}", licenseInfo.Name, licensingException);
                        break;
                    case ErrorCode.FLXERR_FEATURE_INSUFFICIENT_COUNT:
                        builder.AppendFormat("Acquiring {0} count for {1} license : {2}", licenseInfo.Count, licenseInfo.Name, licensingException);
                        break;
                    default:
                        builder.Append(licensingException.ToString());
                        break;
                }
            }
            else
            {
                builder.Append(exc.Message);
            }

            Util.DisplayErrorMessage(builder.ToString());
        }

        private static void DisplayFeatureCounts()
        {
            /* check the feature counts for the license source types */
            if (inputFileData != null)
            {
                DisplayFeatureCount(inputFileData);
            }
            DisplayFeatureCount(LicenseSourceOption.TrustedStorage);
            DisplayFeatureCount(LicenseSourceOption.Trials);
        }

        private static void DisplayFeatureCount(LicenseSourceOption option)
        {
            IFeatureCollection features = licensing.LicenseManager.GetFeatureCollection(option);
            Util.DisplayInfoMessage(String.Format("Number of features loaded from {0} : {1}", option, features.Count));
        }

        private static void DisplayFeatureCount(byte[] data)
        {
            IFeatureCollection features = licensing.LicenseManager.GetFeatureCollection(data);
            Util.DisplayInfoMessage(String.Format("Number of features loaded from {0} : {1}", Path.GetFileName(inputFile), features.Count));
        }

        private static bool ValidateCommandLineArgs(string[] args)
        {
            bool validCommand = false;
            if (args.Length == 0)
            {
                validCommand = true;
            }
            else if (args.Length == 1)
            {
                string arg = args[0].ToLowerInvariant();
                if (arg != "-help" && arg != "-h")
                {
                    validCommand = true;
                    inputFile = args[0];
                }
            }
            return validCommand;
        }

        private static void Usage(string applicationName)
        {
            StringBuilder builder = new StringBuilder();

            builder.AppendLine(String.Empty);
            builder.AppendLine(String.Format("{0} [binary_license_file] ", applicationName));
            builder.AppendLine(String.Empty);
            builder.AppendLine("          Attempts to acquire various features from binary license file, ");
            builder.AppendLine("          trusted storage, and trial license sources. ");

            Util.DisplayMessage(builder.ToString(), "USAGE");
        }
    }

    public class LicenseInfo
    {
        private string name;
        private string version;
        private int count;

        public LicenseInfo(string name, string version, int count)
        {
            this.name = name;
            this.version = version;
            this.count = count;
        }

        public string Name
        {
            get { return name; }
        }

        public string Version
        {
            get { return version; }
        }

        public int Count
        {
            get { return count; }
        }
    }
}
