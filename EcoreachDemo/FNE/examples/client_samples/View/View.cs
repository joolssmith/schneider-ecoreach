// <copyright file="View.cs" company="Flexera Software LLC">
//     Copyright (c) 2011-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

using System;
using System.IO;
using System.Text;
using FlxDotNetClient;
using DemoUtilities;
using IdentityData;
using System.Collections.Generic;


/****************************************************************************
    View.cs

    This example program enables you to:
    1. View features presented in the provided buffer or served buffer
       license file and trusted storage.
    2. Find out the validity status of the features.
*****************************************************************************/

namespace View
{
    public static class View
    {
        private static readonly string emptyIdentity =
@"License-enabled code requires client identity data,
which you create with pubidutil and printbin -CS.
See the User Guide for more information.";

        private static readonly string invalidFile =
@"Specified file does not contain buffer or 
served buffer license data.";

        private struct FeatureAttributes
        {
            public int       Index;
            public string    Name;
            public string    Version;
            public DateTime? ExpirationDate;
            public bool      IsPerpetual;
            public bool      IsUncounted;
            public Int64     Count;
            public string    VendorString;
            public string    Issuer;
            public DateTime? IssuedDate;
            public string    Notice;
            public string    SerialNumber;
            public DateTime? StartDate;
            public ErrorCode AcquireStatus;
            public bool      IsMetered;
            public bool      IsMeteredReusable;
            public TimeSpan? MeteredUndoInterval;
            public Int64     MeteredAvailableCount;
            public Dictionary<HostIdEnum, List<string>> HostIds;
        }

        private static string     inputFile = null;
        private static ILicensing licensing;
 
        private static bool ValidateCommandLineArgs(string[] args)
        {
            bool validCommand = false;
            if (args.Length == 0)
            {
                validCommand = true;
            }
            else if (args.Length == 1)
            {
                string firstArg = args[0].ToLowerInvariant();
                if (firstArg != "-h" && firstArg != "-help")
                {
                    inputFile = args[0];
                    validCommand = true;
                }
            }
            return validCommand;
        }

        public static int Main(string[] args)
        {
            byte[] licenseData = null;

            if (!ValidateCommandLineArgs(args))
            {
                Usage(Path.GetFileName(Environment.GetCommandLineArgs()[0]));
                return -1;
            }

            if (IdentityClient.IdentityData == null || IdentityClient.IdentityData.Length == 0)
            {
                Console.WriteLine(emptyIdentity);
                return -1;
            }

            if (!String.IsNullOrEmpty(inputFile) && !ReadFileData(inputFile, out licenseData))
            {
                return -1;
            }

            try
            {
                string strPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + Path.DirectorySeparatorChar;
			    // Initialize ILicensing interface with identity data using the Windows common document 
                // respository as the trusted storage location and the hard-coded string hostid "1234567890".
                using (licensing = LicensingFactory.GetLicensing(
                          IdentityClient.IdentityData,
                          strPath,
                          "1234567890"))
                {
                    licensing.LicenseManager.EnableClockWindbackDetection(86400, 86400);
                    IFeatureCollection bufferFeatureCollection = null;
                    if (licenseData != null)
                    {
                        MessageTypeEnum licenseDataType = licensing.LicenseManager.MessageType(licenseData);
                        if (licenseDataType != MessageTypeEnum.FLX_MESSAGE_TYPE_BUFFER_LICENSE &&
                            licenseDataType != MessageTypeEnum.FLX_MESSAGE_TYPE_BUFFER_CAPABILITY_RESPONSE)
                        {
                            Console.WriteLine(invalidFile);
                        }
                        bufferFeatureCollection = licensing.LicenseManager.GetFeatureCollection(licenseData, true);
                    }
                    IFeatureCollection trustedStoreFeatureCollection = licensing.LicenseManager.GetFeatureCollection(LicenseSourceOption.TrustedStorage, true);
                    IFeatureCollection trialsFeatureCollection = licensing.LicenseManager.GetFeatureCollection(LicenseSourceOption.Trials, true);
                    IFeatureCollection shortCodeFeatureCollection = licensing.LicenseManager.GetFeatureCollection(LicenseSourceOption.ShortCode, true);

                    // print buffer feature information
                    StringBuilder builder = new StringBuilder();
                    builder.AppendLine(String.Empty);
                    builder.AppendLine("==============================================");
                    if (bufferFeatureCollection != null)
                    {
                        builder.AppendLine(String.Format("Features found in {0}:", inputFile));
                        builder.AppendLine(String.Empty);
                        GetFeatures(builder, bufferFeatureCollection);
                        builder.AppendLine("==============================================");
                        builder.AppendLine(String.Empty);
                        Util.DisplayInfoMessage(builder.ToString());
                        builder = new StringBuilder();
                        builder.AppendLine(String.Empty);
                        builder.AppendLine("==============================================");
                    }

                    // print trusted storage feature information 
                    builder.AppendLine("Features found in trusted storage:");
                    builder.AppendLine(String.Empty);
                    GetFeatures(builder, trustedStoreFeatureCollection);
                    builder.AppendLine("==============================================");

                    // print trial storage feature information 
                    builder.AppendLine("Features found in trials storage:");
                    builder.AppendLine(String.Empty);
                    GetFeatures(builder, trialsFeatureCollection);
                    builder.AppendLine("==============================================");

                    // print short code storage feature information 
                    builder.AppendLine("Features found in short-code storage:");
                    builder.AppendLine(String.Empty);
                    GetFeatures(builder, shortCodeFeatureCollection);
                    builder.AppendLine("==============================================");
                    Util.DisplayInfoMessage(builder.ToString());
                }
            }
            catch (Exception exc)
            {
                HandleException(exc);
                return -1;
            }

            return 0;
        }

        private static void HandleException(Exception exc)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(String.Format("{0} encountered: ", exc.GetType()));
            FlxException flxException = exc as FlxException;
            if (flxException != null)
            {
                builder.Append(flxException.ToString());
            }
            else
            {
                builder.Append(exc.Message);
            }
            Util.DisplayErrorMessage(builder.ToString());
        }

        private static void GetFeatures(StringBuilder builder, IFeatureCollection featureCollection)
        {
            int index = 0;
            // list all of the features for a given feature collection
            foreach (IFeature feature in featureCollection)
            {
                GetFeatureInfo(builder, index++, feature);
            }
        }

        private static void GetFeatureInfo(StringBuilder builder, int index, IFeature feature)
        {
            FeatureAttributes featureAttributes = new FeatureAttributes();

            featureAttributes.Index = index;
            featureAttributes.Name = feature.Name;
            featureAttributes.Version = feature.Version;
            featureAttributes.StartDate = feature.StartDate;
            featureAttributes.ExpirationDate = feature.Expiration;
            featureAttributes.IsPerpetual = feature.IsPerpetual;
            featureAttributes.IsUncounted = feature.IsUncounted;
            featureAttributes.Count = feature.Count;
            featureAttributes.IsMetered = feature.IsMetered;
            featureAttributes.IsMeteredReusable = feature.IsMeteredReusable;
            featureAttributes.VendorString = feature.VendorString;
            featureAttributes.Issuer = feature.Issuer;
            featureAttributes.IssuedDate = feature.Issued;
            featureAttributes.Notice = feature.Notice;
            featureAttributes.SerialNumber = feature.SerialNumber;
            featureAttributes.AcquireStatus = feature.ValidStatusForAcquisition();
            featureAttributes.IsMetered = feature.IsMetered;
            featureAttributes.IsMeteredReusable = feature.IsMeteredReusable;
            featureAttributes.MeteredUndoInterval = feature.MeteredUndoInterval;
            featureAttributes.MeteredAvailableCount = featureAttributes.IsMetered ? feature.AvailableAcquisitionCount : 0;
            featureAttributes.HostIds = feature.HostIds;

            // check clock windback
            if (licensing.LicenseManager.ClockWindbackDetected)
            {
                throw new ApplicationException("Clock windback detected. Restore the clock to run the application.");
            }
            PrintFeatureAttributes(builder, ref featureAttributes);
        }

        private static void PrintFeatureAttributes(StringBuilder builder, ref FeatureAttributes featureAttributes)
        {
            builder.Append(featureAttributes.Index + 1);
            builder.Append(String.Format(": {0} ", featureAttributes.Name));
            builder.Append(featureAttributes.Version);
            if (featureAttributes.ExpirationDate.HasValue)
            {
                if (featureAttributes.IsPerpetual)
                {
                    builder.Append(" permanent");
                }
                else
                {
                    builder.Append(" ");
                    builder.Append(featureAttributes.ExpirationDate.Value.ToString("dd-MMM-yyyy"));
                }
            }
            builder.Append(" ");
            builder.Append(featureAttributes.IsUncounted ? "uncounted" : featureAttributes.Count.ToString());
            if (featureAttributes.IsMetered)
            {
                builder.Append(String.Format(" MODEL=metered{0}",
                    featureAttributes.IsMeteredReusable ? " REUSABLE" : String.Empty));
                if (featureAttributes.MeteredUndoInterval.HasValue && featureAttributes.MeteredUndoInterval.Value.Ticks > 0)
                {
                    builder.Append(String.Format(" UNDO_INTERVAL={0}", featureAttributes.MeteredUndoInterval.Value.TotalSeconds));
                }
            }
            if (!String.IsNullOrEmpty(featureAttributes.VendorString))
            {
                builder.Append(String.Format(" VENDOR_STRING={0}", featureAttributes.VendorString));
            }
            if (!String.IsNullOrEmpty(featureAttributes.Issuer))
            {
                builder.Append(String.Format(" ISSUER={0}", featureAttributes.Issuer));
            }
            if (featureAttributes.IssuedDate.HasValue)
            {
                builder.Append(String.Format(" ISSUED={0}", featureAttributes.IssuedDate.Value.ToString("dd-MMM-yyyy")));
            }
            if (!String.IsNullOrEmpty(featureAttributes.Notice))
            {
                builder.Append(String.Format(" NOTICE={0}", featureAttributes.Notice));
            }
            if (!String.IsNullOrEmpty(featureAttributes.SerialNumber))
            {
                builder.Append(String.Format(" SN={0}", featureAttributes.SerialNumber));
            }
            if (featureAttributes.StartDate.HasValue)
            {
                builder.Append(String.Format(" START={0}", featureAttributes.StartDate.Value.ToString("dd-MMM-yyyy")));
            }
            
            // Hostid(s)
            if (featureAttributes.HostIds.Keys.Count > 0)
            {
                string hostIdString = String.Empty;
                string decorateLeft = featureAttributes.HostIds.Count > 1 ? " Hostids=[" : " Hostid=";
                string decorateRight = featureAttributes.HostIds.Count > 1 ? "]" : String.Empty;
                foreach (KeyValuePair<HostIdEnum, List<string>> HostId in featureAttributes.HostIds)
                {
                    string lparen = HostId.Value.Count > 1 ? "(" : String.Empty;
                    string rparen = HostId.Value.Count > 1 ? ") " : " ";
                    if (HostId.Key != HostIdEnum.FLX_HOSTID_TYPE_ANY)
                    {
                        hostIdString += HostId.Key.ToString().Remove(0, 16) + "=";
                    }
                    string value = String.Empty;
                    foreach (string str in HostId.Value)
                    {
                        value += (String.IsNullOrEmpty(value) ? String.Empty : ",") + str;
                    }
                    hostIdString += lparen + value + rparen;
                }
                builder.Append(decorateLeft + hostIdString.TrimEnd() + decorateRight);
            }
            builder.AppendLine(String.Empty);
            if (featureAttributes.AcquireStatus == 0)
            {
                if (featureAttributes.IsMetered && featureAttributes.MeteredAvailableCount == 0)
                {
                    builder.AppendLine("     Entire count consumed");
                }
                else if (featureAttributes.IsMetered && featureAttributes.MeteredAvailableCount > 0)
                {
                    builder.AppendLine(String.Format("     Available for acquisition: {0}", featureAttributes.MeteredAvailableCount));
                }
                else
                {
                    builder.AppendLine("     Valid for acquisition");
                }
            }
            else
            {
                builder.AppendLine(String.Format("     Not valid for acquisition: {0}", licensing.GetErrorDescription(featureAttributes.AcquireStatus)));
            }
        }

        private static void Usage(string applicationName)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(String.Empty);
            builder.AppendLine(String.Format("{0} [binary_license_file]", applicationName));
            builder.AppendLine(String.Empty);
            builder.AppendLine("Displays feature keyword information and validity based on contents");
            builder.AppendLine("of a binary license file and trusted storage, when available.");

            Util.DisplayMessage(builder.ToString(), "USAGE");
        }

        //===============================================================================
        //   Utility Functions                                        
        //===============================================================================
        //   
        //        ReadFileData
        //        
        //        Returns the contents of the named file as a Byte array.
        //       
        //        @param fileName   filename relative to where the program is run
        //        @param data       returned data as Byte array
        //        
        //        @return true if successfully read the entire file;
        //                false otherwise
        //===============================================================================
        private static bool ReadFileData(string fileName, out byte[] data)
        {
            Util.DisplayInfoMessage(String.Format("Reading data from {0}", fileName));
            data = Util.ReadData(fileName);
            // make sure the file actually contains data
            if (data == null)
            {
                return false;
            }
            else if (data.Length == 0)
            {
                Util.DisplayErrorMessage(String.Format("File {0} contains no data", fileName));
                return false;
            }

            return true;
        }
    }
}