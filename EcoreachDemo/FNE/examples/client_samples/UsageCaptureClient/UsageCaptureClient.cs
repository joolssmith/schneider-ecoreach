// <copyright file="UsageCaptureClient.cs" company="Flexera Software LLC">
//     Copyright (c) 2013-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using DemoUtilities;
using FlxDotNetClient;
using IdentityData;

//****************************************************************************
//  UsageCaptureClient.cs
//
//  This example program illustrates how to:
//
//  1. Send a capability request describing a usage-capture report, 
//     request, or undo operation to a server.
//  2. Depending on the operation type, process the response.
//
//  These communications can be performed directly over HTTP or using
//  intermediate files for offline operations.
//
//*****************************************************************************

namespace UsageCaptureClient
{
    public static class UsageCaptureClient
    {
        private static readonly string emptyIdentity =
@"License-enabled code requires client identity data,
which you create with pubidutil and printbin -CS.
See the User Guide for more information.";

        private static readonly string processingCapabilityResponse = "Processing capability response";
        private static readonly string acquiringLicense = "Acquiring license";
        private static readonly string attemptingAcquire = "Attempting to acquire license for feature '{0}' version '{1}'";
        private static readonly string licenseAcquired = "License acquisition for feature '{0}' version '{1}' successful";
        private static readonly string attemptingReturn = "Attempting return or release of all acquired licenses";
        private static readonly string returnSuccessful = "Acquired licenses successfully returned or released";
        private static readonly string surveyFeature = "survey";
        private static readonly string version = "1.0";

        private static ILicensing licensing;
        private static RequestType requestType = RequestType.unknown;
        private static CapabilityRequestOperation? operation = null;
        private static string operationType = String.Empty;
        private static string fileName = String.Empty;
        private static string correlationId = String.Empty;
        private static string rightsId = String.Empty;
        private static string serverUrl = String.Empty;

        private static string currentFeature = String.Empty;

        private enum RequestType
        {
            unknown,
            generateCapabilityRequest,
            processCapabilityResponse,
            sendCapabilityRequest,
        }

        private static bool ValidateCommandLineArgs(string[] args)
        {
            bool validCommand = true;
            bool unknownOption = false;

            if (args.Length == 0)
            {
                return false;
            }

            for (int ii = 0; validCommand && ii < args.Length; ii++)
            {
                string option = args[ii].ToLowerInvariant();
                switch (option)
                {
                    case "-h":
                    case "-help":
                        return false;
                    case "-request":
                        operation = CapabilityRequestOperation.Request;
                        operationType = String.Empty;
                        break;
                    case "-report":
                        operation = CapabilityRequestOperation.Report;
                        operationType = "report ";
                        break;
                    case "-undo":
                        operation = CapabilityRequestOperation.Undo;
                        operationType = "undo ";
                        break;
                    case "-correlation":
                        validCommand = (++ii < args.Length && !String.IsNullOrEmpty((correlationId = args[ii])));
                        break;
                    case "-rightsid":
                        validCommand = (++ii < args.Length && !String.IsNullOrEmpty((rightsId = args[ii])));
                        break;
                    case "-generate":
                        requestType = RequestType.generateCapabilityRequest;
                        validCommand = (++ii < args.Length && !String.IsNullOrEmpty((fileName = args[ii])));
                        break;
                    case "-process":
                        requestType = RequestType.processCapabilityResponse;
                        validCommand = (++ii < args.Length && !String.IsNullOrEmpty((fileName = args[ii])));
                        break;
                    case "-server":
                        requestType = RequestType.sendCapabilityRequest;
                        validCommand = (ii + 1 < args.Length &&
                              !String.IsNullOrEmpty((serverUrl = args[++ii])));
                        break;
                    default:
                        Util.DisplayErrorMessage(String.Format("unknown option: {0}", args[ii]));
                        validCommand = false;
                        unknownOption = true;
                        break;
                }
                if (!validCommand && !unknownOption)
                {
                    Util.DisplayErrorMessage(String.Format("invalid specification for option: {0}", option));
                }
            }
            if (validCommand)
            { 
                // Undo operation must have a correlation id. 
                if (requestType != RequestType.processCapabilityResponse && operation.HasValue &&
                    operation.Value == CapabilityRequestOperation.Undo && String.IsNullOrEmpty(correlationId))
                {
                    Util.DisplayErrorMessage("undo option requires correlation ID");
                    validCommand = false;
                }
                else if (requestType != RequestType.processCapabilityResponse && operation.HasValue &&
                         operation.Value == CapabilityRequestOperation.Report && !String.IsNullOrEmpty(rightsId))
                {
                    Util.DisplayErrorMessage("report option is not compatible with rightsid option");
                    validCommand = false;
                }
                else if (requestType == RequestType.processCapabilityResponse &&
                         (operation.HasValue || !String.IsNullOrEmpty(correlationId) || !String.IsNullOrEmpty(rightsId)))
                {
                    Util.DisplayErrorMessage("process option is not compatible with request attributes");
                    validCommand = false;
                }
                else if (requestType != RequestType.processCapabilityResponse && !operation.HasValue)
                {
                    operation = CapabilityRequestOperation.Request;
                }
                else
                {
                    validCommand = (requestType != RequestType.unknown);
                }
            }
            return validCommand;
        }

        public static void Main(string[] args)
        {
            if (!ValidateCommandLineArgs(args))
            {
                Usage(Path.GetFileName(Environment.GetCommandLineArgs()[0]));
                return;
            }

		    if (IdentityClient.IdentityData == null || IdentityClient.IdentityData.Length == 0)
            {
                Console.WriteLine(emptyIdentity);
			    return;
		    }

            try
            {
                string strPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + Path.DirectorySeparatorChar;
			    // Initialize ILicensing interface with identity data using the Windows common document 
                // respository as the trusted storage location and the hard-coded string hostid "1234567890".
                using (licensing = LicensingFactory.GetLicensing(
                          IdentityClient.IdentityData,
                          strPath,
                          "1234567890"))
                {
                    ShowTSFeatures();
                    switch (requestType)
                    {
                        case RequestType.generateCapabilityRequest:
                            GenerateCapabilityRequest();
                            break;
                        case RequestType.processCapabilityResponse:
                            ProcessCapabilityResponse();
                            break;
                        case RequestType.sendCapabilityRequest:
                            SendCapabilityRequest();
                            break;
                    }
                }
            }
            catch (Exception exc)
            {
                HandleException(exc);
            }
        }

        private static void GenerateCapabilityRequest()
        {
            // saving the capablity request to a file
            Util.DisplayInfoMessage(String.Format("Creating the {0}capability request", operationType));
            ICapabilityRequestData capabilityRequestData = licensing.LicenseManager.CreateCapabilityRequest(GenerateRequestOptions());
            if (Util.WriteData(fileName, capabilityRequestData.ToArray()))
            {
                Util.DisplayInfoMessage(String.Format("Capability request data written to: {0}", fileName));
            }
        }

        private static void ProcessCapabilityResponse()
        {
            // read the capability response from a file and process it
            Util.DisplayInfoMessage(String.Format("Reading capability response data from: {0}", fileName));
            byte[] binCapResponse = Util.ReadData(fileName);
            if (binCapResponse == null || binCapResponse.Length == 0)
            {
                return;
            }
            ProcessCapabilityResponse(binCapResponse);
        }

        private static void SendCapabilityRequest()
        {
            Util.DisplayInfoMessage(String.Format("Creating the {0}capability request", operationType));

            // create the capability request
            ICapabilityRequestData capabilityRequestData = licensing.LicenseManager.CreateCapabilityRequest(GenerateRequestOptions());
            Util.DisplayInfoMessage(String.Format("Sending the capability request to: {0}", serverUrl));
            byte[] binCapResponse = null;

            // send the capability request to the server and receive the server response
            CommFactory.Create(serverUrl).SendBinaryMessage(capabilityRequestData.ToArray(), out binCapResponse);
            Util.DisplayInfoMessage("Successfully sent capability request");
            if (operation != CapabilityRequestOperation.Report ||
                (binCapResponse != null && binCapResponse.Length > 0))
            {
                ProcessCapabilityResponse(binCapResponse);
            }
        }

        private static void ProcessCapabilityResponse(byte[] binCapResponse)
        {
            Util.DisplayInfoMessage("Processing capability response");
            licensing.LicenseManager.AddTrustedStorageLicenseSource();
            ICapabilityResponse response = licensing.LicenseManager.ProcessCapabilityResponse(binCapResponse);
            Util.DisplayInfoMessage("Capability response processed");
            ShowCapabilityResponseDetails(response);
            ShowTSFeatures();
            AcquireReturn(surveyFeature, version);
        }

        private static void ShowCapabilityResponseDetails(ICapabilityResponse response)
        {
            Util.DisplayInfoMessage("Obtaining capability response details");

            Util.DisplayInfoMessage(String.Format("Capability response correlation ID: {0}", response.CorrelationId ?? "none"));

            // get vendor dictionary from response
            ShowDictionary(response.VendorDictionary, "vendor");

            // get status information
            Util.DisplayInfoMessage(String.Format("Capability response contains {0} status item{1}", 
                response.Status.Count, response.Status.Count == 1 ? String.Empty : "s"));
            if (response.Status.Count > 0)
            {
                foreach (IResponseStatus statusItem in response.Status)
                {
                    Util.DisplayInfoMessage(String.Format("Status - category: {0}, code: {1}{2}, details: {3}",
                        statusItem.TypeDescription, (int)statusItem.Code,
                        String.IsNullOrEmpty(statusItem.CodeDescription) ? String.Empty : " (" + statusItem.CodeDescription + ")",
                        statusItem.Details));
                }
            }
        }

        private static void ShowDictionary(ReadOnlyDictionary dictionary, string dictionaryType)
        {
            Util.DisplayInfoMessage(String.Format("Capability response contains {0} {1} dictionary item{2}",
                dictionary.Count, dictionaryType, dictionary.Count == 1 ? String.Empty : "s"));
            string itemType;
            foreach (KeyValuePair<string, object> item in dictionary)
            {
                if (item.Value is string)
                {
                    itemType = "string";
                }
                else if (item.Value is int)
                {
                    itemType = "integer";
                }
                else
                {
                    itemType = "unknown";
                }
                Util.DisplayInfoMessage(String.Format("{0} dictionary {1} item type: {2}={3}", 
                                        dictionaryType.Substring(0, 1).ToUpperInvariant() + dictionaryType.Substring(1),                                        
                                        itemType, item.Key, item.Value));
            }
        }

        private static void ShowTSFeatures()
        {
            // display the number of features found in trusted storage
            IFeatureCollection collection = licensing.LicenseManager.GetFeatureCollection(LicenseSourceOption.TrustedStorage);
            Util.DisplayInfoMessage(String.Format("Features loaded from trusted storage: {0}", collection.Count));
        }

        private static ICapabilityRequestOptions GenerateRequestOptions()
        {
            // create the capability request options object
            ICapabilityRequestOptions options = licensing.LicenseManager.CreateCapabilityRequestOptions();

            // set the capability request opertion
            options.Operation = operation.Value;

            // set the correlation ID if specified
            if (!String.IsNullOrEmpty(correlationId))
            {
                options.CorrelationId = correlationId;
                Util.DisplayInfoMessage(String.Format("Capability request correlation ID: {0}", options.CorrelationId));
            }

            // set the rights ID if specified 
            // note that rights ID specification is incompatible with a report type capability request
            if (operation != CapabilityRequestOperation.Report && !String.IsNullOrEmpty(rightsId))
            {
                options.AddRightsId(rightsId, 1);
            }

            // set Requestor ID
            options.RequestorId = "Example Application";
            // set Acquisition ID
            options.AcquisitionId = "High resolution surveying";
            // set Enterprise ID
            options.EnterpriseId = "5551212";

            if (operation != CapabilityRequestOperation.Undo)
            {
                // specify usage data; to return a metered feature, set the count to a negative value
                options.DesiredFeatures.Add(new FeatureData("survey", "1.0", 1));
            }

            // force capability response from server even if nothing has changed
            options.ForceResponse = true;

            return options;
        }

        private static void AcquireReturn(string requestedFeature, string requestedVersion)
        {
            // acquire license
            string currentFeature = requestedFeature;
            Util.DisplayInfoMessage(String.Format(attemptingAcquire, requestedFeature, requestedVersion));
            try
            {
                ILicense acquiredLicense = licensing.LicenseManager.Acquire(requestedFeature, requestedVersion);
                try
                {
                    currentFeature = acquiredLicense.Name;
                    Util.DisplayInfoMessage(String.Format(licenseAcquired, currentFeature, requestedVersion));
                    //// application logic here
                }
                finally
                {
                    // return non-metered and relase all metered license. 
                    Util.DisplayInfoMessage(attemptingReturn);
                    licensing.LicenseManager.ReturnAllLicenses();
                    Util.DisplayInfoMessage(returnSuccessful);
                }
            }
            catch (Exception exc)
            {
                HandleException(exc);
            }
        }

        private static void HandleException(Exception exc)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(String.Format("{0} encountered: ", exc.GetType()));
            PublicLicensingException flxException = exc as PublicLicensingException;
            if (flxException != null)
            {
                switch (flxException.ErrorCode)
                {
                    case ErrorCode.FLXERR_RESPONSE_STALE:
                    case ErrorCode.FLXERR_RESPONSE_EXPIRED:
                    case ErrorCode.FLXERR_CAPABILITY_RESPONSE_DATA_MISSING:
                        builder.Append(String.Format("{0}: {1}", processingCapabilityResponse, flxException));
                        break;
                    case ErrorCode.FLXERR_FEATURE_NOT_FOUND:
                        builder.Append(String.Format("{0} {1}: {2}", acquiringLicense, currentFeature, flxException));
                        break;
                    default:
                        builder.Append(flxException.ToString());
                        break;
                }
            }
            else
            {
                builder.Append(exc.Message);
            }
            Util.DisplayErrorMessage(builder.ToString());
        }

        private static void Usage(string applicationName)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(String.Empty);
            builder.AppendLine(String.Format("{0} [-request|-report|-undo] [-correlation id] [-rightsid rights-id]", applicationName));
            builder.AppendLine("          [-server url|-generate outputfile|-process inputfile]");
            builder.AppendLine(String.Empty);
            builder.AppendLine("where:");
            builder.AppendLine("-request  Request operation. This is the default operation type if unspecified.");
            builder.AppendLine("-report   Report operation. Incompatible with -rightsid specification.");
            builder.AppendLine("-undo     Undo operation. Correlation ID must be set with -correlation.");
            builder.AppendLine(String.Empty);
            builder.AppendLine("-correlation Sets the correlation ID.");
            builder.AppendLine(String.Empty);
            builder.AppendLine("-rightsid    Sets the rights ID.");
            builder.AppendLine(String.Empty);
            builder.AppendLine("-server   Sends usage-capture message to a server and processes the response.");
            builder.AppendLine("-generate Generates usage-capture message into a file.");
            builder.AppendLine("-process  Processes server's response from a file.");

            Util.DisplayMessage(builder.ToString(), "USAGE");
        }

    }
}