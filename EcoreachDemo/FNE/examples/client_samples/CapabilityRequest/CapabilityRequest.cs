// <copyright file="CapabilityRequest.cs" company="Flexera Software LLC">
//     Copyright (c) 2011-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using DemoUtilities;
using FlxDotNetClient;
using IdentityData;

/****************************************************************************
    CapabilityRequest.cs

    This example program allows you to:
    1. Send a capability request via http to the server and process response,
       saving data into trusted storage.
    2. Write capability request to a file. This request can be fed into server
       to generate response.
    3. Read capability response from a file and process accordingly.
*****************************************************************************/

namespace CapabilityRequest
{
    public static class CapabilityRequest
    {
        private static readonly string emptyIdentity =
@"License-enabled code requires client identity data,
which you create with pubidutil and printbin -CS.
See the User Guide for more information.";

        private static readonly string processingCapabilityResponse = "Processing capability response";
        private static readonly string acquiringLicense = "Acquiring license";
        private static readonly string attemptingAcquire = "Attempting to acquire license for feature '{0}' version '{1}'";
        private static readonly string licenseAcquired = "License acquisition for feature '{0}' version '{1}' successful";
        private static readonly string attemptingReturn = "Attempting to return license for feature '{0}' version '{1}'";
        private static readonly string licenseReturned = "License for feature '{0}' version '{1}' successfully returned";
        private static readonly string surveyFeature = "survey";
        private static readonly string version = "1.0";

        private static readonly string dictionaryKey1 = "StringKey";
        private static readonly string dictionaryKey2 = "Integer Key";

        private static ILicensing licensing;
        private static RequestType requestType;
        private static string fileName = String.Empty;
        private static string serverUrl = String.Empty;

        private static string currentFeature = String.Empty;

        private enum RequestType
        {
            generateCapabilityRequest,
            processCapabilityResponse,
            sendCapabilityRequest,
        }

        private static bool ValidateCommandLineArgs(string[] args)
        {
            bool validCommand = false;
            bool invalidSpec = false;
            if (args.Length >= 1)
            {
                switch (args[0].ToLowerInvariant())
                {
                    case "-h":
                    case "-help":
                        break;
                    case "-generate":
                        if (!(invalidSpec = (args.Length != 2)))
                        {
                            fileName = args[1];
                            requestType = RequestType.generateCapabilityRequest;
                            validCommand = true;
                        }
                        break;
                    case "-process":
                        if (!(invalidSpec = (args.Length != 2)))
                        {
                            fileName = args[1];
                            requestType = RequestType.processCapabilityResponse;
                            validCommand = true;
                        }
                        break;
                    case "-server":
                        if (!(invalidSpec = (args.Length != 2 )))
                        {
                            serverUrl = args[1];
                            requestType = RequestType.sendCapabilityRequest;
                            validCommand = true;
                        }
                        break;
                    default:
                        Util.DisplayErrorMessage(String.Format("unknown option: {0}", args[0]));
                        break;
                }
            }
            if (!validCommand && invalidSpec)
            {
                Util.DisplayErrorMessage(String.Format("invalid specification for option: {0}", args[0]));
            }
            return validCommand;
        }

        public static void Main(string[] args)
        {
            if (!ValidateCommandLineArgs(args))
            {
                Usage(Path.GetFileName(Environment.GetCommandLineArgs()[0]));
                return;
            }

            if (IdentityClient.IdentityData == null || IdentityClient.IdentityData.Length == 0)
            {
                Console.WriteLine(emptyIdentity);
                return;
            }

            try
            {
                string strPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + Path.DirectorySeparatorChar;
			    // Initialize ILicensing interface with identity data using the Windows common document 
                // respository as the trusted storage location and the hard-coded string hostid "1234567890".
                using (licensing = LicensingFactory.GetLicensing(
                          IdentityClient.IdentityData,
                          strPath,
                          "1234567890"))
                {
                    // The optional host name is typically set by a user as a friendly name for the host.  
                    // The host name is not used for license enforcement.                  
                    licensing.LicenseManager.HostName = "Sample Device";
                    // The host type is typically a name set by the implementer, and is not modifiable by the user.
                    // While optional, the host type may be used in certain scenarios by some back-office systems such as FlexNet Operations.
                    licensing.LicenseManager.HostType = "Sample Device Type";
                    ShowTSFeatures();
                    switch (requestType)
                    {
                        case RequestType.generateCapabilityRequest:
                            GenerateCapabilityRequest();
                            break;
                        case RequestType.processCapabilityResponse:
                            ProcessCapabilityResponse();
                            break;
                        case RequestType.sendCapabilityRequest:
                            SendCapabilityRequest();
                            break;
                    }
                }
            }
            catch (Exception exc)
            {
                HandleException(exc);
            }
        }

        private static void GenerateCapabilityRequest()
        {
            // saving the capablity request to a file
            Util.DisplayInfoMessage("Creating the capability request");
            ICapabilityRequestData capabilityRequestData = licensing.LicenseManager.CreateCapabilityRequest(GenerateRequestOptions());
            if (Util.WriteData(fileName, capabilityRequestData.ToArray()))
            {
                Util.DisplayInfoMessage(String.Format("Capability request data written to: {0}", fileName));
            }
        }

        private static void ProcessCapabilityResponse()
        {
            // read the capability response from a file and process it
            Util.DisplayInfoMessage(String.Format("Reading capability response data from: {0}", fileName));
            byte[] binCapResponse = Util.ReadData(fileName);
            if (binCapResponse == null)
            {
                return;
            }
            ProcessCapabilityResponse(binCapResponse);
        }

        private static void SendCapabilityRequest()
        {
            Util.DisplayInfoMessage("Creating the capability request");

            // create the capability request
            ICapabilityRequestOptions options = GenerateRequestOptions();
            ICapabilityRequestData capabilityRequestData = licensing.LicenseManager.CreateCapabilityRequest(options);
            Util.DisplayInfoMessage(String.Format("Sending the capability request to: {0}", serverUrl));
            byte[] binCapResponse = null;

            // send the capability request to the server and receive the server response
            CommFactory.Create(serverUrl).SendBinaryMessage(capabilityRequestData.ToArray(), out binCapResponse);
            if (binCapResponse != null && binCapResponse.Length > 0)
            {
                Util.DisplayInfoMessage("Response received");
            }
            if (options.Operation != CapabilityRequestOperation.Preview)
            {
                ProcessCapabilityResponse(binCapResponse);
            }
            else
            {
                ShowPreviewResponse(binCapResponse);
            }
        }

        private static void ProcessCapabilityResponse(byte[] binCapResponse)
        {
            Util.DisplayInfoMessage("Processing capability response");
            ICapabilityResponse response = licensing.LicenseManager.ProcessCapabilityResponse(binCapResponse);
            Util.DisplayInfoMessage("Capability response processed");
            ShowCapabilityResponseDetails(response);
            ShowTSFeatures();
            AcquireReturn(surveyFeature, version);
        }

        private static void ShowPreviewResponse(byte[] binCapResponse)
        {
            Util.DisplayInfoMessage("Examining preview capability response");
            ICapabilityResponse response = licensing.LicenseManager.GetResponseDetails(binCapResponse);
            ShowCapabilityResponseDetails(response);
            ShowCapabilityResponseFeatures(response);
        }

        private static void ShowCapabilityResponseDetails(ICapabilityResponse response)
        {
            Util.DisplayInfoMessage("Obtaining capability response details");

            // get machine type from response */
            switch (response.VirtualMachineType)
            {
                case MachineTypeEnum.FLX_MACHINE_TYPE_PHYSICAL:
                    Util.DisplayInfoMessage("Machine type: PHYSICAL");
                    break;
                case MachineTypeEnum.FLX_MACHINE_TYPE_VIRTUAL:
                    Util.DisplayInfoMessage("Machine type: VIRTUAL");
                    // get virtual machine dictionary from response
                    ShowDictionary(response.VirtualMachineInfo, "virtual machine");
                    break;
                case MachineTypeEnum.FLX_MACHINE_TYPE_UNKNOWN:
                default:
                    Util.DisplayInfoMessage("Machine type: UNKNOWN");
                    break;
            }

            // get vendor dictionary from response
            ShowDictionary(response.VendorDictionary, "vendor");

            // get status information
            Util.DisplayInfoMessage(String.Format("Capability response contains {0} status item{1}", 
                response.Status.Count, response.Status.Count == 1 ? String.Empty : "s"));
            if (response.Status.Count > 0)
            {
                foreach (IResponseStatus statusItem in response.Status)
                {
                    Util.DisplayInfoMessage(String.Format("Status - category: {0}, code: {1}{2}, details: {3}",
                        statusItem.TypeDescription, (int)statusItem.Code,
                        String.IsNullOrEmpty(statusItem.CodeDescription) ? String.Empty : " (" + statusItem.CodeDescription + ")",
                        statusItem.Details));
                }
            }

            // determine whether or not a confirmation request is needed
            Util.DisplayInfoMessage(String.Format("Confirmation request is {0}needed", response.ConfirmationRequestNeeded ? String.Empty : "not "));
        }

        private static void ShowDictionary(ReadOnlyDictionary dictionary, string dictionaryType)
        {
            Util.DisplayInfoMessage(String.Format("Capability response contains {0} {1} dictionary item{2}",
                dictionary.Count, dictionaryType, dictionary.Count == 1 ? String.Empty : "s"));
            string itemType;
            foreach (KeyValuePair<string, object> item in dictionary)
            {
                if (item.Value is string)
                {
                    itemType = "string";
                }
                else if (item.Value is int)
                {
                    itemType = "integer";
                }
                else
                {
                    itemType = "unknown";
                }
                Util.DisplayInfoMessage(String.Format("{0} dictionary {1} item type: {2}={3}", 
                                        dictionaryType.Substring(0, 1).ToUpperInvariant() + dictionaryType.Substring(1),                                        
                                        itemType, item.Key, item.Value));
            }
        }

        private static void ShowTSFeatures()
        {
            // display the features found in the trusted storage
            IFeatureCollection collection = licensing.LicenseManager.GetFeatureCollection(LicenseSourceOption.TrustedStorage);
            StringBuilder builder = new StringBuilder();
            builder.Append(String.Format("Features loaded from trusted storage: {0}", collection.Count));
            /*
            foreach (IFeature feature in collection)
            {
                builder.AppendLine(String.Empty);
                builder.Append(feature.ToString());
            }
            */ 
            Util.DisplayInfoMessage(builder.ToString());
        }

        private static void ShowCapabilityResponseFeatures(ICapabilityResponse response)
        {
            // display the features found in the capability response
            Util.DisplayInfoMessage("==============================================");
            Util.DisplayInfoMessage(String.Format("Features found in {0}capability response:", response.IsPreview ? "preview " : "") +
                                    Environment.NewLine);

            IFeatureCollection collection = response.FeatureCollection;
            int index = 1;
            foreach (IFeature feature in collection)
            {
                StringBuilder builder = new StringBuilder();
                builder.Append(String.Format("{0}: {1} {2}", index, feature.Name, feature.Version));
                if (feature.IsPreview)
                {
                    builder.Append(String.Format(" TYPE=preview COUNT={0} MAXCOUNT={1}", feature.IsUncounted ? "uncounted" : feature.Count.ToString(),
                        feature.MaxCount == feature.UncountedValue ? "uncounted" : feature.MaxCount.ToString()));
                }
                else
                {
                    builder.Append(String.Format(" COUNT={0}", feature.IsUncounted ? "uncounted" : feature.Count.ToString()));
                }
                if (feature.Expiration.HasValue)
                {
                    builder.Append(String.Format(" EXPIRATION={0}", feature.IsPerpetual ? "permanent" : feature.Expiration.ToString()));
                }
                if (feature.IsMetered)
                {
                    builder.Append(String.Format(" MODEL=metered{0}{1}", feature.IsMeteredReusable ? " REUSABLE" : "",
                        feature.MeteredUndoInterval.HasValue ? " UNDO_INTERVAL=" + feature.MeteredUndoInterval.ToString() : ""));
                }
                if (!String.IsNullOrEmpty(feature.VendorString))
                {
                    builder.Append(" VENDOR_STRING=\"" + feature.VendorString + "\"");
                }
                if (!String.IsNullOrEmpty(feature.Issuer))
                {
                    builder.Append(" ISSUER=\"" + feature.Issuer + "\"");
                }
                if (feature.Issued.HasValue)
                {
                    builder.Append(" ISSUED=" + feature.Issued.ToString());
                }
                if (!String.IsNullOrEmpty(feature.Notice))
                {
                    builder.Append(" NOTICE=\"" + feature.Notice + "\"");
                }
                if (!String.IsNullOrEmpty(feature.SerialNumber))
                {
                    builder.Append(" SN=\"" + feature.SerialNumber + "\"");
                }
                if (feature.StartDate.HasValue)
                {
                    builder.Append(" START=" + feature.StartDate.ToString());
                }
                Util.DisplayInfoMessage(builder.ToString());
                index++;
            }
        }

        private static ICapabilityRequestOptions GenerateRequestOptions()
        {
            // create the capability request options object
            ICapabilityRequestOptions options = licensing.LicenseManager.CreateCapabilityRequestOptions();

            // Requesting licenses.
            //
            // Add requested license information here, such as desired features or rights IDs
            //
            // options.AddRightsId("ACT-TEST", 1);
            // options.AddDesiredFeature(new FeatureData(surveyFeature, version, 1));
            //
            //
            // Previewing available licenses.
            //
            // If not generating a capability request to be serviced by a back-office license server.
            // you may uncomment the following code to create a preview capability request. The license
            // server will return details for the specified features or, if options.RequestAllFeatures is
            // set to true, return details for all features that could potentially be served to this client.
            //
            // Caution: You may not specify desired features and also set 'request all features' to true
            // on the same preview capability request.
            //
            // options.Operation = CapabilityRequestOperation.Preview;
            //
            // Add specific features to preview...
            // options.AddDesiredFeature(new FeatureData(surveyFeature, version, 5));
            //
            // ... or alternatively preview all possible features.
            // options.RequestAllFeatures = true;
            //

            // Optionally add capability requeest vendor dictionary items.
            options.AddVendorDictionaryItem(dictionaryKey1, "Some string value");
            options.AddVendorDictionaryItem(dictionaryKey2, 123);

            // force capability response from server even if nothing has changed
            options.ForceResponse = true;

            return options;
        }

        private static void AcquireReturn(string requestedFeature, string requestedVersion)
        {
            // acquire license
            string currentFeature = requestedFeature;
            Util.DisplayInfoMessage(String.Format(attemptingAcquire, requestedFeature, requestedVersion));
            try
            {
                ILicense acquiredLicense = licensing.LicenseManager.Acquire(requestedFeature, requestedVersion);
                try
                {
                    currentFeature = acquiredLicense.Name;
                    Util.DisplayInfoMessage(String.Format(licenseAcquired, currentFeature, requestedVersion));
                    //// application logic here
                }
                finally
                {
                    // return license 
                    Util.DisplayInfoMessage(String.Format(attemptingReturn, currentFeature, requestedVersion));
                    acquiredLicense.ReturnLicense();
                    Util.DisplayInfoMessage(String.Format(licenseReturned, currentFeature, requestedVersion));
                }
            }
            catch (Exception exc)
            {
                HandleException(exc);
            }
        }

        private static void HandleException(Exception exc)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(String.Format("{0} encountered: ", exc.GetType()));
            PublicLicensingException flxException = exc as PublicLicensingException;
            if (flxException != null)
            {
                switch (flxException.ErrorCode)
                {
                    case ErrorCode.FLXERR_RESPONSE_STALE:
                    case ErrorCode.FLXERR_RESPONSE_EXPIRED:
                    case ErrorCode.FLXERR_CAPABILITY_RESPONSE_DATA_MISSING:
                    case ErrorCode.FLXERR_PREVIEW_RESPONSE_NOT_PROCESSED:
                        builder.Append(String.Format("{0}: {1}", processingCapabilityResponse, flxException));
                        break;
                    case ErrorCode.FLXERR_FEATURE_NOT_FOUND:
                        builder.Append(String.Format("{0} {1}: {2}", acquiringLicense, currentFeature, flxException));
                        break;
                    default:
                        builder.Append(flxException.ToString());
                        break;
                }
            }
            else
            {
                builder.Append(exc.Message);
            }
            Util.DisplayErrorMessage(builder.ToString());
        }

        private static void Usage(string applicationName)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(String.Empty);
            builder.AppendLine(String.Format("{0} [-generate outputfile]", applicationName));
            builder.AppendLine(String.Format("{0} [-process inputfile]", applicationName));
            builder.AppendLine(String.Format("{0} [-server url]", applicationName));
            builder.AppendLine(String.Empty);
            builder.AppendLine("where:");
	        builder.AppendLine("-generate Generates capability request into a file.");
            builder.AppendLine("-process  Processes capability response from a file.");
            builder.AppendLine("-server   Sends request to a server and processes the response.");
            builder.AppendLine("          For the test back-office server, use");
            builder.AppendLine("          http://hostname:8080/request.");
            builder.AppendLine("          For FlexNet Operations, use");
            builder.AppendLine("          http://hostname:8888/flexnet/deviceservices.");
            builder.AppendLine("          For FlexNet Embedded License Server, use");
            builder.AppendLine("          http://hostname:7070/fne/bin/capability.");
            builder.AppendLine("          For Cloud License Server, use");
            builder.AppendLine("          https://<tenant>.compliance.flexnetoperations.com/instances/<instance-id>/request.");
            builder.AppendLine("          For FNO Cloud, use");
            builder.AppendLine("          https://<tenant>.compliance.flexnetoperations.com/deviceservices.");

            Util.DisplayMessage(builder.ToString(), "USAGE");
        }
    }
}