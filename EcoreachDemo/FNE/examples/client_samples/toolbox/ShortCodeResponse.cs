﻿// <copyright file="SaveAs.cs" company="Flexera Software LLC">
//     Copyright (c) 2012-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using FlxDotNetClient;

namespace DotNetDemo
{
    public partial class ShortCodeResponse : Form
    {
        IShortCodeEnvironment shortCodeEnv;
        int segmentSize = 0;

        bool segmentedResponseDone = false;

        ErrorProvider errorProvider;

        public ShortCodeResponse()
        {
            InitializeComponent();
            ShortCodeResponse_SystemColorsChanged(this, new EventArgs());
            errorProvider = new ErrorProvider(this);
            errorProvider.BlinkRate = 500;
        }

        public IShortCodeEnvironment ShortCodeEnvironment
        {
            set
            {
                shortCodeEnv = value;
            }
        }

        public int SegmentSize
        {
            set
            {
                segmentSize = value;
            }
        }

        private void btnSCSegmentAdd_Click(object sender, EventArgs e)
        {
            try
            {
                shortCodeEnv.AddResponseSegment(txtSCSegment.Text);
                txtSCResponse.Text += txtSCSegment.Text;
                if (txtSCSegment.Text.Length < segmentSize)
                {
                    segmentedResponseDone = true;
                    txtSCSegment.Enabled = false;
                    btnSCSegmentAdd.Enabled = false;
                    lblSCResponseComplete.Visible = true;
                }
                txtSCSegment.Text = string.Empty;
            }
            catch (Exception exc)
            {
                errorProvider.SetIconAlignment(txtSCSegment, ErrorIconAlignment.MiddleLeft);
                errorProvider.SetError(txtSCSegment, exc.Message);
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            errorProvider.Clear();
            if (segmentSize == 0)
            {
                try
                {
                    shortCodeEnv.SetResponse(txtSCResponse.Text);
                    this.DialogResult = DialogResult.OK;
                }
                catch (Exception exc)
                {
                    errorProvider.SetIconAlignment(txtSCResponse, ErrorIconAlignment.MiddleLeft);
                    errorProvider.SetError(txtSCResponse, exc.Message);
                }
            }
            else if (segmentedResponseDone)
            {
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                try
                {
                    shortCodeEnv.AddResponseSegment(null);
                    this.DialogResult = DialogResult.OK;
                }
                catch (Exception exc)
                {
                    errorProvider.SetIconAlignment(txtSCSegment, ErrorIconAlignment.MiddleLeft);
                    errorProvider.SetError(txtSCSegment, exc.Message);
                }
            }
        }
 
        private void errorprovider_reset(object sender, EventArgs e)
        {
            errorProvider.Clear();
        }

        private void ShortCodeResponse_SystemColorsChanged(object sender, EventArgs e)
        {
            lblSCResponseComplete.ForeColor = SystemInformation.HighContrast ? Color.LightBlue : Color.Blue;
            btnSCSegmentAdd.BackColor = SystemInformation.HighContrast ? Color.SteelBlue : Color.LightSteelBlue;
            btnOK.BackColor = SystemInformation.HighContrast ? Color.Gray : Color.Gainsboro;
        }

        private void ShortCodeResponse_Load(object sender, EventArgs e)
        {
            if (shortCodeEnv != null)
            {
                if (segmentSize == 0)
                {
                    lblSCSegment.Visible = txtSCSegment.Visible = btnSCSegmentAdd.Visible = false;
                    txtSCResponse.Size = new Size(txtSCResponse.Size.Width,
                        txtSCResponse.Size.Height + (txtSCResponse.Location.Y - txtSCSegment.Location.Y));
                    lblSCResponse.Location = new Point(lblSCResponse.Location.X, lblSCSegment.Location.Y);
                    txtSCResponse.Location = new Point(txtSCResponse.Location.X, txtSCSegment.Location.Y);
                }
                else
                {
                    txtSCSegment.MaxLength = segmentSize;
                    txtSCResponse.ReadOnly = true;
                }
            }
        }
 
    }
}
