// <copyright file="FeatureProperties.Designer.cs" company="Flexera Software LLC">
//     Copyright (c) 2012-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

namespace DotNetDemo
{
    partial class FeatureProperties
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblFPVersion = new System.Windows.Forms.Label();
            this.lblFPname = new System.Windows.Forms.Label();
            this.txtFPName = new System.Windows.Forms.TextBox();
            this.txtFPVersion = new System.Windows.Forms.TextBox();
            this.lblFPCount = new System.Windows.Forms.Label();
            this.txtFPCount = new System.Windows.Forms.TextBox();
            this.lblFPIssued = new System.Windows.Forms.Label();
            this.txtFPIssued = new System.Windows.Forms.TextBox();
            this.lblFPStart = new System.Windows.Forms.Label();
            this.txtFPStart = new System.Windows.Forms.TextBox();
            this.lblFPExpiration = new System.Windows.Forms.Label();
            this.txtFPExpiration = new System.Windows.Forms.TextBox();
            this.lblFPHostIds = new System.Windows.Forms.Label();
            this.lblFPVendorString = new System.Windows.Forms.Label();
            this.txtFPVendorString = new System.Windows.Forms.TextBox();
            this.lblFPIssuer = new System.Windows.Forms.Label();
            this.txtFPIssuer = new System.Windows.Forms.TextBox();
            this.lblFPNotice = new System.Windows.Forms.Label();
            this.txtFPNotice = new System.Windows.Forms.TextBox();
            this.lblFPSN = new System.Windows.Forms.Label();
            this.txtFPSN = new System.Windows.Forms.TextBox();
            this.lblFPAcqStatus = new System.Windows.Forms.Label();
            this.txtFPHostIds = new System.Windows.Forms.TextBox();
            this.txtFPAcqStatus = new System.Windows.Forms.TextBox();
            this.lblFPSrvStatus = new System.Windows.Forms.Label();
            this.txtFPSrvStatus = new System.Windows.Forms.TextBox();
            this.ckbFPMetered = new System.Windows.Forms.CheckBox();
            this.ckbFPReusable = new System.Windows.Forms.CheckBox();
            this.lblFPAvailable = new System.Windows.Forms.Label();
            this.txtFPAvailable = new System.Windows.Forms.TextBox();
            this.lblFPUndoInterval = new System.Windows.Forms.Label();
            this.txtFPUndoInterval = new System.Windows.Forms.TextBox();
            this.lblFPUndoRemaining = new System.Windows.Forms.Label();
            this.txtFPUndoRemaining = new System.Windows.Forms.TextBox();
            this.undoTimer = new System.Windows.Forms.Timer(this.components);
            this.ckbFPStale = new System.Windows.Forms.CheckBox();
            this.lblFPFinalExpiration = new System.Windows.Forms.Label();
            this.txtFPFinalExpiration = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblFPVersion
            // 
            this.lblFPVersion.AutoSize = true;
            this.lblFPVersion.Location = new System.Drawing.Point(15, 45);
            this.lblFPVersion.Name = "lblFPVersion";
            this.lblFPVersion.Size = new System.Drawing.Size(45, 13);
            this.lblFPVersion.TabIndex = 3;
            this.lblFPVersion.Text = "Version:";
            // 
            // lblFPname
            // 
            this.lblFPname.AutoSize = true;
            this.lblFPname.Location = new System.Drawing.Point(15, 15);
            this.lblFPname.Name = "lblFPname";
            this.lblFPname.Size = new System.Drawing.Size(38, 13);
            this.lblFPname.TabIndex = 1;
            this.lblFPname.Text = "Name:";
            // 
            // txtFPName
            // 
            this.txtFPName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFPName.Location = new System.Drawing.Point(100, 13);
            this.txtFPName.Name = "txtFPName";
            this.txtFPName.ReadOnly = true;
            this.txtFPName.Size = new System.Drawing.Size(250, 20);
            this.txtFPName.TabIndex = 2;
            // 
            // txtFPVersion
            // 
            this.txtFPVersion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFPVersion.Location = new System.Drawing.Point(100, 43);
            this.txtFPVersion.Name = "txtFPVersion";
            this.txtFPVersion.ReadOnly = true;
            this.txtFPVersion.Size = new System.Drawing.Size(250, 20);
            this.txtFPVersion.TabIndex = 4;
            // 
            // lblFPCount
            // 
            this.lblFPCount.AutoSize = true;
            this.lblFPCount.Location = new System.Drawing.Point(15, 75);
            this.lblFPCount.Name = "lblFPCount";
            this.lblFPCount.Size = new System.Drawing.Size(38, 13);
            this.lblFPCount.TabIndex = 5;
            this.lblFPCount.Text = "Count:";
            // 
            // txtFPCount
            // 
            this.txtFPCount.Location = new System.Drawing.Point(100, 73);
            this.txtFPCount.Name = "txtFPCount";
            this.txtFPCount.ReadOnly = true;
            this.txtFPCount.Size = new System.Drawing.Size(150, 20);
            this.txtFPCount.TabIndex = 6;
            // 
            // lblFPIssued
            // 
            this.lblFPIssued.AutoSize = true;
            this.lblFPIssued.Location = new System.Drawing.Point(15, 135);
            this.lblFPIssued.Name = "lblFPIssued";
            this.lblFPIssued.Size = new System.Drawing.Size(41, 13);
            this.lblFPIssued.TabIndex = 16;
            this.lblFPIssued.Text = "Issued:";
            // 
            // txtFPIssued
            // 
            this.txtFPIssued.Location = new System.Drawing.Point(100, 133);
            this.txtFPIssued.Name = "txtFPIssued";
            this.txtFPIssued.ReadOnly = true;
            this.txtFPIssued.Size = new System.Drawing.Size(150, 20);
            this.txtFPIssued.TabIndex = 17;
            // 
            // lblFPStart
            // 
            this.lblFPStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFPStart.AutoSize = true;
            this.lblFPStart.Location = new System.Drawing.Point(265, 135);
            this.lblFPStart.Name = "lblFPStart";
            this.lblFPStart.Size = new System.Drawing.Size(56, 13);
            this.lblFPStart.TabIndex = 18;
            this.lblFPStart.Text = "Start date:";
            // 
            // txtFPStart
            // 
            this.txtFPStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFPStart.Location = new System.Drawing.Point(325, 133);
            this.txtFPStart.Name = "txtFPStart";
            this.txtFPStart.ReadOnly = true;
            this.txtFPStart.Size = new System.Drawing.Size(150, 20);
            this.txtFPStart.TabIndex = 19;
            // 
            // lblFPExpiration
            // 
            this.lblFPExpiration.AutoSize = true;
            this.lblFPExpiration.Location = new System.Drawing.Point(15, 165);
            this.lblFPExpiration.Name = "lblFPExpiration";
            this.lblFPExpiration.Size = new System.Drawing.Size(56, 13);
            this.lblFPExpiration.TabIndex = 20;
            this.lblFPExpiration.Text = "Expiration:";
            // 
            // txtFPExpiration
            // 
            this.txtFPExpiration.Location = new System.Drawing.Point(100, 163);
            this.txtFPExpiration.Name = "txtFPExpiration";
            this.txtFPExpiration.ReadOnly = true;
            this.txtFPExpiration.Size = new System.Drawing.Size(150, 20);
            this.txtFPExpiration.TabIndex = 21;
            // 
            // lblFPHostIds
            // 
            this.lblFPHostIds.AutoSize = true;
            this.lblFPHostIds.Location = new System.Drawing.Point(15, 195);
            this.lblFPHostIds.Name = "lblFPHostIds";
            this.lblFPHostIds.Size = new System.Drawing.Size(48, 13);
            this.lblFPHostIds.TabIndex = 24;
            this.lblFPHostIds.Text = "Host ids:";
            // 
            // lblFPVendorString
            // 
            this.lblFPVendorString.AutoSize = true;
            this.lblFPVendorString.Location = new System.Drawing.Point(15, 255);
            this.lblFPVendorString.Name = "lblFPVendorString";
            this.lblFPVendorString.Size = new System.Drawing.Size(72, 13);
            this.lblFPVendorString.TabIndex = 26;
            this.lblFPVendorString.Text = "Vendor string:";
            // 
            // txtFPVendorString
            // 
            this.txtFPVendorString.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFPVendorString.Location = new System.Drawing.Point(100, 253);
            this.txtFPVendorString.Multiline = true;
            this.txtFPVendorString.Name = "txtFPVendorString";
            this.txtFPVendorString.ReadOnly = true;
            this.txtFPVendorString.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtFPVendorString.Size = new System.Drawing.Size(375, 50);
            this.txtFPVendorString.TabIndex = 27;
            // 
            // lblFPIssuer
            // 
            this.lblFPIssuer.AutoSize = true;
            this.lblFPIssuer.Location = new System.Drawing.Point(15, 315);
            this.lblFPIssuer.Name = "lblFPIssuer";
            this.lblFPIssuer.Size = new System.Drawing.Size(38, 13);
            this.lblFPIssuer.TabIndex = 28;
            this.lblFPIssuer.Text = "Issuer:";
            // 
            // txtFPIssuer
            // 
            this.txtFPIssuer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFPIssuer.Location = new System.Drawing.Point(120, 313);
            this.txtFPIssuer.Name = "txtFPIssuer";
            this.txtFPIssuer.ReadOnly = true;
            this.txtFPIssuer.Size = new System.Drawing.Size(355, 20);
            this.txtFPIssuer.TabIndex = 29;
            // 
            // lblFPNotice
            // 
            this.lblFPNotice.AutoSize = true;
            this.lblFPNotice.Location = new System.Drawing.Point(15, 345);
            this.lblFPNotice.Name = "lblFPNotice";
            this.lblFPNotice.Size = new System.Drawing.Size(41, 13);
            this.lblFPNotice.TabIndex = 30;
            this.lblFPNotice.Text = "Notice:";
            // 
            // txtFPNotice
            // 
            this.txtFPNotice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFPNotice.Location = new System.Drawing.Point(120, 343);
            this.txtFPNotice.Name = "txtFPNotice";
            this.txtFPNotice.ReadOnly = true;
            this.txtFPNotice.Size = new System.Drawing.Size(355, 20);
            this.txtFPNotice.TabIndex = 31;
            // 
            // lblFPSN
            // 
            this.lblFPSN.AutoSize = true;
            this.lblFPSN.Location = new System.Drawing.Point(15, 375);
            this.lblFPSN.Name = "lblFPSN";
            this.lblFPSN.Size = new System.Drawing.Size(74, 13);
            this.lblFPSN.TabIndex = 32;
            this.lblFPSN.Text = "Serial number:";
            // 
            // txtFPSN
            // 
            this.txtFPSN.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFPSN.Location = new System.Drawing.Point(120, 373);
            this.txtFPSN.Name = "txtFPSN";
            this.txtFPSN.ReadOnly = true;
            this.txtFPSN.Size = new System.Drawing.Size(355, 20);
            this.txtFPSN.TabIndex = 33;
            // 
            // lblFPAcqStatus
            // 
            this.lblFPAcqStatus.AutoSize = true;
            this.lblFPAcqStatus.Location = new System.Drawing.Point(15, 405);
            this.lblFPAcqStatus.Name = "lblFPAcqStatus";
            this.lblFPAcqStatus.Size = new System.Drawing.Size(92, 13);
            this.lblFPAcqStatus.TabIndex = 34;
            this.lblFPAcqStatus.Text = "Acquisition status:";
            // 
            // txtFPHostIds
            // 
            this.txtFPHostIds.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFPHostIds.Location = new System.Drawing.Point(100, 193);
            this.txtFPHostIds.Multiline = true;
            this.txtFPHostIds.Name = "txtFPHostIds";
            this.txtFPHostIds.ReadOnly = true;
            this.txtFPHostIds.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtFPHostIds.Size = new System.Drawing.Size(375, 50);
            this.txtFPHostIds.TabIndex = 25;
            // 
            // txtFPAcqStatus
            // 
            this.txtFPAcqStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFPAcqStatus.Location = new System.Drawing.Point(120, 403);
            this.txtFPAcqStatus.Name = "txtFPAcqStatus";
            this.txtFPAcqStatus.ReadOnly = true;
            this.txtFPAcqStatus.Size = new System.Drawing.Size(355, 20);
            this.txtFPAcqStatus.TabIndex = 35;
            // 
            // lblFPSrvStatus
            // 
            this.lblFPSrvStatus.AutoSize = true;
            this.lblFPSrvStatus.Location = new System.Drawing.Point(15, 435);
            this.lblFPSrvStatus.Name = "lblFPSrvStatus";
            this.lblFPSrvStatus.Size = new System.Drawing.Size(77, 13);
            this.lblFPSrvStatus.TabIndex = 36;
            this.lblFPSrvStatus.Text = "Serving status:";
            // 
            // txtFPSrvStatus
            // 
            this.txtFPSrvStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFPSrvStatus.Location = new System.Drawing.Point(120, 433);
            this.txtFPSrvStatus.Name = "txtFPSrvStatus";
            this.txtFPSrvStatus.ReadOnly = true;
            this.txtFPSrvStatus.Size = new System.Drawing.Size(355, 20);
            this.txtFPSrvStatus.TabIndex = 37;
            // 
            // ckbFPMetered
            // 
            this.ckbFPMetered.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ckbFPMetered.AutoSize = true;
            this.ckbFPMetered.Location = new System.Drawing.Point(370, 15);
            this.ckbFPMetered.Name = "ckbFPMetered";
            this.ckbFPMetered.Size = new System.Drawing.Size(65, 17);
            this.ckbFPMetered.TabIndex = 7;
            this.ckbFPMetered.Text = "Metered";
            this.ckbFPMetered.UseVisualStyleBackColor = true;
            // 
            // ckbFPReusable
            // 
            this.ckbFPReusable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ckbFPReusable.AutoSize = true;
            this.ckbFPReusable.Location = new System.Drawing.Point(370, 45);
            this.ckbFPReusable.Name = "ckbFPReusable";
            this.ckbFPReusable.Size = new System.Drawing.Size(71, 17);
            this.ckbFPReusable.TabIndex = 8;
            this.ckbFPReusable.Text = "Reusable";
            this.ckbFPReusable.UseVisualStyleBackColor = true;
            // 
            // lblFPAvailable
            // 
            this.lblFPAvailable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFPAvailable.AutoSize = true;
            this.lblFPAvailable.Location = new System.Drawing.Point(265, 75);
            this.lblFPAvailable.Name = "lblFPAvailable";
            this.lblFPAvailable.Size = new System.Drawing.Size(53, 13);
            this.lblFPAvailable.TabIndex = 10;
            this.lblFPAvailable.Text = "Available:";
            // 
            // txtFPAvailable
            // 
            this.txtFPAvailable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFPAvailable.Location = new System.Drawing.Point(325, 73);
            this.txtFPAvailable.Name = "txtFPAvailable";
            this.txtFPAvailable.ReadOnly = true;
            this.txtFPAvailable.Size = new System.Drawing.Size(150, 20);
            this.txtFPAvailable.TabIndex = 11;
            // 
            // lblFPUndoInterval
            // 
            this.lblFPUndoInterval.AutoSize = true;
            this.lblFPUndoInterval.Location = new System.Drawing.Point(15, 105);
            this.lblFPUndoInterval.Name = "lblFPUndoInterval";
            this.lblFPUndoInterval.Size = new System.Drawing.Size(73, 13);
            this.lblFPUndoInterval.TabIndex = 12;
            this.lblFPUndoInterval.Text = "Undo interval:";
            // 
            // txtFPUndoInterval
            // 
            this.txtFPUndoInterval.Location = new System.Drawing.Point(100, 103);
            this.txtFPUndoInterval.Name = "txtFPUndoInterval";
            this.txtFPUndoInterval.ReadOnly = true;
            this.txtFPUndoInterval.Size = new System.Drawing.Size(150, 20);
            this.txtFPUndoInterval.TabIndex = 13;
            // 
            // lblFPUndoRemaining
            // 
            this.lblFPUndoRemaining.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFPUndoRemaining.AutoSize = true;
            this.lblFPUndoRemaining.Location = new System.Drawing.Point(265, 105);
            this.lblFPUndoRemaining.Name = "lblFPUndoRemaining";
            this.lblFPUndoRemaining.Size = new System.Drawing.Size(60, 13);
            this.lblFPUndoRemaining.TabIndex = 14;
            this.lblFPUndoRemaining.Text = "Remaining:";
            // 
            // txtFPUndoRemaining
            // 
            this.txtFPUndoRemaining.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFPUndoRemaining.Location = new System.Drawing.Point(325, 103);
            this.txtFPUndoRemaining.Name = "txtFPUndoRemaining";
            this.txtFPUndoRemaining.ReadOnly = true;
            this.txtFPUndoRemaining.Size = new System.Drawing.Size(150, 20);
            this.txtFPUndoRemaining.TabIndex = 15;
            // 
            // undoTimer
            // 
            this.undoTimer.Interval = 1000;
            // 
            // ckbFPStale
            // 
            this.ckbFPStale.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ckbFPStale.AutoSize = true;
            this.ckbFPStale.Location = new System.Drawing.Point(370, 75);
            this.ckbFPStale.Name = "ckbFPStale";
            this.ckbFPStale.Size = new System.Drawing.Size(50, 17);
            this.ckbFPStale.TabIndex = 9;
            this.ckbFPStale.Text = "Stale";
            this.ckbFPStale.UseVisualStyleBackColor = true;
            // 
            // lblFPFinalExpiration
            // 
            this.lblFPFinalExpiration.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFPFinalExpiration.AutoSize = true;
            this.lblFPFinalExpiration.Location = new System.Drawing.Point(265, 165);
            this.lblFPFinalExpiration.Name = "lblFPFinalExpiration";
            this.lblFPFinalExpiration.Size = new System.Drawing.Size(52, 13);
            this.lblFPFinalExpiration.TabIndex = 22;
            this.lblFPFinalExpiration.Text = "Final exp:";
            // 
            // txtFPFinalExpiration
            // 
            this.txtFPFinalExpiration.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFPFinalExpiration.Location = new System.Drawing.Point(325, 163);
            this.txtFPFinalExpiration.Name = "txtFPFinalExpiration";
            this.txtFPFinalExpiration.ReadOnly = true;
            this.txtFPFinalExpiration.Size = new System.Drawing.Size(150, 20);
            this.txtFPFinalExpiration.TabIndex = 23;
            // 
            // FeatureProperties
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 462);
            this.Controls.Add(this.txtFPFinalExpiration);
            this.Controls.Add(this.lblFPFinalExpiration);
            this.Controls.Add(this.txtFPUndoRemaining);
            this.Controls.Add(this.lblFPUndoRemaining);
            this.Controls.Add(this.txtFPUndoInterval);
            this.Controls.Add(this.lblFPUndoInterval);
            this.Controls.Add(this.lblFPAvailable);
            this.Controls.Add(this.ckbFPReusable);
            this.Controls.Add(this.ckbFPMetered);
            this.Controls.Add(this.txtFPSrvStatus);
            this.Controls.Add(this.lblFPSrvStatus);
            this.Controls.Add(this.txtFPAcqStatus);
            this.Controls.Add(this.txtFPHostIds);
            this.Controls.Add(this.lblFPAcqStatus);
            this.Controls.Add(this.txtFPSN);
            this.Controls.Add(this.lblFPSN);
            this.Controls.Add(this.txtFPNotice);
            this.Controls.Add(this.lblFPNotice);
            this.Controls.Add(this.txtFPIssuer);
            this.Controls.Add(this.lblFPIssuer);
            this.Controls.Add(this.txtFPVendorString);
            this.Controls.Add(this.lblFPVendorString);
            this.Controls.Add(this.lblFPHostIds);
            this.Controls.Add(this.txtFPExpiration);
            this.Controls.Add(this.lblFPExpiration);
            this.Controls.Add(this.txtFPStart);
            this.Controls.Add(this.lblFPStart);
            this.Controls.Add(this.txtFPIssued);
            this.Controls.Add(this.lblFPIssued);
            this.Controls.Add(this.txtFPCount);
            this.Controls.Add(this.lblFPCount);
            this.Controls.Add(this.txtFPVersion);
            this.Controls.Add(this.lblFPVersion);
            this.Controls.Add(this.lblFPname);
            this.Controls.Add(this.txtFPName);
            this.Controls.Add(this.ckbFPStale);
            this.Controls.Add(this.txtFPAvailable);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(500, 500);
            this.Name = "FeatureProperties";
            this.SystemColorsChanged += new System.EventHandler(this.FeatureProperties_SystemColorsChanged);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFPVersion;
        private System.Windows.Forms.Label lblFPname;
        private System.Windows.Forms.TextBox txtFPName;
        private System.Windows.Forms.TextBox txtFPVersion;
        private System.Windows.Forms.Label lblFPCount;
        private System.Windows.Forms.TextBox txtFPCount;
        private System.Windows.Forms.Label lblFPIssued;
        private System.Windows.Forms.TextBox txtFPIssued;
        private System.Windows.Forms.Label lblFPStart;
        private System.Windows.Forms.TextBox txtFPStart;
        private System.Windows.Forms.Label lblFPExpiration;
        private System.Windows.Forms.TextBox txtFPExpiration;
        private System.Windows.Forms.Label lblFPHostIds;
        private System.Windows.Forms.Label lblFPVendorString;
        private System.Windows.Forms.TextBox txtFPVendorString;
        private System.Windows.Forms.Label lblFPIssuer;
        private System.Windows.Forms.TextBox txtFPIssuer;
        private System.Windows.Forms.Label lblFPNotice;
        private System.Windows.Forms.TextBox txtFPNotice;
        private System.Windows.Forms.Label lblFPSN;
        private System.Windows.Forms.TextBox txtFPSN;
        private System.Windows.Forms.Label lblFPAcqStatus;
        private System.Windows.Forms.TextBox txtFPHostIds;
        private System.Windows.Forms.TextBox txtFPAcqStatus;
        private System.Windows.Forms.Label lblFPSrvStatus;
        private System.Windows.Forms.TextBox txtFPSrvStatus;
        private System.Windows.Forms.CheckBox ckbFPMetered;
        private System.Windows.Forms.CheckBox ckbFPReusable;
        private System.Windows.Forms.Label lblFPAvailable;
        private System.Windows.Forms.TextBox txtFPAvailable;
        private System.Windows.Forms.Label lblFPUndoInterval;
        private System.Windows.Forms.TextBox txtFPUndoInterval;
        private System.Windows.Forms.Label lblFPUndoRemaining;
        private System.Windows.Forms.TextBox txtFPUndoRemaining;
        private System.Windows.Forms.Timer undoTimer;
        private System.Windows.Forms.CheckBox ckbFPStale;
        private System.Windows.Forms.Label lblFPFinalExpiration;
        private System.Windows.Forms.TextBox txtFPFinalExpiration;

    }
}