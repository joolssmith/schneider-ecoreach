﻿// <copyright file="ErrorStatusDetails.cs" company="Flexera Software LLC">
//     Copyright (c) 2012-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

using System;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using FlxDotNetClient;

namespace DotNetDemo
{
    public partial class ErrorStatusDetails : Form
    {

        private FlxException exception;

        public ErrorStatusDetails()
        {
            InitializeComponent();
        }

        public FlxException StatusException
        {
            get { return exception; }
            set 
            {
                exception = value;
                if (exception == null)
                {
                    txtErrorStatus.Text = string.Empty;
                }
                else
                {
                    txtErrorStatus.Lines = Regex.Split(exception.ToString(), "\r\n|\r|\n");
                }
                txtErrorStatus.Select(0, 0);
            }
        }
    }
}
