// <copyright file="ShortCodeRequest.Designer.cs" company="Flexera Software LLC">
//     Copyright (c) 2012-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

namespace DotNetDemo
{
    partial class ShortCodeRequest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSCRequest = new System.Windows.Forms.Label();
            this.lblSaveSCRequestFile = new System.Windows.Forms.Label();
            this.txtSCRequest = new System.Windows.Forms.TextBox();
            this.txtSaveSCRequestFile = new System.Windows.Forms.TextBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnSaveSCRequest = new System.Windows.Forms.Button();
            this.btnSaveSCRequestFile = new System.Windows.Forms.Button();
            this.lblSCRequestSaved = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblSCRequest
            // 
            this.lblSCRequest.AutoSize = true;
            this.lblSCRequest.Location = new System.Drawing.Point(10, 10);
            this.lblSCRequest.Name = "lblSCRequest";
            this.lblSCRequest.Size = new System.Drawing.Size(106, 13);
            this.lblSCRequest.TabIndex = 1;
            this.lblSCRequest.Text = "Short Code Request:";
            // 
            // lblSaveSCRequestFile
            // 
            this.lblSaveSCRequestFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblSaveSCRequestFile.AutoSize = true;
            this.lblSaveSCRequestFile.Location = new System.Drawing.Point(10, 185);
            this.lblSaveSCRequestFile.Name = "lblSaveSCRequestFile";
            this.lblSaveSCRequestFile.Size = new System.Drawing.Size(66, 13);
            this.lblSaveSCRequestFile.TabIndex = 3;
            this.lblSaveSCRequestFile.Text = "Save to File:";
            // 
            // txtSCRequest
            // 
            this.txtSCRequest.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSCRequest.Location = new System.Drawing.Point(140, 8);
            this.txtSCRequest.MaxLength = 1048576;
            this.txtSCRequest.Multiline = true;
            this.txtSCRequest.Name = "txtSCRequest";
            this.txtSCRequest.ReadOnly = true;
            this.txtSCRequest.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtSCRequest.Size = new System.Drawing.Size(417, 160);
            this.txtSCRequest.TabIndex = 2;
            this.txtSCRequest.TextChanged += new System.EventHandler(this.errorprovider_reset);
            // 
            // txtSaveSCRequestFile
            // 
            this.txtSaveSCRequestFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSaveSCRequestFile.Location = new System.Drawing.Point(140, 183);
            this.txtSaveSCRequestFile.Name = "txtSaveSCRequestFile";
            this.txtSaveSCRequestFile.Size = new System.Drawing.Size(370, 20);
            this.txtSaveSCRequestFile.TabIndex = 4;
            this.txtSaveSCRequestFile.TextChanged += new System.EventHandler(this.txtSCRequestFile_TextChanged);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.BackColor = System.Drawing.Color.Gainsboro;
            this.btnOK.Location = new System.Drawing.Point(482, 225);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 8;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = false;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnSaveSCRequest
            // 
            this.btnSaveSCRequest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveSCRequest.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnSaveSCRequest.Enabled = false;
            this.btnSaveSCRequest.Location = new System.Drawing.Point(390, 225);
            this.btnSaveSCRequest.Name = "btnSaveSCRequest";
            this.btnSaveSCRequest.Size = new System.Drawing.Size(75, 23);
            this.btnSaveSCRequest.TabIndex = 7;
            this.btnSaveSCRequest.Text = "Save";
            this.btnSaveSCRequest.UseVisualStyleBackColor = false;
            this.btnSaveSCRequest.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnSaveSCRequestFile
            // 
            this.btnSaveSCRequestFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveSCRequestFile.ImageIndex = 1;
            this.btnSaveSCRequestFile.Location = new System.Drawing.Point(525, 181);
            this.btnSaveSCRequestFile.Name = "btnSaveSCRequestFile";
            this.btnSaveSCRequestFile.Size = new System.Drawing.Size(32, 23);
            this.btnSaveSCRequestFile.TabIndex = 5;
            this.btnSaveSCRequestFile.UseVisualStyleBackColor = true;
            this.btnSaveSCRequestFile.Click += new System.EventHandler(this.btnSCRequestFile_Click);
            // 
            // lblSCRequestSaved
            // 
            this.lblSCRequestSaved.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSCRequestSaved.AutoSize = true;
            this.lblSCRequestSaved.ForeColor = System.Drawing.Color.Blue;
            this.lblSCRequestSaved.Location = new System.Drawing.Point(250, 229);
            this.lblSCRequestSaved.Name = "lblSCRequestSaved";
            this.lblSCRequestSaved.Size = new System.Drawing.Size(129, 13);
            this.lblSCRequestSaved.TabIndex = 6;
            this.lblSCRequestSaved.Text = "Short code request saved";
            this.lblSCRequestSaved.Visible = false;
            // 
            // ShortCodeRequest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 262);
            this.Controls.Add(this.lblSCRequestSaved);
            this.Controls.Add(this.btnSaveSCRequestFile);
            this.Controls.Add(this.btnSaveSCRequest);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.txtSaveSCRequestFile);
            this.Controls.Add(this.txtSCRequest);
            this.Controls.Add(this.lblSaveSCRequestFile);
            this.Controls.Add(this.lblSCRequest);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(300, 300);
            this.Name = "ShortCodeRequest";
            this.Text = "Generated Short Code Request";
            this.SystemColorsChanged += new System.EventHandler(this.ShortCodeRequest_SystemColorsChanged);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSCRequest;
        private System.Windows.Forms.Label lblSaveSCRequestFile;
        private System.Windows.Forms.TextBox txtSCRequest;
        private System.Windows.Forms.TextBox txtSaveSCRequestFile;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnSaveSCRequest;
        private System.Windows.Forms.Button btnSaveSCRequestFile;
        private System.Windows.Forms.Label lblSCRequestSaved;

    }
}