﻿// <copyright file="SaveAs.cs" company="Flexera Software LLC">
//     Copyright (c) 2012-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using FlxDotNetClient;

namespace DotNetDemo
{
    public partial class ShortCodeRequest : Form
    {
        ErrorProvider errorProvider;
        string strSCRequestFilePath = null;

        public ShortCodeRequest()
        {
            InitializeComponent();
            btnSaveSCRequestFile.Image = Properties.Resources.document_open_2.ToBitmap();
            ShortCodeRequest_SystemColorsChanged(this, new EventArgs());
            errorProvider = new ErrorProvider(this);
            errorProvider.BlinkRate = 500;
        }

        public string SCRequest
        {
            set
            {
                txtSCRequest.Text = value;
            }
        }

        public string SCRequestFilePath
        {
            get { return strSCRequestFilePath; }
            set { strSCRequestFilePath = value; }
        }

        public string SCRequestFile
        {
            get { return txtSaveSCRequestFile.Text; }
            set { txtSaveSCRequestFile.Text = value;  }
        }

        private void txtSCRequestFile_TextChanged(object sender, EventArgs e)
        {
            errorprovider_reset(null, null);
            btnSaveSCRequest.Enabled = !string.IsNullOrEmpty(txtSaveSCRequestFile.Text);
        }

        private void btnSCRequestFile_Click(object sender, EventArgs e)
        {
            errorProvider.Clear();
            using (OpenFileDialog dialog = new OpenFileDialog())
            {
                dialog.AddExtension = true;
                dialog.Filter = DotNetDemoForm.strTextFilter;
                dialog.CheckFileExists = false;
                dialog.CheckPathExists = true;
                dialog.Title = DotNetDemoForm.strOpenSCRequestTitle;
                if (!string.IsNullOrEmpty(strSCRequestFilePath))
                {
                    dialog.InitialDirectory = strSCRequestFilePath;
                }
                DialogResult result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    txtSaveSCRequestFile.Text = dialog.FileName;
                    strSCRequestFilePath = Path.GetDirectoryName(dialog.FileName);
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            saveRequest();
            btnSaveSCRequest.Enabled = false;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = saveRequest() ? DialogResult.OK : DialogResult.None;
        }

        private bool saveRequest()
        {
            errorprovider_reset(null, null);
            if (btnSaveSCRequest.Enabled && !string.IsNullOrEmpty(txtSaveSCRequestFile.Text))
            {
                try
                {
                    File.WriteAllText(txtSaveSCRequestFile.Text, txtSCRequest.Text, Encoding.UTF8);
                    lblSCRequestSaved.Visible = true;
                }
                catch (Exception exc)
                {
                    errorProvider.SetIconAlignment(txtSaveSCRequestFile, ErrorIconAlignment.MiddleLeft);
                    errorProvider.SetError(txtSaveSCRequestFile, exc.Message);
                    return false;
                }
            }
            return true;
        }

        private void errorprovider_reset(object sender, EventArgs e)
        {
            errorProvider.Clear();
            lblSCRequestSaved.Visible = false;
        }

        private void ShortCodeRequest_SystemColorsChanged(object sender, EventArgs e)
        {
            lblSCRequestSaved.ForeColor = SystemInformation.HighContrast ? Color.LightBlue : Color.Blue;
            btnSaveSCRequest.BackColor = SystemInformation.HighContrast ? Color.SteelBlue : Color.LightSteelBlue;
            btnOK.BackColor = SystemInformation.HighContrast ? Color.Gray : Color.Gainsboro;
        }
 
    }
}
