﻿// <copyright file="FeatureView.cs" company="Flexera Software LLC">
//     Copyright (c) 2012-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using FlxDotNetClient;

namespace DotNetDemo
{
    public partial class FeatureView : Form
    {
        private const string strUncounted = "Uncounted";
        private const string strPerpetual = "Perpetual";
        private const string strFileType = "File Type: ";
        private const int iTypeHeaderIndex = 0;
        private const int iNameHeaderIndex = 1;
        private const int iVersionHeaderIndex = 2;
        private const int iCountHeaderIndex = 3;
        private const int iMCHeaderIndex = 4;
        private const int iExpHeaderIndex = 5;
        private const int iVFAHeaderIndex = 6;
        private const int iVSHeaderIndex = 7;
        private string sourceType = string.Empty;
        private ICapabilityResponse response = null;
        private Button btnResponseDetails = null;
        private const int btnWidth = 98;
        private const int btnHeight = 23;
        private const int btnMargin = 3;

        private Dictionary<Color, List<Color>> lvColors = new Dictionary<Color,List<Color>>();
        private readonly Color[] steelBlues = { Color.SteelBlue, Color.LightSteelBlue };
        private readonly Color[] blues = { Color.DarkBlue, Color.LightBlue };
        private readonly Color[] greys = { Color.DimGray, Color.Silver };
        private readonly Color[] goldens = { Color.DarkGoldenrod, Color.NavajoWhite };


        public FeatureView()
        {
            List<Color> lstSteelBlues = new List<Color>(steelBlues);
            List<Color> lsBlues = new List<Color>(blues);
            List<Color> lsGreys = new List<Color>(greys);
            List<Color> lsGoldens = new List<Color>(goldens);
            lvColors.Add(Color.SteelBlue, lstSteelBlues);
            lvColors.Add(Color.LightSteelBlue, lstSteelBlues);
            lvColors.Add(Color.DarkBlue, lsBlues);
            lvColors.Add(Color.LightBlue, lsBlues);
            lvColors.Add(Color.DimGray, lsGreys);
            lvColors.Add(Color.Silver, lsGreys);
            lvColors.Add(Color.DarkGoldenrod, lsGoldens);
            lvColors.Add(Color.NavajoWhite, lsGoldens);
            InitializeComponent();
            FeatureView_SystemColorsChanged(this, new EventArgs());
        }

        public IFeatureCollection Features
        {
            set
            {
                lvFeatureView.BeginUpdate();
                string type;
                bool showVendorString = false;
                bool isPreview = false;
                foreach (IFeature feature in value)
                {
                    if (!string.IsNullOrEmpty(feature.VendorString))
                    {
                        showVendorString = true;
                        break;
                    }
                }
                foreach (IFeature feature in value)
                {
                    if (feature.IsPreview)
                    {
                        isPreview = true;
                        break;
                    }
                }
                if (!showVendorString)
                {
                    // I should be able to remove this column by Name however there is a bug
                    // and the name in the designer is not available at runtime (set to "").
                    lvFeatureView.Columns.RemoveAt(iVSHeaderIndex);
                }
                if (isPreview)
                {   // Removing "Valid" column gives us back column width
                    lvFeatureView.Columns.RemoveAt(iVFAHeaderIndex);
                    lvFeatureView.Columns[iNameHeaderIndex].Width += 40;
                    lvFeatureView.Columns[iVersionHeaderIndex].Width += 26;
                    lvFeatureView.Columns[iCountHeaderIndex].Width += 15;
                    lvFeatureView.Columns[iMCHeaderIndex].Width += 15;
                    lvFeatureView.Columns[iExpHeaderIndex].Width += 10;
                }
                else
                {
                    lvFeatureView.Columns.RemoveAt(iMCHeaderIndex);
                }

                ListViewItem listViewItem;
                foreach (IFeature feature in value)
                {
                    type = feature.IsPreview ? "P" : String.Empty;
                    type += feature.IsMetered ? "M" : "C";
                    if (feature.IsMetered)
                    {
                        if (feature.IsMeteredReusable)
                        {
                            type += "R";
                        }
                        else if (feature.MeteredUndoInterval.HasValue && feature.MeteredUndoInterval.Value.Ticks > 0)
                        {
                            type += "U";
                        }
                    }
                    if (isPreview)
                    {
                        if (showVendorString)
                        {
                            listViewItem = new ListViewItem(new string[] { 
                            type,
                            feature.Name,
                            feature.Version,
                            feature.IsUncounted ? strUncounted : feature.Count.ToString(),
                            feature.MaxCount == 0x7fffffff ? strUncounted : feature.MaxCount.ToString(),
                            feature.IsPerpetual ? strPerpetual : feature.Expiration.ToString(),
                            feature.VendorString == null ? string.Empty : feature.VendorString });
                        }
                        else
                        {
                            listViewItem = new ListViewItem(new string[] { 
                            type,
                            feature.Name,
                            feature.Version,
                            feature.IsUncounted ? strUncounted : feature.Count.ToString(),
                            feature.MaxCount == 0x7fffffff ? strUncounted : feature.MaxCount.ToString(),
                            feature.IsPerpetual ? strPerpetual : feature.Expiration.ToString() });
                        }
                    }
                    else if (showVendorString)
                    {
                        listViewItem = new ListViewItem(new string[] { 
                            type,
                            feature.Name,
                            feature.Version,
                            feature.IsUncounted ? strUncounted : feature.Count.ToString(),
                            feature.IsPerpetual ? strPerpetual : feature.Expiration.ToString(),
                            feature.ValidStatusForAcquisition().ToString(),
                            feature.VendorString == null ? string.Empty : feature.VendorString });
                    }
                    else
                    {
                        listViewItem = new ListViewItem(new string[] { 
                            type,
                            feature.Name,
                            feature.Version,
                            feature.IsUncounted ? strUncounted : feature.Count.ToString(),
                            feature.IsPerpetual ? strPerpetual : feature.Expiration.ToString(),
                            feature.ValidStatusForAcquisition().ToString()});

                    }
                    listViewItem.ToolTipText = feature.ToString();
                    listViewItem.Tag = feature;
                    lvFeatureView.Items.Add(listViewItem);
                }
                lvFeatureView.EndUpdate();
            }
        }

        public Color LVBackColor
        {
            get { return lvFeatureView.BackColor; }
            set { lvFeatureView.BackColor = value; }
        }

        public ICapabilityResponse CapabilityResponse
        {
            get { return response; }
            set { response = value; }
        }

        public string SourceType
        {
            get { return sourceType; }
            set
            {
                sourceType = value;
                lblSourceType.Text = strFileType + SourceType;
            }
        }

        private void FeatureView_Load(object sender, System.EventArgs e)
        {
            if (response != null)
            {
                lvFeatureView.Size = new Size(lvFeatureView.Size.Width, lvFeatureView.Size.Height - (btnHeight + 2 * btnMargin));
                btnResponseDetails = new Button();
                btnResponseDetails.Size = new Size(btnWidth, btnHeight);
                btnResponseDetails.Location = new Point(lvFeatureView.Location.X + lvFeatureView.Size.Width / 2 - btnWidth / 2,
                                                        lvFeatureView.Location.Y + lvFeatureView.Size.Height + btnMargin);
                btnResponseDetails.Text = "Response Details";
                btnResponseDetails.BackColor = SystemInformation.HighContrast ? Color.DarkGoldenrod : Color.Beige;
                btnResponseDetails.Click += new System.EventHandler(btnResponseDetails_Click);
                this.Controls.Add(btnResponseDetails);
            }
        }

        void btnResponseDetails_Click(object sender, System.EventArgs e)
        {
            using (ResponseDetails dialog = new ResponseDetails())
            {
                dialog.Icon = this.Icon;
                dialog.Response = response;
                dialog.StartPosition = FormStartPosition.CenterParent;
                dialog.ShowDialog();
            }
        }

        private void FeatureView_SystemColorsChanged(object sender, System.EventArgs e)
        {
            if (btnResponseDetails != null)
            {
                btnResponseDetails.BackColor = SystemInformation.HighContrast ? Color.DarkGoldenrod : Color.Beige;
            }
            if (lvColors.ContainsKey(lvFeatureView.BackColor))
            {
                lvFeatureView.BackColor = lvColors[lvFeatureView.BackColor][SystemInformation.HighContrast ? 0 : 1];
            }
        }

        private void lvFeatureView_DoubleClick(object sender, EventArgs e)
        {
            if (lvFeatureView.SelectedItems != null    &&
                lvFeatureView.SelectedItems.Count > 0  &&
                lvFeatureView.SelectedItems[0].Tag is IFeature)
            {
                using (FeatureProperties dialog = new FeatureProperties())
                {
                    dialog.Icon = this.Icon;
                    dialog.Text = "Feature Properties";
                    dialog.Feature = lvFeatureView.SelectedItems[0].Tag as IFeature;
                    dialog.TextBackColor = lvFeatureView.BackColor;
                    dialog.StartPosition = FormStartPosition.CenterParent;
                    dialog.ShowDialog();
                }
            }
        }

    }
}
