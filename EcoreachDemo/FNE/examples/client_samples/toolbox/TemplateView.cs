﻿// <copyright file="TemplateView.cs" company="Flexera Software LLC">
//     Copyright (c) 2012-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using FlxDotNetClient;

namespace DotNetDemo
{
    public partial class TemplateView : Form
    {
        private const string strUncounted = "Uncounted";
        private const string strPerpetual = "Perpetual";
        private const int iVSHeaderIndex = 6;

        public TemplateView()
        {
            InitializeComponent();
            TemplateView_SystemColorsChanged(this, new EventArgs());
        }

        public ILicenseTemplate LicenseTemplate
        {
            set
            {
                if (value != null)
                {
                    ID = value.Id;
                    Expiration = value.Expiration;
                    MachineType = value.MachineType;
                    Features = value.FeatureCollection;
                    VendorDictionary = value.VendorDictionary;
                }
                else
                {
                    txtTemplateID.Text = txtTemplateExpiration.Text = txtTemplateMachineType.Text = string.Empty;
                    Features = null;
                    VendorDictionary = null;
                }
            }
        }

        public IFeatureCollection Features
        {
            set
            {
                lvTemplateFeatures.BeginUpdate();
                lvTemplateFeatures.Items.Clear();
                int index = 0;
                bool showVendorString = false;
                if (lvTemplateFeatures.Columns.Count > iVSHeaderIndex)
                {
                    foreach (IFeature feature in value)
                    {
                        if (!string.IsNullOrEmpty(feature.VendorString))
                        {
                            showVendorString = true;
                            break;
                        } 
                    }
                    if (!showVendorString)
                    {
                        // I should be able to remove this column by Name however there is a bug
                        // and the name in the designer is not available at runtime (set to "").
                        lvTemplateFeatures.Columns.RemoveAt(iVSHeaderIndex);
                    }
                }

                ListViewItem listViewItem;
                foreach (IFeature feature in value)
                {
                    index++;
                    if (showVendorString)
                    {
                        listViewItem = new ListViewItem(new string[] { 
                            index.ToString(),
                            feature.Name,
                            feature.Version,
                            feature.IsUncounted ? strUncounted : feature.Count.ToString(),
                            feature.IsPerpetual ? strPerpetual : feature.Expiration.ToString(),
                            feature.ValidStatusForAcquisition().ToString(),
                            feature.VendorString == null ? string.Empty : feature.VendorString });
                    }
                    else
                    {
                        listViewItem = new ListViewItem(new string[] { 
                            index.ToString(),
                            feature.Name,
                            feature.Version,
                            feature.IsUncounted ? strUncounted : feature.Count.ToString(),
                            feature.IsPerpetual ? strPerpetual : feature.Expiration.ToString(),
                            feature.ValidStatusForAcquisition().ToString()});

                    }
                    listViewItem.ToolTipText = feature.ToString();
                    listViewItem.Tag = feature;
                    lvTemplateFeatures.Items.Add(listViewItem);
                }
                lvTemplateFeatures.EndUpdate();
            }
        }

        public ReadOnlyDictionary VendorDictionary
        {
            set
            {
                lvTemplateVD.BeginUpdate();
                lvTemplateVD.Items.Clear();
                if (value == null || value.Count <= 0)
                {
                    if (tabTemplateDetails.TabPages.Contains(pageTemplateVD))
                    {
                        tabTemplateDetails.TabPages.Remove(pageTemplateVD);
                    }
                }
                else
                {
                    string itemType;
                    foreach (KeyValuePair<string, object> kvp in value)
                    {
                        if (kvp.Value is string)
                        {
                            itemType = "String";
                        }
                        else if (kvp.Value is int)
                        {
                            itemType = "Integer";
                        }
                        else
                        {
                            itemType = kvp.Value.GetType().ToString();
                        }
                        lvTemplateVD.Items.Add(new ListViewItem(new string[] { itemType, kvp.Key.ToString(), kvp.Value.ToString() }));
                    }
                    if (!tabTemplateDetails.TabPages.Contains(pageTemplateVD))
                    {
                        tabTemplateDetails.TabPages.Add(pageTemplateVD);
                    }
                }
                lvTemplateVD.EndUpdate();
            }
        }

        public int ID
        {
            set { txtTemplateID.Text = value.ToString(); }
        }

        public DateTime? Expiration
        {
            set { txtTemplateExpiration.Text = value.HasValue ? value.Value.ToString() : string.Empty; }
        }

        public MachineTypeEnum MachineType
        {
            set
            {
                switch (value)
                {
                    case MachineTypeEnum.FLX_MACHINE_TYPE_PHYSICAL:
                        txtTemplateMachineType.Text = "Physical";
                        break;
                    case MachineTypeEnum.FLX_MACHINE_TYPE_VIRTUAL:
                        txtTemplateMachineType.Text = "Virtual";
                        break;
                    default:
                        txtTemplateMachineType.Text = "Unknown";
                        break;
                }
            }
        }

        private void TemplateView_SystemColorsChanged(object sender, System.EventArgs e)
        {
            lvTemplateFeatures.BackColor = SystemInformation.HighContrast ? Color.Teal : Color.PaleTurquoise;
            lvTemplateVD.BackColor = SystemInformation.HighContrast ? Color.DarkGray : Color.Gainsboro;
        }

        private void lvTemplateFeatures_DoubleClick(object sender, EventArgs e)
        {
            if (lvTemplateFeatures.SelectedItems != null    &&
                lvTemplateFeatures.SelectedItems.Count > 0  &&
                lvTemplateFeatures.SelectedItems[0].Tag is IFeature)
            {
                using (FeatureProperties dialog = new FeatureProperties())
                {
                    dialog.Icon = this.Icon;
                    dialog.Text = "Feature Properties";
                    dialog.Feature = lvTemplateFeatures.SelectedItems[0].Tag as IFeature;
                    dialog.TextBackColor = lvTemplateFeatures.BackColor;
                    dialog.StartPosition = FormStartPosition.CenterParent;
                    dialog.ShowDialog();
                }
            }
        }

    }
}
