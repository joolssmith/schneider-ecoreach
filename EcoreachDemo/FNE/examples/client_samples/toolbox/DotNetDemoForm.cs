﻿// <copyright file="DotNetDemoForm.cs" company="Flexera Software LLC">
//     Copyright (c) 2012-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using FlxDotNetClient;

namespace DotNetDemo
{
    public partial class DotNetDemoForm : Form
    {
        internal const string strDotNetAssembly = "FlxLicensingClient";

        internal const string strLocal = "Local license server";
        internal const string strBackOffice = "Test back-office server";
        internal const string strFNO = "FlexNet Operations";
        // internal const string strFNOOD = "FlexNet Operations On-Demand";
        internal const string strCloud = "Cloud license server";
        internal const string strRequest = "Request";
        internal const string strReport = "Report";
        internal const string strUndo = "Undo";
        internal const string strPreview = "Preview";
        internal const string strDefault = "Default";
        internal const string strXMLFilter = "XML Files|*.xml";
        internal const string strXMLSuffix = ".xml";
        internal const string strBinaryFilter = "Binary Files|*.bin|All Files|*.*";
        internal const string strTextFilter = "Text Files|*.txt|All Files|*.*";
        internal const string strAllFilter = "All Files|*.*";
        internal const string strOpenSCRequestTitle = "Save Short Code Request Data To File";
        internal const string strOpenSCResponseTitle = "Specify Short Code Response Data File";
        internal const string strOpenSCRequestPDTitle = "Specify Short Code Request Publisher Data File";
        internal const string strSaveSCResponsePDTitle = "Save Short Code Response Publisher Data To File";

        internal const string strBlank = " ";
        internal const string strComma = ",";

        internal const string strDefaultTitle = "FlexNet Embedded .NET Toolbox";
        internal const string strConfigSaved = "Configuration {0} saved.";
        internal const string strConfigSaveFailed = "Save of configuration {0} failed.";
        internal const string strConfigSavedAs = "Configuration saved as {0}.";
        internal const string strConfigSaveAsFailed = "Save of configuration as {0} failed.";
        internal const string strConfigDeleted = "Configuration {0} deleted.";
        internal const string strConfigDeleteFailed = "Delete of configuration {0} failed.";
        internal const string strConfigExists = "Configuration name already exists.";
        internal const string strConfigToolTip1 = "Configurations contained in {0}.";
        internal const string strConfigToolTip2 = "No configuration file open.";
        internal const string strDefaultConfigurationFile = "DotNetDemo.xml";
        internal const string strDefaultConfiguration = "Default";
        internal const string strDefaultIdentityFile = "IdentityClient.bin";
        internal const string strMBTS = "Using memory based trusted storage.";
        internal const string strAllCaption = "All Features ({0})";
        internal const string strTSCaption = "Trusted Storage ({0})";
        internal const string strTrialsCaption = "Trials ({0})";
        internal const string strSCCaption = "Short Codes ({0})";
        internal const string strBuffersCaption = "Buffers ({0})";
        internal const string strClearOK = "{0} successfully cleared.";
        internal const string strUncounted = "Uncounted";
        internal const string strPerpetual = "Perpetual";
        internal const string strCustomId = "Custom string";
        internal const string strDemoHostId = "1234567890";
        internal const string strSetupWarning = "Licensing setup must be completed.";
        internal const string strInvalidLicenseFile = "Specified file is not a valid license file.";
        internal const string strEmptyLicenseFile = "License file is empty.";
        internal const string strEmptyLicenseFile2 = "No features contained in specified license file.";
        internal const string strEmptyTemplateFile = "License template file is empty.";
        internal const string strEmptyTemplateFile2 = "No features contained in specified template file.";
        internal const string strEmptyResponseFile = "Short code response file is empty.";
        internal const string strDeleteTemplateLicenses = "Licenses related to template id {0} have been successfully deleted from trusted storage.";
        internal const string strSourceCapResponseFile = "Trusted storage capability response";
        internal const string strSourcePreviewResponseFile = "Preview capability response";
        internal const string strSourceTrialFile = "Trial license";
        internal const string strSourceBufferFile = "Buffer license";
        internal const string strBufferLoaded = "Buffer license file {0} successfully loaded.";
        internal const string strTrialLoaded = "Trial license file {0} successfully loaded.";
        internal const string strCRLoaded = "Capability response license file {0} successfully processed.";
        internal const string strTemplateLoaded = "License template file {0} successfully loaded.";
        internal const string strTemplatesLoaded = "{0} license template files successfully loaded, {1} failed.";
        internal const string strErrorFile = "File {0} contains error data which may not be loaded.";
        internal const string strPreviewFile = "File {0} contains preview feature data which may not be loaded.";
        internal const string strUnknownFile = "Unrecognized file content.";
        internal const string strBufferNameRoot = "Buffer-";
        internal const string strDays = "Days";
        internal const string strHours = "Hours";
        internal const string strMinutes = "Minutes";
        internal const string strSeconds = "Seconds";
        internal const string strInteger = "Integer";
        internal const string strString = "String";
        internal const string strInvalidServerURL = "Invalid server URL specified.";
        internal const string strUnsupportedScheme = "Unsupported scheme:";
        internal const string strInvalidPort = "Invalid port value.";
        internal const string strInvalidBorrowInterval = "Invalid borrow interval value.";
        internal const string strInvalidCRRespDirectory = "Directory does not exist.";
        internal const string strInvalidCRRespFile = "File already exists.";
        internal const string strCRRespFileExistsCaption = "Replace {0}?";
        internal const string strCRRespFileExistsMsg = "The specified response file currently exists. Click 'Yes' to replace this file with new response data.";
        internal const string strRequiredCorrelationID = "Correlation ID is required for undo operation.";
        internal const string strMissingRightsID = "Rights ID not specified.";
        internal const string strMissingRightsCount = "Rights count not specified.";
        internal const string strInvalidRightsCount = "Invalid rights count.";
        internal const string strMissingFeatureName = "Desired feature name not specified.";
        internal const string strMissingFeatureVersion = "Desired feature version not specified.";
        internal const string strMissingFeatureCount = "Desired feature count not specified.";
        internal const string strInvalidFeatureCount = "Invalid desired feature count.";
        internal const string strMissingDictionaryKey = "Vendor dictionary key not specified.";
        internal const string strMissingIntDictionaryItem = "Integer vendor dictionary value not specified.";
        internal const string strInvalidIntDictionaryItem = "Invalid integer vendor dictionary value.";
        internal const string strFSMissingDictionaryKey = "Feature selector item dictionary key not specified.";
        internal const string strAuxHostIdMissingValue = "Auxiliary host id value not specified.";
        internal const string strMissingUsageName = "Usage feature name not specified.";
        internal const string strMissingUsageVersion = "Usage feature version not specified.";
        internal const string strMissingUsageCount = "Feature usage count not specified.";
        internal const string strInvalidUsageCount = "Invalid feature usage count.";
        internal const string strMissingAcquireName = "Feature name not specified.";
        internal const string strMissingAcquireVersion = "Feature version not specified.";
        internal const string strMissingAcquireCount = "Feature count not specified.";
        internal const string strInvalidAcquireCount = "Invalid feature count.";

        internal const string strSaveCRTitle = "Save Capability Request Data";
        internal const string strCRSaved = "Capability request saved to {0}.";
        internal const string strCRSaveError = "Capability request save to {0} failed.";
        internal const string strSaveIMTitle = "Save Information Message Data";
        internal const string strIMSaved = "Information message saved to {0}.";
        internal const string strIMSaveError = "Information message save to {0} failed.";
        internal const string strSending = "Sending...";
        internal const string strCRSendOK = "Capability request successfully sent.";
        internal const string strCRSendError = "Error encountered sending capability request.";
        internal const string strCRProcessRespError = "Error encoutered processing capability response.";
        internal const string strCRSendNoResp = "No response received.";
        internal const string strCRSendRespData = "Response data received.";
        internal const string strIMSendOK = "Information message successfully sent.";
        internal const string strIMSendError = "Error encountered sending information message.";
        internal const string strInternalError = "Internal error: invalid send parameters.";

        internal const string strOpenConfigTitle = "Open Configuration File";
        internal const string strOpenIdentityTitle = "Open Client Identity Data File";
        internal const string strOpenLicenseTitle = "Open License Data File";
        internal const string strOpenCRRespTitle = "Specify Capability Response File";
        internal const string strTSBrowserDescription = "Select Trusted Storage Path";
        internal const string strOpenTemplateTitle = "Open License Template Files";
        internal const string strSaveCaption = "Save Current Configuration Changes?";
        internal const string strSaveMsg = "Changes exist to the current configuration. Click 'Yes' to save changes.";
        internal const string strDeleteCaption = "Delete Current Configuration?";
        internal const string strDeleteMsg = "Are you sure you want to delete configuration '{0}'.";
        internal const string strAboutCaption = "About DotNetDemo";

        internal const string strBase10 = "Base 10";
        internal const string strBase16 = "Base 16";
        internal const string strBase34 = "Base 34";
        internal const string strBase36 = "Base 36";
        internal const string strCustom = "Custom";
        internal const string strNone = "None";
        private  string[] SCEncodings = new string[] { strBase10, strBase16, strBase34, strBase36, strCustom, strNone };

        internal const string strFeatureDialogTitle = "Features contained in ";
        internal const string strTemplateDialogTitle = "Template file details - ";
        internal const string strTempTSName = "Temp";
        internal const string strSCTemplateReqd = "No template selected for short code request.";
        internal const string strInvalidSegmentSize = "Invalid segment size value.";
        internal const string strNoCustomChars = "Custom encoding character set not provided.";
        internal const string strSCRequestFileReqd = "Short code request file specification required with encoding of 'none'.";
        internal const string strSCResponseFileReqd = "Short code response file specification required with encoding of 'none'.";
        internal const string strInvalidHexString1 = "Invalid number of hexadecimal digits.";
        internal const string strInvalidHexString2 = "Invalid hexadecimal digits in data.";
        internal const string strSCRequestToFile = "Short code request written to '{0}'.";
        internal const string strSCRequestCaption = "Generated Short Code Request";
        internal const string strSCResponseRead = "Short code response successfully read.";
        internal const string strSCResponseProcessed = "Short code response successfully processed.";
        internal const string strSCTemplateNotFound = "Short code response template not found.";
        
        internal const string strGenerateVSDialogTitle = "Generate Cloud Virtual Server Via Example Back-Office Server";

        internal const string strHTTP = "http";
        internal const string strHTTPS = "https";
        internal const string strSchemeSeparator = "://";

        private string strPathSep = new string(new char[] {Path.DirectorySeparatorChar});
        private string fneVersion;
        private ILicensing licensing;
        private IShortCodeEnvironment shortCodeEnv;
        private Dictionary<HostIdEnum, List<string>> hostIds;
        private Dictionary<string, HostIdEnum> hostIdTypesByText = new Dictionary<string, HostIdEnum>();
        private SortedDictionary<string, List<string>> featureVersions = new SortedDictionary<string, List<string>>();
        private SortedDictionary<int, string> templateFiles = new SortedDictionary<int, string>();

        private string strConfigurationFile = string.Empty;
        private Dictionary<string, Configuration> configurations = new Dictionary<string,Configuration>();
        private Configuration currentConfiguration = new Configuration();

        Color baseTextColor = System.Drawing.Color.FromKnownColor(KnownColor.ControlText);
        Color baseToolTipTextColor = System.Drawing.Color.FromKnownColor(KnownColor.InfoText);
        Color HCToolTipTextColor = System.Drawing.Color.FromKnownColor(KnownColor.HighlightText);

        ErrorProvider errorProvider;
        ContextMenuStrip rightClickMenu;
        ToolStripMenuItem copyItem;
        List<CheckBox> ckbTSInstances = new List<CheckBox>((int)LicenseServerInstance.Max);
        List<Button> btnTSInstances = new List<Button>((int)LicenseServerInstance.Max);
        ColumnHeader crRespMaxCountHeader = new ColumnHeader();
        private const int iCRRespMCHeaderIndex = 4;
        private int currentTabPageIndex = 0;
        private int bufferCount;
        private List<string> loadedLicenseFiles = new List<string>();
        private List<string> loadedTemplateFiles = new List<string>();
        private List<string> loadedSCResponseFiles = new List<string>();
        private List<int> invalidatedItems = new List<int>();
        private int       tooltipItemIndex = -1;
        private string strLastConfigFilePath;
        private string strLastIdFilePath;
        private string strLastTSPath;
        private string strLastLicenseFilePath;
        private string strLastCapabilityResponseFilePath;
        private string strLastTemplateFilePath;
        private string strLastSCRequestPDFilePath;
        private string strLastSCRequestFilePath;
        private string strLastSCResponseFilePath;
        private string strLastSCResponsePDFilePath;

        private bool showingToolTip = false;

        private Thread sendingThread = null;
        private delegate void SendCompleteDelegate(DotNetDemoSendParameters parms);

        Func<bool?, bool> IsSet = x => (x.HasValue && x.Value == true) ? true : false; 

        #region Form constructor, save and close

        public DotNetDemoForm()
        {
            InitializeDemoForm(null);
        }

        public DotNetDemoForm(string[] args)
        {
            InitializeDemoForm(args);
        }
        
        private void InitializeDemoForm(string[] args)
        {
            InitializeComponent();
            DotNetDemoForm_SystemColorsChanged(this, new EventArgs());

            tabLicenses.TabPages.Remove(pageTrustedStorageLicenses);
            tabLicenses.TabPages.Remove(pageTrialLicenses);
            tabLicenses.TabPages.Remove(pageShortCodeLicenses);
            tabLicenses.TabPages.Remove(pageBufferLicenses);

            errorProvider = new ErrorProvider(this);
            errorProvider.BlinkRate = 500;
            tabLicenses.DrawMode = TabDrawMode.OwnerDrawFixed;

            rightClickMenu = new ContextMenuStrip(this.components);
            copyItem = new ToolStripMenuItem("Copy");
            copyItem.Click += new EventHandler(copyItem_Click);
            rightClickMenu.Items.Add(copyItem);
            statusStrip.ContextMenuStrip = rightClickMenu;
            cbxToolStripConfig.ComboBox.DrawMode = DrawMode.OwnerDrawFixed;
            cbxToolStripConfig.ComboBox.DrawItem += new DrawItemEventHandler(cbxToolStripConfig_DrawItem);

            cbxServerType.Items.AddRange(new string[] {strBackOffice, strFNO, strLocal, strCloud});
            cbxServerType.SelectedIndex = 0;
            cbxBorrowGranularity.Items.AddRange(new string[] {strDays, strHours, strMinutes, strSeconds});
            cbxBorrowGranularity.SelectedIndex = 0;
            cbxVDType.Items.AddRange(new string[] { strInteger, strString });
            cbxVDType.SelectedIndex = 0;
            cbxFSType.SelectedIndex = 0;
            btnInspectLicenseFile.Enabled = btnLoadLicenseFile.Enabled = false;
            btnInspectTemplateFile.Enabled = btnLoadTemplateFile.Enabled = false;
            cbxEncodingType.Items.AddRange(SCEncodings);
            cbxDecodingType.Items.AddRange(SCEncodings);
            cbxEncodingType.Text = cbxDecodingType.Text = strBase34;
            btnSetIdentity.Enabled = btnSetHostId.Enabled = false;
            pnlHostId.Enabled = pnlHostInfo.Enabled = false;

            string defaultIdentity = Application.StartupPath + Path.DirectorySeparatorChar + strDefaultIdentityFile;
            if (File.Exists(defaultIdentity))
            {
                currentConfiguration.IdentityData = defaultIdentity;
            }
            currentConfiguration.TrustedStoragePath = Environment.CurrentDirectory;

            cbxHostIdType.SelectedIndexChanged -= new EventHandler(cbxHostIdType_SelectedIndexChanged);
            txtHostId.TextChanged -= new EventHandler(txtHostId_TextChanged);
            cbxHostIdType.Items.Clear();
            cbxHostIdType.Items.Add(strCustomId);
            cbxHostIdType.SelectedIndex = 0;
            cbxHostIdType.Text = strCustomId;
            cbxHostId.Visible = false;
            txtHostId.Text = strDemoHostId;
            txtHostId.TextChanged += new EventHandler(txtHostId_TextChanged);
            cbxHostIdType.SelectedIndexChanged += new EventHandler(cbxHostIdType_SelectedIndexChanged);

            dtIMExpiration.Value = dtIMExpiration.Value.AddYears(1);
            if (dtIMExpiration.Value.Hour < 23)
            {
                dtIMExpiration.Value = dtIMExpiration.Value.AddHours(23 - dtIMExpiration.Value.Hour);
            }
            if (dtIMExpiration.Value.Minute < 59)
            {
                dtIMExpiration.Value = dtIMExpiration.Value.AddMinutes(59 - dtIMExpiration.Value.Minute);
            }
            if (dtIMExpiration.Value.Second < 59)
            {
                dtIMExpiration.Value = dtIMExpiration.Value.AddSeconds(59 - dtIMExpiration.Value.Second);
            }
            string strInitialConfigFile;
            string strInitialConfig = strDefaultConfiguration;
            if (args != null && args.Length > 0 && !string.IsNullOrEmpty(args[0]))
            {
                strInitialConfigFile = args[0].Trim();
                if (args.Length > 1 && !string.IsNullOrEmpty(args[1]))
                {
                    strInitialConfig = args[1].Trim();
                }
            }
            else
            {
                strInitialConfigFile = Environment.CurrentDirectory + Path.DirectorySeparatorChar + strDefaultConfigurationFile;
            }
            if (File.Exists(strInitialConfigFile))
            {
                DotNetDemoConfigurationItem initialConfigItem = new DotNetDemoConfigurationItem(strInitialConfig, SystemColors.Window);
                ConfigFileChanged(strInitialConfigFile, false, false);
                if (strConfigurationFile == strInitialConfigFile &&
                    cbxToolStripConfig.Items.Count > 1 &&
                    cbxToolStripConfig.Items.Contains(initialConfigItem))
                {
                    cbxToolStripConfig.SelectedIndex = cbxToolStripConfig.Items.IndexOf(initialConfigItem);
                    return;
                }
            }
            InitTSInstances();
            InitHostIdTypes();
            LoadConfiguration();

            // Make temporary adjustments her to hide shortcode controls until properly supported by back-office serer
            tabMain.TabPages.Remove(pageShortCodes);
            tabLicenses.TabPages.Remove(pageShortCodeLicenses);
            ckbShortCode.Visible = false;
            btnResetSC.Visible = false;
            int delta = (ckbShortCode.Location.Y - ckbTrials.Location.Y)/2;
            ckbTrials.Location = new Point(ckbTrials.Location.X, ckbTrials.Location.Y + delta);
            btnResetTrials.Location = new Point(btnResetTrials.Location.X, btnResetTrials.Location.Y + delta);
            lblTrustedStorage.Location = new Point(lblTrustedStorage.Location.X, lblTrustedStorage.Location.Y + 5);
            btnTrustedStorage.Location = new Point(btnTrustedStorage.Location.X, btnTrustedStorage.Location.Y + 5);
            btnResetTS.Location = new Point(btnResetTS.Location.X, btnResetTS.Location.Y + 5);
            btnClear.Location = new Point(btnClear.Location.X, btnClear.Location.Y + 5);
            crRespMaxCountHeader.DisplayIndex = iCRRespMCHeaderIndex;
            crRespMaxCountHeader.Text = "Max Count";
            crRespMaxCountHeader.Width = 70;
        }

        private void InitTSInstances()
        {
            cbxTSInstance.Items.Clear();
            Array instanceArray = Enum.GetValues(typeof(LicenseServerInstance));
            int vPos = 3;
            int spacing = 25;
            int prevValue = -1;
            foreach (LicenseServerInstance serverInstance in instanceArray)
            {
                if (serverInstance != LicenseServerInstance.Unknown && prevValue != (int)serverInstance)
                {
                    CheckBox checkBox = new CheckBox();
                    checkBox.Location = new Point(3, vPos + 4);
                    checkBox.Size = new Size(80, 17);
                    checkBox.Text = serverInstance.ToString();
                    checkBox.Tag = serverInstance;
                    checkBox.BackColor = Color.LightSteelBlue;
                    checkBox.CheckedChanged += new EventHandler(ckbTrustedStorageInstance_CheckedChanged);

                    Button button = new Button();
                    button.Location = new Point(120, vPos);
                    button.Size = new Size(50, 23);
                    button.Text = "Reset";
                    button.Tag = serverInstance;
                    button.BackColor = Color.LightSteelBlue;
                    button.Click += new EventHandler(btnResetTSInstance_Click);

                    pnlTSInstances.Controls.Add(checkBox);
                    ckbTSInstances.Add(checkBox);
                    pnlTSInstances.Controls.Add(button);
                    btnTSInstances.Add(button);
                    vPos += spacing;
                    cbxTSInstance.Items.Add(serverInstance);
                }
                prevValue = (int)serverInstance;
            }
            pnlTSInstances.Size = new Size(pnlTSInstances.Size.Width, vPos);
            cbxTSInstance.SelectedIndex = 0;
        }

        private void InitHostIdTypes()
        {
            int selectedIndex = 0;
            cbxAuxHostIdType.Items.Clear();
            foreach (HostIdEnum hostIdType in Enum.GetValues(typeof(HostIdEnum)))
            {
                if (hostIdType != HostIdEnum.FLX_HOSTID_TYPE_UNKNOWN &&
                    hostIdType != HostIdEnum.FLX_HOSTID_TYPE_UNSUPPORTED &&
                    hostIdType != HostIdEnum.FLX_HOSTID_TYPE_ANY)
                {
                    if (hostIdType == HostIdEnum.FLX_HOSTID_TYPE_STRING)
                    {
                        selectedIndex = cbxAuxHostIdType.Items.Count;
                    }
                    cbxAuxHostIdType.Items.Add(hostIdType);
                    hostIdTypesByText.Add(hostIdType.ToString(), hostIdType);
                }
            }
            cbxAuxHostIdType.SelectedIndex = selectedIndex;
        }

        private void DotNetDemoForm_Load(object sender, EventArgs e)
        {
            if (txtIdentityFile.CanFocus)
            {
                txtIdentityFile.Focus();
            }
        }

        private void DotNetDemo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
            else if (e.KeyCode == Keys.Enter && this.ActiveControl is CheckBox)
            {
                CheckBox cbxTemp = this.ActiveControl as CheckBox;
                cbxTemp.Checked = !cbxTemp.Checked;
            }
            else if (e.Control && e.KeyCode == Keys.S)     // Ctrl-S Save
            {
                if (cbxToolStripConfig.SelectedIndex != 0)
                {
                    btnToolStripSave_Click(btnToolStripSave, new EventArgs());
                }
                else
                {
                    btnToolStripSaveAs_Click(btnToolStripSaveAs, new EventArgs());
                }
                e.SuppressKeyPress = true;  
            }
        }

        private void DotNetDemoForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (CurrentConfigurationChanged()) 
            {
                if (!AskSave())
                {
                    e.Cancel = true;
                    return;
                }
            }
            if (licensing != null)
            {
                licensing.Dispose();
                licensing = null;
            }
        }

        #endregion

        #region Properties


        private string FNEVersion
        {
            get
            {
                if (string.IsNullOrEmpty(fneVersion))
                {
                    // This forces a load of FlxLicensingClient so we can reliably grab the version.
                    ILicensingOptions options = LicensingFactory.GetLicensingOptions();
                    AppDomain currentDomain = AppDomain.CurrentDomain;
                    Assembly[] currentAssemblies = currentDomain.GetAssemblies();
                    foreach (Assembly asm in currentAssemblies)
                    {
                        if (asm.GetName().Name == strDotNetAssembly)
                        {
                            fneVersion = asm.GetName().Version.ToString();
                            break;
                        }
                    }
                }
                return fneVersion;
            }
        }

        private ILicenseManager LicenseManager
        {
            get
            {
                return licensing == null ? null : licensing.LicenseManager;
            }
        }

        #endregion

        #region Main tabcontrol events

        private void tabMain_Selecting(object sender, TabControlCancelEventArgs e)
        {
            string hostId = null;
            if (licensing != null && string.IsNullOrEmpty(hostId = licensing.CustomHostId))
            {
                hostId = LicenseManager.HostId;
            }
            if (e.TabPageIndex != 0 && string.IsNullOrEmpty(hostId))
            {
                ResetError();
                Control control = (licensing == null) ? (Control)btnSetIdentity : (Control)btnSetHostId;
                errorProvider.SetIconAlignment(control, ErrorIconAlignment.MiddleLeft);
                errorProvider.SetError(control, strSetupWarning);
                e.Cancel = true;
            }
            else if (currentTabPageIndex == 0)
            {
                string hostType = GetConfigString(txtHostType.Text);
                string hostName = GetConfigString(txtHostName.Text);
                if (!CompareConfigString(hostType, LicenseManager.HostType))
                {
                    LicenseManager.HostType = hostType ?? string.Empty;
                    currentConfiguration.HostType = hostType;
                }
                if (!CompareConfigString(hostName, LicenseManager.HostName))
                {
                    LicenseManager.HostName = hostName ?? string.Empty;
                    currentConfiguration.HostName = hostName;
                }
            }
            currentTabPageIndex = e.TabPageIndex;
         }

        private void tabMain_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetError();
            ResetStatus();
            cbxLicenseFile.Text = string.Empty;
            cbxTemplateFile.Text = string.Empty;
            lblLoadComplete.Visible = false;
            lblTemplateLoadComplete.Visible = false;
            lblDoSelect.Visible = false;
        }

        private void tabMain_ClientSizeChanged(object sender, EventArgs e)
        {
            // Needed to handle a TabControl repaint error fixed in .NET 4.0.
            // http://stackoverflow.com/questions/12338241/winforms-tabpage-not-repainting-on-form-resize
            tabMain.Refresh();
        }

        #endregion

        #region toolStrip

        private void btnToolStripOpen_Click(object sender, EventArgs e)
        {
            ResetError();
            using (OpenFileDialog dialog = new OpenFileDialog())
            {
                dialog.AddExtension = false;
                dialog.Filter = strXMLFilter;
                dialog.Title = strOpenConfigTitle;
                dialog.CheckFileExists = false;
                dialog.CheckPathExists = true;
                dialog.InitialDirectory = Application.StartupPath;
                dialog.RestoreDirectory = true;
                if (!string.IsNullOrEmpty(strLastConfigFilePath))
                {
                    dialog.InitialDirectory = strLastConfigFilePath;
                }
                DialogResult result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    strLastConfigFilePath = Path.GetDirectoryName(dialog.FileName);
                    ConfigFileChanged(dialog.FileName.Trim(), true, true);
                }
            }
        }

        private void btnToolStripSave_Click(object sender, EventArgs e)
        {
            ResetError();
            currentConfiguration.HostType = GetConfigString(txtHostType.Text);
            currentConfiguration.HostName = GetConfigString(txtHostName.Text);
            if (CurrentConfigurationChanged())
            {
                Configuration saveConfig = configurations[currentConfiguration.Name];
                configurations[currentConfiguration.Name] = (Configuration)currentConfiguration.Clone();
                if (!SaveConfigurations(strConfigSaved, strConfigSaveFailed))
                {
                    configurations[currentConfiguration.Name] = saveConfig;
                }
            }
        }

        private void btnToolStripDelete_Click(object sender, EventArgs e)
        {
            ResetError();
            if (currentConfiguration != null &&
                !string.IsNullOrEmpty(currentConfiguration.Name) &&
                configurations.ContainsKey(currentConfiguration.Name))
            {
                bool doDelete = false;
                if (MessageBox.Show(this, string.Format(strDeleteMsg, currentConfiguration.Name),
                    strDeleteCaption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    doDelete = true;
                }
                if (doDelete)
                {
                    bool deleteFailed = true;
                    configurations.Remove(currentConfiguration.Name);
                    if (configurations.Count == 0)
                    {
                        try
                        {
                            File.Delete(strConfigurationFile);
                            deleteFailed = false;
                            strConfigurationFile = string.Empty;
                        }
                        catch
                        {
                            SetStatus(string.Format(strConfigDeleteFailed, currentConfiguration.Name), true);
                        }
                    }
                    else if (SaveConfigurations(strConfigDeleted, strConfigDeleteFailed))
                    {
                        deleteFailed = false;
                    }
                    if (deleteFailed)
                    {
                        configurations.Add(currentConfiguration.Name, (Configuration)currentConfiguration.Clone());
                    }
                    else
                    {
                        cbxToolStripConfig.BeginUpdate();
                        cbxToolStripConfig.Items.Remove(new DotNetDemoConfigurationItem(currentConfiguration.Name, SystemColors.Window));
                        cbxToolStripConfig.SelectedIndex = 0;
                        cbxToolStripConfig.Enabled = cbxToolStripConfig.Items.Count > 0;
                        cbxToolStripConfig.EndUpdate();
                        currentConfiguration.Name = null;
                    }
                }
            }
            btnToolStripSave.Enabled = btnToolStripDelete.Enabled = !string.IsNullOrEmpty(currentConfiguration.Name);
        }

        private void btnToolStripSaveAs_Click(object sender, EventArgs e)
        {
            ResetError();
            currentConfiguration.HostType = GetConfigString(txtHostType.Text);
            currentConfiguration.HostName = GetConfigString(txtHostName.Text);
            Configuration saveConfig = (Configuration)currentConfiguration.Clone();
            DialogResult result = DialogResult.Cancel;
            string strSaveAsConfigFile = string.Empty;
            string strSaveAsConfigName = string.Empty;
            using (SaveAs dialog = new SaveAs())
            {
                dialog.Config = currentConfiguration;
                dialog.ConfigFile = strConfigurationFile;
                dialog.ConfigColor = Color.FromArgb(currentConfiguration.ConfigColor);
                dialog.StartPosition = FormStartPosition.CenterParent;
                result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    strSaveAsConfigFile = GetConfigString(dialog.ConfigFile);
                    strSaveAsConfigName = GetConfigString(dialog.ConfigName);
                    currentConfiguration.FormTitle = GetConfigString(dialog.ConfigTitle);
                    currentConfiguration.Color = dialog.ConfigColor;
                }
                else
                {
                    return;
                }
            }

            if (strSaveAsConfigFile != strConfigurationFile)
            {   // Switching both files and configuration name
                ConfigFileChanged(strSaveAsConfigFile, true, false);
                if (strSaveAsConfigFile != strConfigurationFile)
                {   // File switch failed
                    currentConfiguration = saveConfig;
                    return;
                }
            }
            currentConfiguration.Name = strSaveAsConfigName;
            if (configurations.ContainsKey(currentConfiguration.Name))
            {
                SetStatus(string.Format(strConfigSaveAsFailed, currentConfiguration.Name) + strBlank + strConfigExists, true);
                currentConfiguration.Name = null;
            }
            else
            {
                List<string> saveLicenseFiles = currentConfiguration.LicenseFiles;
                List<string> saveTemplateFiles = currentConfiguration.TemplateFiles;
                List<string> saveSCResponseFiles = currentConfiguration.ScResponseFiles;
                currentConfiguration.LicenseFiles = new List<string>(loadedLicenseFiles);
                currentConfiguration.TemplateFiles = new List<string>(loadedTemplateFiles);
                currentConfiguration.ScResponseFiles = new List<string>(loadedSCResponseFiles);
                configurations.Add(currentConfiguration.Name, (Configuration)currentConfiguration.Clone());
                if (SaveConfigurations(strConfigSavedAs, strConfigSaveAsFailed))
                {
                    cbxToolStripConfig.SelectedIndexChanged -= new EventHandler(cbxToolStripConfig_SelectedIndexChanged);
                    cbxToolStripConfig.BeginUpdate();
                    cbxToolStripConfig.BackColor = currentConfiguration.Color;
                    cbxToolStripConfig.Items.Clear();
                    List<DotNetDemoConfigurationItem> configItems = new List<DotNetDemoConfigurationItem>();
                    configItems.Add(new DotNetDemoConfigurationItem(String.Empty, SystemColors.Window));
                    foreach (KeyValuePair<string, Configuration> configKvp in configurations)
                    {
                        configItems.Add(new DotNetDemoConfigurationItem(configKvp.Key, configKvp.Value.Color));
                    }
                    configItems.Sort();
                    cbxToolStripConfig.Items.AddRange(configItems.ToArray());
                    cbxToolStripConfig.SelectedIndex = cbxToolStripConfig.Items.IndexOf(new DotNetDemoConfigurationItem(currentConfiguration.Name, currentConfiguration.Color));
                    cbxToolStripConfig.SelectedIndexChanged += new EventHandler(cbxToolStripConfig_SelectedIndexChanged);
                    cbxToolStripConfig.EndUpdate();
                    cbxToolStripConfig.Enabled = cbxToolStripConfig.Items.Count > 0;
                    btnToolStripSave.Enabled = btnToolStripDelete.Enabled = true;
                    SetFormTitle();
                    cbxLicenseFile.BeginUpdate();
                    cbxLicenseFile.Items.Clear();
                    cbxLicenseFile.Items.AddRange(currentConfiguration.LicenseFiles.ToArray());
                    cbxLicenseFile.EndUpdate();
                    cbxTemplateFile.BeginUpdate();
                    cbxTemplateFile.Items.Clear();
                    cbxTemplateFile.Items.AddRange(currentConfiguration.TemplateFiles.ToArray());
                    cbxTemplateFile.EndUpdate();
                    cbxSCResponseFile.BeginUpdate();
                    cbxSCResponseFile.Items.Clear();
                    cbxSCResponseFile.Items.AddRange(currentConfiguration.ScResponseFiles.ToArray());
                    cbxSCResponseFile.EndUpdate();
                }
                else
                {   
                    configurations.Remove(currentConfiguration.Name);
                    currentConfiguration.LicenseFiles = saveLicenseFiles;
                    currentConfiguration.TemplateFiles = saveTemplateFiles;
                    currentConfiguration.ScResponseFiles = saveSCResponseFiles;
                    currentConfiguration.Name = null;
                }
            }
        }

        private void cbxToolStripConfig_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetError();
            if (CurrentConfigurationChanged() && !AskSave())
            {
                cbxToolStripConfig.SelectedIndexChanged -= new EventHandler(cbxToolStripConfig_SelectedIndexChanged);
                cbxToolStripConfig.SelectedIndex = cbxToolStripConfig.Items.IndexOf(new DotNetDemoConfigurationItem(currentConfiguration.Name, currentConfiguration.Color));
                cbxToolStripConfig.SelectedIndexChanged -= new EventHandler(cbxToolStripConfig_SelectedIndexChanged);
                return;
            }
            if (cbxToolStripConfig.SelectedIndex > 0)
            {
                if (configurations.ContainsKey(cbxToolStripConfig.Text) &&
                    configurations[cbxToolStripConfig.Text].Name != currentConfiguration.Name)
                {
                    currentConfiguration = (Configuration)configurations[cbxToolStripConfig.Text].Clone();
                    tabMain.SelectedIndex = 0;
                    btnRestart_Click(this, new EventArgs());
                    LoadConfiguration();
                }
                else
                {
                    cbxToolStripConfig.SelectedIndex = 0;
                    currentConfiguration.Name = null;
                }
            }
            btnToolStripSave.Enabled = btnToolStripDelete.Enabled = cbxToolStripConfig.SelectedIndex > 0;
            if (cbxToolStripConfig.SelectedIndex == 0)
            {
                cbxToolStripConfig.BackColor = SystemColors.Window;
            }
        }

        private void cbxToolStripConfig_DrawItem(object sender, DrawItemEventArgs e)
        {
            Brush backBrush = new SolidBrush(SystemColors.Window);
            string text = String.Empty;
            if (e.Index >= 0)
            {
                DotNetDemoConfigurationItem configItem = cbxToolStripConfig.Items[e.Index] as DotNetDemoConfigurationItem;
                if (configItem != null)
                {
                    backBrush = new SolidBrush(configItem.configColor);
                    text = configItem.configName;
                }
            }
            e.Graphics.FillRectangle(backBrush, e.Bounds);
            Brush textBrush = new SolidBrush(SystemInformation.HighContrast ? Color.White : Color.Black);
            e.Graphics.DrawString(text, e.Font, textBrush, e.Bounds.X, e.Bounds.Y);
        }

        private void ConfigFileChanged(string strNewConfigFile, bool showErrors, bool askSave)
        {
            if (askSave && CurrentConfigurationChanged() && !AskSave())
            {
                return;
            }
            strConfigurationFile = strNewConfigFile;
            cbxToolStripConfig.BeginUpdate();
            cbxToolStripConfig.Items.Clear();
            if (!string.IsNullOrEmpty(strConfigurationFile))
            {
                if (strConfigurationFile.EndsWith(strXMLSuffix, StringComparison.OrdinalIgnoreCase))
                {
                    configurations.Clear();
                    currentConfiguration.Name = null;
                    cbxToolStripConfig.Items.Add(new DotNetDemoConfigurationItem(String.Empty, SystemColors.Window));
                    if (File.Exists(strConfigurationFile) && new FileInfo(strConfigurationFile).Length > 0)
                    {
 
                        try
                        {
                            List<DotNetDemoConfigurationItem> configItems = new List<DotNetDemoConfigurationItem>();
                            foreach (Configuration configuration in Configuration.DeserializeFromXML(strConfigurationFile))
                            {
                                configurations.Add(configuration.Name, configuration);
                                configItems.Add(new DotNetDemoConfigurationItem(configuration.Name, configuration.Color));
                            }
                            configItems.Sort();
                            cbxToolStripConfig.Items.AddRange(configItems.ToArray());
                        }
                        catch (Exception exc)
                        {
                            if (showErrors)
                            {
                                FlxException flxException = exc as FlxException;
                                SetStatus(flxException == null ? exc.Message : flxException.ToString(), true);
                            }
                            configurations.Clear();
                            cbxToolStripConfig.Items.Clear();
                        }
                    }
                    if (cbxToolStripConfig.Items.Count > 0)
                    {
                        cbxToolStripConfig.SelectedIndex = 0;
                    }
                }
            }
            cbxToolStripConfig.Enabled = cbxToolStripConfig.Items.Count > 0;
            if (cbxToolStripConfig.Enabled)
            {
                cbxToolStripConfig.ToolTipText = string.Format(strConfigToolTip1, strConfigurationFile);
            }
            else
            {
                strConfigurationFile = string.Empty;
                cbxToolStripConfig.ToolTipText = strConfigToolTip2;
            }
            cbxToolStripConfig.EndUpdate();
        }

        private bool CurrentConfigurationChanged()
        {
            if (currentConfiguration != null &&
                !string.IsNullOrEmpty(currentConfiguration.Name) &&
                configurations.ContainsKey(currentConfiguration.Name) &&
                currentConfiguration != configurations[currentConfiguration.Name])
            {
                return true;
            }
            return false;
        }

        private bool AskSave()
        {
            DialogResult result = MessageBox.Show(this, strSaveMsg, strSaveCaption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            if (result == DialogResult.Cancel)
            {
                return false;
            }
            else if (result == DialogResult.Yes)
            {
                btnToolStripSave_Click(btnToolStripSave, new EventArgs());
                if (CurrentConfigurationChanged())
                {
                    return false;
                }
            }
            return true;
        }

        private void LoadConfiguration()
        {
            SetFormTitle();
            cbxToolStripConfig.BackColor = currentConfiguration.Color;
            txtIdentityFile.Text = currentConfiguration.IdentityData;
            txtStoragePath.Text = currentConfiguration.TrustedStoragePath;
            ckbVMDetectionEnabled.Checked = currentConfiguration.VMDetectionEnabled;
            if (!string.IsNullOrEmpty(currentConfiguration.HostIdType))
            {
                cbxHostIdType.SelectedIndexChanged -= new EventHandler(cbxHostIdType_SelectedIndexChanged);
                txtHostId.TextChanged -= new EventHandler(txtHostId_TextChanged);
                cbxHostIdType.Items.Clear();
                cbxHostIdType.Items.Add(currentConfiguration.HostIdType);
                cbxHostIdType.SelectedIndex = 0;
                cbxHostId.Visible = false;
                txtHostId.Text = currentConfiguration.HostId;
                txtHostId.TextChanged += new EventHandler(txtHostId_TextChanged);
                cbxHostIdType.SelectedIndexChanged += new EventHandler(cbxHostIdType_SelectedIndexChanged);
            }
            txtHostType.Text = currentConfiguration.HostType;
            txtHostName.Text = currentConfiguration.HostName;
            cbxLicenseFile.BeginUpdate();
            cbxLicenseFile.Items.Clear();
            cbxLicenseFile.Items.AddRange(currentConfiguration.LicenseFiles.ToArray());
            cbxLicenseFile.EndUpdate();
            cbxOperation.SelectedIndexChanged -= new EventHandler(cbxOperation_SelectedIndexChanged);
            if (cbxServerType.Text != currentConfiguration.ServerType && cbxServerType.Items.Contains(currentConfiguration.ServerType))
            {
                cbxServerType.SelectedIndex = cbxServerType.Items.IndexOf(currentConfiguration.ServerType);
            }
            cbxOperation.SelectedIndexChanged += new EventHandler(cbxOperation_SelectedIndexChanged);
            if (cbxTSInstance.Text != currentConfiguration.ServerInstance)
            {
                LicenseServerInstance? serverInstance = GetEnumFromDescription(typeof(LicenseServerInstance), currentConfiguration.ServerInstance) as LicenseServerInstance?;
                if (serverInstance.HasValue && cbxTSInstance.Items.Contains(serverInstance.Value))
                {
                    cbxTSInstance.SelectedIndex = cbxTSInstance.Items.IndexOf(serverInstance.Value);
                }
            }
            if (cbxOperation.Text != currentConfiguration.Operation && cbxOperation.Items.Contains(currentConfiguration.Operation))
            {
                cbxOperation.SelectedIndex = cbxOperation.Items.IndexOf(currentConfiguration.Operation);
            }
            ckbOTA.Checked = ckbOTA.Enabled && currentConfiguration.OneTimeActivation;
            ckbIncremental.Checked = ckbIncremental.Enabled && currentConfiguration.Incremental;
            ckbRequestAll.Checked = ckbRequestAll.Enabled && currentConfiguration.RequestAll;
            txtCorrelationID.Text = currentConfiguration.CorrelationID;
            txtAcquisitionID.Text = currentConfiguration.AcquisitionID;
            txtEnterpriseID.Text = currentConfiguration.EnterpriseID;
            txtRequestorID.Text = currentConfiguration.RequestorID;
            if (cbxBorrowGranularity.Text != currentConfiguration.BorrowGranularity & cbxBorrowGranularity.Items.Contains(currentConfiguration.BorrowGranularity))
            {
                cbxBorrowGranularity.SelectedIndex = cbxBorrowGranularity.Items.IndexOf(currentConfiguration.BorrowGranularity);
            }
            txtBorrowInterval.Text = currentConfiguration.BorrowInterval;
            ckbProcessResponse.Checked = currentConfiguration.ProcessResponse;
            ckbForceResponse.Checked = currentConfiguration.ForceResponse;
            txtRespFile.Text = currentConfiguration.ResponseFile;
            lvBORights.BeginUpdate();
            lvBORights.Items.Clear();
            foreach (RightsInfo rights in currentConfiguration.Rights)
            {
                lvBORights.Items.Add(new ListViewItem(new string[] { rights.Id, rights.Count, rights.Partial == true ? "Y" : "N" }));
            }
            lvBORights.EndUpdate();
            lvLLSFeatures.BeginUpdate();
            lvLLSFeatures.Items.Clear();
            foreach (FeatureInfo feature in currentConfiguration.Features)
            {
                lvLLSFeatures.Items.Add(new ListViewItem(new string[] { feature.Name, feature.Version, feature.Count, feature.Partial == true ? "Y" : "N" }));
            }
            lvLLSFeatures.EndUpdate();
            lvFeatureSelectors.BeginUpdate();
            lvFeatureSelectors.Items.Clear();
            foreach (FeatureSelector fs in currentConfiguration.FeatureSelectors)
            {
                lvFeatureSelectors.Items.Add(new ListViewItem(new string[] { fs.Type, fs.Key, fs.Value }));
            }
            lvFeatureSelectors.EndUpdate();
            lvAuxHostIds.BeginUpdate();
            lvAuxHostIds.Items.Clear();
            foreach (AuxiliaryHostId auxHostId in currentConfiguration.AuxiliaryHostIds)
            {
                if (hostIdTypesByText.ContainsKey(auxHostId.Type))
                {
                    lvAuxHostIds.Items.Add(new ListViewItem(new string[] { auxHostId.Type.ToString(), auxHostId.Value }));
                }
            }
            lvAuxHostIds.EndUpdate();
            ckbIMAddExisting.Checked = currentConfiguration.UseIMExistingFeatures;
            lvIMFeatures.BeginUpdate();
            lvIMFeatures.Items.Clear();
            ListViewItem newIMItem;
            foreach (UsageInfo usage in currentConfiguration.Usage)
            {
                newIMItem = new ListViewItem(new string[] { usage.Name, usage.Version, usage.Count, usage.Expiration.ToString() });
                newIMItem.Tag = usage.Expiration;
                lvIMFeatures.Items.Add(newIMItem);    
            }
            lvIMFeatures.EndUpdate();
            cbxTemplateFile.Items.AddRange(currentConfiguration.TemplateFiles.ToArray());
            if (!string.IsNullOrEmpty(currentConfiguration.ScRequestEncoding))
            {
                cbxEncodingType.Text = currentConfiguration.ScRequestEncoding;
            }
            txtEncodingSegSize.Text = currentConfiguration.ScRequestSegmentSize.ToString();
            txtSCRequestPD.Text = string.Empty;
            rbSCRequestPDHex.Checked = currentConfiguration.ScRequestPDHex;
            txtSCRequestPD.Text = currentConfiguration.ScRequestPD;
            if (!string.IsNullOrEmpty(currentConfiguration.ScResponseDecoding))
            {
                cbxDecodingType.Text = currentConfiguration.ScResponseDecoding;
            }
            txtDecodingSegSize.Text = currentConfiguration.ScResponseSegmentSize.ToString();
            cbxSCResponseFile.Items.AddRange(currentConfiguration.ScResponseFiles.ToArray());
         }

        private void btnToolStripAbout_Click(object sender, EventArgs e)
        {
            using (About dialog = new About())
            {
                dialog.Icon = this.Icon;
                dialog.Image = this.Icon.ToBitmap();
                dialog.Caption = strAboutCaption;
                string info = Assembly.GetExecutingAssembly().GetName().Name + Environment.NewLine;
                info += "Version " + Assembly.GetExecutingAssembly().GetName().Version + Environment.NewLine;
                info += "\u00a9 2016 Flexera Software LLC" + Environment.NewLine;
                info += "All rights reserved." + Environment.NewLine + Environment.NewLine;
                info += strDotNetAssembly + Environment.NewLine;
                info += "Version " + FNEVersion + Environment.NewLine;
                info += "\u00a9 2016 Flexera Software LLC" + Environment.NewLine;
                info += "All rights reserved.";
                dialog.Info = info;
                dialog.StartPosition = FormStartPosition.CenterParent;
                dialog.ShowDialog();
            }
        }

        #endregion

        #region pageSetup events

        #region pnlIdentity

        private void txtIdentityFile_TextChanged(object sender, EventArgs e)
        {
            ResetError();
            btnSetIdentity.Enabled = (txtIdentityFile.Text != null && !string.IsNullOrEmpty(txtIdentityFile.Text.Trim()));
            currentConfiguration.IdentityData = GetConfigString(txtIdentityFile.Text);
        }

        private void btnBrowseIdentity_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dialog = new OpenFileDialog())
            {
                dialog.AddExtension = false;
                dialog.Filter = strBinaryFilter;
                dialog.Title = strOpenIdentityTitle;
                if (!string.IsNullOrEmpty(strLastIdFilePath))
                {
                    dialog.InitialDirectory = strLastIdFilePath;
                }
                DialogResult result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    txtIdentityFile.Text = dialog.FileName;
                    strLastIdFilePath = Path.GetDirectoryName(dialog.FileName);
                }
            }
        }

        private void txtStoragePath_TextChanged(object sender, EventArgs e)
        {
            ResetError();
            currentConfiguration.TrustedStoragePath = GetConfigString(txtStoragePath.Text);
        }

        private void btnBrowsePath_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog dialog = new FolderBrowserDialog())
            {
                dialog.Description = strTSBrowserDescription;
                if (!string.IsNullOrEmpty(strLastTSPath))
                {
                    dialog.SelectedPath = strLastTSPath;
                }
                DialogResult result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    txtStoragePath.Text = strLastTSPath = dialog.SelectedPath;
                }
            }
        }

        private void btnSetIdentity_Click(object sender, EventArgs e)
        {
            ILicensingOptions options = LicensingFactory.GetLicensingOptions();
            try
            {
                options.Identity = File.ReadAllBytes(txtIdentityFile.Text.Trim());
            }
            catch (Exception exc)
            {
                errorProvider.SetIconAlignment(txtIdentityFile, ErrorIconAlignment.MiddleLeft);
                FlxException flxException = exc as FlxException;
                errorProvider.SetError(txtIdentityFile, flxException == null ? exc.Message : flxException.ToString());
                return;
            }
            if (txtStoragePath.Text != null && !string.IsNullOrEmpty(txtStoragePath.Text.Trim()))
            {
                string path = txtStoragePath.Text;
                if (!path.EndsWith(strPathSep))
                {
                    path += strPathSep; 
                }
                options.StoragePath = path;
            }
            try
            {
                licensing = LicensingFactory.GetLicensing(options);
                licensing.LicenseManager.VMDetectionEnabled = ckbVMDetectionEnabled.Checked;
            }
            catch (Exception exc)
            {
                errorProvider.SetIconAlignment(btnSetIdentity, ErrorIconAlignment.MiddleLeft);
                FlxException flxException = exc as FlxException;
                errorProvider.SetError(btnSetIdentity, flxException == null ? exc.Message : flxException.ToString());
                return;
            }
            pnlIdentity.Enabled = false;
            if (txtStoragePath.Text == null || string.IsNullOrEmpty(txtStoragePath.Text.Trim()))
            {
                lblSetupNotice.Text = strMBTS;
                lblSetupNotice.Visible = true;
            }
            currentConfiguration.IdentityData = GetConfigString(txtIdentityFile.Text);
            currentConfiguration.TrustedStoragePath = GetConfigString(txtStoragePath.Text);
            currentConfiguration.VMDetectionEnabled = ckbVMDetectionEnabled.Checked;
            hostIds = LicenseManager.HostIds;
            cbxHostIdType.BeginUpdate();
            cbxHostIdType.Items.Clear();
            cbxHostIdType.Items.Add(strCustomId);
            foreach (HostIdEnum hostIdType in hostIds.Keys)
            {
                cbxHostIdType.Items.Add(hostIdType);
            }

            int index;
            if (!string.IsNullOrEmpty(currentConfiguration.HostIdType) && FindHostIdTypeIndex(currentConfiguration.HostIdType, out index))
            {
                cbxHostIdType.SelectedIndex = index;
                if (cbxHostIdType.SelectedIndex <= 0)
                {
                    txtHostId.Text = currentConfiguration.HostId;
                }
                else if (!string.IsNullOrEmpty(currentConfiguration.HostId) && cbxHostId.Items.Contains(currentConfiguration.HostId))
                {
                    cbxHostId.SelectedIndex = cbxHostId.Items.IndexOf(currentConfiguration.HostId);
                }
            }
            else
            {
                cbxHostIdType.SelectedIndex = 0;
            }
            cbxHostIdType.EndUpdate();
            pnlHostId.Enabled = true;
            if (cbxHostIdType.CanFocus)
            {
                cbxHostIdType.Focus();
            }
        }

        bool FindHostIdTypeIndex(string description, out int index)
        {
            index = -1;
            for (int i = 0; i < cbxHostIdType.Items.Count; i++)
            {
                object item = cbxHostIdType.Items[i];
                if ((item is String && (string)item == description) ||
                    (item is Enum && GetEnumDescription((Enum)item) == description))
                {
                    index = i;
                    return true;
                }
            }
            return false;
        }

        #endregion

        #region pnlHostId

        private void txtHostId_TextChanged(object sender, EventArgs e)
        {
            ResetError();
            btnSetHostId.Enabled = (txtHostId.Text != null && !string.IsNullOrEmpty(txtHostId.Text.Trim()));
            currentConfiguration.HostId = GetConfigString(txtHostId.Text);
        }

        private void cbxHostIdType_Format(object sender, ListControlConvertEventArgs e)
        {
            if (e.Value is Enum)
            {
                e.Value = GetEnumDescription((Enum)e.Value);
            }
        }

        private void cbxHostIdType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetError();
            cbxHostId.Visible = (cbxHostIdType.SelectedIndex != 0);
            txtHostId.Visible = !cbxHostId.Visible;
            if (cbxHostId.Visible && cbxHostIdType.SelectedItem != null) 
            {
                cbxHostId.BeginUpdate();
                cbxHostId.Items.Clear();
                if (hostIds != null)
                {
                    foreach (KeyValuePair<HostIdEnum, List<string>> kvp in hostIds)
                    {
                        if ((HostIdEnum)(cbxHostIdType.SelectedItem) == kvp.Key)
                        {
                            foreach (string value in kvp.Value)
                            {
                                cbxHostId.Items.Add(value);
                            }
                            cbxHostId.SelectedIndex = 0;
                            break;
                        }
                    }
                }
                cbxHostId.EndUpdate();
            }
            btnSetHostId.Enabled = (cbxHostId.Visible || GetConfigString(txtHostId.Text) != null);
            currentConfiguration.HostIdType = cbxHostIdType.Text;
            currentConfiguration.HostId = cbxHostId.Visible ? cbxHostId.Text : GetConfigString(txtHostId.Text);
        }

        private void cbxHostId_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetError();
            currentConfiguration.HostId = GetConfigString(cbxHostId.Text);
        }

        private void btnSetHostId_Click(object sender, EventArgs e)
        {
            ResetError();
            try
            {
                if (cbxHostId.Visible)
                {
                    LicenseManager.SetHostId((HostIdEnum)(cbxHostIdType.SelectedItem), (string)(cbxHostId.SelectedItem));
                }
                else
                {
                    licensing.CustomHostId = txtHostId.Text;
                }
                pnlHostId.Enabled = false;
                btnContinue.Enabled = true;
                if (txtHostType.CanFocus)
                {
                    txtHostType.Focus();
                }
                currentConfiguration.HostIdType = cbxHostIdType.Text;
                currentConfiguration.HostId = cbxHostIdType.Text == strCustomId ? GetConfigString(txtHostId.Text) : cbxHostId.Text;
                pnlHostInfo.Enabled = true;
            }
            catch (Exception exc)
            {
                errorProvider.SetIconAlignment(btnSetHostId, ErrorIconAlignment.MiddleLeft);
                FlxException flxException = exc as FlxException;
                errorProvider.SetError(btnSetHostId, flxException == null ? exc.Message : flxException.ToString());
                return;
            }
        }

        private void btnContinue_Click(object sender, EventArgs e)
        {
            tabMain.SelectedIndex = 1;
        }

        #endregion

        private void btnRestart_Click(object sender, EventArgs e)
        {
            if (licensing != null)
            {
                btnReturnAll_Click(null, new EventArgs());
                lvCRResponse.Items.Clear();
                btnCRResponseDetails.Enabled = false;
                btnCRResponseDetails.Tag = null;
                btnClear_Click(null, new EventArgs());
                licensing.Dispose();
                licensing = null;
                hostIds = null;
            }
            else
            {
                ResetError();
                ResetStatus();
            }
            pnlIdentity.Enabled = true;
            pnlHostId.Enabled = pnlHostInfo.Enabled = btnContinue.Enabled = false;
            lblSetupNotice.Text = string.Empty;
            lblSetupNotice.Visible = false;
        }

        #endregion
 
        #region License source collection

        private bool TSLicenseSourcesExist()
        {
            foreach (CheckBox checkBox in ckbTSInstances)
            {
                if (checkBox.Checked)
                {
                    return true;
                }
            }
            return false;
        }

        private void SetLicensesTabs()
        {
            tabLicenses.TabPages.Clear();
            tabLicenses.TabPages.Add(pageAllLicenses);
            if (TSLicenseSourcesExist())
            {
                tabLicenses.TabPages.Add(pageTrustedStorageLicenses);
            }
            if (ckbTrials.Checked)
            {
                tabLicenses.TabPages.Add(pageTrialLicenses);
            }
            if (ckbShortCode.Checked)
            {
                tabLicenses.TabPages.Add(pageShortCodeLicenses);
            }
            if (lvBufferFeatures.Items.Count > 0)
            {
                tabLicenses.TabPages.Add(pageBufferLicenses);
            }
        }

        private void btnResetTS_Click(object sender, EventArgs e)
        {
            ResetError();
            try
            {
                licensing.Administration.Delete(DeleteOption.TrustedStorage);
                if (TSLicenseSourcesExist())
                {
                    RefreshTrustedStorageFeatures();
                    RefreshAllFeatures();
                    RefreshAcquiredLicenses();
                }
                SetStatus(string.Format(strClearOK, "Trusted storage"), false);
            }
            catch (Exception exc)
            {
                errorProvider.SetIconAlignment(btnResetTS, ErrorIconAlignment.MiddleLeft);
                FlxException flxException = exc as FlxException;
                errorProvider.SetError(btnResetTS, flxException == null ? exc.Message : flxException.ToString());
            }
        }

        private void btnResetTSInstance_Click(object sender, EventArgs e)
        {
            ResetError();
            Button button = sender as Button;
            if (button != null)
            {
                try
                {
                    LicenseServerInstance serverInstance = (LicenseServerInstance)button.Tag;
                    licensing.Administration.DeleteTrustedStorageServerInstance(serverInstance);
                    if (TSLicenseSourcesExist())
                    {
                        RefreshTrustedStorageFeatures();
                        RefreshAllFeatures();
                        RefreshAcquiredLicenses();
                    }
                    SetStatus(string.Format(strClearOK, serverInstance.ToString() + " trusted storage instance"), false);
                }
                catch (Exception exc)
                {
                    errorProvider.SetIconAlignment(button, ErrorIconAlignment.MiddleLeft);
                    FlxException flxException = exc as FlxException;
                    errorProvider.SetError(button, flxException == null ? exc.Message : flxException.ToString());
                }
            }
        }

        private void btnResetTrials_Click(object sender, EventArgs e)
        {
            ResetError();
            try
            {
                licensing.Administration.Delete(DeleteOption.Trials);
                if (ckbTrials.Checked)
                {
                    RefreshTrialFeatures();
                    RefreshAllFeatures();
                    RefreshAcquiredLicenses();
                }
                SetStatus(string.Format(strClearOK, "Trials"), false);
            }
            catch (Exception exc)
            {
                errorProvider.SetIconAlignment(btnResetTrials, ErrorIconAlignment.MiddleLeft);
                FlxException flxException = exc as FlxException;
                errorProvider.SetError(btnResetTrials, flxException == null ? exc.Message : flxException.ToString());
            }
        }

        private void btnResetSC_Click(object sender, EventArgs e)
        {
            ResetError();
            try
            {
                licensing.Administration.Delete(DeleteOption.ShortCode);
                if (ckbShortCode.Checked)
                {
                    RefreshShortCodeFeatures();
                    RefreshAllFeatures();
                    RefreshAcquiredLicenses();
                }
                SetStatus(string.Format(strClearOK, "Short codes"), false);
            }
            catch (Exception exc)
            {
                errorProvider.SetIconAlignment(btnResetSC, ErrorIconAlignment.MiddleLeft);
                FlxException flxException = exc as FlxException;
                errorProvider.SetError(btnResetSC, flxException == null ? exc.Message : flxException.ToString());
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            LicenseManager.Reset();
            bufferCount = 0;
            lvTrustedStorageFeatures.Items.Clear();
            lvTrialFeatures.Items.Clear();
            lvBufferFeatures.Items.Clear();
            foreach (CheckBox checkBox in ckbTSInstances)
            {
                if (checkBox.Checked)
                {
                    checkBox.Checked = false;
                }
            }
            ckbShortCode.Checked = false;
            ckbTrials.Checked = false;
            SetLicensesTabs();
            RefreshAllFeatures();
            RefreshAcquiredLicenses();
            loadedLicenseFiles.Clear();
            loadedTemplateFiles.Clear();
            loadedSCResponseFiles.Clear();
            ResetError();
            ResetStatus();
            if (btnClear.CanFocus)
            {
                btnClear.Focus();
            }
        }

        private void RefreshFeatures()
        {
            RefreshTrustedStorageFeatures();
            RefreshTrialFeatures();
            RefreshShortCodeFeatures();
            RefreshBufferFeatures();
            RefreshAllFeatures();
        }

        private void RefreshTrustedStorageFeatures()
        {
            if (TSLicenseSourcesExist())
            {
                bool clearFeatures = true;
                foreach (CheckBox checkBox in ckbTSInstances)
                {
                    if (checkBox.Checked)
                    {
                        IFeatureCollection tsFeatures = LicenseManager.GetTrustedStorageFeatureCollection((LicenseServerInstance)checkBox.Tag);
                        InsertFeaturesToListView(LicenseSourceType.FLX_LICENSE_SOURCE_TRUSTED_STORAGE, tsFeatures, lvTrustedStorageFeatures, clearFeatures);
                        clearFeatures = false;
                    }
                }
                pageTrustedStorageLicenses.Text = string.Format(strTSCaption, lvTrustedStorageFeatures.Items.Count);
                pageTrustedStorageLicenses.Tag = tabLicenses.SelectedTab == pageTrustedStorageLicenses ? null : new object();
            }
        }

        private void RefreshTrialFeatures()
        {
            if (ckbTrials.Checked)
            {
                IFeatureCollection trialFeatures = LicenseManager.GetFeatureCollection(LicenseSourceOption.Trials);
                InsertFeaturesToListView(LicenseSourceType.FLX_LICENSE_SOURCE_TRIALS, trialFeatures, lvTrialFeatures, true);
                pageTrialLicenses.Text = string.Format(strTrialsCaption, lvTrialFeatures.Items.Count);
                pageTrialLicenses.Tag = tabLicenses.SelectedTab == pageTrialLicenses ? null : new object();
            }
        }

        private void RefreshShortCodeFeatures()
        {
            if (ckbShortCode.Checked)
            {
                IFeatureCollection scFeatures = LicenseManager.GetFeatureCollection(LicenseSourceOption.ShortCode);
                InsertFeaturesToListView(LicenseSourceType.FLX_LICENSE_SOURCE_SHORT_CODE, scFeatures, lvShortCodeFeatures, true);
                pageShortCodeLicenses.Text = string.Format(strSCCaption, lvShortCodeFeatures.Items.Count);
                pageShortCodeLicenses.Tag = tabLicenses.SelectedTab == pageShortCodeLicenses ? null : new object();
            }
        }

        private void RefreshBufferFeatures()
        {
            if (bufferCount > 0)
            {
                lvBufferFeatures.Items.Clear();
                for (int i = 1; i <= bufferCount; i++)
                {
                    IFeatureCollection bufferFeatures = LicenseManager.GetFeatureCollection(strBufferNameRoot + i);
                    InsertFeaturesToListView(LicenseSourceType.FLX_LICENSE_SOURCE_BUFFER, bufferFeatures, lvBufferFeatures, false);
                }
                pageBufferLicenses.Text = string.Format(strBuffersCaption, lvBufferFeatures.Items.Count);
                pageBufferLicenses.Tag = tabLicenses.SelectedTab == pageBufferLicenses ? null : new object();
            }
        }

        private void RefreshAllFeatures()
        {
            invalidatedItems.Clear();
            tooltipItemIndex = -1;
            lvAllFeatures.Items.Clear();
            foreach (KeyValuePair<LicenseSourceType, IFeatureCollection> kvp in LicenseManager.GetFeatureCollection())
            {
                InsertFeaturesToListView(kvp.Key, kvp.Value, lvAllFeatures, false);
            }
            pageAllLicenses.Text = string.Format(strAllCaption, lvAllFeatures.Items.Count);
            pageAllLicenses.Tag = tabLicenses.SelectedTab == pageAllLicenses ? null : new object();
            cbxFeatureName.Items.Clear();
            cbxVersion.Items.Clear();
            featureVersions = new SortedDictionary<string, List<string>>();
            foreach (ListViewItem item in lvAllFeatures.Items)
            {
                if (!featureVersions.ContainsKey(item.SubItems[1].Text))
                {
                    featureVersions.Add(item.SubItems[1].Text, new List<string>(new string[]{ item.SubItems[2].Text }));
                }
                else if (!featureVersions[item.SubItems[1].Text].Contains(item.SubItems[2].Text))
                {
                    featureVersions[item.SubItems[1].Text].Add(item.SubItems[2].Text);
                }
            }
            foreach (KeyValuePair<string, List<string>> kvp in featureVersions)
            {
                kvp.Value.Sort();
            }
            string[] features = new string[featureVersions.Count];
            featureVersions.Keys.CopyTo(features, 0);
            cbxFeatureName.Items.AddRange(features);
            if (!string.IsNullOrEmpty(cbxFeatureName.Text) && featureVersions.ContainsKey(cbxFeatureName.Text))
            {
                cbxVersion.Items.AddRange(featureVersions[cbxFeatureName.Text].ToArray());
            }
        }

        private void lvAllFeatures_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void lvAllFeatures_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            // Draw the background and focus rectangle for a selected item.
            Brush brush = SystemInformation.HighContrast ? Brushes.DarkGray : Brushes.Gainsboro;
            DotNetDemoFeatureInfo featureInfo = e.Item.Tag as DotNetDemoFeatureInfo;
            if (featureInfo != null)
            {
                switch (featureInfo.lsType)
                {
                    case LicenseSourceType.FLX_LICENSE_SOURCE_TRUSTED_STORAGE:
                        brush = SystemInformation.HighContrast ? Brushes.SteelBlue : Brushes.LightSteelBlue;
                        break;
                    case LicenseSourceType.FLX_LICENSE_SOURCE_TRIALS:
                        brush = SystemInformation.HighContrast ? Brushes.DarkBlue : Brushes.LightBlue;
                        break;
                    case LicenseSourceType.FLX_LICENSE_SOURCE_SHORT_CODE:
                        brush = SystemInformation.HighContrast ? Brushes.Teal : Brushes.PaleTurquoise;
                        break;
                    case LicenseSourceType.FLX_LICENSE_SOURCE_SERVED_BUFFER:
                        brush = SystemInformation.HighContrast ? Brushes.DarkGoldenrod : Brushes.NavajoWhite;
                        break;
                    case LicenseSourceType.FLX_LICENSE_SOURCE_BUFFER:
                    case LicenseSourceType.FLX_LICENSE_SOURCE_CERTIFICATE:
                        brush = SystemInformation.HighContrast ? Brushes.DimGray : Brushes.Silver;
                        break;
                }
            }
            e.Graphics.FillRectangle(brush, e.Bounds);
            e.DrawFocusRectangle();
        }

        private void lvAllFeatures_DrawSubItem(object sender, DrawListViewSubItemEventArgs e)
        {
            if (e.ColumnIndex == 0 && e.Item.ImageList != null && e.Item.ImageIndex >= 0)
            {
                e.Graphics.DrawImage(e.Item.ImageList.Images[e.Item.ImageIndex],
                    new Rectangle(e.SubItem.Bounds.X + 3, e.SubItem.Bounds.Y, 16, 16));
                Brush textBrush = new SolidBrush(e.Item.ForeColor);
                int heightAdjust = (e.Item.Font.Height >= e.SubItem.Bounds.Height ? 0 : (e.SubItem.Bounds.Height - e.Item.Font.Height)/2); 
                e.Graphics.DrawString(e.SubItem.Text, e.Item.Font, textBrush,
                    new RectangleF(e.SubItem.Bounds.X + 19, e.SubItem.Bounds.Y + heightAdjust, e.SubItem.Bounds.Width - 19, e.SubItem.Bounds.Height - (heightAdjust * 2)));
            }
            else if (e.ColumnIndex == 0)
            {
                e.DrawText(TextFormatFlags.Right | TextFormatFlags.VerticalCenter | TextFormatFlags.NoPadding);
            }
            else
            {
                e.DrawText(TextFormatFlags.Left | TextFormatFlags.VerticalCenter | TextFormatFlags.NoPadding);
            }
        }

        // Handle issue with Windows XP OS support of ListView
        // http://connect.microsoft.com/VisualStudio/feedback/details/117181/drawitem-and-drawsubitem-not-being-called-correctly-for-listview
        private void lvAllFeatures_MouseMove(object sender, MouseEventArgs e)
        {
            ListViewHitTestInfo hitTest = lvAllFeatures.HitTest(e.X, e.Y);
            ListViewItem item = hitTest.Item;
            if (showingToolTip &&  
                (item == null || hitTest.SubItem == null || hitTest.SubItem != item.SubItems[0] || hitTest.Item.Index != tooltipItemIndex))
            {
                FNEToolTip.Hide(lvAllFeatures);
                showingToolTip = false;
                tooltipItemIndex = -1;
            }
            if (!showingToolTip && item != null && hitTest.SubItem != null && hitTest.SubItem == item.SubItems[0] &&
                !string.IsNullOrEmpty(hitTest.SubItem.Tag as string))
            {
                FNEToolTip.Show((string)hitTest.SubItem.Tag, lvAllFeatures, new Point(e.X, e.Y + 21));
                showingToolTip = true;
                tooltipItemIndex = hitTest.Item.Index;
            }
            if (item != null && !invalidatedItems.Contains(item.Index))
            {
                lvAllFeatures.Invalidate(item.Bounds);
                if (hitTest.SubItem != item.SubItems[0])
                {
                    invalidatedItems.Add(item.Index);
                }
            }
        }

        private void lvAllFeatures_MouseLeave(object sender, EventArgs e)
        {
            FNEToolTip.Hide(lvAllFeatures);
            showingToolTip = false;
            tooltipItemIndex = -1;
        }

        private void ckbTrustedStorageInstance_CheckedChanged(object sender, EventArgs e)
        {
            ResetError();
            SetLicensesTabs();
            CheckBox checkBox = sender as CheckBox;
            if (checkBox != null)
            {
                if (checkBox.Checked == true)
                {
                    try
                    {
                        Cursor.Current = Cursors.WaitCursor;
                        try
                        {
                            LicenseManager.AddTrustedStorageLicenseSource((LicenseServerInstance)checkBox.Tag);
                            SelectNextControl(checkBox, true, true, true, true);
                        }
                        catch (Exception exc)
                        {
                            errorProvider.SetIconAlignment(checkBox, ErrorIconAlignment.MiddleRight);
                            FlxException flxException = exc as FlxException;
                            errorProvider.SetError(checkBox, flxException == null ? exc.Message : flxException.ToString());
                            checkBox.Checked = false;
                        }
                        RefreshFeatures();
                    }
                    finally
                    {
                        Cursor.Current = Cursors.Default;
                    }
                }
                checkBox.Enabled = (checkBox.Checked == false);
            }
        }

        private void ckbTrials_CheckedChanged(object sender, EventArgs e)
        {
            ResetError();
            SetLicensesTabs();
            ckbTrials.Enabled = (ckbTrials.Checked == false);
            if (ckbTrials.Checked == true)
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    try
                    {
                        LicenseManager.AddTrialLicenseSource();
                        SelectNextControl(ckbTrials, true, true, true, true);
                    }
                    catch (Exception exc)
                    {
                        errorProvider.SetIconAlignment(ckbTrials, ErrorIconAlignment.MiddleLeft);
                        FlxException flxException = exc as FlxException;
                        errorProvider.SetError(ckbTrials, flxException == null ? exc.Message : flxException.ToString());
                        ckbTrials.Checked = false;
                    }
                    RefreshFeatures();
                }
                finally
                {
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        private void ckbShortCode_CheckedChanged(object sender, EventArgs e)
        {
            ResetError();
            SetLicensesTabs();
            ckbShortCode.Enabled = (ckbShortCode.Checked == false);
            if (ckbShortCode.Checked == true)
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    try
                    {
                        LicenseManager.AddShortCodeLicenseSource();
                        SelectNextControl(ckbShortCode, true, true, true, true);
                    }
                    catch (Exception exc)
                    {
                        errorProvider.SetIconAlignment(ckbShortCode, ErrorIconAlignment.MiddleLeft);
                        FlxException flxException = exc as FlxException;
                        errorProvider.SetError(ckbShortCode, flxException == null ? exc.Message : flxException.ToString());
                        ckbShortCode.Checked = false;
                    }
                    RefreshFeatures();
                }
                finally
                {
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        private void tabLicenses_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabLicenses.SelectedTab != null)
            {
                tabLicenses.SelectedTab.Tag = null;
            }
            lblLoadComplete.Visible = false;
        }

        private void tabLicenses_DrawItem(object sender, DrawItemEventArgs e)
        {
            Font tabFont;
            Color backColor = e.BackColor;
            foreach (Control control in tabLicenses.TabPages[e.Index].Controls)
            {
                ListView lv = control as ListView;
                if (lv != null)
                {
                    backColor = lv.BackColor;
                    break;
                }
            }
            Brush backBrush = new SolidBrush(backColor);
            Brush foreBrush;
            if (e.Index == tabLicenses.SelectedIndex)
            {
                foreBrush = new SolidBrush(baseTextColor);
                tabFont = new Font(e.Font, FontStyle.Bold);
            }
            else
            {
                Color textColor;
                if (SystemInformation.HighContrast)
                {
                    textColor = tabLicenses.TabPages[e.Index].Tag == null ? baseTextColor : Color.Gold;
                }
                else
                {
                    textColor = tabLicenses.TabPages[e.Index].Tag == null ? baseTextColor : Color.MidnightBlue;
                }
                foreBrush = new SolidBrush(textColor);
                tabFont = e.Font;
            }
            string tabName = tabLicenses.TabPages[e.Index].Text;
            StringFormat sf = new StringFormat();
            sf.Alignment = StringAlignment.Center;
            e.Graphics.FillRectangle(backBrush, e.Bounds);
            Rectangle r = e.Bounds;
            r = new Rectangle(r.X, r.Y + 3, r.Width, r.Height - 3);
            e.Graphics.DrawString(tabName, tabFont, foreBrush, r, sf);
            //Dispose objects
            sf.Dispose();
            if (e.Index == tabLicenses.SelectedIndex)
            {
                tabFont.Dispose();
            }
            backBrush.Dispose();
            foreBrush.Dispose();
        }

        #endregion

        #region File Inspect/Load

        private void btnLicenseFileBrowse_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dialog = new OpenFileDialog())
            {
                dialog.AddExtension = false;
                dialog.Filter = strBinaryFilter;
                dialog.Title = strOpenLicenseTitle;
                if (!string.IsNullOrEmpty(strLastLicenseFilePath))
                {
                    dialog.InitialDirectory = strLastLicenseFilePath;
                }
                else if (cbxLicenseFile.Items.Count >= 1)
                {
                    dialog.InitialDirectory = Path.GetDirectoryName(cbxLicenseFile.Items[0].ToString());
                }
                DialogResult result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    cbxLicenseFile.Text = dialog.FileName;
                    strLastLicenseFilePath = Path.GetDirectoryName(dialog.FileName);
                }
            }
        }

        private void cbxLicenseFile_TextChanged(object sender, EventArgs e)
        {
            btnInspectLicenseFile.Enabled = btnLoadLicenseFile.Enabled = (cbxLicenseFile.Text != null && !string.IsNullOrEmpty(cbxLicenseFile.Text.Trim()));
            lblLoadComplete.Visible = false;
            ResetError();
        }

        private void btnInspectLicenseFile_Click(object sender, EventArgs e)
        {
            byte[] fileContent = null;
            try
            {
                fileContent = File.ReadAllBytes(cbxLicenseFile.Text);
                if (fileContent.Length == 0)
                {
                    btnInspectLicenseFile.Enabled = btnLoadLicenseFile.Enabled = false;
                    errorProvider.SetIconAlignment(cbxLicenseFile, ErrorIconAlignment.MiddleRight);
                    errorProvider.SetError(cbxLicenseFile, strEmptyLicenseFile);
                    SetStatus(strEmptyLicenseFile, true);
                    return;
                }
                MessageTypeEnum type = LicenseManager.MessageType(fileContent);
                if (type == MessageTypeEnum.FLX_MESSAGE_TYPE_ERROR)
                {
                    ShowErrorStatusResponse(fileContent);
                    return;
                }

                IFeatureCollection features = null;
                ICapabilityResponse response = null;
                string sourceType = string.Empty;
                Color lvColor = SystemInformation.HighContrast ? Color.DarkGray : Color.Gainsboro;
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    switch (type)
                    {
                        case MessageTypeEnum.FLX_MESSAGE_TYPE_SERVED_CAPABILITY_RESPONSE:
                        case MessageTypeEnum.FLX_MESSAGE_TYPE_CAPABILITY_RESPONSE:
                            response = LicenseManager.GetResponseDetails(fileContent);
                            features = response.FeatureCollection;
                            lvColor =  lvTrustedStorageFeatures.BackColor;
                            sourceType = response.IsPreview ? strSourcePreviewResponseFile : strSourceCapResponseFile;
                            break;
                        case MessageTypeEnum.FLX_MESSAGE_TYPE_TRIAL:
                            features = GetTrialDetails(fileContent);
                            lvColor = lvTrialFeatures.BackColor;
                            sourceType = strSourceTrialFile;
                            break;
                        case MessageTypeEnum.FLX_MESSAGE_TYPE_BUFFER_LICENSE:
                            features = LicenseManager.GetFeatureCollection(fileContent, true);
                            lvColor = lvBufferFeatures.BackColor;
                            sourceType = strSourceBufferFile;
                            break;
                        default:
                            break;
                    }
                }
                finally
                {
                    Cursor.Current = Cursors.Default;
                }
                if (String.IsNullOrEmpty(sourceType))
                {
                    errorProvider.SetIconAlignment(cbxLicenseFile, ErrorIconAlignment.MiddleRight);
                    errorProvider.SetError(cbxLicenseFile, strInvalidLicenseFile);
                    SetStatus(strInvalidLicenseFile, true);
                }
                else if (features == null || features.Count <= 0)
                {
                    errorProvider.SetIconAlignment(cbxLicenseFile, ErrorIconAlignment.MiddleRight);
                    errorProvider.SetError(cbxLicenseFile, strEmptyLicenseFile2);
                    SetStatus(strEmptyLicenseFile2, true);
                }
                else
                {
                    using (FeatureView dialog = new FeatureView())
                    {
                        dialog.Icon = this.Icon;
                        string filename = string.IsNullOrEmpty(cbxLicenseFile.Text) ? null : Path.GetFileName(cbxLicenseFile.Text);
                        dialog.Text = strFeatureDialogTitle + filename ?? string.Empty;
                        dialog.SourceType = sourceType;
                        dialog.Features = features;
                        dialog.CapabilityResponse = response;
                        dialog.LVBackColor = lvColor;
                        dialog.StartPosition = FormStartPosition.CenterParent;
                        dialog.ShowDialog();
                    }
                }
            }
            catch (Exception exc)
            {
                errorProvider.SetIconAlignment(cbxLicenseFile, ErrorIconAlignment.MiddleRight);
                FlxException flxException = exc as FlxException;
                string excMessage = flxException == null ? exc.Message : flxException.ToString();
                errorProvider.SetError(cbxLicenseFile, excMessage);
                SetStatus(excMessage, true);
            }
        }

        private void ShowErrorStatusResponse(byte[] errorResponse)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                LicenseManager.GetResponseDetails(errorResponse);
            }
            catch (FlxException exc)
            {
                using (ErrorStatusDetails dialog = new ErrorStatusDetails())
                {
                    dialog.Icon = this.Icon;
                    dialog.StatusException = exc;
                    dialog.StartPosition = FormStartPosition.CenterParent;
                    dialog.ShowDialog();
                }
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void btnLoadLicenseFile_Click(object sender, EventArgs e)
        {
            ResetError();
            if (cbxLicenseFile.Text == null || string.IsNullOrEmpty(cbxLicenseFile.Text.Trim()))
            {
                btnInspectLicenseFile.Enabled = btnLoadLicenseFile.Enabled = false;
                return;
            }
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                byte[] fileContent = File.ReadAllBytes(cbxLicenseFile.Text);
                if (fileContent.Length == 0)
                {
                    btnInspectLicenseFile.Enabled = btnLoadLicenseFile.Enabled = false;
                    errorProvider.SetIconAlignment(cbxLicenseFile, ErrorIconAlignment.MiddleRight);
                    errorProvider.SetError(cbxLicenseFile, strEmptyLicenseFile);
                    SetStatus(strEmptyLicenseFile, true);
                    return;
                }

                MessageTypeEnum type = LicenseManager.MessageType(fileContent);
                if (type == MessageTypeEnum.FLX_MESSAGE_TYPE_BUFFER_LICENSE)
                {
                    string bufferSourceName = strBufferNameRoot + ++bufferCount;
                    LicenseManager.AddBufferLicenseSource(fileContent, bufferSourceName);
                    lblLoadComplete.Text = string.Format(strBufferLoaded, cbxLicenseFile.Text);
                    lblLoadComplete.Visible = true;
                    RefreshFeatures();
                    SetLicensesTabs();
                    RefreshLicenseFiles();
                }
                else if (type == MessageTypeEnum.FLX_MESSAGE_TYPE_TRIAL)
                {
                    LicenseManager.ProcessTrial(fileContent);
                    lblLoadComplete.Text = string.Format(strTrialLoaded, cbxLicenseFile.Text);
                    lblLoadComplete.Visible = true;
                    if (ckbTrials.Checked == false)
                    {
                        ckbTrials.Checked = true;
                    }
                    else
                    {
                        ckbTrials_CheckedChanged(null, new EventArgs());
                    }
                    RefreshLicenseFiles();
                }
                else if (type == MessageTypeEnum.FLX_MESSAGE_TYPE_SERVED_CAPABILITY_RESPONSE ||
                         type == MessageTypeEnum.FLX_MESSAGE_TYPE_CAPABILITY_RESPONSE)
                {
                    ICapabilityResponse response = LicenseManager.GetResponseDetails(fileContent);
                    if (response.IsPreview)
                    {
                        btnLoadLicenseFile.Enabled = false;
                        string previewLoadError = string.Format(strPreviewFile, cbxLicenseFile.Text);
                        errorProvider.SetError(cbxLicenseFile, previewLoadError);
                        SetStatus(previewLoadError, true);
                    }
                    else
                    {
                        LicenseServerInstance serverInstance = response.ServerInstance;
                        if (serverInstance == LicenseServerInstance.Unknown)
                        {
                            using (ServerInstanceSelect dialog = new ServerInstanceSelect())
                            {
                                dialog.Icon = this.Icon;
                                dialog.StartPosition = FormStartPosition.CenterParent;
                                DialogResult result = dialog.ShowDialog();
                                if (result == DialogResult.OK)
                                {
                                    serverInstance = dialog.SelectedServerInstance;
                                }
                                else
                                {
                                    return;
                                }
                            }
                        }
                        try
                        {
                            LicenseManager.ProcessCapabilityResponse(fileContent, serverInstance);
                        }
                        finally
                        {
                            // Processing of the capability response can change acquired license status (e.g. stale).
                            RefreshAcquiredLicenses();
                        }
                        lblLoadComplete.Text = string.Format(strCRLoaded, cbxLicenseFile.Text);
                        lblLoadComplete.Visible = true;
                        foreach (CheckBox checkBox in ckbTSInstances)
                        {
                            if ((LicenseServerInstance)checkBox.Tag == serverInstance)
                            {
                                if (!checkBox.Checked)
                                {
                                    checkBox.Checked = true;
                                }
                                else
                                {
                                    ckbTrustedStorageInstance_CheckedChanged(checkBox, new EventArgs());
                                }
                            }
                        }
                        RefreshLicenseFiles();
                    }
                }
                else if (type == MessageTypeEnum.FLX_MESSAGE_TYPE_ERROR)
                {
                    btnInspectLicenseFile.Enabled = btnLoadLicenseFile.Enabled = false;
                    errorProvider.SetError(cbxLicenseFile, string.Format(strErrorFile, cbxLicenseFile.Text));
                }
                else
                {
                    btnInspectLicenseFile.Enabled = btnLoadLicenseFile.Enabled = false;
                    errorProvider.SetError(cbxLicenseFile, strUnknownFile);
                }
            }
            catch (Exception exc)
            {
                errorProvider.SetIconAlignment(cbxLicenseFile, ErrorIconAlignment.MiddleRight);
                FlxException flxException = exc as FlxException;
                string excMessage = flxException == null ? exc.Message : flxException.ToString();
                errorProvider.SetError(cbxLicenseFile, excMessage);
                SetStatus(excMessage, true);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void RefreshLicenseFiles()
        {
            string file = string.IsNullOrEmpty(cbxLicenseFile.Text) ? null : cbxLicenseFile.Text.Trim();
            if (!string.IsNullOrEmpty(file))
            {
                if (!currentConfiguration.LicenseFiles.Contains(file))
                {
                    cbxLicenseFile.BeginUpdate();
                    cbxLicenseFile.Items.Clear();
                    currentConfiguration.LicenseFiles.Add(file);
                    currentConfiguration.LicenseFiles.Sort();
                    cbxLicenseFile.Items.AddRange(currentConfiguration.LicenseFiles.ToArray());
                    cbxLicenseFile.EndUpdate();
                }
                if (!loadedLicenseFiles.Contains(file))
                {
                    loadedLicenseFiles.Add(file);
                    loadedLicenseFiles.Sort();
                }
            }
            cbxLicenseFile.Text = string.Empty;
        }

        #endregion

        #region Server requests

        private void cbxServerType_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool isLocal = cbxServerType.Text.Equals(strLocal);
            bool isCloud = cbxServerType.Text.Equals(strCloud);
            if (isLocal && !tabServerRequest.TabPages.Contains(pageInfoMessage))
            {
                tabServerRequest.TabPages.Add(pageInfoMessage);
            }
            else if (!isLocal && tabServerRequest.TabPages.Contains(pageInfoMessage))
            {
                tabServerRequest.TabPages.Remove(pageInfoMessage);
            }
            currentConfiguration.ServerType = cbxServerType.Text;
            lblBorrowGranularity.Visible =
              cbxBorrowGranularity.Visible =
              lblBorrowInterval.Visible =
              txtBorrowInterval.Visible = isLocal;
            ServerInfo serverInfo = GetServerInfo();
            if (serverInfo != null)
            {
                txtServerURL.Text = "http" + (serverInfo.UseHTTPS ? "s" : String.Empty) + strSchemeSeparator +
                    serverInfo.Host + (serverInfo.Port.Equals("0") ? String.Empty : ":" + serverInfo.Port) +
                    (!String.IsNullOrEmpty(serverInfo.Endpoint) ? "/" + serverInfo.Endpoint : String.Empty);
            }
            else
            {
                txtServerURL.Text = String.Empty;
            }
            string currentOperation = cbxOperation.Text;
            cbxOperation.BeginUpdate();
            cbxOperation.Items.Clear();
            if (isCloud)
            {
                cbxOperation.Items.AddRange(new string[] { strRequest, strReport, strUndo, strPreview });
            }
            else if (isLocal)
            {
                cbxOperation.Items.AddRange(new string[] { strRequest, strPreview });
            }
            else
            {
                cbxOperation.Items.AddRange(new string[] { strRequest });
            }
            if (cbxOperation.Items.Contains(currentOperation))
            {
                cbxOperation.SelectedIndex = cbxOperation.Items.IndexOf(currentOperation);
            }
            else
            {
                cbxOperation.SelectedIndex = 0;
            }
            cbxOperation.Enabled = cbxOperation.Items.Count > 1;
            cbxOperation.EndUpdate();
            ckbRequestAll.Enabled = (isLocal || isCloud) && currentOperation.Equals(strPreview);
            ckbIncremental.Enabled = (isLocal || isCloud);
            ckbOTA.Enabled = isLocal;
            SetCRTabPages();
        }

        private void ckbHTTPS_CheckedChanged(object sender, EventArgs e)
        {
            int defaultPort = 0;
            ResetError();
            ServerInfo serverInfo = GetServerInfo();
            if (serverInfo != null)
            {
                serverInfo.UseHTTPS = ckbHTTPS.Checked;
                switch (cbxServerType.Text)
                {
                    case strLocal:
                        defaultPort = serverInfo.UseHTTPS ? 443 : 7070;
                        break;
                    case strFNO:
                        defaultPort = serverInfo.UseHTTPS ? 443 : 8888;
                        break;
                    case strCloud:
                        defaultPort = serverInfo.UseHTTPS ? 443 : 0;
                        break;
                }
            }
            string serverURL = txtServerURL.Text;
            try
            {
                // Avoiding Uri and UriBuilder here because they want to canonicalize strings
                // and that's problematical here. 
                UriBuilder serverUri = new UriBuilder(serverURL);
                serverUri.Scheme = (ckbHTTPS.Checked ? Uri.UriSchemeHttps : Uri.UriSchemeHttp);
                if (ckbHTTPS.Checked && (serverUri.Port == 0 || serverUri.Port != defaultPort))
                {
                    serverUri.Port = defaultPort;
                }
                else if (!ckbHTTPS.Checked && defaultPort != 0 && (serverUri.Port == 0 || serverUri.Port == 443))
                {
                    serverUri.Port = defaultPort;
                }
                // Do not use Uri.ToString() here because to not want to canonicalize the url just yet
                string strEndpoint = GetEndpoint(txtServerURL.Text);
                serverURL = serverUri.Scheme.ToLowerInvariant() + strSchemeSeparator +
                    serverUri.Host + (serverUri.Port == 0 ? String.Empty : ":" + serverUri.Port.ToString()) +
                    (String.IsNullOrEmpty(strEndpoint) ? String.Empty : "/" + strEndpoint);
            }
            catch {}
            txtServerURL.Text = serverURL;
        }

        private void txtServerURL_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Uri serverUri = new Uri(txtServerURL.Text);
                ServerInfo serverInfo = GetServerInfo();
                if (serverInfo != null)
                {
                    serverInfo.UseHTTPS = serverUri.Scheme.Equals(Uri.UriSchemeHttps);
                    serverInfo.Host = serverUri.Host;
                    serverInfo.Port = serverUri.Port.ToString();
                    serverInfo.Endpoint = GetEndpoint(txtServerURL.Text);
                    if (serverInfo.UseHTTPS != ckbHTTPS.Checked)
                    {
                        ckbHTTPS.CheckedChanged -= new EventHandler(ckbHTTPS_CheckedChanged);
                        ckbHTTPS.Checked = serverInfo.UseHTTPS;
                        ckbHTTPS.CheckedChanged += new EventHandler(ckbHTTPS_CheckedChanged);
                    }
                }
            }
            catch { }
        }

        private string GetEndpoint(string serverURL)
        {
            string temp = serverURL;
            int index = temp.IndexOf(strSchemeSeparator);
            if (index >= 0)
            {
                temp = temp.Substring(index + strSchemeSeparator.Length);
            }
            index = temp.IndexOf("/");
            temp = ((index > 0 && index < (temp.Length - 1)) ? temp.Substring(index + 1) : String.Empty);
            return temp;
        }

        private ServerInfo GetServerInfo()
        {
            switch (cbxServerType.Text)
            {
                case strLocal:
                    return currentConfiguration.LocalServerInfo;
                case strBackOffice:
                    return currentConfiguration.TestBOServerInfo;
                case strFNO:
                    return currentConfiguration.FnoServerInfo;
                case strCloud:
                    return currentConfiguration.CloudServerInfo;
            }
            return null;
        }

        private void btnServerAdvanced_Click(object sender, EventArgs e)
        {
            using (AdvancedServer dialog = new AdvancedServer())
            {
                dialog.Icon = this.Icon;
                dialog.StartPosition = FormStartPosition.CenterParent;
                dialog.ProxyServer = currentConfiguration.ProxyServer;
                dialog.ProxyPort = currentConfiguration.ProxyPort;
                dialog.HttpHeaders = currentConfiguration.HttpHeaders;
                DialogResult result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    currentConfiguration.ProxyServer = dialog.ProxyServer;
                    currentConfiguration.ProxyPort = dialog.ProxyPort;
                    currentConfiguration.HttpHeaders = dialog.HttpHeaders;
                }
                else
                {
                    return;
                }
            }
        }

        private void cbxOperation_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetError();
            bool isLocal = cbxServerType.Text.Equals(strLocal);
            bool isCloud = cbxServerType.Text.Equals(strCloud);
            currentConfiguration.Operation = cbxOperation.Text;
            txtCorrelationID.Enabled = true;
            switch (cbxOperation.Text)
            {
                case strUndo:
                    ckbForceResponse.Enabled = true;
                    ckbIncremental.Enabled = ckbOTA.Enabled = ckbRequestAll.Enabled = false;
                    if (string.IsNullOrEmpty(txtCorrelationID.Text))
                    {
                        string cid = LicenseManager.GetTrustedStorageCorrelationId((LicenseServerInstance)cbxTSInstance.SelectedItem);
                        cid = (cid == null ? cid : cid.Trim());
                        if (!string.IsNullOrEmpty(cid))
                        {
                            txtCorrelationID.Text = cid;
                        }
                        else
                        {
                            errorProvider.SetIconAlignment(txtCorrelationID, ErrorIconAlignment.MiddleLeft);
                            errorProvider.SetError(txtCorrelationID, strRequiredCorrelationID);
                        }
                    }
                    break;
                case strReport:
                    ckbForceResponse.Checked = ckbForceResponse.Enabled = false;
                    ckbIncremental.Enabled = ckbOTA.Enabled = ckbRequestAll.Enabled = false; 
                    break;
                case strPreview:
                    ckbForceResponse.Checked = true;
                    ckbForceResponse.Enabled = false;
                    ckbProcessResponse.Checked = ckbProcessResponse.Enabled = false;
                    ckbIncremental.Enabled = ckbOTA.Enabled = false;
                    ckbRequestAll.Enabled = true;
                    txtCorrelationID.Text = String.Empty;
                    txtCorrelationID.Enabled = false;
                    break;
                default:
                    ckbForceResponse.Checked = ckbForceResponse.Enabled = true;
                    ckbProcessResponse.Checked = ckbProcessResponse.Enabled = true;
                    ckbIncremental.Enabled = (isLocal || isCloud);
                    ckbOTA.Enabled = isLocal;
                    ckbRequestAll.Enabled = false;
                    break;
            }
            cbxTSInstance.Enabled = !cbxOperation.Text.Equals(strReport) && !cbxOperation.Text.Equals(strPreview);
            SetCRTabPages();
        }

        private void cbxTSInstance_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentConfiguration.ServerInstance = cbxTSInstance.Text;
        }

        private void txtCorrelationID_TextChanged(object sender, EventArgs e)
        {
            ResetError();
            currentConfiguration.CorrelationID = txtCorrelationID.Text;
            if (cbxOperation.Text.Equals(strUndo) && string.IsNullOrEmpty(txtCorrelationID.Text))
            {
                errorProvider.SetIconAlignment(txtCorrelationID, ErrorIconAlignment.MiddleLeft);
                errorProvider.SetError(txtCorrelationID, strRequiredCorrelationID);
            }
        }

        private void SetCRTabPages()
        {
            tabCapabilityRequest.TabPages.Clear();
            tabCapabilityRequest.TabPages.Add(pageCR_Options);
            bool isLocal = cbxServerType.Text.Equals(strLocal);
            bool isCloud = cbxServerType.Text.Equals(strCloud);
            bool isRequestAll = ckbRequestAll.Enabled && ckbRequestAll.Checked;
            if (isCloud)
            {
                if (!cbxOperation.Text.Equals(strUndo) && !isRequestAll)
                {
                    tabCapabilityRequest.TabPages.Add(pageCR_LLS);
                }
                tabCapabilityRequest.TabPages.Add(pageCR_BO);
            }
            else if (isLocal)
            {
                if (!isRequestAll)
                {
                    tabCapabilityRequest.TabPages.Add(pageCR_LLS);
                }
            }
            else
            {
                tabCapabilityRequest.TabPages.Add(pageCR_BO);
            }
            tabCapabilityRequest.TabPages.Add(pageCR_VD);
            if (!isRequestAll)
            {
                tabCapabilityRequest.TabPages.Add(pageCR_FS);
            }
            tabCapabilityRequest.TabPages.Add(pageCR_AuxHostIds);
            tabCapabilityRequest.TabPages.Add(pageCR_Resp);
        }

        #region Capability request

        private void ckbForceResponse_CheckedChanged(object sender, EventArgs e)
        {
            currentConfiguration.ForceResponse = ckbForceResponse.Checked;
        }

        private void ckbProcessResponse_CheckedChanged(object sender, EventArgs e)
        {
            currentConfiguration.ProcessResponse = ckbProcessResponse.Checked;
        }

        private void txtRespFile_TextChanged(object sender, EventArgs e)
        {
            currentConfiguration.ResponseFile = GetConfigString(txtRespFile.Text);
        }

        private void btnRespFile_Click(object sender, EventArgs e)
        {
            ResetError();
            using (OpenFileDialog dialog = new OpenFileDialog())
            {
                dialog.AddExtension = true;
                dialog.Filter = strBinaryFilter;
                dialog.CheckFileExists = false;
                dialog.CheckPathExists = true;
                dialog.Title = strOpenCRRespTitle;
                if (!string.IsNullOrEmpty(strLastCapabilityResponseFilePath))
                {
                    dialog.InitialDirectory = strLastCapabilityResponseFilePath;
                }
                DialogResult result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    txtRespFile.Text = dialog.FileName;
                    strLastCapabilityResponseFilePath = Path.GetDirectoryName(dialog.FileName);
                }
            }
        }

        #region Capability request options


        private void ckbOTA_CheckedChanged(object sender, EventArgs e)
        {
            cbxBorrowGranularity.SelectedIndex = 0;
            cbxBorrowGranularity.Enabled = txtBorrowInterval.Enabled = !ckbOTA.Checked;
            currentConfiguration.OneTimeActivation = ckbOTA.Checked;
        }

        private void ckbIncremental_CheckedChanged(object sender, EventArgs e)
        {
            currentConfiguration.Incremental = ckbIncremental.Checked;
        }

        private void ckbRequestAll_CheckedChanged(object sender, EventArgs e)
        {
            currentConfiguration.RequestAll = ckbRequestAll.Checked;
            SetCRTabPages();
        }

        private void txtAcquisitionID_TextChanged(object sender, EventArgs e)
        {
            currentConfiguration.AcquisitionID = txtAcquisitionID.Text;
        }

        private void txtEnterpriseID_TextChanged(object sender, EventArgs e)
        {
            currentConfiguration.EnterpriseID = txtEnterpriseID.Text;
        }

        private void txtRequestorID_TextChanged(object sender, EventArgs e)
        {
            currentConfiguration.RequestorID = txtRequestorID.Text;
        }

        private void cbxBorrowGranularity_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentConfiguration.BorrowGranularity = cbxBorrowGranularity.Text;
        }

        private void txtBorrowInterval_TextChanged(object sender, EventArgs e)
        {
            ResetError();
            if (txtBorrowInterval.Text != null && !string.IsNullOrEmpty(txtBorrowInterval.Text.Trim()))
            {
                ulong interval;
                if (!GetCount(txtBorrowInterval.Text.Trim(), out interval))
                {
                    errorProvider.SetIconAlignment(txtBorrowInterval, ErrorIconAlignment.MiddleRight);
                    errorProvider.SetError(txtBorrowInterval, strInvalidBorrowInterval);
                }
                else
                {
                    currentConfiguration.BorrowInterval = txtBorrowInterval.Text;
                }
            }
        }

        #endregion

        #region Rights IDs

        private void txtBOCount_TextChanged(object sender, EventArgs e)
        {
            ResetError();
            if (txtBOCount.Text != null && !string.IsNullOrEmpty(txtBOCount.Text.Trim()))
            {
                ulong count;
                if (!GetCount(txtBOCount.Text.Trim(), out count))
                {
                    errorProvider.SetIconAlignment(txtBOCount, ErrorIconAlignment.MiddleRight);
                    errorProvider.SetError(txtBOCount, strInvalidRightsCount);
                }
            }
        }

        private void btnBOAdd_Click(object sender, EventArgs e)
        {
            ResetError();
            if (txtBORights.Text == null || string.IsNullOrEmpty(txtBORights.Text.Trim()))
            {
                errorProvider.SetIconAlignment(txtBORights, ErrorIconAlignment.MiddleRight);
                errorProvider.SetError(txtBORights, strMissingRightsID);
            }
            else if (txtBOCount.Text == null || string.IsNullOrEmpty(txtBOCount.Text.Trim()))
            {
                errorProvider.SetIconAlignment(txtBOCount, ErrorIconAlignment.MiddleRight);
                errorProvider.SetError(txtBOCount, strMissingRightsCount);
            }
            else
            {
                foreach (ListViewItem item in lvBORights.Items)
                {
                    if (item.SubItems[0].Text.Equals(txtBORights.Text.Trim(), StringComparison.OrdinalIgnoreCase))
                    {
                        lvBORights.Items.Remove(item);
                        break;
                    }
                }
                lvBORights.Items.Add(new ListViewItem(new string[] { txtBORights.Text.Trim(), txtBOCount.Text, ckbBOPartial.Checked ? "Y" : "N" }));
                UpdateConfigRights();
            }
        }

        private void btnBORemove_Click(object sender, EventArgs e)
        {
            ResetError();
            lvBORights.BeginUpdate();
            foreach (ListViewItem item in lvBORights.SelectedItems)
            {
                lvBORights.Items.Remove(item);
            }
            lvBORights.EndUpdate();
            UpdateConfigRights();
        }

        private void UpdateConfigRights()
        {
            currentConfiguration.Rights.Clear();
            foreach (ListViewItem item in lvBORights.Items)
            {
                currentConfiguration.Rights.Add(new RightsInfo(item.SubItems[0].Text, item.SubItems[1].Text,
                                                               item.SubItems[2].Text[0] == 'Y' ? true : false));
            }
        }

        private void lvBORights_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnBORemove.Enabled = lvBORights.SelectedIndices.Count > 0;
        }

        #endregion

        #region Desired features

        private void txtLLSCount_TextChanged(object sender, EventArgs e)
        {
            ResetError();
            if (txtLLSCount.Text != null && !string.IsNullOrEmpty(txtLLSCount.Text.Trim()))
            {
                long count;
                if (!GetCount(txtLLSCount.Text.Trim(), out count))
                {
                    errorProvider.SetIconAlignment(txtLLSCount, ErrorIconAlignment.MiddleRight);
                    errorProvider.SetError(txtLLSCount, strInvalidFeatureCount);
                }
            }
        }

        private void btnLLSAdd_Click(object sender, EventArgs e)
        {
            ResetError();
            if (txtLLSFeature == null || string.IsNullOrEmpty(txtLLSFeature.Text.Trim()))
            {
                errorProvider.SetIconAlignment(txtLLSFeature, ErrorIconAlignment.MiddleRight);
                errorProvider.SetError(txtLLSFeature, strMissingFeatureName);
            }
            else if (txtLLSVersion == null || string.IsNullOrEmpty(txtLLSVersion.Text.Trim()))
            {
                errorProvider.SetIconAlignment(txtLLSVersion, ErrorIconAlignment.MiddleRight);
                errorProvider.SetError(txtLLSVersion, strMissingFeatureVersion);
            }
            else if (txtLLSCount.Text == null || string.IsNullOrEmpty(txtLLSCount.Text.Trim()))
            {
                errorProvider.SetIconAlignment(txtLLSCount, ErrorIconAlignment.MiddleRight);
                errorProvider.SetError(txtLLSCount, strMissingFeatureCount);
            }
            else
            {
                foreach (ListViewItem item in lvLLSFeatures.Items)
                {
                    if (item.SubItems[0].Text.Equals(txtLLSFeature.Text.Trim(), StringComparison.OrdinalIgnoreCase) &&
                        item.SubItems[1].Text.Equals(txtLLSVersion.Text.Trim(), StringComparison.OrdinalIgnoreCase))
                    {
                        lvLLSFeatures.Items.Remove(item);
                        break;
                    }
                }
                lvLLSFeatures.Items.Add(new ListViewItem(new string[] { txtLLSFeature.Text.Trim(), txtLLSVersion.Text.Trim(), txtLLSCount.Text,
                                                                        ckbLLSPartial.Checked ? "Y" : "N" }));
                UpdateConfigFeatures();
            }
        }

        private void btnLLSRemove_Click(object sender, EventArgs e)
        {
            ResetError();
            lvLLSFeatures.BeginUpdate();
            foreach (ListViewItem item in lvLLSFeatures.SelectedItems)
            {
                lvLLSFeatures.Items.Remove(item);
            }
            lvLLSFeatures.EndUpdate();
            UpdateConfigFeatures();
         }

        private void UpdateConfigFeatures()
        {
            currentConfiguration.Features.Clear();
            foreach (ListViewItem item in lvLLSFeatures.Items)
            {
                currentConfiguration.Features.Add(new FeatureInfo(item.SubItems[0].Text, item.SubItems[1].Text, item.SubItems[2].Text,
                                                                  item.SubItems[3].Text[0] == 'Y' ? true : false ));
            }
        }

        private void lvLLSFeatures_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnLLSRemove.Enabled = lvLLSFeatures.SelectedIndices.Count > 0;
        }

        #endregion

        #region Capabilty request vendor dictionary

        private void cbxVDType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxVDType.Text == strInteger)
            {
                txtVDValue_TextChanged(null, new EventArgs());
            }
        }

        private void txtVDValue_TextChanged(object sender, EventArgs e)
        {
            ResetError();
            if (cbxVDType.Text == strInteger && txtVDValue.Text != null && !string.IsNullOrEmpty(txtVDValue.Text.Trim()))
            {
                long count;
                if (!GetCount(txtVDValue.Text.Trim(), out count))
                {
                    errorProvider.SetIconAlignment(txtVDValue, ErrorIconAlignment.MiddleRight);
                    errorProvider.SetError(txtVDValue, strInvalidIntDictionaryItem);
                }
            }
        }

        private void btnVDAdd_Click(object sender, EventArgs e)
        {
            ResetError();
            string vdKey = string.IsNullOrEmpty(txtVDKey.Text) ? string.Empty : txtVDKey.Text.Trim();
            string vdValue = string.IsNullOrEmpty(txtVDValue.Text) ? string.Empty : txtVDValue.Text;
            if (string.IsNullOrEmpty(vdKey))
            {
                errorProvider.SetIconAlignment(txtVDKey, ErrorIconAlignment.MiddleRight);
                errorProvider.SetError(txtVDKey, strMissingDictionaryKey);
            }
            else if (cbxVDType.Text == strInteger && string.IsNullOrEmpty(vdValue.Trim()))
            {
                errorProvider.SetIconAlignment(txtVDValue, ErrorIconAlignment.MiddleRight);
                errorProvider.SetError(txtVDValue, strMissingIntDictionaryItem);
            }
            else 
            {
                foreach (ListViewItem item in lvVendorDictionary.Items)
                {
                    if (item.SubItems[1].Text.Equals(vdKey, StringComparison.OrdinalIgnoreCase))
                    {
                        lvVendorDictionary.Items.Remove(item);
                        break;
                    }
                }
                lvVendorDictionary.Items.Add(new ListViewItem(new string[] {cbxVDType.Text, vdKey, vdValue}));
            }
        }

        private void btnVDRemove_Click(object sender, EventArgs e)
        {
            ResetError();
            lvVendorDictionary.BeginUpdate();
            foreach (ListViewItem item in lvVendorDictionary.SelectedItems)
            {
                lvVendorDictionary.Items.Remove(item);
            }
            lvVendorDictionary.EndUpdate();
        }

        private void lvVendorDictionary_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnVDRemove.Enabled = lvVendorDictionary.SelectedIndices.Count > 0;
        }

        #endregion

        #region Capabilty request feature selectors dictionary

        private void btnFSAdd_Click(object sender, EventArgs e)
        {
            ResetError();
            string fsKey = string.IsNullOrEmpty(txtFSKey.Text) ? string.Empty : txtFSKey.Text.Trim();
            string fsValue = string.IsNullOrEmpty(txtFSValue.Text) ? string.Empty : txtFSValue.Text.Trim();
            if (string.IsNullOrEmpty(fsKey))
            {
                errorProvider.SetIconAlignment(txtFSKey, ErrorIconAlignment.MiddleRight);
                errorProvider.SetError(txtFSKey, strFSMissingDictionaryKey);
            }
            else
            {
                foreach (ListViewItem item in lvFeatureSelectors.Items)
                {
                    if (item.SubItems[1].Text.Equals(fsKey, StringComparison.OrdinalIgnoreCase))
                    {
                        lvFeatureSelectors.Items.Remove(item);
                        break;
                    }
                }
                lvFeatureSelectors.Items.Add(new ListViewItem(new string[] { cbxFSType.Text, fsKey, fsValue }));
                UpdateConfigFeatureSelectors();
            }
        }

        private void btnFSRemove_Click(object sender, EventArgs e)
        {
            ResetError();
            lvFeatureSelectors.BeginUpdate();
            foreach (ListViewItem item in lvFeatureSelectors.SelectedItems)
            {
                lvFeatureSelectors.Items.Remove(item);
            }
            lvFeatureSelectors.EndUpdate();
            UpdateConfigFeatureSelectors();
        }

        private void lvFeatureSelectors_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnFSRemove.Enabled = lvFeatureSelectors.SelectedIndices.Count > 0;
        }

        private void UpdateConfigFeatureSelectors()
        {
            currentConfiguration.FeatureSelectors.Clear();
            foreach (ListViewItem item in lvFeatureSelectors.Items)
            {
                currentConfiguration.FeatureSelectors.Add(new FeatureSelector(item.SubItems[0].Text, item.SubItems[1].Text, item.SubItems[2].Text));
            }
        }

        #endregion

        #region Capabilty request auxiliary host id collection

        private void btnAuxHostIdAdd_Click(object sender, EventArgs e)
        {
            ResetError();
            HostIdEnum auxHostIdType = (HostIdEnum)cbxAuxHostIdType.SelectedItem;
            string auxHostIdValue = string.IsNullOrEmpty(txtAuxHostIdValue.Text) ? string.Empty : txtAuxHostIdValue.Text.Trim();
            if (string.IsNullOrEmpty(auxHostIdValue))
            {
                errorProvider.SetIconAlignment(txtAuxHostIdValue, ErrorIconAlignment.MiddleLeft);
                errorProvider.SetError(txtAuxHostIdValue, strAuxHostIdMissingValue);
            }
            else
            {
                lvAuxHostIds.Items.Add(new ListViewItem(new string[] { auxHostIdType.ToString(), auxHostIdValue }));
                UpdateConfigAuxiliaryHostIds();
            }
        }

        private void btnAuxHostIdRemove_Click(object sender, EventArgs e)
        {
            ResetError();
            lvAuxHostIds.BeginUpdate();
            foreach (ListViewItem item in lvAuxHostIds.SelectedItems)
            {
                lvAuxHostIds.Items.Remove(item);
            }
            lvAuxHostIds.EndUpdate();
            UpdateConfigAuxiliaryHostIds();
        }

        private void lvAuxHostIds_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnAuxHostIdRemove.Enabled = lvAuxHostIds.SelectedIndices.Count > 0;
        }

        private void UpdateConfigAuxiliaryHostIds()
        {
            currentConfiguration.AuxiliaryHostIds.Clear();
            foreach (ListViewItem item in lvAuxHostIds.Items)
            {
                currentConfiguration.AuxiliaryHostIds.Add(new AuxiliaryHostId(item.SubItems[0].Text, item.SubItems[1].Text));
            }
        }

        #endregion

        private void btnCRSend_Click(object sender, EventArgs e)
        {
            ulong count = 0;
            ResetError();
            lvCRResponse.Items.Clear();
            btnCRResponseDetails.Enabled = false;
            btnCRResponseDetails.Tag = null;
            Uri serverUri = null;
            try
            {
                serverUri = new Uri(txtServerURL.Text);
                if (!serverUri.Scheme.StartsWith(strHTTP, StringComparison.OrdinalIgnoreCase))
                {
                    errorProvider.SetIconAlignment(lblServerURL, ErrorIconAlignment.MiddleLeft);
                    errorProvider.SetError(lblServerURL,
                        String.Format("{0} {1} '{2}'.", strInvalidServerURL, strUnsupportedScheme, serverUri.Scheme));
                    return;
                }
            }
            catch (Exception uriException)
            {
                errorProvider.SetIconAlignment(txtServerURL, ErrorIconAlignment.MiddleLeft);
                errorProvider.SetError(txtServerURL, strInvalidServerURL + uriException.Message);
                return;
            }

            if (txtBorrowInterval.Text != null &&
                !string.IsNullOrEmpty(txtBorrowInterval.Text.Trim()) &&
                !GetCount(txtBorrowInterval.Text.Trim(), out count))
            {
                errorProvider.SetIconAlignment(txtBorrowInterval, ErrorIconAlignment.MiddleRight);
                errorProvider.SetError(txtBorrowInterval, strInvalidBorrowInterval);
                return;
            }
            else if (txtRespFile.Text != null && !string.IsNullOrEmpty(txtRespFile.Text.Trim()))
            {
                if (!Directory.Exists(Path.GetDirectoryName(txtRespFile.Text)))
                {
                    errorProvider.SetIconAlignment(txtRespFile, ErrorIconAlignment.MiddleRight);
                    errorProvider.SetError(txtRespFile, strInvalidCRRespDirectory);
                    return;
                }
                else if (File.Exists(txtRespFile.Text.Trim()) &&
                         MessageBox.Show(this, strCRRespFileExistsMsg,
                                    string.Format(strCRRespFileExistsCaption, txtRespFile.Text.Trim()),
                                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != DialogResult.Yes)
                {
                    errorProvider.SetIconAlignment(txtRespFile, ErrorIconAlignment.MiddleRight);
                    errorProvider.SetError(txtRespFile, strInvalidCRRespFile);
                    return;
                }
            }

            ICapabilityRequestOptions options = CreateCROptions();
            try
            {
                btnCRSend.Enabled = false;
                byte[] capRequest = CreateCapabilityRequest(options);
                SetStatus(strSending, false);
                toolStripProgressBar.Visible = toolStripProgressBar.Enabled = true;
                Cursor.Current = Cursors.WaitCursor;
                sendingThread = new Thread(this.CRSendToServer);
                sendingThread.Start(new DotNetDemoSendParameters(serverUri.ToString(), capRequest, options.ServerInstance));
            }
            catch (Exception exc)
            {
                CRServerException(exc, strCRSendError);
            }
         }

        private void btnCRSave_Click(object sender, EventArgs e)
        {
            ulong count = 0;
            ResetError();
            if (txtBorrowInterval.Text != null                       &&
                !string.IsNullOrEmpty(txtBorrowInterval.Text.Trim()) &&
                !GetCount(txtBorrowInterval.Text.Trim(), out count))
            {
                errorProvider.SetIconAlignment(txtBorrowInterval, ErrorIconAlignment.MiddleRight);
                errorProvider.SetError(txtBorrowInterval, strInvalidBorrowInterval);
                return;
            }
            ICapabilityRequestOptions options = CreateCROptions();
            using (SaveFileDialog dialog = new SaveFileDialog())
            {
                dialog.AddExtension = false;
                dialog.Filter = strBinaryFilter;
                dialog.Title = strSaveCRTitle;
                dialog.CheckFileExists = false;
                dialog.CheckPathExists = true;
                dialog.RestoreDirectory = true;
                DialogResult result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    try
                    {
                        byte[] capRequest = CreateCapabilityRequest(options);
                        File.WriteAllBytes(dialog.FileName, capRequest);
                        SetStatus(string.Format(strCRSaved, dialog.FileName), false);
                    }
                    catch (Exception exc)
                    {
                        SetStatus(string.Format(strCRSaveError, dialog.FileName), true);
                        errorProvider.SetIconAlignment(btnCRSave, ErrorIconAlignment.MiddleRight);
                        FlxException flxException = exc as FlxException;
                        errorProvider.SetError(btnCRSave, flxException == null ? exc.Message : flxException.ToString());
                    }
                }
            }
        }

        private ICapabilityRequestOptions CreateCROptions()
        {
            long count = 0;
            ICapabilityRequestOptions options = LicenseManager.CreateCapabilityRequestOptions(
                cbxTSInstance.Enabled ? (LicenseServerInstance)cbxTSInstance.SelectedItem : LicenseServerInstance.Unknown);
            bool isLocal = cbxServerType.Text.Equals(strLocal);
            bool isCloud = cbxServerType.Text.Equals(strCloud);

            if (isLocal)
            {
                if (ckbOTA.Enabled && ckbOTA.Checked)
                {
                    options.OneTimeActivation = true;
                }
                else
                {
                    if (cbxBorrowGranularity.Enabled && txtBorrowInterval.Enabled)
                    {
                        switch (cbxBorrowGranularity.Text)
                        {
                            case strDays:
                                break;
                            case strHours:
                                options.BorrowGranularity = ExpirationValidationIntervalEnum.FLX_EVI_HOUR;
                                break;
                            case strMinutes:
                                options.BorrowGranularity = ExpirationValidationIntervalEnum.FLX_EVI_MINUTE;
                                break;
                            case strSeconds:
                                options.BorrowGranularity = ExpirationValidationIntervalEnum.FLX_EVI_SECOND;
                                break;
                        }
                        if (txtBorrowInterval.Text != null && !string.IsNullOrEmpty(txtBorrowInterval.Text.Trim()))
                        {
                            ulong uInterval;
                            GetCount(txtBorrowInterval.Text.Trim(), out uInterval);
                            options.BorrowInterval = (uint)uInterval;
                        }
                    }
                }
            }

            switch (cbxOperation.Text)
            {
                case strReport:
                    options.Operation = CapabilityRequestOperation.Report;
                    break;
                case strUndo:
                    options.ForceResponse = ckbForceResponse.Checked;
                    options.Operation = CapabilityRequestOperation.Undo;
                    break;
                case strPreview:
                    options.ForceResponse = ckbForceResponse.Checked;
                    options.Operation = CapabilityRequestOperation.Preview;
                    if (ckbRequestAll.Enabled && ckbRequestAll.Checked)
                    {
                        options.RequestAllFeatures = true;
                    }
                    break;
                default:
                    options.ForceResponse = ckbForceResponse.Checked;
                    options.Incremental = ckbIncremental.Checked;
                    break;
            }

            if (!string.IsNullOrEmpty(txtCorrelationID.Text) && !IsSet(options.RequestAllFeatures))
            {
                options.CorrelationId = txtCorrelationID.Text;
            }
            if (!string.IsNullOrEmpty(txtAcquisitionID.Text))
            {
                options.AcquisitionId = txtAcquisitionID.Text;
            }
            if (!string.IsNullOrEmpty(txtEnterpriseID.Text))
            {
                options.EnterpriseId = txtEnterpriseID.Text;
            }
            if (!string.IsNullOrEmpty(txtRequestorID.Text))
            {
                options.RequestorId = txtRequestorID.Text;
            }
            count = 0;
            if (tabCapabilityRequest.TabPages.Contains(pageCR_BO))
            {
                IRightsIdOptions rightsOptions = LicenseManager.CreateRightsIdOptions();
                foreach (ListViewItem item in lvBORights.Items)
                {
                    GetCount(item.SubItems[1].Text.Trim(), out count);
                    rightsOptions.PartialFulfillment = (item.SubItems[2].Text[0] == 'Y');
                    options.AddRightsId(new RightsIdData(item.SubItems[0].Text, count, rightsOptions));
                }
            }
            if (tabCapabilityRequest.TabPages.Contains(pageCR_LLS))
            {
                IDesiredFeatureOptions featureOptions = LicenseManager.CreateDesiredFeatureOptions();
                foreach (ListViewItem item in lvLLSFeatures.Items)
                {
                    GetCount(item.SubItems[2].Text.Trim(), out count);
                    featureOptions.PartialFulfillment = (item.SubItems[3].Text[0] == 'Y');
                    options.AddDesiredFeature(new FeatureData(item.SubItems[0].Text, item.SubItems[1].Text, count, featureOptions));
                }
            }
            if (lvVendorDictionary.Items.Count > 0)
            {
                foreach (ListViewItem vdItem in lvVendorDictionary.Items)
                {
                    if (vdItem.SubItems[0].Text == strInteger)
                    {
                        long vdValue;
                        GetCount(vdItem.SubItems[2].Text, out vdValue);
                        options.AddVendorDictionaryItem(vdItem.SubItems[1].Text, (int)vdValue);
                    }
                    else
                    {
                        options.AddVendorDictionaryItem(vdItem.SubItems[1].Text, vdItem.SubItems[2].Text);
                    }
                }
            }
            if (lvFeatureSelectors.Items.Count > 0)
            {
                foreach (ListViewItem fsItem in lvFeatureSelectors.Items)
                {
                    options.AddFeatureSelectorItem(fsItem.SubItems[1].Text, fsItem.SubItems[2].Text);
                }
            }
            if (lvAuxHostIds.Items.Count > 0)
            {
                foreach (ListViewItem auxHostIdItem in lvAuxHostIds.Items)
                {
                    if (hostIdTypesByText.ContainsKey(auxHostIdItem.SubItems[0].Text))
                    {
                        options.AddAuxiliaryHostId(hostIdTypesByText[auxHostIdItem.SubItems[0].Text], auxHostIdItem.SubItems[1].Text);
                    } 
                }
            }

            return options;
        }

        private byte[] CreateCapabilityRequest(ICapabilityRequestOptions options)
        {
            return LicenseManager.CreateCapabilityRequest(options).ToArray();
        }

        private void CRSendToServer(object parameters)
        {
            DotNetDemoSendParameters sendParameters = null;
            try
            {
                sendParameters = parameters as DotNetDemoSendParameters;
                if (sendParameters == null)
                {
                    throw new InvalidParameterException(strInternalError);
                }
                SendBinaryMessage(sendParameters);
            }
            catch (Exception exc)
            {
                if (sendParameters == null)
                {
                    sendParameters = new DotNetDemoSendParameters(null, null, LicenseServerInstance.Unknown);
                }
                sendParameters.exception = exc;
            }
            finally
            {
                this.BeginInvoke(new SendCompleteDelegate(CRSendComplete), sendParameters);
            }
        }

        private void CRSendComplete(DotNetDemoSendParameters sendParameters)
        {
            sendingThread.Join(10000);
            sendingThread = null;
            bool errorResponse = false;
            Cursor.Current = Cursors.Default;
            if (sendParameters.exception != null)
            {
                CRServerException(sendParameters.exception, strCRSendError);
                return;
            }

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                if (sendParameters.response != null && sendParameters.response.Length > 0 && 
                    txtRespFile.Text != null && !string.IsNullOrEmpty(txtRespFile.Text.Trim()))
                {
                    File.WriteAllBytes(txtRespFile.Text.Trim(), sendParameters.response);
                }
                if (sendParameters.response == null || sendParameters.response.Length <= 0)
                {
                    SetStatus(strCRSendOK + strBlank + strCRSendNoResp, ckbForceResponse.Checked ? true : false);
                }
                else
                {
                    ICapabilityResponse response = null;
                    IFeatureCollection features = null;
                    if (ckbProcessResponse.Checked)
                    {
                        try
                        {
                            MessageTypeEnum msgType = LicenseManager.MessageType(sendParameters.response);
                            if (msgType == MessageTypeEnum.FLX_MESSAGE_TYPE_ERROR)
                            {
                                errorResponse = true;
                            }
                            response = LicenseManager.ProcessCapabilityResponse(sendParameters.response, sendParameters.serverInstance);
                        }
                        catch (FlxException exc)
                        {
                            if (errorResponse)
                            {
                                using (ErrorStatusDetails dialog = new ErrorStatusDetails())
                                {
                                    dialog.Icon = this.Icon;
                                    dialog.StatusException = exc;
                                    dialog.StartPosition = FormStartPosition.CenterParent;
                                    dialog.ShowDialog();
                                }
                            }
                            throw;
                        }
                        finally
                        {
                            // Processing of the capability response can change acquired license status (e.g. stale).
                            RefreshAcquiredLicenses();
                        }
                        if (response.Type == MessageTypeEnum.FLX_MESSAGE_TYPE_BUFFER_CAPABILITY_RESPONSE)
                        {
                            RefreshFeatures();
                            SetLicensesTabs();
                        }
                        else
                        {
                            foreach (CheckBox checkBox in ckbTSInstances)
                            {
                                if ((LicenseServerInstance)checkBox.Tag == sendParameters.serverInstance)
                                {
                                    if (!checkBox.Checked)
                                    {
                                        checkBox.Checked = true;
                                    }
                                    else
                                    {
                                        ckbTrustedStorageInstance_CheckedChanged(checkBox, new EventArgs());
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        response = LicenseManager.GetResponseDetails(sendParameters.response);
                    }
                    if (response != null)
                    {
                        if (response.IsPreview)
                        {
                            if (lvCRResponse.Columns.Count == 6)
                            {
                                colRespFeature.Width -= 30;
                                colRespVersion.Width -= 30;
                                colRespCount.Width -= 10;
                                crRespMaxCountHeader.Width = 70;
                                lvCRResponse.Columns.Insert(iCRRespMCHeaderIndex, crRespMaxCountHeader);
                            }
                        }
                        else if (lvCRResponse.Columns.Count == 7)
                        {
                            lvCRResponse.Columns.RemoveAt(iCRRespMCHeaderIndex);
                            colRespFeature.Width += 30;
                            colRespVersion.Width += 30;
                            colRespCount.Width += 10;
                        }
                        features = response.FeatureCollection;
                        if (features != null && features.Count > 0)
                        {

                            InsertFeaturesToListView(LicenseSourceType.FLX_LICENSE_SOURCE_TRUSTED_STORAGE, features, lvCRResponse, true);
                        }
                        btnCRResponseDetails.Tag = response;
                        btnCRResponseDetails.Enabled = true;
                        SetStatus(strCRSendOK, false);
                        tabCapabilityRequest.SelectedTab = pageCR_Resp;
                    }
                    else
                    {   // Really shouldn't hit this
                        SetStatus(strCRSendOK + strBlank + strCRSendRespData, false);
                    }
                }
            }
            catch (Exception exc)
            {
                CRServerException(exc, strCRProcessRespError);
            }
            finally
            {
                btnCRSend.Enabled = true;
                Cursor.Current = Cursors.Default;
                toolStripProgressBar.Visible = toolStripProgressBar.Enabled = false;
            }
        }

        private void CRServerException(Exception exc, string desc)
        {
            btnCRSend.Enabled = true;
            SetStatus(desc, true);
            toolStripProgressBar.Visible = toolStripProgressBar.Enabled = false;
            errorProvider.SetIconAlignment(btnCRSend, ErrorIconAlignment.MiddleRight);
            FlxException flxException = exc as FlxException;
            errorProvider.SetError(btnCRSend, flxException == null ? exc.Message : flxException.ToString());
        }

        private void btnCRResponseDetails_Click(object sender, EventArgs e)
        {
            ICapabilityResponse response = btnCRResponseDetails.Tag as ICapabilityResponse;
            if (response == null)
            {
                btnCRResponseDetails.Enabled = false;
                return;
            }

            using (ResponseDetails dialog = new ResponseDetails())
            {
                dialog.Icon = this.Icon;
                dialog.Response = response;
                dialog.StartPosition = FormStartPosition.CenterParent;
                dialog.ShowDialog();
            }
        }

        #endregion

        #region Information message

        private void ckbIMAddExisting_CheckedChanged(object sender, EventArgs e)
        {
            ResetError();
            pnlFeatureUsage.Enabled = !ckbIMAddExisting.Checked;
        }

        private void txtIMCount_TextChanged(object sender, EventArgs e)
        {
            ResetError();
            if (txtIMCount.Text != null && !string.IsNullOrEmpty(txtIMCount.Text.Trim()))
            {
                ulong count;
                if (!GetCount(txtIMCount.Text.Trim(), out count) || count == 0)
                {
                    errorProvider.SetIconAlignment(txtIMCount, ErrorIconAlignment.MiddleRight);
                    errorProvider.SetError(txtIMCount, strInvalidUsageCount);
                }
            }
        }

        private void lvIMFeatures_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnIMRemove.Enabled = lvIMFeatures.SelectedIndices.Count > 0;
        }

        private void btnIMAdd_Click(object sender, EventArgs e)
        {
            ResetError();
            if (txtIMFeature.Text == null || string.IsNullOrEmpty(txtIMFeature.Text.Trim()))
            {
                errorProvider.SetIconAlignment(txtIMFeature, ErrorIconAlignment.MiddleRight);
                errorProvider.SetError(txtIMFeature, strMissingUsageName);
            }
            else if (txtIMVersion.Text == null || string.IsNullOrEmpty(txtIMVersion.Text.Trim()))
            {
                errorProvider.SetIconAlignment(txtIMVersion, ErrorIconAlignment.MiddleRight);
                errorProvider.SetError(txtIMVersion, strMissingUsageVersion);
            }
            else if (txtIMCount.Text == null || string.IsNullOrEmpty(txtIMCount.Text.Trim()))
            {
                errorProvider.SetIconAlignment(txtIMCount, ErrorIconAlignment.MiddleRight);
                errorProvider.SetError(txtIMCount, strMissingUsageCount);
            }
            else
            {
                DateTime? dt = dtIMExpiration.Value;
                foreach (ListViewItem item in lvIMFeatures.Items)
                {
                    if (item.SubItems[0].Text.Equals(txtIMFeature.Text.Trim(), StringComparison.OrdinalIgnoreCase) &&
                        item.SubItems[1].Text.Equals(txtIMVersion.Text.Trim(), StringComparison.OrdinalIgnoreCase))
                    {
                        lvIMFeatures.Items.Remove(item);
                        break;
                    }
                }
                ListViewItem newIMItem = new ListViewItem(new string[] {txtIMFeature.Text.Trim(), txtIMVersion.Text.Trim(), txtIMCount.Text, dt.ToString()});
                newIMItem.Tag = dt;
                lvIMFeatures.Items.Add(newIMItem);
                UpdateConfigUsage();
            }
        }

        private void btnIMRemove_Click(object sender, EventArgs e)
        {
            ResetError();
            lvIMFeatures.BeginUpdate();
            foreach (ListViewItem item in lvIMFeatures.SelectedItems)
            {
                lvIMFeatures.Items.Remove(item);
            }
            lvIMFeatures.EndUpdate();
            UpdateConfigUsage();
        }

        private void UpdateConfigUsage()
        {
            currentConfiguration.Usage.Clear();
            foreach (ListViewItem item in lvIMFeatures.Items)
            {
                currentConfiguration.Usage.Add(new UsageInfo(item.SubItems[0].Text, item.SubItems[1].Text, item.SubItems[2].Text, (DateTime?)item.Tag));
            }
        }

        private void btnIMSend_Click(object sender, EventArgs e)
        {
            ResetError();
            Uri serverUri = null;
            try
            {
                serverUri = new Uri(txtServerURL.Text);
                if (!serverUri.Scheme.StartsWith(strHTTP, StringComparison.OrdinalIgnoreCase))
                {
                    errorProvider.SetIconAlignment(lblServerURL, ErrorIconAlignment.MiddleLeft);
                    errorProvider.SetError(lblServerURL,
                        String.Format("{0} {1} '{2}'.", strInvalidServerURL, strUnsupportedScheme, serverUri.Scheme));
                    return;
                }
            }
            catch (Exception uriException)
            {
                errorProvider.SetIconAlignment(txtServerURL, ErrorIconAlignment.MiddleLeft);
                errorProvider.SetError(txtServerURL, strInvalidServerURL + uriException.Message);
                return;
            }
            IInformationMessageOptions options = CreateIMOptions();
            try
            {
                btnIMSend.Enabled = false;
                byte[] IMRequest = LicenseManager.CreateInformationMessage(options).ToArray();
                SetStatus(strSending, false);
                toolStripProgressBar.Visible = toolStripProgressBar.Enabled = true;
                Cursor.Current = Cursors.WaitCursor;
                sendingThread = new Thread(this.IMSendToServer);
                sendingThread.Start(new DotNetDemoSendParameters(serverUri.ToString(), IMRequest, options.ServerInstance)); 
            }
            catch (Exception exc)
            {
                IMSendException(exc);
            }
        }

        private void btnIMSave_Click(object sender, EventArgs e)
        {
            IInformationMessageOptions options = CreateIMOptions();
            using (SaveFileDialog dialog = new SaveFileDialog())
            {
                dialog.AddExtension = false;
                dialog.Filter = strBinaryFilter;
                dialog.Title = strSaveIMTitle;
                dialog.RestoreDirectory = true;
                DialogResult result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    try
                    {
                        byte[] IMRequest = LicenseManager.CreateInformationMessage(options).ToArray();
                        File.WriteAllBytes(dialog.FileName, IMRequest);
                        SetStatus(string.Format(strIMSaved, dialog.FileName), false);
                    }
                    catch (Exception exc)
                    {
                        SetStatus(string.Format(strIMSaveError, dialog.FileName), true);
                        errorProvider.SetIconAlignment(btnIMSave, ErrorIconAlignment.MiddleRight);
                        FlxException flxException = exc as FlxException;
                        errorProvider.SetError(btnIMSave, flxException == null ? exc.Message : flxException.ToString());
                    }
                }
            }
        }

        private IInformationMessageOptions CreateIMOptions()
        {
            IInformationMessageOptions options = LicenseManager.CreateInformationMessageOptions();
            if (ckbIMAddExisting.Checked)
            {
                options.AddExistingFeatures = true;
            }
            else
            {
                DateTime? expiration;
                ulong count;
                foreach (ListViewItem item in lvIMFeatures.Items)
                {
                    expiration = item.Tag as DateTime?;
                    GetCount(item.SubItems[2].Text.Trim(), out count);
                    options.AddFeatureUsage(item.SubItems[0].Text.Trim(),
                                            item.SubItems[1].Text.Trim(),
                                            (int)count, expiration);
                }
            }
            return options;
        }

        private void IMSendToServer(object parameters)
        {
            DotNetDemoSendParameters sendParameters = null;
            try
            {
                sendParameters = parameters as DotNetDemoSendParameters;
                if (sendParameters == null)
                {
                    throw new InvalidParameterException(strInternalError);
                }
                SendBinaryMessage(sendParameters);
            }
            catch (Exception exc)
            {
                if (sendParameters == null)
                {
                    sendParameters = new DotNetDemoSendParameters(null, null, LicenseServerInstance.Unknown);
                }
                sendParameters.exception = exc;
            }
            finally
            {
                this.BeginInvoke(new SendCompleteDelegate(IMSendComplete), sendParameters);
            }
        }

        private void IMSendComplete(DotNetDemoSendParameters sendParameters)
        {
            sendingThread.Join(10000);
            sendingThread = null;
            toolStripProgressBar.Visible = toolStripProgressBar.Enabled = false;
            Cursor.Current = Cursors.Default;
            if (sendParameters.exception != null)
            {
                IMSendException(sendParameters.exception);
            }
            else
            {
                btnIMSend.Enabled = true;
                SetStatus(strIMSendOK, false);
            }
        }

        private void IMSendException(Exception exc)
        {
            btnIMSend.Enabled = true;
            SetStatus(strIMSendError, true);
            errorProvider.SetIconAlignment(btnIMSend, ErrorIconAlignment.MiddleRight);
            FlxException flxException = exc as FlxException;
            errorProvider.SetError(btnIMSend, flxException == null ? exc.Message : flxException.ToString());
        }

        #endregion

        #endregion

        #region Short Code handling

        #region Short Code templates

        private void btnBrowseTemplateFile_Click(object sender, EventArgs e)
        {
            ResetError();
            cbxTemplateFile.Text = string.Empty;
            using (OpenFileDialog dialog = new OpenFileDialog())
            {
                dialog.AddExtension = false;
                dialog.Filter = strBinaryFilter;
                dialog.Title = strOpenTemplateTitle;
                dialog.Multiselect = true;
                if (!string.IsNullOrEmpty(strLastTemplateFilePath))
                {
                    dialog.InitialDirectory = strLastTemplateFilePath;
                }
                else if (cbxTemplateFile.Items.Count >= 1)
                {
                    dialog.InitialDirectory = cbxTemplateFile.Items[0].ToString();
                }
                DialogResult result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    strLastTemplateFilePath = Path.GetDirectoryName(dialog.FileNames[0]);
                    if (dialog.FileNames.Length > 1)
                    {
                        int goodFiles = 0;
                        int badFiles = 0;
                        string excMessage = string.Empty;

                        foreach (string fileName in dialog.FileNames)
                        {
                            try
                            {
                                loadTemplateFile(fileName);
                                UpdateTemplateFiles(fileName);
                                goodFiles++;
                            }
                            catch (Exception exc)
                            {
                                badFiles++;
                                if (!string.IsNullOrEmpty(excMessage))
                                {
                                    excMessage += Environment.NewLine;
                                }
                                FlxException flxException = exc as FlxException;
                                excMessage += flxException == null ? exc.Message : flxException.ToString();
                            }
                        }
                        if (badFiles > 0)
                        {
                            SetStatus(excMessage, true);
                        }
                        if (goodFiles > 0)
                        {
                            lblTemplateLoadComplete.Text = string.Format(strTemplatesLoaded, goodFiles, badFiles);
                            lblTemplateLoadComplete.Visible = true;
                            RefreshAllTemplates();
                        }
                    }
                    else
                    {
                        cbxTemplateFile.Text = dialog.FileNames[0];
                    }
                }
            }
        }

        private void cbxTemplateFile_TextChanged(object sender, EventArgs e)
        {
            btnInspectTemplateFile.Enabled = btnLoadTemplateFile.Enabled = (cbxTemplateFile.Text != null && !string.IsNullOrEmpty(cbxTemplateFile.Text.Trim()));
            lblTemplateLoadComplete.Visible = false;
            ResetError();
        }

        private void btnInspectTemplateFile_Click(object sender, EventArgs e)
        {
            byte[] fileContent = null;
            try
            {
                fileContent = File.ReadAllBytes(cbxTemplateFile.Text);
                if (fileContent.Length == 0)
                {
                    btnInspectTemplateFile.Enabled = btnLoadTemplateFile.Enabled = false;
                    errorProvider.SetIconAlignment(cbxTemplateFile, ErrorIconAlignment.MiddleLeft);
                    errorProvider.SetError(cbxTemplateFile, strEmptyTemplateFile);
                    SetStatus(strEmptyTemplateFile, true);
                    return;
                }

                ILicenseTemplate licenseTemplate = LicenseManager.CreateLicenseTemplate(fileContent);
                IFeatureCollection features = licenseTemplate.FeatureCollection;
                ReadOnlyDictionary vendorDictionary = licenseTemplate.VendorDictionary;

                if (features == null || features.Count <= 0)
                {
                    errorProvider.SetIconAlignment(cbxTemplateFile, ErrorIconAlignment.MiddleLeft);
                    errorProvider.SetError(cbxTemplateFile, strEmptyTemplateFile2);
                    SetStatus(strEmptyTemplateFile2, true);
                }
                else
                {
                    using (TemplateView dialog = new TemplateView())
                    {
                        dialog.Icon = this.Icon;
                        string filename = Path.GetFileName(cbxTemplateFile.Text);
                        dialog.Text = strTemplateDialogTitle + filename ?? string.Empty;
                        dialog.LicenseTemplate = licenseTemplate;
                        dialog.StartPosition = FormStartPosition.CenterParent;
                        dialog.ShowDialog();
                    }
                }
            }
            catch (Exception exc)
            {
                errorProvider.SetIconAlignment(cbxTemplateFile, ErrorIconAlignment.MiddleLeft);
                FlxException flxException = exc as FlxException;
                string excMessage = flxException == null ? exc.Message : flxException.ToString();
                errorProvider.SetError(cbxTemplateFile, excMessage);
                SetStatus(excMessage, true);
            }
        }

        private void btnLoadTemplateFile_Click(object sender, EventArgs e)
        {
            if (cbxTemplateFile.Text == null || string.IsNullOrEmpty(cbxTemplateFile.Text.Trim()))
            {
                btnInspectTemplateFile.Enabled = btnLoadTemplateFile.Enabled = false;
                return;
            }

            try
            {
                loadTemplateFile(cbxTemplateFile.Text);
                lblTemplateLoadComplete.Text = string.Format(strTemplateLoaded, cbxTemplateFile.Text);
                lblTemplateLoadComplete.Visible = true;
                UpdateTemplateFiles(cbxTemplateFile.Text);
                cbxTemplateFile.Text = string.Empty;
                RefreshAllTemplates();
            }
            catch (Exception exc)
            {
                errorProvider.SetIconAlignment(cbxTemplateFile, ErrorIconAlignment.MiddleLeft);
                FlxException flxException = exc as FlxException;
                string excMessage = flxException == null ? exc.Message : flxException.ToString();
                errorProvider.SetError(cbxTemplateFile, excMessage);
                SetStatus(excMessage, true);
            }
        }

        private int loadTemplateFile(string templateFileName)
        {
            byte[] fileContent = File.ReadAllBytes(templateFileName);
            if (fileContent.Length == 0)
            {
                throw new Exception(strEmptyTemplateFile);
            }

            if (shortCodeEnv == null)
            {
                shortCodeEnv = LicenseManager.CreateShortCodeEnvironment();
            }
            int templateID = shortCodeEnv.AddTemplate(templateFileName);
            if (templateFiles.ContainsKey(templateID))
            {
                templateFiles[templateID] = templateFileName;
            }
            else
            {
                templateFiles.Add(templateID, templateFileName);
            }
            return templateID;
        }

        private void UpdateTemplateFiles(string templateFile)
        {
            string file = string.IsNullOrEmpty(templateFile) ? null : templateFile.Trim();
            if (!string.IsNullOrEmpty(file))
            {
                if (!currentConfiguration.TemplateFiles.Contains(file))
                {
                    currentConfiguration.TemplateFiles.Add(file);
                    currentConfiguration.TemplateFiles.Sort();
                    cbxTemplateFile.BeginUpdate();
                    cbxTemplateFile.Items.Clear();
                    cbxTemplateFile.Items.AddRange(currentConfiguration.TemplateFiles.ToArray());
                    cbxTemplateFile.EndUpdate();
                }
                if (!loadedTemplateFiles.Contains(file))
                {
                    loadedTemplateFiles.Add(file);
                    loadedTemplateFiles.Sort();
                }
            }
        }

        private void RefreshAllTemplates()
        {
            int? selectedTemplateId = null;
            ILicenseTemplate selectedTemplate = null;
            lvTemplates.BeginUpdate();
            if (lvTemplates.SelectedItems.Count > 0 &&
                (selectedTemplate = lvTemplates.SelectedItems[0].Tag as ILicenseTemplate) != null)
            {
                selectedTemplateId = selectedTemplate.Id;
            }

            lvTemplates.Items.Clear();
            if (shortCodeEnv != null)
            {
                List<ILicenseTemplate> templates = shortCodeEnv.GetTemplateCollection();

                foreach (ILicenseTemplate template in templates)
                {
                    string machineType;
                    switch (template.MachineType)
                    {
                        case MachineTypeEnum.FLX_MACHINE_TYPE_PHYSICAL:
                            machineType = "Physical";
                            break;
                        case MachineTypeEnum.FLX_MACHINE_TYPE_VIRTUAL:
                            machineType = "Virtual";
                            break;
                        default:
                            machineType = "Unknown";
                            break;
                    }
                    string templateFile = string.Empty;
                    if (templateFiles.ContainsKey(template.Id))
                    {
                        templateFile = templateFiles[template.Id];
                    }
                    ListViewItem newTemplateItem = new ListViewItem(new string[] { template.Id.ToString(), template.Expiration.HasValue ? template.Expiration.ToString() : strPerpetual,
                                                                                   machineType, Path.GetFileName(templateFile) });
                    newTemplateItem.Tag = template;
                    if (!selectedTemplateId.HasValue || selectedTemplateId.Value == template.Id)
                    {
                        selectedTemplateId = template.Id;
                        newTemplateItem.Selected = true;
                    }
                    lvTemplates.Items.Add(newTemplateItem);
                }
            }
            lvTemplates.EndUpdate();
        }

        private void btnClearTemplates_Click(object sender, EventArgs e)
        {
            ResetError();
            ResetStatus();
            lblTemplateLoadComplete.Visible = false;
            if (shortCodeEnv != null)
            {
                shortCodeEnv.Dispose();
                shortCodeEnv = null;
                txtSCResponseID.Text = txtSCResponsePD.Text = string.Empty;
                btnProcessSCResponse.Enabled = false;
            }
            lvTemplates.BeginUpdate();
            lvTemplates.Items.Clear();
            lvTemplates.EndUpdate();
            lvTemplates_SelectedIndexChanged(null, null);
            templateFiles = new SortedDictionary<int, string>();
        }

        private void btnDeleteTemplateLicenses_Click(object sender, EventArgs e)
        {
            ResetError();
            ResetStatus(); 
            if (lvTemplates.SelectedItems.Count > 0)
            {
                string templateId = lvTemplates.SelectedItems[0].SubItems[0].Text;
                int id;
                if (int.TryParse(templateId, out id))
                {
                    try
                    {
                        licensing.Administration.DeleteShortCode(id);
                        if (ckbShortCode.Checked)
                        {
                            RefreshShortCodeFeatures();
                            RefreshAllFeatures();
                            RefreshAcquiredLicenses();
                        }
                        SetStatus(string.Format(strDeleteTemplateLicenses, id), false);
                    }
                    catch (Exception exc)
                    {
                        errorProvider.SetIconAlignment(btnDeleteTemplateLicenses, ErrorIconAlignment.MiddleRight);
                        FlxException flxException = exc as FlxException;
                        errorProvider.SetError(btnDeleteTemplateLicenses, flxException == null ? exc.Message : flxException.ToString());
                    }
                }
            }
        }

        private void lvTemplates_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetError();
            ILicenseTemplate licenseTemplate;
            if (lvTemplates.SelectedItems.Count > 0 &&
                (licenseTemplate = lvTemplates.SelectedItems[0].Tag as ILicenseTemplate) != null)
            {
                InsertFeaturesToListView(LicenseSourceType.FLX_LICENSE_SOURCE_SHORT_CODE, licenseTemplate.FeatureCollection,
                                         lvTemplateFeatures, true);
                InsertVDToListView(licenseTemplate.VendorDictionary, lvTemplateVD);
                btnGenerateSCRequest.Enabled = btnDeleteTemplateLicenses.Enabled = true;
            }
            else
            {
                lvTemplateFeatures.Items.Clear();
                lvTemplateVD.Items.Clear();
                btnGenerateSCRequest.Enabled = btnDeleteTemplateLicenses.Enabled = false;
            }
        }

        private void tabTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetError();
            lblTemplateLoadComplete.Visible = false;
        }

        #endregion

        #region short code request generation

        private void cbxEncodingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentConfiguration.ScRequestEncoding = cbxEncodingType.Text;
            SetCharacters(cbxEncodingType.Text, txtEncodingChars);
            if (cbxEncodingType.Text == strNone)
            {
                txtEncodingSegSize.Text = "0";
                txtEncodingSegSize.Enabled = false;
            }
            else
            {
                txtEncodingSegSize.Enabled = true;
            }
        }

        private void txtEncodingSegSize_TextChanged(object sender, EventArgs e)
        {
            uint segmentSize = 0;
            ResetError();
            if (!GetCount(txtEncodingSegSize.Text, out segmentSize))
            {
                errorProvider.SetIconAlignment(txtEncodingSegSize, ErrorIconAlignment.MiddleRight);
                errorProvider.SetError(txtEncodingSegSize, strInvalidSegmentSize);
            }
            else
            {
                currentConfiguration.ScRequestSegmentSize = segmentSize;
            }
        }

        private void txtSCRequestPD_TextChanged(object sender, EventArgs e)
        {
            ResetError();
            if (!string.IsNullOrEmpty(txtSCRequestPD.Text) && rbSCRequestPDHex.Checked)
            {
                string hexString = txtSCRequestPD.Text.StartsWith("0x") ? txtSCRequestPD.Text.Substring(2) : txtSCRequestPD.Text;
                if (!StringIsHex(hexString))
                {
                    errorProvider.SetIconAlignment(txtSCRequestPD, ErrorIconAlignment.MiddleLeft);
                    errorProvider.SetError(txtSCRequestPD, strInvalidHexString2);
                }
                else if (!txtSCRequestPD.Text.StartsWith("0x"))
                {   // Will redrive this delegate
                    txtSCRequestPD.Text = "0x" + txtSCRequestPD.Text;
                }
                else
                {
                    currentConfiguration.ScRequestPD = txtSCRequestPD.Text;
                }
            }
        }

        private void btnSCRequestPD_Click(object sender, EventArgs e)
        {
            ResetError();
            txtSCRequestPD.Text = string.Empty;
            using (OpenFileDialog dialog = new OpenFileDialog())
            {
                byte[] pdData;
                dialog.AddExtension = false;
                dialog.Filter = strAllFilter;
                dialog.Title = strOpenSCRequestPDTitle;
                if (!string.IsNullOrEmpty(strLastSCRequestPDFilePath))
                {
                    dialog.InitialDirectory = strLastSCRequestPDFilePath;
                }
                DialogResult result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    strLastSCRequestPDFilePath = Path.GetDirectoryName(dialog.FileName);
                    try
                    {
                        pdData = File.ReadAllBytes(dialog.FileName);
                        if (IsASCII(pdData))
                        {
                            rbSCRequestPDChar.Checked = true;
                            txtSCRequestPD.Text = Encoding.UTF8.GetString(pdData);
                        }
                        else
                        {
                            rbSCRequestPDHex.Checked = true;
                            txtSCRequestPD.Text = "0x" + BitConverter.ToString(pdData).Replace("-", "");
                        }
                    }
                    catch (Exception exc)
                    {
                        errorProvider.SetIconAlignment(btnSCRequestPD, ErrorIconAlignment.MiddleLeft);
                        errorProvider.SetError(btnSCRequestPD, exc.Message);
                        return;
                    }
                }
            }
        }

        private void rbSCRequestPDHex_CheckedChanged(object sender, EventArgs e)
        {
            if (rbSCRequestPDHex.Checked)
            {
                txtSCRequestPD_TextChanged(null, null);
            }
        }

        private void btnSCRequestFile_Click(object sender, EventArgs e)
        {
            ResetError();
            using (OpenFileDialog dialog = new OpenFileDialog())
            {
                dialog.AddExtension = true;
                dialog.Filter = (cbxEncodingType.Text == strNone) ? strBinaryFilter : strTextFilter;
                dialog.CheckFileExists = false;
                dialog.CheckPathExists = true;
                dialog.Title = strOpenSCRequestTitle;
                if (!string.IsNullOrEmpty(strLastSCRequestFilePath))
                {
                    dialog.InitialDirectory = strLastSCRequestFilePath;
                }
                DialogResult result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    txtSCRequestFile.Text = dialog.FileName;
                    strLastSCRequestFilePath = Path.GetDirectoryName(dialog.FileName);
                }
            }
        }

        private void btnGenerateSCRequest_Click(object sender, EventArgs e)
        {
            ResetError();
            ILicenseTemplate licenseTemplate = lvTemplates.SelectedItems[0].Tag as ILicenseTemplate;
            ShortCodeEncoding encoding = ShortCodeEncoding.BASE34;
            uint segmentSize = 0;
            if (lvTemplates.SelectedItems == null    || 
                lvTemplates.SelectedItems.Count <= 0 ||
                (licenseTemplate = lvTemplates.SelectedItems[0].Tag as ILicenseTemplate) == null)
            {
                errorProvider.SetIconAlignment(btnGenerateSCRequest, ErrorIconAlignment.MiddleLeft);
                errorProvider.SetError(btnGenerateSCRequest, strSCTemplateReqd);
                return;
            }
            if (!GetCount(txtEncodingSegSize.Text, out segmentSize))
            {
                errorProvider.SetIconAlignment(txtEncodingSegSize, ErrorIconAlignment.MiddleRight);
                errorProvider.SetError(txtEncodingSegSize, strInvalidSegmentSize);
                return;
            }
            else if (!GetSCEncodingInfo(cbxEncodingType.Text, txtEncodingChars, out encoding))
            {
                return;
            }
            else if (encoding == ShortCodeEncoding.USER && string.IsNullOrEmpty(txtSCRequestFile.Text))
            {
                errorProvider.SetIconAlignment(txtSCRequestFile, ErrorIconAlignment.MiddleLeft);
                errorProvider.SetError(txtSCRequestFile, strSCRequestFileReqd);
                return;
            }
            try
            {
                int segSize = (int)segmentSize;
                if (encoding == ShortCodeEncoding.CUSTOM)
                {
                    shortCodeEnv.SetRequestEncoding(txtEncodingChars.Text, segSize);
                }
                else
                {
                    shortCodeEnv.SetRequestEncoding(encoding, segSize);
                }
                shortCodeEnv.RequestPublisherData = null;
                if (!string.IsNullOrEmpty(txtSCRequestPD.Text))
                {
                    if (rbSCRequestPDChar.Checked)
                    {
                        shortCodeEnv.RequestPublisherData = Encoding.Convert(Encoding.Unicode, Encoding.UTF8,
                            Encoding.Unicode.GetBytes(txtSCRequestPD.Text));
                    }
                    else
                    {
                        string hexData = txtSCRequestPD.Text;
                        if (hexData.StartsWith("0x"))
                        {
                            hexData = hexData.Substring(2);
                        }
                        if (hexData.Length % 2 != 0)
                        {
                            errorProvider.SetIconAlignment(txtSCRequestPD, ErrorIconAlignment.MiddleLeft);
                            errorProvider.SetError(txtSCRequestPD, strInvalidHexString1);
                            return;
                        }
                        else if (!StringIsHex(hexData))
                        {
                            errorProvider.SetIconAlignment(txtSCRequestPD, ErrorIconAlignment.MiddleLeft);
                            errorProvider.SetError(txtSCRequestPD, strInvalidHexString2);
                            return;
                        }
                        shortCodeEnv.RequestPublisherData = ConvertHexStringToByteArray(hexData);
                    }
                }
                byte[] request = LicenseManager.GenerateShortCodeRequest(shortCodeEnv, licenseTemplate);
                if (!string.IsNullOrEmpty(txtSCRequestFile.Text))
                {
                    File.WriteAllBytes(txtSCRequestFile.Text, request);
                    SetStatus(string.Format(strSCRequestToFile, txtSCRequestFile.Text), false);
                }
                else
                {
                    string unicodeRequest = Encoding.Unicode.GetString(Encoding.Convert(Encoding.UTF8, Encoding.Unicode, request));
                    using (ShortCodeRequest dialog = new ShortCodeRequest())
                    {
                        dialog.Icon = this.Icon;
                        dialog.SCRequest = unicodeRequest;
                        dialog.SCRequestFilePath = strLastSCRequestFilePath;
                        dialog.StartPosition = FormStartPosition.CenterParent;
                        dialog.ShowDialog();
                        if (dialog.DialogResult == DialogResult.OK)
                        {
                            strLastSCRequestFilePath = dialog.SCRequestFilePath;
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                errorProvider.SetIconAlignment(btnGenerateSCRequest, ErrorIconAlignment.MiddleLeft);
                FlxException flxException = exc as FlxException;
                string strGenerateError = flxException == null ? exc.Message : flxException.ToString();
                errorProvider.SetError(btnGenerateSCRequest, strGenerateError);
                SetStatus(strGenerateError, true);
            }
        }

        #endregion

        #region short code response processing

        private void cbxDecodingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentConfiguration.ScResponseDecoding = cbxDecodingType.Text;
            SetCharacters(cbxDecodingType.Text, txtDecodingChars);
            if (cbxEncodingType.Text == strNone)
            {
                txtDecodingSegSize.Text = "0";
                txtDecodingSegSize.Enabled = false;
            }
            else
            {
                txtDecodingSegSize.Enabled = true;
            }
        }

        private void txtDecodingSegSize_TextChanged(object sender, EventArgs e)
        {
            uint segmentSize = 0;
            ResetError();
            if (!GetCount(txtDecodingSegSize.Text, out segmentSize))
            {
                errorProvider.SetIconAlignment(txtDecodingSegSize, ErrorIconAlignment.MiddleRight);
                errorProvider.SetError(txtDecodingSegSize, strInvalidSegmentSize);
            }
            else
            {
                currentConfiguration.ScResponseSegmentSize = segmentSize;
            }
        }

        private void btnSCResponseFile_Click(object sender, EventArgs e)
        {
            ResetError();
            cbxSCResponseFile.Text = string.Empty;
            using (OpenFileDialog dialog = new OpenFileDialog())
            {
                dialog.AddExtension = false;
                dialog.Filter = (cbxDecodingType.Text == strNone) ? strBinaryFilter : strTextFilter;
                dialog.Title = strOpenSCResponseTitle;
                if (!string.IsNullOrEmpty(strLastSCResponseFilePath))
                {
                    dialog.InitialDirectory = strLastSCResponseFilePath;
                }
                else if (cbxSCResponseFile.Items.Count >= 1)
                {
                    dialog.InitialDirectory = Path.GetDirectoryName(cbxSCResponseFile.Items[0].ToString());
                }
                DialogResult result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    cbxSCResponseFile.Text = dialog.FileName;
                    strLastSCResponseFilePath = Path.GetDirectoryName(dialog.FileName);
                }
            }
        }

        private void cbxSCResponseFile_TextChanged(object sender, EventArgs e)
        {
            ResetError();
        }

        private void btnReadSCResponse_Click(object sender, EventArgs e)
        {
            ResetError();
            ResetStatus();
            txtSCResponseID.Text = string.Empty;
            txtSCResponseID.Tag = null;
            txtSCResponsePD.Text = string.Empty;
            btnSCResponsePD.Enabled = btnProcessSCResponse.Enabled = false;
            btnProcessSCResponse.Enabled = false;
            try
            {
                if (shortCodeEnv == null)
                {
                    shortCodeEnv = LicenseManager.CreateShortCodeEnvironment();
                }
                ShortCodeEncoding decoding = ShortCodeEncoding.BASE34;
                uint segmentSize = 0;
                if (!GetCount(txtDecodingSegSize.Text, out segmentSize))
                {
                    errorProvider.SetIconAlignment(txtDecodingSegSize, ErrorIconAlignment.MiddleRight);
                    errorProvider.SetError(txtDecodingSegSize, strInvalidSegmentSize);
                    return;
                }
                else if (!GetSCEncodingInfo(cbxDecodingType.Text, txtDecodingChars, out decoding))
                {
                    return;
                }
                else if (decoding == ShortCodeEncoding.USER && string.IsNullOrEmpty(cbxSCResponseFile.Text))
                {
                    errorProvider.SetIconAlignment(cbxSCResponseFile, ErrorIconAlignment.MiddleLeft);
                    errorProvider.SetError(cbxSCResponseFile, strSCResponseFileReqd);
                    return;
                }

                int segSize = (int)segmentSize;
                if (decoding == ShortCodeEncoding.CUSTOM)
                {
                    shortCodeEnv.SetResponseDecoding(txtDecodingChars.Text, segSize);
                }
                else
                {
                    shortCodeEnv.SetResponseDecoding(decoding, (decoding == ShortCodeEncoding.USER) ? 0 : segSize);
                }

                if (string.IsNullOrEmpty(cbxSCResponseFile.Text))
                {
                    using (ShortCodeResponse dialog = new ShortCodeResponse())
                    {
                        dialog.Icon = this.Icon;
                        dialog.ShortCodeEnvironment = shortCodeEnv;
                        dialog.SegmentSize = segSize;
                        dialog.StartPosition = FormStartPosition.CenterParent;
                        dialog.ShowDialog();
                        if (dialog.DialogResult != DialogResult.OK)
                        {
                            return;
                        }
                    }
                }
                else
                {
                    byte[] fileBytes = null;
                    string fileString = null;
                    int fileLength = 0;
                    if (decoding == ShortCodeEncoding.USER)
                    {
                        fileBytes = File.ReadAllBytes(cbxSCResponseFile.Text);
                        fileLength = fileBytes.Length;
                    }
                    else
                    {
                        fileString = File.ReadAllText(cbxSCResponseFile.Text);
                        fileLength = fileString.Length;
                    }
                    if (fileLength == 0)
                    {
                        errorProvider.SetIconAlignment(cbxSCResponseFile, ErrorIconAlignment.MiddleLeft);
                        errorProvider.SetError(cbxSCResponseFile, strEmptyResponseFile);
                        SetStatus(strEmptyResponseFile, true);
                        return;
                    }
                    if (segSize == 0 || fileString.Length < segSize)
                    {
                        if (decoding == ShortCodeEncoding.USER)
                        {
                            shortCodeEnv.SetResponse(fileBytes);
                        }
                        else
                        {
                            shortCodeEnv.SetResponse(fileString);
                        }
                    }
                    else
                    {
                        int remainder = fileString.Length;
                        do
                        {
                            shortCodeEnv.AddResponseSegment(fileString.Substring(fileString.Length - remainder,
                                (segSize > remainder) ? remainder : segSize));
                            remainder -= segSize;
                        } while (remainder >= 0);
                    }
                }
                IShortCodeResponseData responseData = shortCodeEnv.GetResponseDetails();
                if (responseData != null)
                {
                    txtSCResponseID.Tag = responseData.GetTemplateId;
                    txtSCResponseID.Text = ((int)txtSCResponseID.Tag).ToString();
                    byte[] responsePD = responseData.GetPublisherData;
                    if (responsePD != null && responsePD.Length > 0)
                    {
                        if (IsASCII(responsePD))
                        {
                            txtSCResponsePD.Text = Encoding.Unicode.GetString(Encoding.Convert(Encoding.UTF8, Encoding.Unicode, responsePD));
                            txtSCResponsePD.Tag = null;
                        }
                        else
                        {
                            txtSCResponsePD.Text = "0x" + BitConverter.ToString(responsePD).Replace("-", "");
                            txtSCResponsePD.Tag = new object();
                        }
                        btnSCResponsePD.Enabled = true;
                    }
                    btnProcessSCResponse.Enabled = true;
                    SetStatus(strSCResponseRead, false);
                }
            }
            catch (Exception exc)
            {
                errorProvider.SetIconAlignment(btnReadSCResponse, ErrorIconAlignment.MiddleLeft);
                FlxException flxException = exc as FlxException;
                string excMessage = flxException == null ? exc.Message : flxException.ToString();
                errorProvider.SetError(btnReadSCResponse, excMessage);
                SetStatus(excMessage, true);
            }
        }

        private void btnSCResponsePD_Click(object sender, EventArgs e)
        {
            ResetError();
            ResetStatus();
            using (OpenFileDialog dialog = new OpenFileDialog())
            {
                dialog.AddExtension = true;
                dialog.Filter = (txtSCResponsePD.Tag == null) ? strTextFilter : strBinaryFilter;
                dialog.CheckFileExists = false;
                dialog.CheckPathExists = true;
                dialog.Title = strSaveSCResponsePDTitle;
                if (!string.IsNullOrEmpty(strLastSCResponsePDFilePath))
                {
                    dialog.InitialDirectory = strLastSCResponsePDFilePath;
                }
                DialogResult result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    strLastSCResponsePDFilePath = Path.GetDirectoryName(dialog.FileName);
                    try
                    {
                        if (txtSCResponsePD.Tag == null)
                        {
                            File.WriteAllText(dialog.FileName, txtSCResponsePD.Text, Encoding.UTF8);
                        }
                        else
                        {
                            File.WriteAllBytes(dialog.FileName, ConvertHexStringToByteArray(txtSCResponsePD.Text.Substring(2)));
                        }
                    }
                    catch (Exception exc)
                    {
                        errorProvider.SetIconAlignment(btnSCResponsePD, ErrorIconAlignment.MiddleLeft);
                        errorProvider.SetError(btnSCResponsePD, exc.Message);
                        SetStatus(exc.Message, true);
                    }
                }
            }
        }

        private void btnProcessSCResponse_Click(object sender, EventArgs e)
        {
            ResetError();
            ResetStatus();
            if (shortCodeEnv != null)
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    foreach (ListViewItem template in lvTemplates.Items)
                    {
                        if (template.SubItems[0].Text == txtSCResponseID.Text)
                        {
                            LicenseManager.ProcessShortCodeResponse(shortCodeEnv);
                            if (!ckbShortCode.Checked)
                            {
                                ckbShortCode.Checked = true;
                            }
                            else
                            {
                                ckbShortCode_CheckedChanged(null, new EventArgs());
                            }
                            SetStatus(strSCResponseProcessed, false);
                            string file = string.IsNullOrEmpty(cbxSCResponseFile.Text) ? null : cbxSCResponseFile.Text.Trim();
                            if (!string.IsNullOrEmpty(file))
                            {
                                if (!currentConfiguration.ScResponseFiles.Contains(file))
                                {
                                    currentConfiguration.ScResponseFiles.Add(file);
                                    currentConfiguration.ScResponseFiles.Sort();
                                    cbxSCResponseFile.BeginUpdate();
                                    cbxSCResponseFile.Items.Clear();
                                    cbxSCResponseFile.Items.AddRange(currentConfiguration.ScResponseFiles.ToArray());
                                    cbxSCResponseFile.EndUpdate();
                                }
                                if (!loadedSCResponseFiles.Contains(file))
                                {
                                    loadedSCResponseFiles.Add(file);
                                    loadedSCResponseFiles.Sort();
                                }
                            }
                            return;
                        }
                    }
                    SetStatus(strSCTemplateNotFound, true);
                }
                catch (Exception exc)
                {
                    errorProvider.SetIconAlignment(btnProcessSCResponse, ErrorIconAlignment.MiddleLeft);
                    FlxException flxException = exc as FlxException;
                    string excMessage = flxException == null ? exc.Message : flxException.ToString();
                    errorProvider.SetError(btnProcessSCResponse, excMessage);
                    SetStatus(excMessage, true);
                }
                finally
                {
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        #endregion

        #endregion

        #region Acquire/Release

        private void cbxFeatureName_TextChanged(object sender, EventArgs e)
        {
            ResetError();
            if (!string.IsNullOrEmpty(cbxFeatureName.Text) && featureVersions.ContainsKey(cbxFeatureName.Text))
            {
                cbxVersion.Items.Clear();
                cbxVersion.Items.AddRange(featureVersions[cbxFeatureName.Text].ToArray());
                if (!string.IsNullOrEmpty(cbxVersion.Text) && !featureVersions[cbxFeatureName.Text].Contains(cbxVersion.Text))
                {
                    cbxVersion.Text = string.Empty;
                }
            }
        }

        private void lvAcquired_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool returnableLicensesSelected = false;
            for (int i = 0; i < lvAcquired.SelectedIndices.Count; i++)
            {
                returnableLicensesSelected = IsReturnable(lvAcquired.Items[lvAcquired.SelectedIndices[i]].Tag);
                if (returnableLicensesSelected)
                {
                    break;
                }
            }
            btnReturn.Enabled = returnableLicensesSelected;
        }

        private void txtCount_TextChanged(object sender, EventArgs e)
        {
            ResetError();
            lblDoSelect.Visible = false;
            if (txtCount.Text != null && !string.IsNullOrEmpty(txtCount.Text.Trim()))
            {
                ulong count;
                if (!GetCount(txtCount.Text.Trim(), out count) || count == 0)
                {
                    errorProvider.SetError(txtCount, strInvalidAcquireCount);
                }
            }
        }

        private void btnAcquire_Click(object sender, EventArgs e)
        {
            ResetError();
            lblDoSelect.Visible = false;
            ulong count;
            if (ValidateLicenseInfo(out count))
            {
                try
                {
                    LicenseManager.Acquire(cbxFeatureName.Text, cbxVersion.Text, (long)count);
                    RefreshAcquiredLicenses();
                    RefreshFeatures();
                }
                catch (Exception exc)
                {
                    errorProvider.SetIconAlignment(btnAcquire, ErrorIconAlignment.MiddleLeft);
                    FlxException flxException = exc as FlxException;
                    string strAcquireError = flxException == null ? exc.Message : flxException.ToString();
                    errorProvider.SetError(btnAcquire, strAcquireError);
                    SetStatus(strAcquireError, true);
                }
            }
        }

        private void btnReturn_Click(object sender, EventArgs e)
        {
            ResetError();
            lblDoSelect.Visible = false;
            try
            {
                ILicense license;
                if (lvAcquired.SelectedItems.Count > 0)
                {
                    foreach (ListViewItem item in lvAcquired.SelectedItems)
                    {
                        if ((license = item.Tag as ILicense) != null && IsReturnable(license))
                        {
                            license.ReturnLicense();
                        }
                    }
                }
                else
                {
                    lblDoSelect.Visible = true;
                }
            }
            catch (Exception exc)
            {
                errorProvider.SetIconAlignment(btnReturn, ErrorIconAlignment.MiddleLeft);
                FlxException flxException = exc as FlxException;
                errorProvider.SetError(btnReturn, flxException == null ? exc.Message : flxException.ToString());
            }
            RefreshAcquiredLicenses();
            RefreshFeatures();
        }

        private void btnReturnAll_Click(object sender, EventArgs e)
        {
            ResetError();
            lblDoSelect.Visible = false;
            ILicense license;
            try
            {
                foreach (ListViewItem item in lvAcquired.Items)
                {
                    if ((license = item.Tag as ILicense) != null && IsReturnable(license))
                    {
                        license.ReturnLicense();
                    }
                }
            }
            catch (Exception exc)
            {
                errorProvider.SetIconAlignment(btnReturnAll, ErrorIconAlignment.MiddleLeft);
                FlxException flxException = exc as FlxException;
                errorProvider.SetError(btnReturnAll, flxException == null ? exc.Message : flxException.ToString());
            }
            RefreshAcquiredLicenses();
            RefreshFeatures();
        }

        private bool ValidateLicenseInfo(out ulong count)
        {
            count = 0;
            if (cbxFeatureName.Text == null || string.IsNullOrEmpty(cbxFeatureName.Text.Trim()))
            {
                errorProvider.SetIconAlignment(cbxFeatureName, ErrorIconAlignment.MiddleRight);
                errorProvider.SetError(cbxFeatureName, strMissingAcquireName);
            }
            else if (cbxVersion.Text == null || string.IsNullOrEmpty(cbxVersion.Text.Trim()))
            {
                errorProvider.SetIconAlignment(cbxVersion, ErrorIconAlignment.MiddleRight);
                errorProvider.SetError(cbxVersion, strMissingAcquireVersion);
            }
            else if (txtCount.Text == null || string.IsNullOrEmpty(txtCount.Text.Trim()))
            {
                errorProvider.SetIconAlignment(txtCount, ErrorIconAlignment.MiddleRight);
                errorProvider.SetError(txtCount, strMissingAcquireCount);
            }
            else
            {
                GetCount(txtCount.Text.Trim(), out count);
                ResetError();
                return true;
            }
            return false;
        }

        private void RefreshAcquiredLicenses()
        {
            lvAcquired.BeginUpdate();
            lvAcquired.Items.Clear();
            ReadOnlyCollection<ILicense> licenseList = LicenseManager.Licenses();
            btnReturnAll.Enabled = false;
            foreach (ILicense license in licenseList)
            {
                ListViewItem item = new ListViewItem(new string[]{GetFeatureType(license), license.Name, license.Version, license.Count.ToString(), 
                                                                  license.IsPerpetual ? strPerpetual : license.Expiration.ToString(),
                                                                  license.VendorString ?? string.Empty});
                if (license.IsStale)
                {
                    item.ImageIndex = 0;
                }
                item.Tag = license;
                item.ToolTipText = license.ToString();
                lvAcquired.Items.Add(item);
                if (!btnReturnAll.Enabled)
                {
                    btnReturnAll.Enabled = IsReturnable(license);
                }
            }
            lvAcquired.EndUpdate();
            lvAcquired_SelectedIndexChanged(lvAcquired, new EventArgs());
        }

        private void lvLicense_DoubleClick(object sender, EventArgs e)
        {
            ListView lv = sender as ListView;
            ILicense license = null;
            if (lv != null && lv.SelectedItems != null && lv.SelectedItems.Count > 0 &&
                (license = lv.SelectedItems[0].Tag as ILicense) != null)
            {
                using (FeatureProperties dialog = new FeatureProperties())
                {
                    Color backColor = Color.Gainsboro;
                    dialog.Icon = this.Icon;
                    dialog.Text = "License Properties";
                    dialog.License = license;
                    dialog.TextBackColor = backColor;
                    dialog.StartPosition = FormStartPosition.CenterParent;
                    dialog.ShowDialog();
                }
            }
        }

        #endregion

        #region Utility methods

        private void errorProvider_reset(object sender, EventArgs e)
        {
            ResetError();
        }

        private void ResetError()
        {
            errorProvider.Clear();
            if (toolStripStatusLabel.Image != null)
            {
                ResetStatus();
            }
        }

        private void copyItem_Click(object sender, EventArgs e)
        {
            Clipboard.SetDataObject(toolStripStatusLabel.Text, true);
        }

        private void ResetStatus()
        {
            SetStatus(string.Empty, false);
        }

        private void SetStatus(string message, bool error)
        {
            toolStripStatusLabel.Text = message;
            toolStripStatusLabel.ToolTipText = message;
            if (error)
            {
                toolStripStatusLabel.ForeColor = SystemInformation.HighContrast ? Color.LightPink : Color.Red;
                toolStripStatusLabel.Image = SystemIcons.Error.ToBitmap();
            }
            else
            {
                toolStripStatusLabel.ForeColor = SystemColors.ControlText;
                toolStripStatusLabel.Image = null;
            }
        }
 
        private bool SaveConfigurations(string successMsg, string failureMsg)
        {
            try
            {
                Configuration.SerializeToXML(new List<Configuration>(configurations.Values), strConfigurationFile);
                SetStatus(string.Format(successMsg, currentConfiguration.Name), false);
                return true;
            }
            catch (Exception exc)
            {
                FlxException flxException = exc as FlxException;
                SetStatus(string.Format(failureMsg, currentConfiguration.Name) + strBlank + flxException == null ? exc.Message : flxException.ToString(), true);
                return false;
            }
        }

        private void SetFormTitle()
        {
            string title;
            if (currentConfiguration == null || string.IsNullOrEmpty(currentConfiguration.FormTitle))
            {
                title = strDefaultTitle;
            }
            else
            {
                title = currentConfiguration.FormTitle;
            }
            this.Text = title;
        }

        private void InsertFeaturesToListView(LicenseSourceType lsType, IFeatureCollection features, ListView listView, bool replace)
        {
            listView.BeginUpdate();
            int colCount = listView.Columns.Count;
            if (replace)
            {
                listView.Items.Clear();
            }

            if (features != null)
            {
                ListViewItem listViewItem;
                foreach (IFeature feature in features)
                {
                    switch (colCount)
                    {
                        case 5:
                            listViewItem = new ListViewItem(new string[] { 
                                    GetFeatureType(feature),
                                    feature.Name,
                                    feature.Version,
                                    feature.IsUncounted ? strUncounted : feature.Count.ToString(),
                                    feature.IsPerpetual ? strPerpetual : feature.Expiration.ToString()});
                            break;
                        case 6:
                            listViewItem = new ListViewItem(new string[] { 
                                    GetFeatureType(feature),
                                    feature.Name,
                                    feature.Version,
                                    feature.IsUncounted ? strUncounted : feature.Count.ToString(),
                                    feature.IsPerpetual ? strPerpetual : feature.Expiration.ToString(),
                                    feature.VendorString ?? string.Empty});
                            break;
                        case 7:
                            listViewItem = new ListViewItem(new string[] { 
                                    GetFeatureType(feature),
                                    feature.Name,
                                    feature.Version,
                                    feature.IsUncounted ? strUncounted : feature.Count.ToString(),
                                    feature.MaxCount == 0x7fffffff ? strUncounted : feature.MaxCount.ToString(),
                                    feature.IsPerpetual ? strPerpetual : feature.Expiration.ToString(),
                                    feature.VendorString ?? string.Empty});
                            break;
                        default:
                            listViewItem = null;
                            break;
                    }
                    if (listViewItem != null)
                    {
                        if (listView.OwnerDraw)
                        {
                            listViewItem.SubItems[0].Tag = feature.ToString();
                        }
                        else
                        {
                            listViewItem.ToolTipText = feature.ToString();
                        }
                        listViewItem.Tag = new DotNetDemoFeatureInfo(lsType, feature);
                        listView.Items.Add(listViewItem);
                    }
                }
            }
            listView.EndUpdate();
        }

        private string GetFeatureType(object target)
        {
            string type = string.Empty;
            IExtendedLicensingAttributes typeObject = target as IExtendedLicensingAttributes;
            if (target != null)
            {
                if (typeObject is IFeature && ((IFeature)typeObject).IsPreview)
                {
                    type += "P";
                }
                type += typeObject.IsMetered ? "M" : "C";
                if (typeObject.IsMetered)
                {
                    if (typeObject.IsMeteredReusable)
                    {
                        type += "R";
                    }
                    if (typeObject.MeteredUndoInterval.HasValue && typeObject.MeteredUndoInterval.Value.Ticks > 0)
                    {
                        type += "U";
                    }
                }
            }
            return type;
        }

        private bool IsReturnable(object target)
        {
            bool isReturnable = false;
            IExtendedLicensingAttributes typeObject = target as IExtendedLicensingAttributes;
            if (target != null)
            {
                if (!typeObject.IsMetered || typeObject.IsMeteredReusable)
                {
                    isReturnable = true;
                }
                else if (typeObject.MeteredUndoInterval.HasValue && typeObject.MeteredUndoInterval.Value.Ticks > 0 &&
                         typeObject.UndoIntervalRemaining.HasValue & typeObject.UndoIntervalRemaining.Value.Ticks > 0)
                {
                    isReturnable = true;
                }
            }
            return isReturnable;
        }

        private void lvFeature_DoubleClick(object sender, EventArgs e)
        {
            ListView lv = sender as ListView;
            DotNetDemoFeatureInfo featureInfo = null;
            if (lv != null && lv.SelectedItems != null && lv.SelectedItems.Count > 0 &&
                (featureInfo = lv.SelectedItems[0].Tag as DotNetDemoFeatureInfo) != null)
            {
                using (FeatureProperties dialog = new FeatureProperties())
                {
                    Color backColor = Color.Gainsboro;
                    switch (featureInfo.lsType)
                    {
                        case LicenseSourceType.FLX_LICENSE_SOURCE_TRUSTED_STORAGE:
                            backColor = Color.LightSteelBlue; break;
                        case LicenseSourceType.FLX_LICENSE_SOURCE_TRIALS:
                            backColor = Color.LightBlue; break;
                        case LicenseSourceType.FLX_LICENSE_SOURCE_SHORT_CODE:
                            backColor = Color.PaleTurquoise; break;
                        case LicenseSourceType.FLX_LICENSE_SOURCE_SERVED_BUFFER:
                            backColor = Color.NavajoWhite; break;
                        case LicenseSourceType.FLX_LICENSE_SOURCE_BUFFER:
                        case LicenseSourceType.FLX_LICENSE_SOURCE_CERTIFICATE:
                            backColor = Color.Silver; break;
                    }
                    dialog.Icon = this.Icon;
                    dialog.Text = "Feature Properties";
                    dialog.Feature = featureInfo.feature;
                    dialog.TextBackColor = backColor;
                    dialog.StartPosition = FormStartPosition.CenterParent;
                    dialog.ShowDialog();
                }
            }
        }

        private void InsertVDToListView(ReadOnlyDictionary vendorDictionary, ListView listView)
        {
            listView.BeginUpdate();
            listView.Items.Clear();
            if (vendorDictionary != null)
            {
                string itemType;
                foreach (KeyValuePair<string, object> kvp in vendorDictionary)
                {
                    if (kvp.Value is string)
                    {
                        itemType = "String";
                    }
                    else if (kvp.Value is int)
                    {
                        itemType = "Integer";
                    }
                    else
                    {
                        itemType = kvp.Value.GetType().ToString();
                    }
                    listView.Items.Add(new ListViewItem(new string[] { itemType, kvp.Key.ToString(), kvp.Value.ToString() }));
                }
            }
            listView.EndUpdate();
        }

        void SetCharacters(string codeType, TextBox tb)
        {
            tb.ReadOnly = true;
            switch (codeType)
            {
                case strBase10:
                    tb.Text = GetEnumDescription(ShortCodeEncoding.BASE10); break;
                case strBase16:
                    tb.Text = GetEnumDescription(ShortCodeEncoding.BASE16); break;
                case strBase34:
                    tb.Text = GetEnumDescription(ShortCodeEncoding.BASE34); break;
                case strBase36:
                    tb.Text = GetEnumDescription(ShortCodeEncoding.BASE36); break;
                case strCustom:
                    tb.ReadOnly = false;
                    tb.Text = string.Empty;
                    if (tb.CanFocus)
                    {
                        tb.Focus();
                    }
                    break;
                default:
                    tb.Text = string.Empty; break;
            }
        }

        bool GetSCEncodingInfo(string codeType, TextBox tb, out ShortCodeEncoding encoding)
        {
            switch (codeType)
            {
                case strBase10:
                    encoding = ShortCodeEncoding.BASE10; break;
                case strBase16:
                    encoding = ShortCodeEncoding.BASE16; break;
                case strBase34:
                    encoding = ShortCodeEncoding.BASE34; break;
                case strBase36:
                    encoding = ShortCodeEncoding.BASE36; break;
                case strCustom:
                    encoding = ShortCodeEncoding.CUSTOM;
                    if (string.IsNullOrEmpty(tb.Text))
                    {
                        errorProvider.SetIconAlignment(tb, ErrorIconAlignment.MiddleLeft);
                        errorProvider.SetError(tb, strNoCustomChars);
                        return false;
                    }
                    break;
                default:
                    encoding = ShortCodeEncoding.USER; break;
            }
            return true;
        }

        internal static bool GetCount(string strCount, out uint count)
        {
            count = 0;
            string strTemp = strCount.Replace(strComma, string.Empty);
            return uint.TryParse(strTemp, out count);
        }

        internal static bool GetCount(string strCount, out ulong count)
        {
            count = 0;
            string strTemp = strCount.Replace(strComma, string.Empty);
            return ulong.TryParse(strTemp, out count);
        }

        internal static bool GetCount(string strCount, out long count)
        {
            count = 0;
            string strTemp = strCount.Replace(strComma, string.Empty);
            return long.TryParse(strTemp, out count);
        }

        private IFeatureCollection GetTrialDetails(byte[] trialData)
        {
            using (ILicensing tempLicensing =
                   LicensingFactory.GetLicensing(File.ReadAllBytes(txtIdentityFile.Text.Trim()), null, null, strTempTSName))
            {
                if (cbxHostId.Visible)
                {
                    tempLicensing.LicenseManager.SetHostId((HostIdEnum)(cbxHostIdType.SelectedItem), (string)(cbxHostId.SelectedItem));
                }
                else
                {
                    tempLicensing.CustomHostId = txtHostId.Text;
                }
                tempLicensing.LicenseManager.ProcessTrial(trialData);
                return tempLicensing.LicenseManager.GetFeatureCollection(LicenseSourceOption.Trials);
            }
        }

        public static string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes != null && attributes.Length > 0)
            {
                return attributes[0].Description;
            }
            else
            {
                return value.ToString();
            }
        }

        internal static Enum GetEnumFromDescription(Type enumType, string description)
        {
            if (!String.IsNullOrEmpty(description))
            {
                foreach (Enum value in Enum.GetValues(enumType))
                {
                    if (description.Equals(GetEnumDescription(value)))
                    {
                        return value;
                    }
                }
            }
            return null;
        }

        private bool CompareConfigString(string configString, string controlString)
        {
            string testString = string.IsNullOrEmpty(controlString) ? controlString : controlString.Trim();
            if (string.IsNullOrEmpty(configString) && string.IsNullOrEmpty(testString))
            {
                return true;
            }
            else
            {
                return configString == testString;
            }
        }

        internal static string GetConfigString(string controlString)
        {
            string testString = string.IsNullOrEmpty(controlString) ? controlString : controlString.Trim();
            return string.IsNullOrEmpty(testString) ? null : testString;
        }

        private bool IsASCII(byte[] data)
        {
            if (data.Length > 0)
            {
                int dataValue;
                for (int index = 0; index < data.Length; index++)
                {
                    dataValue = Convert.ToInt32(data[index]);
                    if (dataValue >= 127 || 
                        (dataValue >= 0 && dataValue <= 8) ||
                        (dataValue >= 11 && dataValue <= 12) ||
                        (dataValue >= 14 && dataValue <= 31))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private bool StringIsHex(string hexString)
        {
            if (!string.IsNullOrEmpty(hexString))
            {
                char currentChar;
                for (int index = 0; index < hexString.Length; index++)
                {
                    currentChar = hexString[index];
                    if ((currentChar >= '0' && currentChar <= '9') ||
                        (currentChar >= 'a' && currentChar <= 'f') ||
                        (currentChar >= 'A' && currentChar <= 'F'))
                    {
                        continue;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private byte[] ConvertHexStringToByteArray(string hexString)
        {
            byte[] retBytes = new byte[hexString.Length / 2];
            for (int index = 0; index < retBytes.Length; index++)
            {
                string byteValue = hexString.Substring(index * 2, 2);
                retBytes[index] = byte.Parse(byteValue, NumberStyles.HexNumber, CultureInfo.InvariantCulture);
            }
            return retBytes;
        }

        private void SendBinaryMessage(DotNetDemoSendParameters sendParameters)
        {
            using (IComm comm = CommFactory.Create(sendParameters.uri))
            {
                if (!String.IsNullOrEmpty(currentConfiguration.ProxyServer))
                {
                    comm.ProxyServer = String.Format("{0}{1}", currentConfiguration.ProxyServer,
                        String.IsNullOrEmpty(currentConfiguration.ProxyPort) ? "" : ":" + currentConfiguration.ProxyPort);
                }
                if (currentConfiguration.HttpHeaders != null && currentConfiguration.HttpHeaders.Count > 0)
                {
                    comm.AdditionalHttpHeaders = currentConfiguration.HttpHeaders;
                }
                comm.SendBinaryMessage(sendParameters.request, out sendParameters.response);
            }
        }

        #endregion

        #region High Contrast support

        private void DotNetDemoForm_SystemColorsChanged(object sender, EventArgs e)
        {
            btnSetIdentity.BackColor =
            btnSetHostId.BackColor =
            btnContinue.BackColor =
            btnResetTS.BackColor =
            btnLoadLicenseFile.BackColor =
            lvTrustedStorageFeatures.BackColor =
            btnRespFile.BackColor =
            btnLLSAdd.BackColor =
            btnBOAdd.BackColor =
            btnIMAdd.BackColor =
            btnVDAdd.BackColor =
            btnFSAdd.BackColor =
            btnServerAdvanced.BackColor =
            btnCRSend.BackColor =
            btnCRSave.BackColor =
            btnIMSend.BackColor =
            btnIMSave.BackColor =
            btnLoadTemplateFile.BackColor = 
            btnGenerateSCRequest.BackColor =
            btnProcessSCResponse.BackColor =
            btnAcquire.BackColor = SystemInformation.HighContrast ? Color.SteelBlue : Color.LightSteelBlue;

            pnlTSInstances.BackColor = SystemInformation.HighContrast ? Color.SteelBlue : Color.LightSteelBlue;
            foreach (CheckBox checkBox in ckbTSInstances)
            {
                checkBox.BackColor = SystemInformation.HighContrast ? Color.SteelBlue : Color.LightSteelBlue;
            }
            foreach (Button button in btnTSInstances)
            {
                button.BackColor = SystemInformation.HighContrast ? Color.SteelBlue : Color.LightSteelBlue;
            }

            btnRestart.BackColor =
            btnClear.BackColor =
            btnLLSRemove.BackColor =
            btnBORemove.BackColor =
            btnIMRemove.BackColor =
            btnVDRemove.BackColor =
            btnFSRemove.BackColor =
            btnClearTemplates.BackColor = 
            btnReturn.BackColor =
            btnReturnAll.BackColor = SystemInformation.HighContrast ? Color.Gray : Color.Gainsboro;

            lvAllFeatures.BackColor =
            lvAcquired.BackColor = SystemInformation.HighContrast ? Color.DarkGray : Color.Gainsboro;

            btnResetTrials.BackColor =
            ckbTrials.BackColor =
            lvTrialFeatures.BackColor = SystemInformation.HighContrast ? Color.DarkBlue : Color.LightBlue;

            btnResetSC.BackColor =
            ckbShortCode.BackColor =
            btnDeleteTemplateLicenses.BackColor =
            lvShortCodeFeatures.BackColor = SystemInformation.HighContrast ? Color.Teal : Color.PaleTurquoise;

            lblSetupNotice.ForeColor =
            lblLoadComplete.ForeColor = 
            lblTemplateLoadComplete.ForeColor = SystemInformation.HighContrast ? Color.LightBlue : Color.Blue;

            lvBufferFeatures.BackColor = SystemInformation.HighContrast ? Color.DimGray : Color.Silver;

            btnInspectLicenseFile.BackColor =
                btnCRResponseDetails.BackColor =
                btnInspectTemplateFile.BackColor = 
                btnReadSCResponse.BackColor = SystemInformation.HighContrast ? Color.DarkGoldenrod : Color.Beige;

            FNEToolTip.ForeColor = SystemInformation.HighContrast ? HCToolTipTextColor : baseToolTipTextColor;
        }

        #endregion

        private void btnTrustedStorage_Click(object sender, EventArgs e)
        {
            btnTrustedStorage.Enabled = false;
            pnlTSInstances.Visible = true;
            pnlTSInstances.BringToFront();
            if (pnlTSInstances.CanFocus)
            {
                pnlTSInstances.Focus();
            }
        }

        private void pnlTSInstances_Leave(object sender, EventArgs e)
        {
            if (!pnlTSInstances.ClientRectangle.Contains(pnlTSInstances.PointToClient(Control.MousePosition)))
            {
                ResetError();
                btnTrustedStorage.Enabled = true;
                pnlTSInstances.Visible = false;
                pnlTSInstances.SendToBack();
            }
        }

    }

    internal class DotNetDemoConfigurationItem : IComparable
    {
        internal string configName;
        internal Color  configColor;

        internal DotNetDemoConfigurationItem(string name, Color color)
        {
            this.configName = name;
            this.configColor = color;
        }

        public override string ToString()
        {
            return configName;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to DotNetDemoConfigurationItem return false.
            DotNetDemoConfigurationItem configItem = obj as DotNetDemoConfigurationItem;
            if (configItem == null)
            {
                return false;
            }

            return this.Equals(configItem);
        }

        public bool Equals(DotNetDemoConfigurationItem configItem)
        {
            // If parameter is null return false.
            if (configItem == null)
            {
                return false;
            }
            return configName.Equals(configItem.configName);
        }

        public override int GetHashCode()
        {
            return configName.GetHashCode();
        }

        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                return -1;
            }

            // If parameter cannot be cast to DotNetDemoConfigurationItem return false.
            DotNetDemoConfigurationItem configItem = obj as DotNetDemoConfigurationItem;
            if (configItem == null)
            {
                return -1;
            }

            return configName.CompareTo(configItem.configName);
        }

    }

    internal class DotNetDemoFeatureInfo
    {
        internal LicenseSourceType lsType;
        internal IFeature          feature;

        internal DotNetDemoFeatureInfo(LicenseSourceType lsType, IFeature feature)
        {
            this.lsType = lsType;
            this.feature = feature;
        }
    }

    internal class DotNetDemoSendParameters
    {
        internal string uri;
        internal LicenseServerInstance serverInstance;
        internal byte[] request;
        internal byte[] response;
        internal Exception exception;

        internal DotNetDemoSendParameters(string uri, byte[] request, LicenseServerInstance serverInstance)
        {
            this.uri = uri;
            this.serverInstance = serverInstance;
            this.request = request;
            this.response = null;
            this.exception = null;
        }
    }
}
