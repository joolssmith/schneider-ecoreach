// <copyright file="DotNetDemoForm.Designer.cs" company="Flexera Software LLC">
//     Copyright (c) 2012-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

namespace DotNetDemo
{
    partial class DotNetDemoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DotNetDemoForm));
            this.tabMain = new System.Windows.Forms.TabControl();
            this.pageSetup = new System.Windows.Forms.TabPage();
            this.btnContinue = new System.Windows.Forms.Button();
            this.pnlHostInfo = new System.Windows.Forms.Panel();
            this.lblSetupHostInfo = new System.Windows.Forms.Label();
            this.txtHostType = new System.Windows.Forms.TextBox();
            this.lblHostName = new System.Windows.Forms.Label();
            this.txtHostName = new System.Windows.Forms.TextBox();
            this.lblHostType = new System.Windows.Forms.Label();
            this.pnlHostId = new System.Windows.Forms.Panel();
            this.lblSetupHostId = new System.Windows.Forms.Label();
            this.lblHostId = new System.Windows.Forms.Label();
            this.btnSetHostId = new System.Windows.Forms.Button();
            this.lblHostIDType = new System.Windows.Forms.Label();
            this.cbxHostIdType = new System.Windows.Forms.ComboBox();
            this.cbxHostId = new System.Windows.Forms.ComboBox();
            this.txtHostId = new System.Windows.Forms.TextBox();
            this.lblSetupNotice = new System.Windows.Forms.Label();
            this.btnRestart = new System.Windows.Forms.Button();
            this.pnlIdentity = new System.Windows.Forms.Panel();
            this.ckbVMDetectionEnabled = new System.Windows.Forms.CheckBox();
            this.lblSetupIdentity = new System.Windows.Forms.Label();
            this.txtIdentityFile = new System.Windows.Forms.TextBox();
            this.lblStoragePath = new System.Windows.Forms.Label();
            this.btnSetIdentity = new System.Windows.Forms.Button();
            this.btnBrowseIdentity = new System.Windows.Forms.Button();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.txtStoragePath = new System.Windows.Forms.TextBox();
            this.btnBrowsePath = new System.Windows.Forms.Button();
            this.lblIdentity = new System.Windows.Forms.Label();
            this.pageLicenseSourceCollection = new System.Windows.Forms.TabPage();
            this.btnTrustedStorage = new System.Windows.Forms.Button();
            this.licenseImageList = new System.Windows.Forms.ImageList(this.components);
            this.lblTrustedStorage = new System.Windows.Forms.Label();
            this.btnResetSC = new System.Windows.Forms.Button();
            this.ckbShortCode = new System.Windows.Forms.CheckBox();
            this.btnResetTrials = new System.Windows.Forms.Button();
            this.btnResetTS = new System.Windows.Forms.Button();
            this.cbxLicenseFile = new System.Windows.Forms.ComboBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.ckbTrials = new System.Windows.Forms.CheckBox();
            this.btnLoadLicenseFile = new System.Windows.Forms.Button();
            this.btnInspectLicenseFile = new System.Windows.Forms.Button();
            this.lblLoadComplete = new System.Windows.Forms.Label();
            this.btnLicenseFileBrowse = new System.Windows.Forms.Button();
            this.lblLicenseFile = new System.Windows.Forms.Label();
            this.tabLicenses = new System.Windows.Forms.TabControl();
            this.pageAllLicenses = new System.Windows.Forms.TabPage();
            this.lvAllFeatures = new System.Windows.Forms.ListView();
            this.colAllType = new System.Windows.Forms.ColumnHeader();
            this.colAllFeatureName = new System.Windows.Forms.ColumnHeader();
            this.colAllFeatureVersion = new System.Windows.Forms.ColumnHeader();
            this.colAllFeatureCount = new System.Windows.Forms.ColumnHeader();
            this.colAllFeatureExpiration = new System.Windows.Forms.ColumnHeader();
            this.colAllVendorString = new System.Windows.Forms.ColumnHeader();
            this.pageTrustedStorageLicenses = new System.Windows.Forms.TabPage();
            this.lvTrustedStorageFeatures = new System.Windows.Forms.ListView();
            this.colTSType = new System.Windows.Forms.ColumnHeader();
            this.colTSFeatureName = new System.Windows.Forms.ColumnHeader();
            this.colTSFeatureVersion = new System.Windows.Forms.ColumnHeader();
            this.colTSFeatureCount = new System.Windows.Forms.ColumnHeader();
            this.colTSFeatureExpiration = new System.Windows.Forms.ColumnHeader();
            this.colTSVendorString = new System.Windows.Forms.ColumnHeader();
            this.pageTrialLicenses = new System.Windows.Forms.TabPage();
            this.lvTrialFeatures = new System.Windows.Forms.ListView();
            this.colTrialType = new System.Windows.Forms.ColumnHeader();
            this.colTrialFeatureName = new System.Windows.Forms.ColumnHeader();
            this.colTrialFeatureVersion = new System.Windows.Forms.ColumnHeader();
            this.colTrialFeatureCount = new System.Windows.Forms.ColumnHeader();
            this.colTrialFeatureExpiration = new System.Windows.Forms.ColumnHeader();
            this.colTrialVendorString = new System.Windows.Forms.ColumnHeader();
            this.pageShortCodeLicenses = new System.Windows.Forms.TabPage();
            this.lvShortCodeFeatures = new System.Windows.Forms.ListView();
            this.colSCType = new System.Windows.Forms.ColumnHeader();
            this.colSCFeature = new System.Windows.Forms.ColumnHeader();
            this.colSCVersion = new System.Windows.Forms.ColumnHeader();
            this.colSCCount = new System.Windows.Forms.ColumnHeader();
            this.colSCExpiration = new System.Windows.Forms.ColumnHeader();
            this.colSCVendorString = new System.Windows.Forms.ColumnHeader();
            this.pageBufferLicenses = new System.Windows.Forms.TabPage();
            this.lvBufferFeatures = new System.Windows.Forms.ListView();
            this.colBufferType = new System.Windows.Forms.ColumnHeader();
            this.colBufferFeatureName = new System.Windows.Forms.ColumnHeader();
            this.colBufferFeatureVersion = new System.Windows.Forms.ColumnHeader();
            this.colBufferFeatrureCount = new System.Windows.Forms.ColumnHeader();
            this.colBufferFeatureExpiration = new System.Windows.Forms.ColumnHeader();
            this.colBufferVendorString = new System.Windows.Forms.ColumnHeader();
            this.pnlTSInstances = new System.Windows.Forms.Panel();
            this.pageServer = new System.Windows.Forms.TabPage();
            this.btnServerAdvanced = new System.Windows.Forms.Button();
            this.ckbHTTPS = new System.Windows.Forms.CheckBox();
            this.tabServerRequest = new System.Windows.Forms.TabControl();
            this.pageCapabilityRequest = new System.Windows.Forms.TabPage();
            this.btnCRSave = new System.Windows.Forms.Button();
            this.btnRespFile = new System.Windows.Forms.Button();
            this.txtRespFile = new System.Windows.Forms.TextBox();
            this.lblRespFile = new System.Windows.Forms.Label();
            this.ckbProcessResponse = new System.Windows.Forms.CheckBox();
            this.btnCRSend = new System.Windows.Forms.Button();
            this.ckbForceResponse = new System.Windows.Forms.CheckBox();
            this.tabCapabilityRequest = new System.Windows.Forms.TabControl();
            this.pageCR_Options = new System.Windows.Forms.TabPage();
            this.cbxTSInstance = new System.Windows.Forms.ComboBox();
            this.lblTSInstance = new System.Windows.Forms.Label();
            this.txtBorrowInterval = new System.Windows.Forms.TextBox();
            this.lblBorrowInterval = new System.Windows.Forms.Label();
            this.cbxBorrowGranularity = new System.Windows.Forms.ComboBox();
            this.lblBorrowGranularity = new System.Windows.Forms.Label();
            this.cbxOperation = new System.Windows.Forms.ComboBox();
            this.lblOperation = new System.Windows.Forms.Label();
            this.txtRequestorID = new System.Windows.Forms.TextBox();
            this.lblRequestorID = new System.Windows.Forms.Label();
            this.txtEnterpriseID = new System.Windows.Forms.TextBox();
            this.lblEnterpriseID = new System.Windows.Forms.Label();
            this.txtAcquisitionID = new System.Windows.Forms.TextBox();
            this.lblAcquisitionID = new System.Windows.Forms.Label();
            this.txtCorrelationID = new System.Windows.Forms.TextBox();
            this.lblCorrelationID = new System.Windows.Forms.Label();
            this.ckbOTA = new System.Windows.Forms.CheckBox();
            this.ckbIncremental = new System.Windows.Forms.CheckBox();
            this.ckbRequestAll = new System.Windows.Forms.CheckBox();
            this.pageCR_LLS = new System.Windows.Forms.TabPage();
            this.ckbLLSPartial = new System.Windows.Forms.CheckBox();
            this.btnLLSRemove = new System.Windows.Forms.Button();
            this.txtLLSVersion = new System.Windows.Forms.TextBox();
            this.txtLLSCount = new System.Windows.Forms.TextBox();
            this.txtLLSFeature = new System.Windows.Forms.TextBox();
            this.lblLLSCount = new System.Windows.Forms.Label();
            this.lblLLSVersion = new System.Windows.Forms.Label();
            this.lblLLSFeature = new System.Windows.Forms.Label();
            this.btnLLSAdd = new System.Windows.Forms.Button();
            this.lvLLSFeatures = new System.Windows.Forms.ListView();
            this.colLLSFeatureName = new System.Windows.Forms.ColumnHeader();
            this.colLLSFeatureVersion = new System.Windows.Forms.ColumnHeader();
            this.colLLSFeatureCount = new System.Windows.Forms.ColumnHeader();
            this.colLLSFeaturePartial = new System.Windows.Forms.ColumnHeader();
            this.pageCR_BO = new System.Windows.Forms.TabPage();
            this.ckbBOPartial = new System.Windows.Forms.CheckBox();
            this.btnBORemove = new System.Windows.Forms.Button();
            this.txtBOCount = new System.Windows.Forms.TextBox();
            this.txtBORights = new System.Windows.Forms.TextBox();
            this.lblBOCount = new System.Windows.Forms.Label();
            this.lblBORights = new System.Windows.Forms.Label();
            this.btnBOAdd = new System.Windows.Forms.Button();
            this.lvBORights = new System.Windows.Forms.ListView();
            this.colBORightsName = new System.Windows.Forms.ColumnHeader();
            this.colBORightsCount = new System.Windows.Forms.ColumnHeader();
            this.colBORightsPartial = new System.Windows.Forms.ColumnHeader();
            this.pageCR_VD = new System.Windows.Forms.TabPage();
            this.cbxVDType = new System.Windows.Forms.ComboBox();
            this.btnVDRemove = new System.Windows.Forms.Button();
            this.txtVDKey = new System.Windows.Forms.TextBox();
            this.txtVDValue = new System.Windows.Forms.TextBox();
            this.lblVDValue = new System.Windows.Forms.Label();
            this.lblVDKey = new System.Windows.Forms.Label();
            this.lblVDType = new System.Windows.Forms.Label();
            this.btnVDAdd = new System.Windows.Forms.Button();
            this.lvVendorDictionary = new System.Windows.Forms.ListView();
            this.colVDType = new System.Windows.Forms.ColumnHeader();
            this.colVDKey = new System.Windows.Forms.ColumnHeader();
            this.colVDValue = new System.Windows.Forms.ColumnHeader();
            this.pageCR_FS = new System.Windows.Forms.TabPage();
            this.cbxFSType = new System.Windows.Forms.ComboBox();
            this.btnFSRemove = new System.Windows.Forms.Button();
            this.txtFSKey = new System.Windows.Forms.TextBox();
            this.txtFSValue = new System.Windows.Forms.TextBox();
            this.lblFSValue = new System.Windows.Forms.Label();
            this.lblFSKey = new System.Windows.Forms.Label();
            this.lblFSType = new System.Windows.Forms.Label();
            this.btnFSAdd = new System.Windows.Forms.Button();
            this.lvFeatureSelectors = new System.Windows.Forms.ListView();
            this.colFSType = new System.Windows.Forms.ColumnHeader();
            this.colFSKey = new System.Windows.Forms.ColumnHeader();
            this.colFSValue = new System.Windows.Forms.ColumnHeader();
            this.pageCR_AuxHostIds = new System.Windows.Forms.TabPage();
            this.cbxAuxHostIdType = new System.Windows.Forms.ComboBox();
            this.btnAuxHostIdRemove = new System.Windows.Forms.Button();
            this.txtAuxHostIdValue = new System.Windows.Forms.TextBox();
            this.lblAuxHostIdValue = new System.Windows.Forms.Label();
            this.lblAuxHostIdType = new System.Windows.Forms.Label();
            this.btnAuxHostIdAdd = new System.Windows.Forms.Button();
            this.lvAuxHostIds = new System.Windows.Forms.ListView();
            this.colAuxHostIdType = new System.Windows.Forms.ColumnHeader();
            this.colAuxHostIdValue = new System.Windows.Forms.ColumnHeader();
            this.pageCR_Resp = new System.Windows.Forms.TabPage();
            this.btnCRResponseDetails = new System.Windows.Forms.Button();
            this.lvCRResponse = new System.Windows.Forms.ListView();
            this.colRespType = new System.Windows.Forms.ColumnHeader();
            this.colRespFeature = new System.Windows.Forms.ColumnHeader();
            this.colRespVersion = new System.Windows.Forms.ColumnHeader();
            this.colRespCount = new System.Windows.Forms.ColumnHeader();
            this.colRespExpiration = new System.Windows.Forms.ColumnHeader();
            this.colRespVendorString = new System.Windows.Forms.ColumnHeader();
            this.pageInfoMessage = new System.Windows.Forms.TabPage();
            this.btnIMSave = new System.Windows.Forms.Button();
            this.btnIMSend = new System.Windows.Forms.Button();
            this.pnlFeatureUsage = new System.Windows.Forms.Panel();
            this.dtIMExpiration = new System.Windows.Forms.DateTimePicker();
            this.lblIMExpiration = new System.Windows.Forms.Label();
            this.btnIMRemove = new System.Windows.Forms.Button();
            this.txtIMVersion = new System.Windows.Forms.TextBox();
            this.txtIMCount = new System.Windows.Forms.TextBox();
            this.txtIMFeature = new System.Windows.Forms.TextBox();
            this.lblIMCount = new System.Windows.Forms.Label();
            this.lblIMVersion = new System.Windows.Forms.Label();
            this.lblIMFeature = new System.Windows.Forms.Label();
            this.btnIMAdd = new System.Windows.Forms.Button();
            this.lvIMFeatures = new System.Windows.Forms.ListView();
            this.colIMFeature = new System.Windows.Forms.ColumnHeader();
            this.colIMVersion = new System.Windows.Forms.ColumnHeader();
            this.colIMCount = new System.Windows.Forms.ColumnHeader();
            this.colIMExpiration = new System.Windows.Forms.ColumnHeader();
            this.ckbIMAddExisting = new System.Windows.Forms.CheckBox();
            this.cbxServerType = new System.Windows.Forms.ComboBox();
            this.lblServerType = new System.Windows.Forms.Label();
            this.lblServerInfo = new System.Windows.Forms.Label();
            this.lblServerURL = new System.Windows.Forms.Label();
            this.txtServerURL = new System.Windows.Forms.TextBox();
            this.pageShortCodes = new System.Windows.Forms.TabPage();
            this.tabTemplate = new System.Windows.Forms.TabControl();
            this.pageTemplateFeatures = new System.Windows.Forms.TabPage();
            this.lvTemplateFeatures = new System.Windows.Forms.ListView();
            this.colTemplateType = new System.Windows.Forms.ColumnHeader();
            this.colTemplateFeature = new System.Windows.Forms.ColumnHeader();
            this.colTemplateVersion = new System.Windows.Forms.ColumnHeader();
            this.colTemplateCount = new System.Windows.Forms.ColumnHeader();
            this.colTemplateExpiry = new System.Windows.Forms.ColumnHeader();
            this.colTemplateVS = new System.Windows.Forms.ColumnHeader();
            this.pageTemplateVD = new System.Windows.Forms.TabPage();
            this.lvTemplateVD = new System.Windows.Forms.ListView();
            this.colTemplateVDType = new System.Windows.Forms.ColumnHeader();
            this.colTemplateVDKey = new System.Windows.Forms.ColumnHeader();
            this.colTemplateVDValue = new System.Windows.Forms.ColumnHeader();
            this.pageTemplateRequest = new System.Windows.Forms.TabPage();
            this.rbSCRequestPDHex = new System.Windows.Forms.RadioButton();
            this.rbSCRequestPDChar = new System.Windows.Forms.RadioButton();
            this.btnSCRequestPD = new System.Windows.Forms.Button();
            this.txtSCRequestPD = new System.Windows.Forms.TextBox();
            this.lblSCRequestPD = new System.Windows.Forms.Label();
            this.btnSCRequestFile = new System.Windows.Forms.Button();
            this.txtSCRequestFile = new System.Windows.Forms.TextBox();
            this.lblSCRequestFile = new System.Windows.Forms.Label();
            this.btnGenerateSCRequest = new System.Windows.Forms.Button();
            this.txtEncodingSegSize = new System.Windows.Forms.TextBox();
            this.lblEncodingSegSize = new System.Windows.Forms.Label();
            this.txtEncodingChars = new System.Windows.Forms.TextBox();
            this.lblEncodingChars = new System.Windows.Forms.Label();
            this.cbxEncodingType = new System.Windows.Forms.ComboBox();
            this.lblEncodingType = new System.Windows.Forms.Label();
            this.pageTemplateResponse = new System.Windows.Forms.TabPage();
            this.cbxSCResponseFile = new System.Windows.Forms.ComboBox();
            this.txtSCResponseID = new System.Windows.Forms.TextBox();
            this.lblSCResponseID = new System.Windows.Forms.Label();
            this.btnReadSCResponse = new System.Windows.Forms.Button();
            this.btnSCResponseFile = new System.Windows.Forms.Button();
            this.txtSCResponsePD = new System.Windows.Forms.TextBox();
            this.lblSCResponsePD = new System.Windows.Forms.Label();
            this.btnSCResponsePD = new System.Windows.Forms.Button();
            this.lblSCResponseFile = new System.Windows.Forms.Label();
            this.btnProcessSCResponse = new System.Windows.Forms.Button();
            this.txtDecodingSegSize = new System.Windows.Forms.TextBox();
            this.lblDecodingSegSize = new System.Windows.Forms.Label();
            this.txtDecodingChars = new System.Windows.Forms.TextBox();
            this.lblDecodingChars = new System.Windows.Forms.Label();
            this.cbxDecodingType = new System.Windows.Forms.ComboBox();
            this.lblDecodingType = new System.Windows.Forms.Label();
            this.panelTemplates = new System.Windows.Forms.Panel();
            this.btnDeleteTemplateLicenses = new System.Windows.Forms.Button();
            this.cbxTemplateFile = new System.Windows.Forms.ComboBox();
            this.btnClearTemplates = new System.Windows.Forms.Button();
            this.lvTemplates = new System.Windows.Forms.ListView();
            this.colTemplateID = new System.Windows.Forms.ColumnHeader();
            this.colTemplateExpiration = new System.Windows.Forms.ColumnHeader();
            this.colTemplateMachineType = new System.Windows.Forms.ColumnHeader();
            this.colFileName = new System.Windows.Forms.ColumnHeader();
            this.btnLoadTemplateFile = new System.Windows.Forms.Button();
            this.lblTemplateLoadComplete = new System.Windows.Forms.Label();
            this.btnInspectTemplateFile = new System.Windows.Forms.Button();
            this.lblTemplates = new System.Windows.Forms.Label();
            this.lblTemplateFile = new System.Windows.Forms.Label();
            this.btnBrowseTemplateFile = new System.Windows.Forms.Button();
            this.pageAcqRet = new System.Windows.Forms.TabPage();
            this.cbxVersion = new System.Windows.Forms.ComboBox();
            this.cbxFeatureName = new System.Windows.Forms.ComboBox();
            this.lblDoSelect = new System.Windows.Forms.Label();
            this.btnReturnAll = new System.Windows.Forms.Button();
            this.btnReturn = new System.Windows.Forms.Button();
            this.btnAcquire = new System.Windows.Forms.Button();
            this.txtCount = new System.Windows.Forms.TextBox();
            this.lblCount = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.lblFeature = new System.Windows.Forms.Label();
            this.lblAcquired = new System.Windows.Forms.Label();
            this.lvAcquired = new System.Windows.Forms.ListView();
            this.colAcquiredFeatureType = new System.Windows.Forms.ColumnHeader();
            this.colAcquiredFeatureName = new System.Windows.Forms.ColumnHeader();
            this.colAcquiredFeatureVersion = new System.Windows.Forms.ColumnHeader();
            this.colAcquiredFeatureCount = new System.Windows.Forms.ColumnHeader();
            this.colAcquiredFeatureExpiration = new System.Windows.Forms.ColumnHeader();
            this.colAcquiredVendorString = new System.Windows.Forms.ColumnHeader();
            this.FNEToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.btnToolStripOpen = new System.Windows.Forms.ToolStripButton();
            this.btnToolStripSave = new System.Windows.Forms.ToolStripButton();
            this.btnToolStripSaveAs = new System.Windows.Forms.ToolStripButton();
            this.btnToolStripDelete = new System.Windows.Forms.ToolStripButton();
            this.grpToolStrip = new System.Windows.Forms.ToolStripSeparator();
            this.btnToolStripAbout = new System.Windows.Forms.ToolStripButton();
            this.cbxToolStripConfig = new System.Windows.Forms.ToolStripComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tabMain.SuspendLayout();
            this.pageSetup.SuspendLayout();
            this.pnlHostInfo.SuspendLayout();
            this.pnlHostId.SuspendLayout();
            this.pnlIdentity.SuspendLayout();
            this.pageLicenseSourceCollection.SuspendLayout();
            this.tabLicenses.SuspendLayout();
            this.pageAllLicenses.SuspendLayout();
            this.pageTrustedStorageLicenses.SuspendLayout();
            this.pageTrialLicenses.SuspendLayout();
            this.pageShortCodeLicenses.SuspendLayout();
            this.pageBufferLicenses.SuspendLayout();
            this.pageServer.SuspendLayout();
            this.tabServerRequest.SuspendLayout();
            this.pageCapabilityRequest.SuspendLayout();
            this.tabCapabilityRequest.SuspendLayout();
            this.pageCR_Options.SuspendLayout();
            this.pageCR_LLS.SuspendLayout();
            this.pageCR_BO.SuspendLayout();
            this.pageCR_VD.SuspendLayout();
            this.pageCR_FS.SuspendLayout();
            this.pageCR_AuxHostIds.SuspendLayout();
            this.pageCR_Resp.SuspendLayout();
            this.pageInfoMessage.SuspendLayout();
            this.pnlFeatureUsage.SuspendLayout();
            this.pageShortCodes.SuspendLayout();
            this.tabTemplate.SuspendLayout();
            this.pageTemplateFeatures.SuspendLayout();
            this.pageTemplateVD.SuspendLayout();
            this.pageTemplateRequest.SuspendLayout();
            this.pageTemplateResponse.SuspendLayout();
            this.panelTemplates.SuspendLayout();
            this.pageAcqRet.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabMain
            // 
            this.tabMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabMain.Controls.Add(this.pageSetup);
            this.tabMain.Controls.Add(this.pageLicenseSourceCollection);
            this.tabMain.Controls.Add(this.pageServer);
            this.tabMain.Controls.Add(this.pageShortCodes);
            this.tabMain.Controls.Add(this.pageAcqRet);
            this.tabMain.HotTrack = true;
            this.tabMain.Location = new System.Drawing.Point(0, 45);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.ShowToolTips = true;
            this.tabMain.Size = new System.Drawing.Size(584, 592);
            this.tabMain.TabIndex = 2;
            this.tabMain.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabMain_Selecting);
            this.tabMain.SelectedIndexChanged += new System.EventHandler(this.tabMain_SelectedIndexChanged);
            this.tabMain.ClientSizeChanged += new System.EventHandler(this.tabMain_ClientSizeChanged);
            // 
            // pageSetup
            // 
            this.pageSetup.Controls.Add(this.btnContinue);
            this.pageSetup.Controls.Add(this.pnlHostInfo);
            this.pageSetup.Controls.Add(this.pnlHostId);
            this.pageSetup.Controls.Add(this.lblSetupNotice);
            this.pageSetup.Controls.Add(this.btnRestart);
            this.pageSetup.Controls.Add(this.pnlIdentity);
            this.pageSetup.Location = new System.Drawing.Point(4, 22);
            this.pageSetup.Name = "pageSetup";
            this.pageSetup.Size = new System.Drawing.Size(576, 566);
            this.pageSetup.TabIndex = 3;
            this.pageSetup.Text = "Setup";
            this.pageSetup.ToolTipText = "Set up licensing environment information";
            this.pageSetup.UseVisualStyleBackColor = true;
            // 
            // btnContinue
            // 
            this.btnContinue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnContinue.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnContinue.Enabled = false;
            this.btnContinue.Image = ((System.Drawing.Image)(resources.GetObject("btnContinue.Image")));
            this.btnContinue.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnContinue.Location = new System.Drawing.Point(483, 525);
            this.btnContinue.Name = "btnContinue";
            this.btnContinue.Size = new System.Drawing.Size(85, 28);
            this.btnContinue.TabIndex = 6;
            this.btnContinue.Text = "Continue";
            this.btnContinue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.FNEToolTip.SetToolTip(this.btnContinue, "Continue to License Source Collection page");
            this.btnContinue.UseVisualStyleBackColor = false;
            this.btnContinue.Click += new System.EventHandler(this.btnContinue_Click);
            // 
            // pnlHostInfo
            // 
            this.pnlHostInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlHostInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlHostInfo.Controls.Add(this.lblSetupHostInfo);
            this.pnlHostInfo.Controls.Add(this.txtHostType);
            this.pnlHostInfo.Controls.Add(this.lblHostName);
            this.pnlHostInfo.Controls.Add(this.txtHostName);
            this.pnlHostInfo.Controls.Add(this.lblHostType);
            this.pnlHostInfo.Location = new System.Drawing.Point(3, 337);
            this.pnlHostInfo.Name = "pnlHostInfo";
            this.pnlHostInfo.Size = new System.Drawing.Size(569, 100);
            this.pnlHostInfo.TabIndex = 3;
            // 
            // lblSetupHostInfo
            // 
            this.lblSetupHostInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSetupHostInfo.AutoSize = true;
            this.lblSetupHostInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSetupHostInfo.Location = new System.Drawing.Point(202, 8);
            this.lblSetupHostInfo.Name = "lblSetupHostInfo";
            this.lblSetupHostInfo.Size = new System.Drawing.Size(100, 13);
            this.lblSetupHostInfo.TabIndex = 1;
            this.lblSetupHostInfo.Text = "Host Information";
            this.lblSetupHostInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtHostType
            // 
            this.txtHostType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHostType.Location = new System.Drawing.Point(100, 36);
            this.txtHostType.Name = "txtHostType";
            this.txtHostType.Size = new System.Drawing.Size(406, 20);
            this.txtHostType.TabIndex = 3;
            this.FNEToolTip.SetToolTip(this.txtHostType, "Optional host type description such as model name");
            // 
            // lblHostName
            // 
            this.lblHostName.AutoSize = true;
            this.lblHostName.Location = new System.Drawing.Point(6, 68);
            this.lblHostName.Name = "lblHostName";
            this.lblHostName.Size = new System.Drawing.Size(63, 13);
            this.lblHostName.TabIndex = 4;
            this.lblHostName.Text = "Host Name:";
            // 
            // txtHostName
            // 
            this.txtHostName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHostName.Location = new System.Drawing.Point(100, 66);
            this.txtHostName.Name = "txtHostName";
            this.txtHostName.Size = new System.Drawing.Size(406, 20);
            this.txtHostName.TabIndex = 5;
            this.FNEToolTip.SetToolTip(this.txtHostName, "Optional user friendly alias name for this host");
            // 
            // lblHostType
            // 
            this.lblHostType.AutoSize = true;
            this.lblHostType.Location = new System.Drawing.Point(6, 38);
            this.lblHostType.Name = "lblHostType";
            this.lblHostType.Size = new System.Drawing.Size(59, 13);
            this.lblHostType.TabIndex = 2;
            this.lblHostType.Text = "Host Type:";
            // 
            // pnlHostId
            // 
            this.pnlHostId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlHostId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlHostId.Controls.Add(this.lblSetupHostId);
            this.pnlHostId.Controls.Add(this.lblHostId);
            this.pnlHostId.Controls.Add(this.btnSetHostId);
            this.pnlHostId.Controls.Add(this.lblHostIDType);
            this.pnlHostId.Controls.Add(this.cbxHostIdType);
            this.pnlHostId.Controls.Add(this.cbxHostId);
            this.pnlHostId.Controls.Add(this.txtHostId);
            this.pnlHostId.Location = new System.Drawing.Point(3, 196);
            this.pnlHostId.Name = "pnlHostId";
            this.pnlHostId.Size = new System.Drawing.Size(569, 136);
            this.pnlHostId.TabIndex = 2;
            // 
            // lblSetupHostId
            // 
            this.lblSetupHostId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSetupHostId.AutoSize = true;
            this.lblSetupHostId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSetupHostId.Location = new System.Drawing.Point(205, 8);
            this.lblSetupHostId.Name = "lblSetupHostId";
            this.lblSetupHostId.Size = new System.Drawing.Size(111, 13);
            this.lblSetupHostId.TabIndex = 1;
            this.lblSetupHostId.Text = "Host Identification";
            this.lblSetupHostId.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblHostId
            // 
            this.lblHostId.AutoSize = true;
            this.lblHostId.Location = new System.Drawing.Point(6, 68);
            this.lblHostId.Name = "lblHostId";
            this.lblHostId.Size = new System.Drawing.Size(46, 13);
            this.lblHostId.TabIndex = 4;
            this.lblHostId.Text = "Host ID:";
            // 
            // btnSetHostId
            // 
            this.btnSetHostId.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnSetHostId.Location = new System.Drawing.Point(225, 96);
            this.btnSetHostId.Name = "btnSetHostId";
            this.btnSetHostId.Size = new System.Drawing.Size(75, 28);
            this.btnSetHostId.TabIndex = 7;
            this.btnSetHostId.Text = "Set";
            this.FNEToolTip.SetToolTip(this.btnSetHostId, "Set HostId information");
            this.btnSetHostId.UseVisualStyleBackColor = false;
            this.btnSetHostId.Click += new System.EventHandler(this.btnSetHostId_Click);
            // 
            // lblHostIDType
            // 
            this.lblHostIDType.AutoSize = true;
            this.lblHostIDType.Location = new System.Drawing.Point(6, 38);
            this.lblHostIDType.Name = "lblHostIDType";
            this.lblHostIDType.Size = new System.Drawing.Size(73, 13);
            this.lblHostIDType.TabIndex = 2;
            this.lblHostIDType.Text = "Host ID Type:";
            // 
            // cbxHostIdType
            // 
            this.cbxHostIdType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxHostIdType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxHostIdType.FormattingEnabled = true;
            this.cbxHostIdType.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cbxHostIdType.Location = new System.Drawing.Point(100, 36);
            this.cbxHostIdType.Name = "cbxHostIdType";
            this.cbxHostIdType.Size = new System.Drawing.Size(246, 21);
            this.cbxHostIdType.TabIndex = 3;
            this.FNEToolTip.SetToolTip(this.cbxHostIdType, "Chose between a custom string hostid type or other types available in this host e" +
                    "nvironment");
            this.cbxHostIdType.SelectedIndexChanged += new System.EventHandler(this.cbxHostIdType_SelectedIndexChanged);
            this.cbxHostIdType.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.cbxHostIdType_Format);
            // 
            // cbxHostId
            // 
            this.cbxHostId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxHostId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxHostId.FormattingEnabled = true;
            this.cbxHostId.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cbxHostId.Location = new System.Drawing.Point(100, 66);
            this.cbxHostId.Name = "cbxHostId";
            this.cbxHostId.Size = new System.Drawing.Size(386, 21);
            this.cbxHostId.TabIndex = 6;
            this.FNEToolTip.SetToolTip(this.cbxHostId, "Choose between the available hostid values for the selected type");
            this.cbxHostId.SelectedIndexChanged += new System.EventHandler(this.cbxHostId_SelectedIndexChanged);
            // 
            // txtHostId
            // 
            this.txtHostId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHostId.Location = new System.Drawing.Point(100, 66);
            this.txtHostId.Name = "txtHostId";
            this.txtHostId.Size = new System.Drawing.Size(386, 20);
            this.txtHostId.TabIndex = 5;
            this.FNEToolTip.SetToolTip(this.txtHostId, "Enter a custom hostid string");
            this.txtHostId.TextChanged += new System.EventHandler(this.txtHostId_TextChanged);
            // 
            // lblSetupNotice
            // 
            this.lblSetupNotice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSetupNotice.ForeColor = System.Drawing.Color.Blue;
            this.lblSetupNotice.Location = new System.Drawing.Point(93, 442);
            this.lblSetupNotice.Name = "lblSetupNotice";
            this.lblSetupNotice.Size = new System.Drawing.Size(395, 13);
            this.lblSetupNotice.TabIndex = 4;
            this.lblSetupNotice.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnRestart
            // 
            this.btnRestart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRestart.BackColor = System.Drawing.Color.Gainsboro;
            this.btnRestart.Location = new System.Drawing.Point(277, 525);
            this.btnRestart.Name = "btnRestart";
            this.btnRestart.Size = new System.Drawing.Size(75, 28);
            this.btnRestart.TabIndex = 5;
            this.btnRestart.Text = "Restart";
            this.FNEToolTip.SetToolTip(this.btnRestart, "Clear the licensing environment and start over");
            this.btnRestart.UseVisualStyleBackColor = false;
            this.btnRestart.Click += new System.EventHandler(this.btnRestart_Click);
            // 
            // pnlIdentity
            // 
            this.pnlIdentity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlIdentity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlIdentity.Controls.Add(this.ckbVMDetectionEnabled);
            this.pnlIdentity.Controls.Add(this.lblSetupIdentity);
            this.pnlIdentity.Controls.Add(this.txtIdentityFile);
            this.pnlIdentity.Controls.Add(this.lblStoragePath);
            this.pnlIdentity.Controls.Add(this.btnSetIdentity);
            this.pnlIdentity.Controls.Add(this.btnBrowseIdentity);
            this.pnlIdentity.Controls.Add(this.txtStoragePath);
            this.pnlIdentity.Controls.Add(this.btnBrowsePath);
            this.pnlIdentity.Controls.Add(this.lblIdentity);
            this.pnlIdentity.Location = new System.Drawing.Point(3, 25);
            this.pnlIdentity.Name = "pnlIdentity";
            this.pnlIdentity.Size = new System.Drawing.Size(569, 166);
            this.pnlIdentity.TabIndex = 1;
            // 
            // ckbVMDetectionEnabled
            // 
            this.ckbVMDetectionEnabled.AutoSize = true;
            this.ckbVMDetectionEnabled.Checked = true;
            this.ckbVMDetectionEnabled.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckbVMDetectionEnabled.Location = new System.Drawing.Point(9, 98);
            this.ckbVMDetectionEnabled.Name = "ckbVMDetectionEnabled";
            this.ckbVMDetectionEnabled.Size = new System.Drawing.Size(133, 17);
            this.ckbVMDetectionEnabled.TabIndex = 8;
            this.ckbVMDetectionEnabled.Text = "VM Detection Enabled";
            this.FNEToolTip.SetToolTip(this.ckbVMDetectionEnabled, "Enable Virtual Machine detection");
            this.ckbVMDetectionEnabled.UseVisualStyleBackColor = true;
            // 
            // lblSetupIdentity
            // 
            this.lblSetupIdentity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSetupIdentity.AutoSize = true;
            this.lblSetupIdentity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSetupIdentity.Location = new System.Drawing.Point(140, 8);
            this.lblSetupIdentity.Name = "lblSetupIdentity";
            this.lblSetupIdentity.Size = new System.Drawing.Size(253, 13);
            this.lblSetupIdentity.TabIndex = 1;
            this.lblSetupIdentity.Text = "Identity Data and Trusted Storage Location";
            this.lblSetupIdentity.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtIdentityFile
            // 
            this.txtIdentityFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtIdentityFile.Location = new System.Drawing.Point(100, 36);
            this.txtIdentityFile.Name = "txtIdentityFile";
            this.txtIdentityFile.Size = new System.Drawing.Size(406, 20);
            this.txtIdentityFile.TabIndex = 3;
            this.FNEToolTip.SetToolTip(this.txtIdentityFile, "Location of binary client identity file generated by PubIdUtil tool");
            this.txtIdentityFile.TextChanged += new System.EventHandler(this.txtIdentityFile_TextChanged);
            // 
            // lblStoragePath
            // 
            this.lblStoragePath.AutoSize = true;
            this.lblStoragePath.Location = new System.Drawing.Point(6, 68);
            this.lblStoragePath.Name = "lblStoragePath";
            this.lblStoragePath.Size = new System.Drawing.Size(72, 13);
            this.lblStoragePath.TabIndex = 5;
            this.lblStoragePath.Text = "Storage Path:";
            // 
            // btnSetIdentity
            // 
            this.btnSetIdentity.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnSetIdentity.Location = new System.Drawing.Point(225, 126);
            this.btnSetIdentity.Name = "btnSetIdentity";
            this.btnSetIdentity.Size = new System.Drawing.Size(75, 28);
            this.btnSetIdentity.TabIndex = 9;
            this.btnSetIdentity.Text = "Set";
            this.FNEToolTip.SetToolTip(this.btnSetIdentity, "Set identity data and trusted storage location");
            this.btnSetIdentity.UseVisualStyleBackColor = false;
            this.btnSetIdentity.Click += new System.EventHandler(this.btnSetIdentity_Click);
            // 
            // btnBrowseIdentity
            // 
            this.btnBrowseIdentity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowseIdentity.ImageIndex = 1;
            this.btnBrowseIdentity.ImageList = this.imageList;
            this.btnBrowseIdentity.Location = new System.Drawing.Point(521, 35);
            this.btnBrowseIdentity.Name = "btnBrowseIdentity";
            this.btnBrowseIdentity.Size = new System.Drawing.Size(32, 23);
            this.btnBrowseIdentity.TabIndex = 4;
            this.btnBrowseIdentity.UseVisualStyleBackColor = true;
            this.btnBrowseIdentity.Click += new System.EventHandler(this.btnBrowseIdentity_Click);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "folder-open.ico");
            this.imageList.Images.SetKeyName(1, "document-open-2.ico");
            // 
            // txtStoragePath
            // 
            this.txtStoragePath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStoragePath.Location = new System.Drawing.Point(100, 66);
            this.txtStoragePath.Name = "txtStoragePath";
            this.txtStoragePath.Size = new System.Drawing.Size(406, 20);
            this.txtStoragePath.TabIndex = 6;
            this.FNEToolTip.SetToolTip(this.txtStoragePath, "Optional trusted storage location. Leave emtpy to use in-memory trusted storage");
            this.txtStoragePath.TextChanged += new System.EventHandler(this.txtStoragePath_TextChanged);
            // 
            // btnBrowsePath
            // 
            this.btnBrowsePath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowsePath.ImageIndex = 0;
            this.btnBrowsePath.ImageList = this.imageList;
            this.btnBrowsePath.Location = new System.Drawing.Point(521, 65);
            this.btnBrowsePath.Name = "btnBrowsePath";
            this.btnBrowsePath.Size = new System.Drawing.Size(32, 23);
            this.btnBrowsePath.TabIndex = 7;
            this.btnBrowsePath.UseVisualStyleBackColor = true;
            this.btnBrowsePath.Click += new System.EventHandler(this.btnBrowsePath_Click);
            // 
            // lblIdentity
            // 
            this.lblIdentity.AutoSize = true;
            this.lblIdentity.Location = new System.Drawing.Point(6, 38);
            this.lblIdentity.Name = "lblIdentity";
            this.lblIdentity.Size = new System.Drawing.Size(63, 13);
            this.lblIdentity.TabIndex = 2;
            this.lblIdentity.Text = "Identity File:";
            // 
            // pageLicenseSourceCollection
            // 
            this.pageLicenseSourceCollection.BackColor = System.Drawing.Color.Transparent;
            this.pageLicenseSourceCollection.Controls.Add(this.btnTrustedStorage);
            this.pageLicenseSourceCollection.Controls.Add(this.lblTrustedStorage);
            this.pageLicenseSourceCollection.Controls.Add(this.btnResetSC);
            this.pageLicenseSourceCollection.Controls.Add(this.ckbShortCode);
            this.pageLicenseSourceCollection.Controls.Add(this.btnResetTrials);
            this.pageLicenseSourceCollection.Controls.Add(this.btnResetTS);
            this.pageLicenseSourceCollection.Controls.Add(this.cbxLicenseFile);
            this.pageLicenseSourceCollection.Controls.Add(this.btnClear);
            this.pageLicenseSourceCollection.Controls.Add(this.ckbTrials);
            this.pageLicenseSourceCollection.Controls.Add(this.btnLoadLicenseFile);
            this.pageLicenseSourceCollection.Controls.Add(this.btnInspectLicenseFile);
            this.pageLicenseSourceCollection.Controls.Add(this.lblLoadComplete);
            this.pageLicenseSourceCollection.Controls.Add(this.btnLicenseFileBrowse);
            this.pageLicenseSourceCollection.Controls.Add(this.lblLicenseFile);
            this.pageLicenseSourceCollection.Controls.Add(this.tabLicenses);
            this.pageLicenseSourceCollection.Controls.Add(this.pnlTSInstances);
            this.pageLicenseSourceCollection.Location = new System.Drawing.Point(4, 22);
            this.pageLicenseSourceCollection.Name = "pageLicenseSourceCollection";
            this.pageLicenseSourceCollection.Padding = new System.Windows.Forms.Padding(3);
            this.pageLicenseSourceCollection.Size = new System.Drawing.Size(576, 566);
            this.pageLicenseSourceCollection.TabIndex = 0;
            this.pageLicenseSourceCollection.Text = "License Source Collection";
            this.pageLicenseSourceCollection.ToolTipText = "Work with local license sources";
            this.pageLicenseSourceCollection.UseVisualStyleBackColor = true;
            // 
            // btnTrustedStorage
            // 
            this.btnTrustedStorage.ImageIndex = 1;
            this.btnTrustedStorage.ImageList = this.licenseImageList;
            this.btnTrustedStorage.Location = new System.Drawing.Point(128, 7);
            this.btnTrustedStorage.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.btnTrustedStorage.Name = "btnTrustedStorage";
            this.btnTrustedStorage.Size = new System.Drawing.Size(24, 18);
            this.btnTrustedStorage.TabIndex = 2;
            this.btnTrustedStorage.UseVisualStyleBackColor = true;
            this.btnTrustedStorage.Click += new System.EventHandler(this.btnTrustedStorage_Click);
            // 
            // licenseImageList
            // 
            this.licenseImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("licenseImageList.ImageStream")));
            this.licenseImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.licenseImageList.Images.SetKeyName(0, "dialog-warning-3.ico");
            this.licenseImageList.Images.SetKeyName(1, "go-down-8.ico");
            // 
            // lblTrustedStorage
            // 
            this.lblTrustedStorage.BackColor = System.Drawing.Color.LightSteelBlue;
            this.lblTrustedStorage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTrustedStorage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrustedStorage.Location = new System.Drawing.Point(22, 9);
            this.lblTrustedStorage.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.lblTrustedStorage.Name = "lblTrustedStorage";
            this.lblTrustedStorage.Size = new System.Drawing.Size(108, 15);
            this.lblTrustedStorage.TabIndex = 1;
            this.lblTrustedStorage.Text = "Add Trusted Storage";
            this.FNEToolTip.SetToolTip(this.lblTrustedStorage, "Add trusted storage license sources to the license source collection");
            // 
            // btnResetSC
            // 
            this.btnResetSC.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnResetSC.BackColor = System.Drawing.Color.PaleTurquoise;
            this.btnResetSC.Location = new System.Drawing.Point(210, 55);
            this.btnResetSC.Name = "btnResetSC";
            this.btnResetSC.Size = new System.Drawing.Size(172, 23);
            this.btnResetSC.TabIndex = 9;
            this.btnResetSC.Text = "Reset Short Codes";
            this.FNEToolTip.SetToolTip(this.btnResetSC, "Delete all short code license source license information");
            this.btnResetSC.UseVisualStyleBackColor = false;
            this.btnResetSC.Click += new System.EventHandler(this.btnResetSC_Click);
            // 
            // ckbShortCode
            // 
            this.ckbShortCode.AutoSize = true;
            this.ckbShortCode.BackColor = System.Drawing.Color.PaleTurquoise;
            this.ckbShortCode.Location = new System.Drawing.Point(22, 56);
            this.ckbShortCode.Name = "ckbShortCode";
            this.ckbShortCode.Size = new System.Drawing.Size(106, 17);
            this.ckbShortCode.TabIndex = 8;
            this.ckbShortCode.Text = "Add Short Codes";
            this.FNEToolTip.SetToolTip(this.ckbShortCode, "Add the short code license source to the license source collection");
            this.ckbShortCode.UseVisualStyleBackColor = false;
            this.ckbShortCode.CheckedChanged += new System.EventHandler(this.ckbShortCode_CheckedChanged);
            // 
            // btnResetTrials
            // 
            this.btnResetTrials.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnResetTrials.BackColor = System.Drawing.Color.LightBlue;
            this.btnResetTrials.Location = new System.Drawing.Point(210, 31);
            this.btnResetTrials.Name = "btnResetTrials";
            this.btnResetTrials.Size = new System.Drawing.Size(172, 23);
            this.btnResetTrials.TabIndex = 7;
            this.btnResetTrials.Text = "Reset Trials";
            this.FNEToolTip.SetToolTip(this.btnResetTrials, "Delete all trials license source license information");
            this.btnResetTrials.UseVisualStyleBackColor = false;
            this.btnResetTrials.Click += new System.EventHandler(this.btnResetTrials_Click);
            // 
            // btnResetTS
            // 
            this.btnResetTS.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnResetTS.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnResetTS.Location = new System.Drawing.Point(210, 7);
            this.btnResetTS.Name = "btnResetTS";
            this.btnResetTS.Size = new System.Drawing.Size(172, 23);
            this.btnResetTS.TabIndex = 4;
            this.btnResetTS.Text = "Reset Trusted Storage";
            this.FNEToolTip.SetToolTip(this.btnResetTS, "Delete all trusted storage license source license information");
            this.btnResetTS.UseVisualStyleBackColor = false;
            this.btnResetTS.Click += new System.EventHandler(this.btnResetTS_Click);
            // 
            // cbxLicenseFile
            // 
            this.cbxLicenseFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxLicenseFile.FormattingEnabled = true;
            this.cbxLicenseFile.Location = new System.Drawing.Point(45, 498);
            this.cbxLicenseFile.MaxDropDownItems = 10;
            this.cbxLicenseFile.Name = "cbxLicenseFile";
            this.cbxLicenseFile.Size = new System.Drawing.Size(455, 21);
            this.cbxLicenseFile.TabIndex = 12;
            this.FNEToolTip.SetToolTip(this.cbxLicenseFile, "Enter a buffer license, trial license or capability response file location");
            this.cbxLicenseFile.TextChanged += new System.EventHandler(this.cbxLicenseFile_TextChanged);
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.BackColor = System.Drawing.Color.Gainsboro;
            this.btnClear.Location = new System.Drawing.Point(446, 7);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(103, 23);
            this.btnClear.TabIndex = 5;
            this.btnClear.Text = "Clear Collection";
            this.FNEToolTip.SetToolTip(this.btnClear, "Return all licenses and clear the license source collection");
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // ckbTrials
            // 
            this.ckbTrials.AutoSize = true;
            this.ckbTrials.BackColor = System.Drawing.Color.LightBlue;
            this.ckbTrials.Location = new System.Drawing.Point(22, 32);
            this.ckbTrials.Name = "ckbTrials";
            this.ckbTrials.Size = new System.Drawing.Size(73, 17);
            this.ckbTrials.TabIndex = 6;
            this.ckbTrials.Text = "Add Trials";
            this.FNEToolTip.SetToolTip(this.ckbTrials, "Add the trial license source to the license source collection");
            this.ckbTrials.UseVisualStyleBackColor = false;
            this.ckbTrials.CheckedChanged += new System.EventHandler(this.ckbTrials_CheckedChanged);
            // 
            // btnLoadLicenseFile
            // 
            this.btnLoadLicenseFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLoadLicenseFile.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnLoadLicenseFile.Location = new System.Drawing.Point(415, 539);
            this.btnLoadLicenseFile.Name = "btnLoadLicenseFile";
            this.btnLoadLicenseFile.Size = new System.Drawing.Size(80, 23);
            this.btnLoadLicenseFile.TabIndex = 16;
            this.btnLoadLicenseFile.Text = "Load File";
            this.FNEToolTip.SetToolTip(this.btnLoadLicenseFile, "Load features contained within the selected license file");
            this.btnLoadLicenseFile.UseVisualStyleBackColor = false;
            this.btnLoadLicenseFile.Click += new System.EventHandler(this.btnLoadLicenseFile_Click);
            // 
            // btnInspectLicenseFile
            // 
            this.btnInspectLicenseFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInspectLicenseFile.BackColor = System.Drawing.Color.Beige;
            this.btnInspectLicenseFile.Location = new System.Drawing.Point(309, 539);
            this.btnInspectLicenseFile.Name = "btnInspectLicenseFile";
            this.btnInspectLicenseFile.Size = new System.Drawing.Size(80, 23);
            this.btnInspectLicenseFile.TabIndex = 15;
            this.btnInspectLicenseFile.Text = "Inspect File";
            this.FNEToolTip.SetToolTip(this.btnInspectLicenseFile, "Inspect features contained within the selected license file");
            this.btnInspectLicenseFile.UseVisualStyleBackColor = false;
            this.btnInspectLicenseFile.Click += new System.EventHandler(this.btnInspectLicenseFile_Click);
            // 
            // lblLoadComplete
            // 
            this.lblLoadComplete.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLoadComplete.AutoEllipsis = true;
            this.lblLoadComplete.ForeColor = System.Drawing.Color.Blue;
            this.lblLoadComplete.Location = new System.Drawing.Point(19, 522);
            this.lblLoadComplete.Name = "lblLoadComplete";
            this.lblLoadComplete.Size = new System.Drawing.Size(534, 13);
            this.lblLoadComplete.TabIndex = 14;
            this.lblLoadComplete.Visible = false;
            // 
            // btnLicenseFileBrowse
            // 
            this.btnLicenseFileBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLicenseFileBrowse.ImageIndex = 1;
            this.btnLicenseFileBrowse.ImageList = this.imageList;
            this.btnLicenseFileBrowse.Location = new System.Drawing.Point(517, 497);
            this.btnLicenseFileBrowse.Name = "btnLicenseFileBrowse";
            this.btnLicenseFileBrowse.Size = new System.Drawing.Size(32, 23);
            this.btnLicenseFileBrowse.TabIndex = 13;
            this.FNEToolTip.SetToolTip(this.btnLicenseFileBrowse, "Browse for a license file");
            this.btnLicenseFileBrowse.UseVisualStyleBackColor = true;
            this.btnLicenseFileBrowse.Click += new System.EventHandler(this.btnLicenseFileBrowse_Click);
            // 
            // lblLicenseFile
            // 
            this.lblLicenseFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLicenseFile.AutoSize = true;
            this.lblLicenseFile.Location = new System.Drawing.Point(15, 501);
            this.lblLicenseFile.Name = "lblLicenseFile";
            this.lblLicenseFile.Size = new System.Drawing.Size(29, 13);
            this.lblLicenseFile.TabIndex = 11;
            this.lblLicenseFile.Text = "File: ";
            // 
            // tabLicenses
            // 
            this.tabLicenses.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabLicenses.Controls.Add(this.pageAllLicenses);
            this.tabLicenses.Controls.Add(this.pageTrustedStorageLicenses);
            this.tabLicenses.Controls.Add(this.pageTrialLicenses);
            this.tabLicenses.Controls.Add(this.pageShortCodeLicenses);
            this.tabLicenses.Controls.Add(this.pageBufferLicenses);
            this.tabLicenses.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabLicenses.Location = new System.Drawing.Point(20, 80);
            this.tabLicenses.Name = "tabLicenses";
            this.tabLicenses.Padding = new System.Drawing.Point(12, 3);
            this.tabLicenses.SelectedIndex = 0;
            this.tabLicenses.ShowToolTips = true;
            this.tabLicenses.Size = new System.Drawing.Size(533, 415);
            this.tabLicenses.TabIndex = 8;
            this.tabLicenses.DoubleClick += new System.EventHandler(this.lvFeature_DoubleClick);
            this.tabLicenses.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.tabLicenses_DrawItem);
            this.tabLicenses.SelectedIndexChanged += new System.EventHandler(this.tabLicenses_SelectedIndexChanged);
            // 
            // pageAllLicenses
            // 
            this.pageAllLicenses.Controls.Add(this.lvAllFeatures);
            this.pageAllLicenses.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pageAllLicenses.Location = new System.Drawing.Point(4, 22);
            this.pageAllLicenses.Margin = new System.Windows.Forms.Padding(2);
            this.pageAllLicenses.Name = "pageAllLicenses";
            this.pageAllLicenses.Size = new System.Drawing.Size(525, 389);
            this.pageAllLicenses.TabIndex = 3;
            this.pageAllLicenses.Text = "All Features (0)";
            this.pageAllLicenses.ToolTipText = "All available features in order of selection";
            this.pageAllLicenses.UseVisualStyleBackColor = true;
            // 
            // lvAllFeatures
            // 
            this.lvAllFeatures.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvAllFeatures.BackColor = System.Drawing.Color.Gainsboro;
            this.lvAllFeatures.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colAllType,
            this.colAllFeatureName,
            this.colAllFeatureVersion,
            this.colAllFeatureCount,
            this.colAllFeatureExpiration,
            this.colAllVendorString});
            this.lvAllFeatures.FullRowSelect = true;
            this.lvAllFeatures.GridLines = true;
            this.lvAllFeatures.Location = new System.Drawing.Point(0, 0);
            this.lvAllFeatures.MultiSelect = false;
            this.lvAllFeatures.Name = "lvAllFeatures";
            this.lvAllFeatures.OwnerDraw = true;
            this.lvAllFeatures.Size = new System.Drawing.Size(525, 389);
            this.lvAllFeatures.SmallImageList = this.licenseImageList;
            this.lvAllFeatures.TabIndex = 10;
            this.lvAllFeatures.UseCompatibleStateImageBehavior = false;
            this.lvAllFeatures.View = System.Windows.Forms.View.Details;
            this.lvAllFeatures.DrawColumnHeader += new System.Windows.Forms.DrawListViewColumnHeaderEventHandler(this.lvAllFeatures_DrawColumnHeader);
            this.lvAllFeatures.DrawItem += new System.Windows.Forms.DrawListViewItemEventHandler(this.lvAllFeatures_DrawItem);
            this.lvAllFeatures.DoubleClick += new System.EventHandler(this.lvFeature_DoubleClick);
            this.lvAllFeatures.MouseMove += new System.Windows.Forms.MouseEventHandler(this.lvAllFeatures_MouseMove);
            this.lvAllFeatures.DrawSubItem += new System.Windows.Forms.DrawListViewSubItemEventHandler(this.lvAllFeatures_DrawSubItem);
            this.lvAllFeatures.MouseLeave += new System.EventHandler(this.lvAllFeatures_MouseLeave);
            // 
            // colAllType
            // 
            this.colAllType.Text = "Type";
            // 
            // colAllFeatureName
            // 
            this.colAllFeatureName.Text = "Feature";
            this.colAllFeatureName.Width = 165;
            // 
            // colAllFeatureVersion
            // 
            this.colAllFeatureVersion.Text = "Version";
            this.colAllFeatureVersion.Width = 70;
            // 
            // colAllFeatureCount
            // 
            this.colAllFeatureCount.Text = "Count";
            this.colAllFeatureCount.Width = 75;
            // 
            // colAllFeatureExpiration
            // 
            this.colAllFeatureExpiration.Text = "Expiration";
            this.colAllFeatureExpiration.Width = 150;
            // 
            // colAllVendorString
            // 
            this.colAllVendorString.Text = "Vendor String";
            this.colAllVendorString.Width = 150;
            // 
            // pageTrustedStorageLicenses
            // 
            this.pageTrustedStorageLicenses.Controls.Add(this.lvTrustedStorageFeatures);
            this.pageTrustedStorageLicenses.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pageTrustedStorageLicenses.Location = new System.Drawing.Point(4, 22);
            this.pageTrustedStorageLicenses.Name = "pageTrustedStorageLicenses";
            this.pageTrustedStorageLicenses.Padding = new System.Windows.Forms.Padding(3);
            this.pageTrustedStorageLicenses.Size = new System.Drawing.Size(525, 389);
            this.pageTrustedStorageLicenses.TabIndex = 0;
            this.pageTrustedStorageLicenses.Text = "Trusted Storage (0)";
            this.pageTrustedStorageLicenses.ToolTipText = "Available features from trusted storage";
            this.pageTrustedStorageLicenses.UseVisualStyleBackColor = true;
            // 
            // lvTrustedStorageFeatures
            // 
            this.lvTrustedStorageFeatures.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvTrustedStorageFeatures.BackColor = System.Drawing.Color.LightSteelBlue;
            this.lvTrustedStorageFeatures.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colTSType,
            this.colTSFeatureName,
            this.colTSFeatureVersion,
            this.colTSFeatureCount,
            this.colTSFeatureExpiration,
            this.colTSVendorString});
            this.lvTrustedStorageFeatures.FullRowSelect = true;
            this.lvTrustedStorageFeatures.GridLines = true;
            this.lvTrustedStorageFeatures.Location = new System.Drawing.Point(0, 0);
            this.lvTrustedStorageFeatures.MultiSelect = false;
            this.lvTrustedStorageFeatures.Name = "lvTrustedStorageFeatures";
            this.lvTrustedStorageFeatures.ShowItemToolTips = true;
            this.lvTrustedStorageFeatures.Size = new System.Drawing.Size(525, 389);
            this.lvTrustedStorageFeatures.SmallImageList = this.licenseImageList;
            this.lvTrustedStorageFeatures.TabIndex = 1;
            this.lvTrustedStorageFeatures.UseCompatibleStateImageBehavior = false;
            this.lvTrustedStorageFeatures.View = System.Windows.Forms.View.Details;
            this.lvTrustedStorageFeatures.DoubleClick += new System.EventHandler(this.lvFeature_DoubleClick);
            // 
            // colTSType
            // 
            this.colTSType.Text = "Type";
            // 
            // colTSFeatureName
            // 
            this.colTSFeatureName.Text = "Feature";
            this.colTSFeatureName.Width = 165;
            // 
            // colTSFeatureVersion
            // 
            this.colTSFeatureVersion.Text = "Version";
            this.colTSFeatureVersion.Width = 70;
            // 
            // colTSFeatureCount
            // 
            this.colTSFeatureCount.Text = "Count";
            this.colTSFeatureCount.Width = 75;
            // 
            // colTSFeatureExpiration
            // 
            this.colTSFeatureExpiration.Text = "Expiration";
            this.colTSFeatureExpiration.Width = 150;
            // 
            // colTSVendorString
            // 
            this.colTSVendorString.Text = "Vendor String";
            this.colTSVendorString.Width = 150;
            // 
            // pageTrialLicenses
            // 
            this.pageTrialLicenses.Controls.Add(this.lvTrialFeatures);
            this.pageTrialLicenses.Location = new System.Drawing.Point(4, 22);
            this.pageTrialLicenses.Margin = new System.Windows.Forms.Padding(2);
            this.pageTrialLicenses.Name = "pageTrialLicenses";
            this.pageTrialLicenses.Size = new System.Drawing.Size(525, 389);
            this.pageTrialLicenses.TabIndex = 2;
            this.pageTrialLicenses.Text = "Trials (0)";
            this.pageTrialLicenses.ToolTipText = "Available features from trial license source";
            this.pageTrialLicenses.UseVisualStyleBackColor = true;
            // 
            // lvTrialFeatures
            // 
            this.lvTrialFeatures.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvTrialFeatures.BackColor = System.Drawing.Color.LightBlue;
            this.lvTrialFeatures.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colTrialType,
            this.colTrialFeatureName,
            this.colTrialFeatureVersion,
            this.colTrialFeatureCount,
            this.colTrialFeatureExpiration,
            this.colTrialVendorString});
            this.lvTrialFeatures.FullRowSelect = true;
            this.lvTrialFeatures.GridLines = true;
            this.lvTrialFeatures.Location = new System.Drawing.Point(0, 0);
            this.lvTrialFeatures.MultiSelect = false;
            this.lvTrialFeatures.Name = "lvTrialFeatures";
            this.lvTrialFeatures.ShowItemToolTips = true;
            this.lvTrialFeatures.Size = new System.Drawing.Size(525, 389);
            this.lvTrialFeatures.TabIndex = 1;
            this.lvTrialFeatures.UseCompatibleStateImageBehavior = false;
            this.lvTrialFeatures.View = System.Windows.Forms.View.Details;
            this.lvTrialFeatures.DoubleClick += new System.EventHandler(this.lvFeature_DoubleClick);
            // 
            // colTrialType
            // 
            this.colTrialType.Text = "Type";
            // 
            // colTrialFeatureName
            // 
            this.colTrialFeatureName.Text = "Feature";
            this.colTrialFeatureName.Width = 165;
            // 
            // colTrialFeatureVersion
            // 
            this.colTrialFeatureVersion.Text = "Version";
            this.colTrialFeatureVersion.Width = 70;
            // 
            // colTrialFeatureCount
            // 
            this.colTrialFeatureCount.Text = "Count";
            this.colTrialFeatureCount.Width = 75;
            // 
            // colTrialFeatureExpiration
            // 
            this.colTrialFeatureExpiration.Text = "Expiration";
            this.colTrialFeatureExpiration.Width = 150;
            // 
            // colTrialVendorString
            // 
            this.colTrialVendorString.Text = "Vendor String";
            this.colTrialVendorString.Width = 150;
            // 
            // pageShortCodeLicenses
            // 
            this.pageShortCodeLicenses.Controls.Add(this.lvShortCodeFeatures);
            this.pageShortCodeLicenses.Location = new System.Drawing.Point(4, 22);
            this.pageShortCodeLicenses.Name = "pageShortCodeLicenses";
            this.pageShortCodeLicenses.Size = new System.Drawing.Size(525, 389);
            this.pageShortCodeLicenses.TabIndex = 4;
            this.pageShortCodeLicenses.Text = "Short Codes (0)";
            this.pageShortCodeLicenses.ToolTipText = "Available features from short code license source";
            this.pageShortCodeLicenses.UseVisualStyleBackColor = true;
            // 
            // lvShortCodeFeatures
            // 
            this.lvShortCodeFeatures.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvShortCodeFeatures.BackColor = System.Drawing.Color.PaleTurquoise;
            this.lvShortCodeFeatures.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colSCType,
            this.colSCFeature,
            this.colSCVersion,
            this.colSCCount,
            this.colSCExpiration,
            this.colSCVendorString});
            this.lvShortCodeFeatures.FullRowSelect = true;
            this.lvShortCodeFeatures.GridLines = true;
            this.lvShortCodeFeatures.Location = new System.Drawing.Point(0, 0);
            this.lvShortCodeFeatures.MultiSelect = false;
            this.lvShortCodeFeatures.Name = "lvShortCodeFeatures";
            this.lvShortCodeFeatures.ShowItemToolTips = true;
            this.lvShortCodeFeatures.Size = new System.Drawing.Size(525, 389);
            this.lvShortCodeFeatures.TabIndex = 1;
            this.lvShortCodeFeatures.UseCompatibleStateImageBehavior = false;
            this.lvShortCodeFeatures.View = System.Windows.Forms.View.Details;
            this.lvShortCodeFeatures.DoubleClick += new System.EventHandler(this.lvFeature_DoubleClick);
            // 
            // colSCType
            // 
            this.colSCType.Text = "Type";
            // 
            // colSCFeature
            // 
            this.colSCFeature.Text = "Feature";
            this.colSCFeature.Width = 165;
            // 
            // colSCVersion
            // 
            this.colSCVersion.Text = "Version";
            this.colSCVersion.Width = 70;
            // 
            // colSCCount
            // 
            this.colSCCount.Text = "Count";
            this.colSCCount.Width = 75;
            // 
            // colSCExpiration
            // 
            this.colSCExpiration.Text = "Expiration";
            this.colSCExpiration.Width = 150;
            // 
            // colSCVendorString
            // 
            this.colSCVendorString.Text = "Vendor String";
            this.colSCVendorString.Width = 150;
            // 
            // pageBufferLicenses
            // 
            this.pageBufferLicenses.Controls.Add(this.lvBufferFeatures);
            this.pageBufferLicenses.Location = new System.Drawing.Point(4, 22);
            this.pageBufferLicenses.Name = "pageBufferLicenses";
            this.pageBufferLicenses.Padding = new System.Windows.Forms.Padding(3);
            this.pageBufferLicenses.Size = new System.Drawing.Size(525, 389);
            this.pageBufferLicenses.TabIndex = 1;
            this.pageBufferLicenses.Text = "Buffers (0)";
            this.pageBufferLicenses.ToolTipText = "Available features from all buffer license sources";
            this.pageBufferLicenses.UseVisualStyleBackColor = true;
            // 
            // lvBufferFeatures
            // 
            this.lvBufferFeatures.Alignment = System.Windows.Forms.ListViewAlignment.SnapToGrid;
            this.lvBufferFeatures.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvBufferFeatures.BackColor = System.Drawing.Color.Silver;
            this.lvBufferFeatures.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colBufferType,
            this.colBufferFeatureName,
            this.colBufferFeatureVersion,
            this.colBufferFeatrureCount,
            this.colBufferFeatureExpiration,
            this.colBufferVendorString});
            this.lvBufferFeatures.FullRowSelect = true;
            this.lvBufferFeatures.GridLines = true;
            this.lvBufferFeatures.Location = new System.Drawing.Point(0, 0);
            this.lvBufferFeatures.MultiSelect = false;
            this.lvBufferFeatures.Name = "lvBufferFeatures";
            this.lvBufferFeatures.ShowItemToolTips = true;
            this.lvBufferFeatures.Size = new System.Drawing.Size(525, 389);
            this.lvBufferFeatures.TabIndex = 1;
            this.lvBufferFeatures.UseCompatibleStateImageBehavior = false;
            this.lvBufferFeatures.View = System.Windows.Forms.View.Details;
            this.lvBufferFeatures.DoubleClick += new System.EventHandler(this.lvFeature_DoubleClick);
            // 
            // colBufferType
            // 
            this.colBufferType.Text = "Type";
            // 
            // colBufferFeatureName
            // 
            this.colBufferFeatureName.Text = "Feature";
            this.colBufferFeatureName.Width = 165;
            // 
            // colBufferFeatureVersion
            // 
            this.colBufferFeatureVersion.Text = "Version";
            this.colBufferFeatureVersion.Width = 70;
            // 
            // colBufferFeatrureCount
            // 
            this.colBufferFeatrureCount.Text = "Count";
            this.colBufferFeatrureCount.Width = 75;
            // 
            // colBufferFeatureExpiration
            // 
            this.colBufferFeatureExpiration.Text = "Expiration";
            this.colBufferFeatureExpiration.Width = 150;
            // 
            // colBufferVendorString
            // 
            this.colBufferVendorString.Text = "Vendor String";
            this.colBufferVendorString.Width = 150;
            // 
            // pnlTSInstances
            // 
            this.pnlTSInstances.BackColor = System.Drawing.Color.LightSteelBlue;
            this.pnlTSInstances.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTSInstances.Location = new System.Drawing.Point(22, 26);
            this.pnlTSInstances.Name = "pnlTSInstances";
            this.pnlTSInstances.Size = new System.Drawing.Size(180, 100);
            this.pnlTSInstances.TabIndex = 3;
            this.pnlTSInstances.Visible = false;
            this.pnlTSInstances.MouseLeave += new System.EventHandler(this.pnlTSInstances_Leave);
            // 
            // pageServer
            // 
            this.pageServer.Controls.Add(this.btnServerAdvanced);
            this.pageServer.Controls.Add(this.ckbHTTPS);
            this.pageServer.Controls.Add(this.tabServerRequest);
            this.pageServer.Controls.Add(this.cbxServerType);
            this.pageServer.Controls.Add(this.lblServerType);
            this.pageServer.Controls.Add(this.lblServerInfo);
            this.pageServer.Controls.Add(this.lblServerURL);
            this.pageServer.Controls.Add(this.txtServerURL);
            this.pageServer.Location = new System.Drawing.Point(4, 22);
            this.pageServer.Name = "pageServer";
            this.pageServer.Padding = new System.Windows.Forms.Padding(3);
            this.pageServer.Size = new System.Drawing.Size(576, 566);
            this.pageServer.TabIndex = 1;
            this.pageServer.Text = "Server Requests";
            this.pageServer.ToolTipText = "Interact with license servers";
            this.pageServer.UseVisualStyleBackColor = true;
            // 
            // btnServerAdvanced
            // 
            this.btnServerAdvanced.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnServerAdvanced.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnServerAdvanced.Location = new System.Drawing.Point(495, 31);
            this.btnServerAdvanced.Name = "btnServerAdvanced";
            this.btnServerAdvanced.Size = new System.Drawing.Size(75, 23);
            this.btnServerAdvanced.TabIndex = 4;
            this.btnServerAdvanced.Text = "Advanced";
            this.FNEToolTip.SetToolTip(this.btnServerAdvanced, "Add a proxy server or custom HTTP headers");
            this.btnServerAdvanced.UseVisualStyleBackColor = false;
            this.btnServerAdvanced.Click += new System.EventHandler(this.btnServerAdvanced_Click);
            // 
            // ckbHTTPS
            // 
            this.ckbHTTPS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ckbHTTPS.AutoSize = true;
            this.ckbHTTPS.Location = new System.Drawing.Point(420, 34);
            this.ckbHTTPS.Name = "ckbHTTPS";
            this.ckbHTTPS.Size = new System.Drawing.Size(62, 17);
            this.ckbHTTPS.TabIndex = 3;
            this.ckbHTTPS.Text = "HTTPS";
            this.FNEToolTip.SetToolTip(this.ckbHTTPS, "Communicate with license server using HTTPS ");
            this.ckbHTTPS.UseVisualStyleBackColor = true;
            this.ckbHTTPS.CheckedChanged += new System.EventHandler(this.ckbHTTPS_CheckedChanged);
            // 
            // tabServerRequest
            // 
            this.tabServerRequest.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabServerRequest.Controls.Add(this.pageCapabilityRequest);
            this.tabServerRequest.Controls.Add(this.pageInfoMessage);
            this.tabServerRequest.Location = new System.Drawing.Point(0, 110);
            this.tabServerRequest.Name = "tabServerRequest";
            this.tabServerRequest.SelectedIndex = 0;
            this.tabServerRequest.ShowToolTips = true;
            this.tabServerRequest.Size = new System.Drawing.Size(576, 453);
            this.tabServerRequest.TabIndex = 7;
            // 
            // pageCapabilityRequest
            // 
            this.pageCapabilityRequest.Controls.Add(this.btnCRSave);
            this.pageCapabilityRequest.Controls.Add(this.btnRespFile);
            this.pageCapabilityRequest.Controls.Add(this.txtRespFile);
            this.pageCapabilityRequest.Controls.Add(this.lblRespFile);
            this.pageCapabilityRequest.Controls.Add(this.ckbProcessResponse);
            this.pageCapabilityRequest.Controls.Add(this.btnCRSend);
            this.pageCapabilityRequest.Controls.Add(this.ckbForceResponse);
            this.pageCapabilityRequest.Controls.Add(this.tabCapabilityRequest);
            this.pageCapabilityRequest.Location = new System.Drawing.Point(4, 22);
            this.pageCapabilityRequest.Name = "pageCapabilityRequest";
            this.pageCapabilityRequest.Padding = new System.Windows.Forms.Padding(3);
            this.pageCapabilityRequest.Size = new System.Drawing.Size(568, 427);
            this.pageCapabilityRequest.TabIndex = 0;
            this.pageCapabilityRequest.Text = "Capability Request";
            this.pageCapabilityRequest.ToolTipText = "Specify capability request detail";
            this.pageCapabilityRequest.UseVisualStyleBackColor = true;
            // 
            // btnCRSave
            // 
            this.btnCRSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCRSave.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnCRSave.Location = new System.Drawing.Point(115, 395);
            this.btnCRSave.Name = "btnCRSave";
            this.btnCRSave.Size = new System.Drawing.Size(75, 23);
            this.btnCRSave.TabIndex = 15;
            this.btnCRSave.Text = "Save";
            this.FNEToolTip.SetToolTip(this.btnCRSave, "Save the binary capability request to a file");
            this.btnCRSave.UseVisualStyleBackColor = false;
            this.btnCRSave.Click += new System.EventHandler(this.btnCRSave_Click);
            // 
            // btnRespFile
            // 
            this.btnRespFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRespFile.ImageIndex = 1;
            this.btnRespFile.ImageList = this.imageList;
            this.btnRespFile.Location = new System.Drawing.Point(522, 36);
            this.btnRespFile.Name = "btnRespFile";
            this.btnRespFile.Size = new System.Drawing.Size(32, 23);
            this.btnRespFile.TabIndex = 6;
            this.btnRespFile.UseVisualStyleBackColor = true;
            this.btnRespFile.Click += new System.EventHandler(this.btnRespFile_Click);
            // 
            // txtRespFile
            // 
            this.txtRespFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRespFile.Location = new System.Drawing.Point(93, 37);
            this.txtRespFile.Name = "txtRespFile";
            this.txtRespFile.Size = new System.Drawing.Size(411, 20);
            this.txtRespFile.TabIndex = 5;
            this.FNEToolTip.SetToolTip(this.txtRespFile, "Optional file in which to store the capability response data");
            this.txtRespFile.TextChanged += new System.EventHandler(this.txtRespFile_TextChanged);
            // 
            // lblRespFile
            // 
            this.lblRespFile.AutoSize = true;
            this.lblRespFile.Location = new System.Drawing.Point(10, 40);
            this.lblRespFile.Name = "lblRespFile";
            this.lblRespFile.Size = new System.Drawing.Size(77, 13);
            this.lblRespFile.TabIndex = 4;
            this.lblRespFile.Text = "Response File:";
            // 
            // ckbProcessResponse
            // 
            this.ckbProcessResponse.AutoSize = true;
            this.ckbProcessResponse.Checked = true;
            this.ckbProcessResponse.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckbProcessResponse.Location = new System.Drawing.Point(350, 10);
            this.ckbProcessResponse.Name = "ckbProcessResponse";
            this.ckbProcessResponse.Size = new System.Drawing.Size(115, 17);
            this.ckbProcessResponse.TabIndex = 3;
            this.ckbProcessResponse.Text = "Process Response";
            this.FNEToolTip.SetToolTip(this.ckbProcessResponse, "Process the capability response into trusted storage");
            this.ckbProcessResponse.UseVisualStyleBackColor = true;
            this.ckbProcessResponse.CheckedChanged += new System.EventHandler(this.ckbProcessResponse_CheckedChanged);
            // 
            // btnCRSend
            // 
            this.btnCRSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCRSend.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnCRSend.Location = new System.Drawing.Point(10, 395);
            this.btnCRSend.Name = "btnCRSend";
            this.btnCRSend.Size = new System.Drawing.Size(75, 23);
            this.btnCRSend.TabIndex = 14;
            this.btnCRSend.Text = "Send";
            this.FNEToolTip.SetToolTip(this.btnCRSend, "Send the capability request to the license server");
            this.btnCRSend.UseVisualStyleBackColor = false;
            this.btnCRSend.Click += new System.EventHandler(this.btnCRSend_Click);
            // 
            // ckbForceResponse
            // 
            this.ckbForceResponse.AutoSize = true;
            this.ckbForceResponse.Checked = true;
            this.ckbForceResponse.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckbForceResponse.Location = new System.Drawing.Point(190, 10);
            this.ckbForceResponse.Name = "ckbForceResponse";
            this.ckbForceResponse.Size = new System.Drawing.Size(104, 17);
            this.ckbForceResponse.TabIndex = 2;
            this.ckbForceResponse.Text = "Force Response";
            this.FNEToolTip.SetToolTip(this.ckbForceResponse, "Force a capability response even if served features have not changed");
            this.ckbForceResponse.UseVisualStyleBackColor = true;
            this.ckbForceResponse.CheckedChanged += new System.EventHandler(this.ckbForceResponse_CheckedChanged);
            // 
            // tabCapabilityRequest
            // 
            this.tabCapabilityRequest.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabCapabilityRequest.Controls.Add(this.pageCR_Options);
            this.tabCapabilityRequest.Controls.Add(this.pageCR_LLS);
            this.tabCapabilityRequest.Controls.Add(this.pageCR_BO);
            this.tabCapabilityRequest.Controls.Add(this.pageCR_VD);
            this.tabCapabilityRequest.Controls.Add(this.pageCR_FS);
            this.tabCapabilityRequest.Controls.Add(this.pageCR_AuxHostIds);
            this.tabCapabilityRequest.Controls.Add(this.pageCR_Resp);
            this.tabCapabilityRequest.Location = new System.Drawing.Point(3, 70);
            this.tabCapabilityRequest.Name = "tabCapabilityRequest";
            this.tabCapabilityRequest.SelectedIndex = 0;
            this.tabCapabilityRequest.ShowToolTips = true;
            this.tabCapabilityRequest.Size = new System.Drawing.Size(562, 320);
            this.tabCapabilityRequest.TabIndex = 7;
            // 
            // pageCR_Options
            // 
            this.pageCR_Options.Controls.Add(this.cbxTSInstance);
            this.pageCR_Options.Controls.Add(this.lblTSInstance);
            this.pageCR_Options.Controls.Add(this.txtBorrowInterval);
            this.pageCR_Options.Controls.Add(this.lblBorrowInterval);
            this.pageCR_Options.Controls.Add(this.cbxBorrowGranularity);
            this.pageCR_Options.Controls.Add(this.lblBorrowGranularity);
            this.pageCR_Options.Controls.Add(this.cbxOperation);
            this.pageCR_Options.Controls.Add(this.lblOperation);
            this.pageCR_Options.Controls.Add(this.txtRequestorID);
            this.pageCR_Options.Controls.Add(this.lblRequestorID);
            this.pageCR_Options.Controls.Add(this.txtEnterpriseID);
            this.pageCR_Options.Controls.Add(this.lblEnterpriseID);
            this.pageCR_Options.Controls.Add(this.txtAcquisitionID);
            this.pageCR_Options.Controls.Add(this.lblAcquisitionID);
            this.pageCR_Options.Controls.Add(this.txtCorrelationID);
            this.pageCR_Options.Controls.Add(this.lblCorrelationID);
            this.pageCR_Options.Controls.Add(this.ckbOTA);
            this.pageCR_Options.Controls.Add(this.ckbIncremental);
            this.pageCR_Options.Controls.Add(this.ckbRequestAll);
            this.pageCR_Options.Location = new System.Drawing.Point(4, 22);
            this.pageCR_Options.Name = "pageCR_Options";
            this.pageCR_Options.Size = new System.Drawing.Size(554, 294);
            this.pageCR_Options.TabIndex = 5;
            this.pageCR_Options.Text = "Options";
            this.pageCR_Options.UseVisualStyleBackColor = true;
            // 
            // cbxTSInstance
            // 
            this.cbxTSInstance.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxTSInstance.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.cbxTSInstance.Location = new System.Drawing.Point(200, 48);
            this.cbxTSInstance.Name = "cbxTSInstance";
            this.cbxTSInstance.Size = new System.Drawing.Size(120, 21);
            this.cbxTSInstance.TabIndex = 4;
            this.FNEToolTip.SetToolTip(this.cbxTSInstance, "Select global license server capability request operation type");
            this.cbxTSInstance.SelectedIndexChanged += new System.EventHandler(this.cbxTSInstance_SelectedIndexChanged);
            // 
            // lblTSInstance
            // 
            this.lblTSInstance.AutoSize = true;
            this.lblTSInstance.Location = new System.Drawing.Point(10, 50);
            this.lblTSInstance.Name = "lblTSInstance";
            this.lblTSInstance.Size = new System.Drawing.Size(164, 13);
            this.lblTSInstance.TabIndex = 3;
            this.lblTSInstance.Text = "Trusted Storage Server Instance:";
            // 
            // txtBorrowInterval
            // 
            this.txtBorrowInterval.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBorrowInterval.Location = new System.Drawing.Point(420, 233);
            this.txtBorrowInterval.Name = "txtBorrowInterval";
            this.txtBorrowInterval.Size = new System.Drawing.Size(125, 20);
            this.txtBorrowInterval.TabIndex = 20;
            this.FNEToolTip.SetToolTip(this.txtBorrowInterval, "Optional served feature borrow time interval");
            this.txtBorrowInterval.TextChanged += new System.EventHandler(this.txtBorrowInterval_TextChanged);
            // 
            // lblBorrowInterval
            // 
            this.lblBorrowInterval.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBorrowInterval.AutoSize = true;
            this.lblBorrowInterval.Location = new System.Drawing.Point(330, 235);
            this.lblBorrowInterval.Name = "lblBorrowInterval";
            this.lblBorrowInterval.Size = new System.Drawing.Size(81, 13);
            this.lblBorrowInterval.TabIndex = 19;
            this.lblBorrowInterval.Text = "Borrow Interval:";
            // 
            // cbxBorrowGranularity
            // 
            this.cbxBorrowGranularity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxBorrowGranularity.Location = new System.Drawing.Point(110, 233);
            this.cbxBorrowGranularity.Name = "cbxBorrowGranularity";
            this.cbxBorrowGranularity.Size = new System.Drawing.Size(121, 21);
            this.cbxBorrowGranularity.TabIndex = 18;
            this.FNEToolTip.SetToolTip(this.cbxBorrowGranularity, "Served feature borrow time granularity");
            this.cbxBorrowGranularity.SelectedIndexChanged += new System.EventHandler(this.cbxBorrowGranularity_SelectedIndexChanged);
            // 
            // lblBorrowGranularity
            // 
            this.lblBorrowGranularity.AutoSize = true;
            this.lblBorrowGranularity.Location = new System.Drawing.Point(10, 235);
            this.lblBorrowGranularity.Name = "lblBorrowGranularity";
            this.lblBorrowGranularity.Size = new System.Drawing.Size(96, 13);
            this.lblBorrowGranularity.TabIndex = 17;
            this.lblBorrowGranularity.Text = "Borrow Granularity:";
            // 
            // cbxOperation
            // 
            this.cbxOperation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxOperation.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.cbxOperation.Location = new System.Drawing.Point(200, 13);
            this.cbxOperation.Name = "cbxOperation";
            this.cbxOperation.Size = new System.Drawing.Size(120, 21);
            this.cbxOperation.TabIndex = 2;
            this.FNEToolTip.SetToolTip(this.cbxOperation, "Select global license server capability request operation type");
            this.cbxOperation.SelectedIndexChanged += new System.EventHandler(this.cbxOperation_SelectedIndexChanged);
            // 
            // lblOperation
            // 
            this.lblOperation.AutoSize = true;
            this.lblOperation.Location = new System.Drawing.Point(10, 15);
            this.lblOperation.Name = "lblOperation";
            this.lblOperation.Size = new System.Drawing.Size(56, 13);
            this.lblOperation.TabIndex = 1;
            this.lblOperation.Text = "Operation:";
            // 
            // txtRequestorID
            // 
            this.txtRequestorID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRequestorID.Location = new System.Drawing.Point(95, 196);
            this.txtRequestorID.Name = "txtRequestorID";
            this.txtRequestorID.Size = new System.Drawing.Size(450, 20);
            this.txtRequestorID.TabIndex = 16;
            this.FNEToolTip.SetToolTip(this.txtRequestorID, "Optional capability request requestor ID");
            this.txtRequestorID.TextChanged += new System.EventHandler(this.txtRequestorID_TextChanged);
            // 
            // lblRequestorID
            // 
            this.lblRequestorID.AutoSize = true;
            this.lblRequestorID.Location = new System.Drawing.Point(10, 198);
            this.lblRequestorID.Name = "lblRequestorID";
            this.lblRequestorID.Size = new System.Drawing.Size(73, 13);
            this.lblRequestorID.TabIndex = 15;
            this.lblRequestorID.Text = "Requestor ID:";
            // 
            // txtEnterpriseID
            // 
            this.txtEnterpriseID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEnterpriseID.Location = new System.Drawing.Point(95, 159);
            this.txtEnterpriseID.Name = "txtEnterpriseID";
            this.txtEnterpriseID.Size = new System.Drawing.Size(450, 20);
            this.txtEnterpriseID.TabIndex = 14;
            this.FNEToolTip.SetToolTip(this.txtEnterpriseID, "Optional capability request enterprise ID");
            this.txtEnterpriseID.TextChanged += new System.EventHandler(this.txtEnterpriseID_TextChanged);
            // 
            // lblEnterpriseID
            // 
            this.lblEnterpriseID.AutoSize = true;
            this.lblEnterpriseID.Location = new System.Drawing.Point(10, 161);
            this.lblEnterpriseID.Name = "lblEnterpriseID";
            this.lblEnterpriseID.Size = new System.Drawing.Size(71, 13);
            this.lblEnterpriseID.TabIndex = 13;
            this.lblEnterpriseID.Text = "Enterprise ID:";
            // 
            // txtAcquisitionID
            // 
            this.txtAcquisitionID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAcquisitionID.Location = new System.Drawing.Point(95, 122);
            this.txtAcquisitionID.Name = "txtAcquisitionID";
            this.txtAcquisitionID.Size = new System.Drawing.Size(450, 20);
            this.txtAcquisitionID.TabIndex = 12;
            this.FNEToolTip.SetToolTip(this.txtAcquisitionID, "Optional capability request acquisition ID");
            this.txtAcquisitionID.TextChanged += new System.EventHandler(this.txtAcquisitionID_TextChanged);
            // 
            // lblAcquisitionID
            // 
            this.lblAcquisitionID.AutoSize = true;
            this.lblAcquisitionID.Location = new System.Drawing.Point(10, 124);
            this.lblAcquisitionID.Name = "lblAcquisitionID";
            this.lblAcquisitionID.Size = new System.Drawing.Size(75, 13);
            this.lblAcquisitionID.TabIndex = 11;
            this.lblAcquisitionID.Text = "Acquisition ID:";
            // 
            // txtCorrelationID
            // 
            this.txtCorrelationID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCorrelationID.Location = new System.Drawing.Point(95, 85);
            this.txtCorrelationID.Name = "txtCorrelationID";
            this.txtCorrelationID.Size = new System.Drawing.Size(450, 20);
            this.txtCorrelationID.TabIndex = 9;
            this.FNEToolTip.SetToolTip(this.txtCorrelationID, "Capability request correlation ID (required with global license server undo reque" +
                    "st)");
            this.txtCorrelationID.TextChanged += new System.EventHandler(this.txtCorrelationID_TextChanged);
            // 
            // lblCorrelationID
            // 
            this.lblCorrelationID.AutoSize = true;
            this.lblCorrelationID.Location = new System.Drawing.Point(10, 87);
            this.lblCorrelationID.Name = "lblCorrelationID";
            this.lblCorrelationID.Size = new System.Drawing.Size(74, 13);
            this.lblCorrelationID.TabIndex = 8;
            this.lblCorrelationID.Text = "Correlation ID:";
            // 
            // ckbOTA
            // 
            this.ckbOTA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ckbOTA.AutoSize = true;
            this.ckbOTA.Location = new System.Drawing.Point(420, 56);
            this.ckbOTA.Name = "ckbOTA";
            this.ckbOTA.Size = new System.Drawing.Size(122, 17);
            this.ckbOTA.TabIndex = 7;
            this.ckbOTA.Text = "One-Time Activation";
            this.FNEToolTip.SetToolTip(this.ckbOTA, "Not typically used except by device manufacturers for device activation");
            this.ckbOTA.UseVisualStyleBackColor = true;
            this.ckbOTA.CheckedChanged += new System.EventHandler(this.ckbOTA_CheckedChanged);
            // 
            // ckbIncremental
            // 
            this.ckbIncremental.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ckbIncremental.AutoSize = true;
            this.ckbIncremental.Location = new System.Drawing.Point(420, 10);
            this.ckbIncremental.Name = "ckbIncremental";
            this.ckbIncremental.Size = new System.Drawing.Size(81, 17);
            this.ckbIncremental.TabIndex = 6;
            this.ckbIncremental.Text = "Incremental";
            this.FNEToolTip.SetToolTip(this.ckbIncremental, "Incremental capability request");
            this.ckbIncremental.UseVisualStyleBackColor = true;
            this.ckbIncremental.CheckedChanged += new System.EventHandler(this.ckbIncremental_CheckedChanged);
            // 
            // ckbRequestAll
            // 
            this.ckbRequestAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ckbRequestAll.AutoSize = true;
            this.ckbRequestAll.Location = new System.Drawing.Point(420, 33);
            this.ckbRequestAll.Name = "ckbRequestAll";
            this.ckbRequestAll.Size = new System.Drawing.Size(124, 17);
            this.ckbRequestAll.TabIndex = 6;
            this.ckbRequestAll.Text = "Request All Features";
            this.FNEToolTip.SetToolTip(this.ckbRequestAll, "Request all available features for a preview operation");
            this.ckbRequestAll.UseVisualStyleBackColor = true;
            this.ckbRequestAll.CheckedChanged += new System.EventHandler(this.ckbRequestAll_CheckedChanged);
            // 
            // pageCR_LLS
            // 
            this.pageCR_LLS.Controls.Add(this.ckbLLSPartial);
            this.pageCR_LLS.Controls.Add(this.btnLLSRemove);
            this.pageCR_LLS.Controls.Add(this.txtLLSVersion);
            this.pageCR_LLS.Controls.Add(this.txtLLSCount);
            this.pageCR_LLS.Controls.Add(this.txtLLSFeature);
            this.pageCR_LLS.Controls.Add(this.lblLLSCount);
            this.pageCR_LLS.Controls.Add(this.lblLLSVersion);
            this.pageCR_LLS.Controls.Add(this.lblLLSFeature);
            this.pageCR_LLS.Controls.Add(this.btnLLSAdd);
            this.pageCR_LLS.Controls.Add(this.lvLLSFeatures);
            this.pageCR_LLS.Location = new System.Drawing.Point(4, 22);
            this.pageCR_LLS.Name = "pageCR_LLS";
            this.pageCR_LLS.Padding = new System.Windows.Forms.Padding(3);
            this.pageCR_LLS.Size = new System.Drawing.Size(554, 294);
            this.pageCR_LLS.TabIndex = 1;
            this.pageCR_LLS.Text = "Desired Features";
            this.pageCR_LLS.ToolTipText = "Local license server desired features";
            this.pageCR_LLS.UseVisualStyleBackColor = true;
            // 
            // ckbLLSPartial
            // 
            this.ckbLLSPartial.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ckbLLSPartial.AutoSize = true;
            this.ckbLLSPartial.Location = new System.Drawing.Point(350, 6);
            this.ckbLLSPartial.Name = "ckbLLSPartial";
            this.ckbLLSPartial.Size = new System.Drawing.Size(132, 17);
            this.ckbLLSPartial.TabIndex = 7;
            this.ckbLLSPartial.Text = "Allow Partial Fulfillment";
            this.FNEToolTip.SetToolTip(this.ckbLLSPartial, "Whether or not the license server can return partial feature fulfillment");
            this.ckbLLSPartial.UseVisualStyleBackColor = true;
            // 
            // btnLLSRemove
            // 
            this.btnLLSRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLLSRemove.BackColor = System.Drawing.Color.Gainsboro;
            this.btnLLSRemove.Enabled = false;
            this.btnLLSRemove.Location = new System.Drawing.Point(445, 53);
            this.btnLLSRemove.Name = "btnLLSRemove";
            this.btnLLSRemove.Size = new System.Drawing.Size(102, 23);
            this.btnLLSRemove.TabIndex = 9;
            this.btnLLSRemove.Text = "Remove Selected";
            this.FNEToolTip.SetToolTip(this.btnLLSRemove, "Remove selected features from the capability request");
            this.btnLLSRemove.UseVisualStyleBackColor = false;
            this.btnLLSRemove.Click += new System.EventHandler(this.btnLLSRemove_Click);
            // 
            // txtLLSVersion
            // 
            this.txtLLSVersion.Location = new System.Drawing.Point(70, 30);
            this.txtLLSVersion.Name = "txtLLSVersion";
            this.txtLLSVersion.Size = new System.Drawing.Size(147, 20);
            this.txtLLSVersion.TabIndex = 4;
            this.FNEToolTip.SetToolTip(this.txtLLSVersion, "Desired feature version");
            this.txtLLSVersion.TextChanged += new System.EventHandler(this.errorProvider_reset);
            // 
            // txtLLSCount
            // 
            this.txtLLSCount.BackColor = System.Drawing.SystemColors.Window;
            this.txtLLSCount.Location = new System.Drawing.Point(70, 54);
            this.txtLLSCount.Name = "txtLLSCount";
            this.txtLLSCount.Size = new System.Drawing.Size(87, 20);
            this.txtLLSCount.TabIndex = 6;
            this.FNEToolTip.SetToolTip(this.txtLLSCount, "Desired feature count");
            this.txtLLSCount.TextChanged += new System.EventHandler(this.txtLLSCount_TextChanged);
            // 
            // txtLLSFeature
            // 
            this.txtLLSFeature.Location = new System.Drawing.Point(70, 6);
            this.txtLLSFeature.Name = "txtLLSFeature";
            this.txtLLSFeature.Size = new System.Drawing.Size(210, 20);
            this.txtLLSFeature.TabIndex = 2;
            this.FNEToolTip.SetToolTip(this.txtLLSFeature, "Desired feature name");
            this.txtLLSFeature.TextChanged += new System.EventHandler(this.errorProvider_reset);
            // 
            // lblLLSCount
            // 
            this.lblLLSCount.AutoSize = true;
            this.lblLLSCount.Location = new System.Drawing.Point(15, 56);
            this.lblLLSCount.Name = "lblLLSCount";
            this.lblLLSCount.Size = new System.Drawing.Size(41, 13);
            this.lblLLSCount.TabIndex = 5;
            this.lblLLSCount.Text = "Count: ";
            // 
            // lblLLSVersion
            // 
            this.lblLLSVersion.AutoSize = true;
            this.lblLLSVersion.Location = new System.Drawing.Point(15, 32);
            this.lblLLSVersion.Name = "lblLLSVersion";
            this.lblLLSVersion.Size = new System.Drawing.Size(48, 13);
            this.lblLLSVersion.TabIndex = 3;
            this.lblLLSVersion.Text = "Version: ";
            // 
            // lblLLSFeature
            // 
            this.lblLLSFeature.AutoSize = true;
            this.lblLLSFeature.Location = new System.Drawing.Point(15, 8);
            this.lblLLSFeature.Name = "lblLLSFeature";
            this.lblLLSFeature.Size = new System.Drawing.Size(46, 13);
            this.lblLLSFeature.TabIndex = 1;
            this.lblLLSFeature.Text = "Feature:";
            // 
            // btnLLSAdd
            // 
            this.btnLLSAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLLSAdd.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnLLSAdd.Location = new System.Drawing.Point(350, 53);
            this.btnLLSAdd.Name = "btnLLSAdd";
            this.btnLLSAdd.Size = new System.Drawing.Size(65, 23);
            this.btnLLSAdd.TabIndex = 8;
            this.btnLLSAdd.Text = "Add";
            this.FNEToolTip.SetToolTip(this.btnLLSAdd, "Add the desired feature to the capability request");
            this.btnLLSAdd.UseVisualStyleBackColor = false;
            this.btnLLSAdd.Click += new System.EventHandler(this.btnLLSAdd_Click);
            // 
            // lvLLSFeatures
            // 
            this.lvLLSFeatures.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvLLSFeatures.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colLLSFeatureName,
            this.colLLSFeatureVersion,
            this.colLLSFeatureCount,
            this.colLLSFeaturePartial});
            this.lvLLSFeatures.FullRowSelect = true;
            this.lvLLSFeatures.GridLines = true;
            this.lvLLSFeatures.Location = new System.Drawing.Point(6, 83);
            this.lvLLSFeatures.Name = "lvLLSFeatures";
            this.lvLLSFeatures.Size = new System.Drawing.Size(540, 208);
            this.lvLLSFeatures.TabIndex = 10;
            this.lvLLSFeatures.UseCompatibleStateImageBehavior = false;
            this.lvLLSFeatures.View = System.Windows.Forms.View.Details;
            this.lvLLSFeatures.SelectedIndexChanged += new System.EventHandler(this.lvLLSFeatures_SelectedIndexChanged);
            // 
            // colLLSFeatureName
            // 
            this.colLLSFeatureName.Text = "Feature";
            this.colLLSFeatureName.Width = 270;
            // 
            // colLLSFeatureVersion
            // 
            this.colLLSFeatureVersion.Text = "Version";
            this.colLLSFeatureVersion.Width = 155;
            // 
            // colLLSFeatureCount
            // 
            this.colLLSFeatureCount.Text = "Count";
            // 
            // colLLSFeaturePartial
            // 
            this.colLLSFeaturePartial.Text = "Partial";
            this.colLLSFeaturePartial.Width = 49;
            // 
            // pageCR_BO
            // 
            this.pageCR_BO.Controls.Add(this.ckbBOPartial);
            this.pageCR_BO.Controls.Add(this.btnBORemove);
            this.pageCR_BO.Controls.Add(this.txtBOCount);
            this.pageCR_BO.Controls.Add(this.txtBORights);
            this.pageCR_BO.Controls.Add(this.lblBOCount);
            this.pageCR_BO.Controls.Add(this.lblBORights);
            this.pageCR_BO.Controls.Add(this.btnBOAdd);
            this.pageCR_BO.Controls.Add(this.lvBORights);
            this.pageCR_BO.Location = new System.Drawing.Point(4, 22);
            this.pageCR_BO.Name = "pageCR_BO";
            this.pageCR_BO.Size = new System.Drawing.Size(554, 294);
            this.pageCR_BO.TabIndex = 3;
            this.pageCR_BO.Text = "Rights IDs";
            this.pageCR_BO.ToolTipText = "Back office license server requested rights";
            this.pageCR_BO.UseVisualStyleBackColor = true;
            // 
            // ckbBOPartial
            // 
            this.ckbBOPartial.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ckbBOPartial.AutoSize = true;
            this.ckbBOPartial.Location = new System.Drawing.Point(350, 6);
            this.ckbBOPartial.Name = "ckbBOPartial";
            this.ckbBOPartial.Size = new System.Drawing.Size(132, 17);
            this.ckbBOPartial.TabIndex = 5;
            this.ckbBOPartial.Text = "Allow Partial Fulfillment";
            this.FNEToolTip.SetToolTip(this.ckbBOPartial, "Whether or not the license server can return partial feature fulfillment");
            this.ckbBOPartial.UseVisualStyleBackColor = true;
            // 
            // btnBORemove
            // 
            this.btnBORemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBORemove.BackColor = System.Drawing.Color.Gainsboro;
            this.btnBORemove.Enabled = false;
            this.btnBORemove.Location = new System.Drawing.Point(445, 31);
            this.btnBORemove.Name = "btnBORemove";
            this.btnBORemove.Size = new System.Drawing.Size(102, 23);
            this.btnBORemove.TabIndex = 7;
            this.btnBORemove.Text = "Remove Selected";
            this.FNEToolTip.SetToolTip(this.btnBORemove, "Remove selected rights information from the capability request");
            this.btnBORemove.UseVisualStyleBackColor = false;
            this.btnBORemove.Click += new System.EventHandler(this.btnBORemove_Click);
            // 
            // txtBOCount
            // 
            this.txtBOCount.BackColor = System.Drawing.SystemColors.Window;
            this.txtBOCount.Location = new System.Drawing.Point(70, 30);
            this.txtBOCount.Name = "txtBOCount";
            this.txtBOCount.Size = new System.Drawing.Size(87, 20);
            this.txtBOCount.TabIndex = 4;
            this.FNEToolTip.SetToolTip(this.txtBOCount, "Requested rights count");
            this.txtBOCount.TextChanged += new System.EventHandler(this.txtBOCount_TextChanged);
            // 
            // txtBORights
            // 
            this.txtBORights.Location = new System.Drawing.Point(70, 6);
            this.txtBORights.Name = "txtBORights";
            this.txtBORights.Size = new System.Drawing.Size(210, 20);
            this.txtBORights.TabIndex = 2;
            this.FNEToolTip.SetToolTip(this.txtBORights, "Requested rights id");
            this.txtBORights.TextChanged += new System.EventHandler(this.errorProvider_reset);
            // 
            // lblBOCount
            // 
            this.lblBOCount.AutoSize = true;
            this.lblBOCount.Location = new System.Drawing.Point(15, 32);
            this.lblBOCount.Name = "lblBOCount";
            this.lblBOCount.Size = new System.Drawing.Size(41, 13);
            this.lblBOCount.TabIndex = 3;
            this.lblBOCount.Text = "Count: ";
            // 
            // lblBORights
            // 
            this.lblBORights.AutoSize = true;
            this.lblBORights.Location = new System.Drawing.Point(15, 8);
            this.lblBORights.Name = "lblBORights";
            this.lblBORights.Size = new System.Drawing.Size(54, 13);
            this.lblBORights.TabIndex = 1;
            this.lblBORights.Text = "Rights ID:";
            // 
            // btnBOAdd
            // 
            this.btnBOAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBOAdd.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnBOAdd.Location = new System.Drawing.Point(350, 31);
            this.btnBOAdd.Name = "btnBOAdd";
            this.btnBOAdd.Size = new System.Drawing.Size(65, 23);
            this.btnBOAdd.TabIndex = 6;
            this.btnBOAdd.Text = "Add";
            this.FNEToolTip.SetToolTip(this.btnBOAdd, "Add the rights information to the capability request");
            this.btnBOAdd.UseVisualStyleBackColor = false;
            this.btnBOAdd.Click += new System.EventHandler(this.btnBOAdd_Click);
            // 
            // lvBORights
            // 
            this.lvBORights.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvBORights.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colBORightsName,
            this.colBORightsCount,
            this.colBORightsPartial});
            this.lvBORights.FullRowSelect = true;
            this.lvBORights.GridLines = true;
            this.lvBORights.Location = new System.Drawing.Point(6, 64);
            this.lvBORights.Name = "lvBORights";
            this.lvBORights.Size = new System.Drawing.Size(540, 227);
            this.lvBORights.TabIndex = 8;
            this.lvBORights.UseCompatibleStateImageBehavior = false;
            this.lvBORights.View = System.Windows.Forms.View.Details;
            this.lvBORights.SelectedIndexChanged += new System.EventHandler(this.lvBORights_SelectedIndexChanged);
            // 
            // colBORightsName
            // 
            this.colBORightsName.Text = "Rights ID";
            this.colBORightsName.Width = 405;
            // 
            // colBORightsCount
            // 
            this.colBORightsCount.Text = "Count";
            this.colBORightsCount.Width = 80;
            // 
            // colBORightsPartial
            // 
            this.colBORightsPartial.Text = "Partial";
            this.colBORightsPartial.Width = 49;
            // 
            // pageCR_VD
            // 
            this.pageCR_VD.Controls.Add(this.cbxVDType);
            this.pageCR_VD.Controls.Add(this.btnVDRemove);
            this.pageCR_VD.Controls.Add(this.txtVDKey);
            this.pageCR_VD.Controls.Add(this.txtVDValue);
            this.pageCR_VD.Controls.Add(this.lblVDValue);
            this.pageCR_VD.Controls.Add(this.lblVDKey);
            this.pageCR_VD.Controls.Add(this.lblVDType);
            this.pageCR_VD.Controls.Add(this.btnVDAdd);
            this.pageCR_VD.Controls.Add(this.lvVendorDictionary);
            this.pageCR_VD.Location = new System.Drawing.Point(4, 22);
            this.pageCR_VD.Name = "pageCR_VD";
            this.pageCR_VD.Size = new System.Drawing.Size(554, 294);
            this.pageCR_VD.TabIndex = 4;
            this.pageCR_VD.Text = "Vendor Dictionary";
            this.pageCR_VD.ToolTipText = "Specify capability request vendor dictionary items";
            this.pageCR_VD.UseVisualStyleBackColor = true;
            // 
            // cbxVDType
            // 
            this.cbxVDType.Location = new System.Drawing.Point(70, 6);
            this.cbxVDType.Name = "cbxVDType";
            this.cbxVDType.Size = new System.Drawing.Size(121, 21);
            this.cbxVDType.TabIndex = 2;
            this.FNEToolTip.SetToolTip(this.cbxVDType, "Select string or integer vendor dictionary item type");
            this.cbxVDType.SelectedIndexChanged += new System.EventHandler(this.cbxVDType_SelectedIndexChanged);
            // 
            // btnVDRemove
            // 
            this.btnVDRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVDRemove.BackColor = System.Drawing.Color.Gainsboro;
            this.btnVDRemove.Enabled = false;
            this.btnVDRemove.Location = new System.Drawing.Point(445, 53);
            this.btnVDRemove.Name = "btnVDRemove";
            this.btnVDRemove.Size = new System.Drawing.Size(102, 23);
            this.btnVDRemove.TabIndex = 8;
            this.btnVDRemove.Text = "Remove Selected";
            this.FNEToolTip.SetToolTip(this.btnVDRemove, "Remove selected feature selector items from the capability request");
            this.btnVDRemove.UseVisualStyleBackColor = false;
            this.btnVDRemove.Click += new System.EventHandler(this.btnVDRemove_Click);
            // 
            // txtVDKey
            // 
            this.txtVDKey.Location = new System.Drawing.Point(70, 30);
            this.txtVDKey.Name = "txtVDKey";
            this.txtVDKey.Size = new System.Drawing.Size(230, 20);
            this.txtVDKey.TabIndex = 4;
            this.FNEToolTip.SetToolTip(this.txtVDKey, "Specify feature selector item key");
            this.txtVDKey.TextChanged += new System.EventHandler(this.errorProvider_reset);
            // 
            // txtVDValue
            // 
            this.txtVDValue.BackColor = System.Drawing.SystemColors.Window;
            this.txtVDValue.Location = new System.Drawing.Point(70, 54);
            this.txtVDValue.Name = "txtVDValue";
            this.txtVDValue.Size = new System.Drawing.Size(230, 20);
            this.txtVDValue.TabIndex = 6;
            this.FNEToolTip.SetToolTip(this.txtVDValue, "Specify feature selector item value");
            this.txtVDValue.TextChanged += new System.EventHandler(this.txtVDValue_TextChanged);
            // 
            // lblVDValue
            // 
            this.lblVDValue.AutoSize = true;
            this.lblVDValue.Location = new System.Drawing.Point(15, 56);
            this.lblVDValue.Name = "lblVDValue";
            this.lblVDValue.Size = new System.Drawing.Size(37, 13);
            this.lblVDValue.TabIndex = 5;
            this.lblVDValue.Text = "Value:";
            // 
            // lblVDKey
            // 
            this.lblVDKey.AutoSize = true;
            this.lblVDKey.Location = new System.Drawing.Point(15, 32);
            this.lblVDKey.Name = "lblVDKey";
            this.lblVDKey.Size = new System.Drawing.Size(28, 13);
            this.lblVDKey.TabIndex = 3;
            this.lblVDKey.Text = "Key:";
            // 
            // lblVDType
            // 
            this.lblVDType.AutoSize = true;
            this.lblVDType.Location = new System.Drawing.Point(15, 8);
            this.lblVDType.Name = "lblVDType";
            this.lblVDType.Size = new System.Drawing.Size(34, 13);
            this.lblVDType.TabIndex = 1;
            this.lblVDType.Text = "Type:";
            // 
            // btnVDAdd
            // 
            this.btnVDAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVDAdd.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnVDAdd.Location = new System.Drawing.Point(370, 53);
            this.btnVDAdd.Name = "btnVDAdd";
            this.btnVDAdd.Size = new System.Drawing.Size(65, 23);
            this.btnVDAdd.TabIndex = 7;
            this.btnVDAdd.Text = "Add";
            this.FNEToolTip.SetToolTip(this.btnVDAdd, "Add feature selector item to the capability request");
            this.btnVDAdd.UseVisualStyleBackColor = false;
            this.btnVDAdd.Click += new System.EventHandler(this.btnVDAdd_Click);
            // 
            // lvVendorDictionary
            // 
            this.lvVendorDictionary.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvVendorDictionary.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colVDType,
            this.colVDKey,
            this.colVDValue});
            this.lvVendorDictionary.FullRowSelect = true;
            this.lvVendorDictionary.GridLines = true;
            this.lvVendorDictionary.Location = new System.Drawing.Point(6, 83);
            this.lvVendorDictionary.Name = "lvVendorDictionary";
            this.lvVendorDictionary.Size = new System.Drawing.Size(540, 208);
            this.lvVendorDictionary.TabIndex = 9;
            this.lvVendorDictionary.UseCompatibleStateImageBehavior = false;
            this.lvVendorDictionary.View = System.Windows.Forms.View.Details;
            this.lvVendorDictionary.SelectedIndexChanged += new System.EventHandler(this.lvVendorDictionary_SelectedIndexChanged);
            // 
            // colVDType
            // 
            this.colVDType.Text = "Type";
            this.colVDType.Width = 77;
            // 
            // colVDKey
            // 
            this.colVDKey.Text = "Key";
            this.colVDKey.Width = 230;
            // 
            // colVDValue
            // 
            this.colVDValue.Text = "Value";
            this.colVDValue.Width = 230;
            // 
            // pageCR_FS
            // 
            this.pageCR_FS.Controls.Add(this.cbxFSType);
            this.pageCR_FS.Controls.Add(this.btnFSRemove);
            this.pageCR_FS.Controls.Add(this.txtFSKey);
            this.pageCR_FS.Controls.Add(this.txtFSValue);
            this.pageCR_FS.Controls.Add(this.lblFSValue);
            this.pageCR_FS.Controls.Add(this.lblFSKey);
            this.pageCR_FS.Controls.Add(this.lblFSType);
            this.pageCR_FS.Controls.Add(this.btnFSAdd);
            this.pageCR_FS.Controls.Add(this.lvFeatureSelectors);
            this.pageCR_FS.Location = new System.Drawing.Point(4, 22);
            this.pageCR_FS.Name = "pageCR_FS";
            this.pageCR_FS.Size = new System.Drawing.Size(554, 294);
            this.pageCR_FS.TabIndex = 6;
            this.pageCR_FS.Text = "Feature Selectors";
            this.pageCR_FS.UseVisualStyleBackColor = true;
            // 
            // cbxFSType
            // 
            this.cbxFSType.Enabled = false;
            this.cbxFSType.Items.AddRange(new object[] {
            "String"});
            this.cbxFSType.Location = new System.Drawing.Point(71, 5);
            this.cbxFSType.Name = "cbxFSType";
            this.cbxFSType.Size = new System.Drawing.Size(121, 21);
            this.cbxFSType.TabIndex = 2;
            this.FNEToolTip.SetToolTip(this.cbxFSType, "Only a feature selector type of string is currently supported");
            // 
            // btnFSRemove
            // 
            this.btnFSRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFSRemove.BackColor = System.Drawing.Color.Gainsboro;
            this.btnFSRemove.Enabled = false;
            this.btnFSRemove.Location = new System.Drawing.Point(446, 52);
            this.btnFSRemove.Name = "btnFSRemove";
            this.btnFSRemove.Size = new System.Drawing.Size(102, 23);
            this.btnFSRemove.TabIndex = 8;
            this.btnFSRemove.Text = "Remove Selected";
            this.FNEToolTip.SetToolTip(this.btnFSRemove, "Remove selected vendor dictionary items from the capability request");
            this.btnFSRemove.UseVisualStyleBackColor = false;
            this.btnFSRemove.Click += new System.EventHandler(this.btnFSRemove_Click);
            // 
            // txtFSKey
            // 
            this.txtFSKey.Location = new System.Drawing.Point(71, 29);
            this.txtFSKey.Name = "txtFSKey";
            this.txtFSKey.Size = new System.Drawing.Size(230, 20);
            this.txtFSKey.TabIndex = 4;
            this.FNEToolTip.SetToolTip(this.txtFSKey, "Specify vendor dictionary item key");
            this.txtFSKey.TextChanged += new System.EventHandler(this.errorProvider_reset);
            // 
            // txtFSValue
            // 
            this.txtFSValue.BackColor = System.Drawing.SystemColors.Window;
            this.txtFSValue.Location = new System.Drawing.Point(71, 53);
            this.txtFSValue.Name = "txtFSValue";
            this.txtFSValue.Size = new System.Drawing.Size(230, 20);
            this.txtFSValue.TabIndex = 6;
            this.FNEToolTip.SetToolTip(this.txtFSValue, "Specify vendor dictionary item value");
            this.txtFSValue.TextChanged += new System.EventHandler(this.errorProvider_reset);
            // 
            // lblFSValue
            // 
            this.lblFSValue.AutoSize = true;
            this.lblFSValue.Location = new System.Drawing.Point(16, 55);
            this.lblFSValue.Name = "lblFSValue";
            this.lblFSValue.Size = new System.Drawing.Size(37, 13);
            this.lblFSValue.TabIndex = 5;
            this.lblFSValue.Text = "Value:";
            // 
            // lblFSKey
            // 
            this.lblFSKey.AutoSize = true;
            this.lblFSKey.Location = new System.Drawing.Point(16, 31);
            this.lblFSKey.Name = "lblFSKey";
            this.lblFSKey.Size = new System.Drawing.Size(28, 13);
            this.lblFSKey.TabIndex = 3;
            this.lblFSKey.Text = "Key:";
            // 
            // lblFSType
            // 
            this.lblFSType.AutoSize = true;
            this.lblFSType.Location = new System.Drawing.Point(16, 7);
            this.lblFSType.Name = "lblFSType";
            this.lblFSType.Size = new System.Drawing.Size(34, 13);
            this.lblFSType.TabIndex = 1;
            this.lblFSType.Text = "Type:";
            // 
            // btnFSAdd
            // 
            this.btnFSAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFSAdd.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnFSAdd.Location = new System.Drawing.Point(371, 52);
            this.btnFSAdd.Name = "btnFSAdd";
            this.btnFSAdd.Size = new System.Drawing.Size(65, 23);
            this.btnFSAdd.TabIndex = 7;
            this.btnFSAdd.Text = "Add";
            this.FNEToolTip.SetToolTip(this.btnFSAdd, "Add vendor dictionary item to the capability request");
            this.btnFSAdd.UseVisualStyleBackColor = false;
            this.btnFSAdd.Click += new System.EventHandler(this.btnFSAdd_Click);
            // 
            // lvFeatureSelectors
            // 
            this.lvFeatureSelectors.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvFeatureSelectors.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colFSType,
            this.colFSKey,
            this.colFSValue});
            this.lvFeatureSelectors.FullRowSelect = true;
            this.lvFeatureSelectors.GridLines = true;
            this.lvFeatureSelectors.Location = new System.Drawing.Point(7, 82);
            this.lvFeatureSelectors.Name = "lvFeatureSelectors";
            this.lvFeatureSelectors.Size = new System.Drawing.Size(540, 208);
            this.lvFeatureSelectors.TabIndex = 9;
            this.lvFeatureSelectors.UseCompatibleStateImageBehavior = false;
            this.lvFeatureSelectors.View = System.Windows.Forms.View.Details;
            this.lvFeatureSelectors.SelectedIndexChanged += new System.EventHandler(this.lvFeatureSelectors_SelectedIndexChanged);
            // 
            // colFSType
            // 
            this.colFSType.Text = "Type";
            this.colFSType.Width = 77;
            // 
            // colFSKey
            // 
            this.colFSKey.Text = "Key";
            this.colFSKey.Width = 230;
            // 
            // colFSValue
            // 
            this.colFSValue.Text = "Value";
            this.colFSValue.Width = 230;
            // 
            // pageCR_AuxHostIds
            // 
            this.pageCR_AuxHostIds.Controls.Add(this.cbxAuxHostIdType);
            this.pageCR_AuxHostIds.Controls.Add(this.btnAuxHostIdRemove);
            this.pageCR_AuxHostIds.Controls.Add(this.txtAuxHostIdValue);
            this.pageCR_AuxHostIds.Controls.Add(this.lblAuxHostIdValue);
            this.pageCR_AuxHostIds.Controls.Add(this.lblAuxHostIdType);
            this.pageCR_AuxHostIds.Controls.Add(this.btnAuxHostIdAdd);
            this.pageCR_AuxHostIds.Controls.Add(this.lvAuxHostIds);
            this.pageCR_AuxHostIds.Location = new System.Drawing.Point(4, 22);
            this.pageCR_AuxHostIds.Name = "pageCR_AuxHostIds";
            this.pageCR_AuxHostIds.Size = new System.Drawing.Size(554, 294);
            this.pageCR_AuxHostIds.TabIndex = 7;
            this.pageCR_AuxHostIds.Text = "Aux Host IDs";
            this.pageCR_AuxHostIds.UseVisualStyleBackColor = true;
            // 
            // cbxAuxHostIdType
            // 
            this.cbxAuxHostIdType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxAuxHostIdType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxAuxHostIdType.FormattingEnabled = true;
            this.cbxAuxHostIdType.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cbxAuxHostIdType.Location = new System.Drawing.Point(105, 4);
            this.cbxAuxHostIdType.Name = "cbxAuxHostIdType";
            this.cbxAuxHostIdType.Size = new System.Drawing.Size(246, 21);
            this.cbxAuxHostIdType.TabIndex = 2;
            this.FNEToolTip.SetToolTip(this.cbxAuxHostIdType, "Chose between a custom string hostid type or other types available in this host e" +
                    "nvironment");
            this.cbxAuxHostIdType.SelectedIndexChanged += new System.EventHandler(this.errorProvider_reset);
            // 
            // btnAuxHostIdRemove
            // 
            this.btnAuxHostIdRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAuxHostIdRemove.BackColor = System.Drawing.Color.Gainsboro;
            this.btnAuxHostIdRemove.Enabled = false;
            this.btnAuxHostIdRemove.Location = new System.Drawing.Point(446, 57);
            this.btnAuxHostIdRemove.Name = "btnAuxHostIdRemove";
            this.btnAuxHostIdRemove.Size = new System.Drawing.Size(102, 23);
            this.btnAuxHostIdRemove.TabIndex = 6;
            this.btnAuxHostIdRemove.Text = "Remove Selected";
            this.FNEToolTip.SetToolTip(this.btnAuxHostIdRemove, "Remove selected features from the capability request");
            this.btnAuxHostIdRemove.UseVisualStyleBackColor = false;
            this.btnAuxHostIdRemove.Click += new System.EventHandler(this.btnAuxHostIdRemove_Click);
            // 
            // txtAuxHostIdValue
            // 
            this.txtAuxHostIdValue.Location = new System.Drawing.Point(105, 31);
            this.txtAuxHostIdValue.Name = "txtAuxHostIdValue";
            this.txtAuxHostIdValue.Size = new System.Drawing.Size(440, 20);
            this.txtAuxHostIdValue.TabIndex = 4;
            this.FNEToolTip.SetToolTip(this.txtAuxHostIdValue, "Desired feature version");
            this.txtAuxHostIdValue.TextChanged += new System.EventHandler(this.errorProvider_reset);
            // 
            // lblAuxHostIdValue
            // 
            this.lblAuxHostIdValue.AutoSize = true;
            this.lblAuxHostIdValue.Location = new System.Drawing.Point(16, 33);
            this.lblAuxHostIdValue.Name = "lblAuxHostIdValue";
            this.lblAuxHostIdValue.Size = new System.Drawing.Size(76, 13);
            this.lblAuxHostIdValue.TabIndex = 3;
            this.lblAuxHostIdValue.Text = "Host ID Value:";
            // 
            // lblAuxHostIdType
            // 
            this.lblAuxHostIdType.AutoSize = true;
            this.lblAuxHostIdType.Location = new System.Drawing.Point(16, 7);
            this.lblAuxHostIdType.Name = "lblAuxHostIdType";
            this.lblAuxHostIdType.Size = new System.Drawing.Size(73, 13);
            this.lblAuxHostIdType.TabIndex = 1;
            this.lblAuxHostIdType.Text = "Host ID Type:";
            // 
            // btnAuxHostIdAdd
            // 
            this.btnAuxHostIdAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAuxHostIdAdd.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnAuxHostIdAdd.Location = new System.Drawing.Point(351, 57);
            this.btnAuxHostIdAdd.Name = "btnAuxHostIdAdd";
            this.btnAuxHostIdAdd.Size = new System.Drawing.Size(65, 23);
            this.btnAuxHostIdAdd.TabIndex = 18;
            this.btnAuxHostIdAdd.Text = "Add";
            this.FNEToolTip.SetToolTip(this.btnAuxHostIdAdd, "Add the desired feature to the capability request");
            this.btnAuxHostIdAdd.UseVisualStyleBackColor = false;
            this.btnAuxHostIdAdd.Click += new System.EventHandler(this.btnAuxHostIdAdd_Click);
            // 
            // lvAuxHostIds
            // 
            this.lvAuxHostIds.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvAuxHostIds.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colAuxHostIdType,
            this.colAuxHostIdValue});
            this.lvAuxHostIds.FullRowSelect = true;
            this.lvAuxHostIds.GridLines = true;
            this.lvAuxHostIds.Location = new System.Drawing.Point(7, 82);
            this.lvAuxHostIds.Name = "lvAuxHostIds";
            this.lvAuxHostIds.Size = new System.Drawing.Size(540, 208);
            this.lvAuxHostIds.TabIndex = 7;
            this.lvAuxHostIds.UseCompatibleStateImageBehavior = false;
            this.lvAuxHostIds.View = System.Windows.Forms.View.Details;
            this.lvAuxHostIds.SelectedIndexChanged += new System.EventHandler(this.lvAuxHostIds_SelectedIndexChanged);
            // 
            // colAuxHostIdType
            // 
            this.colAuxHostIdType.Text = "Type";
            this.colAuxHostIdType.Width = 180;
            // 
            // colAuxHostIdValue
            // 
            this.colAuxHostIdValue.Text = "Value";
            this.colAuxHostIdValue.Width = 356;
            // 
            // pageCR_Resp
            // 
            this.pageCR_Resp.Controls.Add(this.btnCRResponseDetails);
            this.pageCR_Resp.Controls.Add(this.lvCRResponse);
            this.pageCR_Resp.Location = new System.Drawing.Point(4, 22);
            this.pageCR_Resp.Name = "pageCR_Resp";
            this.pageCR_Resp.Size = new System.Drawing.Size(554, 294);
            this.pageCR_Resp.TabIndex = 2;
            this.pageCR_Resp.Text = "Response";
            this.pageCR_Resp.ToolTipText = "Features contained in the capability response";
            this.pageCR_Resp.UseVisualStyleBackColor = true;
            // 
            // btnCRResponseDetails
            // 
            this.btnCRResponseDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCRResponseDetails.BackColor = System.Drawing.Color.Beige;
            this.btnCRResponseDetails.Enabled = false;
            this.btnCRResponseDetails.Location = new System.Drawing.Point(200, 268);
            this.btnCRResponseDetails.Name = "btnCRResponseDetails";
            this.btnCRResponseDetails.Size = new System.Drawing.Size(98, 23);
            this.btnCRResponseDetails.TabIndex = 2;
            this.btnCRResponseDetails.Text = "Response Details";
            this.FNEToolTip.SetToolTip(this.btnCRResponseDetails, "Additional capability response details");
            this.btnCRResponseDetails.UseVisualStyleBackColor = false;
            this.btnCRResponseDetails.Click += new System.EventHandler(this.btnCRResponseDetails_Click);
            // 
            // lvCRResponse
            // 
            this.lvCRResponse.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvCRResponse.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colRespType,
            this.colRespFeature,
            this.colRespVersion,
            this.colRespCount,
            this.colRespExpiration,
            this.colRespVendorString});
            this.lvCRResponse.FullRowSelect = true;
            this.lvCRResponse.GridLines = true;
            this.lvCRResponse.Location = new System.Drawing.Point(0, 0);
            this.lvCRResponse.MultiSelect = false;
            this.lvCRResponse.Name = "lvCRResponse";
            this.lvCRResponse.ShowItemToolTips = true;
            this.lvCRResponse.Size = new System.Drawing.Size(554, 265);
            this.lvCRResponse.TabIndex = 1;
            this.lvCRResponse.UseCompatibleStateImageBehavior = false;
            this.lvCRResponse.View = System.Windows.Forms.View.Details;
            this.lvCRResponse.DoubleClick += new System.EventHandler(this.lvFeature_DoubleClick);
            // 
            // colRespType
            // 
            this.colRespType.Text = "Type";
            this.colRespType.Width = 55;
            // 
            // colRespFeature
            // 
            this.colRespFeature.Text = "Feature";
            this.colRespFeature.Width = 175;
            // 
            // colRespVersion
            // 
            this.colRespVersion.Text = "Version";
            this.colRespVersion.Width = 100;
            // 
            // colRespCount
            // 
            this.colRespCount.Text = "Count";
            this.colRespCount.Width = 80;
            // 
            // colRespExpiration
            // 
            this.colRespExpiration.Text = "Expiration";
            this.colRespExpiration.Width = 140;
            // 
            // colRespVendorString
            // 
            this.colRespVendorString.Text = "Vendor String";
            this.colRespVendorString.Width = 150;
            // 
            // pageInfoMessage
            // 
            this.pageInfoMessage.Controls.Add(this.btnIMSave);
            this.pageInfoMessage.Controls.Add(this.btnIMSend);
            this.pageInfoMessage.Controls.Add(this.pnlFeatureUsage);
            this.pageInfoMessage.Controls.Add(this.ckbIMAddExisting);
            this.pageInfoMessage.Location = new System.Drawing.Point(4, 22);
            this.pageInfoMessage.Name = "pageInfoMessage";
            this.pageInfoMessage.Padding = new System.Windows.Forms.Padding(3);
            this.pageInfoMessage.Size = new System.Drawing.Size(568, 427);
            this.pageInfoMessage.TabIndex = 1;
            this.pageInfoMessage.Text = "Information Message";
            this.pageInfoMessage.ToolTipText = "Specify information message detail";
            this.pageInfoMessage.UseVisualStyleBackColor = true;
            // 
            // btnIMSave
            // 
            this.btnIMSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnIMSave.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnIMSave.Location = new System.Drawing.Point(115, 395);
            this.btnIMSave.Name = "btnIMSave";
            this.btnIMSave.Size = new System.Drawing.Size(75, 23);
            this.btnIMSave.TabIndex = 4;
            this.btnIMSave.Text = "Save";
            this.FNEToolTip.SetToolTip(this.btnIMSave, "Save the binary information message to a file");
            this.btnIMSave.UseVisualStyleBackColor = false;
            this.btnIMSave.Click += new System.EventHandler(this.btnIMSave_Click);
            // 
            // btnIMSend
            // 
            this.btnIMSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnIMSend.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnIMSend.Location = new System.Drawing.Point(10, 395);
            this.btnIMSend.Name = "btnIMSend";
            this.btnIMSend.Size = new System.Drawing.Size(75, 23);
            this.btnIMSend.TabIndex = 3;
            this.btnIMSend.Text = "Send";
            this.FNEToolTip.SetToolTip(this.btnIMSend, "Send the information message to the feature backup or usage server");
            this.btnIMSend.UseVisualStyleBackColor = false;
            this.btnIMSend.Click += new System.EventHandler(this.btnIMSend_Click);
            // 
            // pnlFeatureUsage
            // 
            this.pnlFeatureUsage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlFeatureUsage.Controls.Add(this.dtIMExpiration);
            this.pnlFeatureUsage.Controls.Add(this.lblIMExpiration);
            this.pnlFeatureUsage.Controls.Add(this.btnIMRemove);
            this.pnlFeatureUsage.Controls.Add(this.txtIMVersion);
            this.pnlFeatureUsage.Controls.Add(this.txtIMCount);
            this.pnlFeatureUsage.Controls.Add(this.txtIMFeature);
            this.pnlFeatureUsage.Controls.Add(this.lblIMCount);
            this.pnlFeatureUsage.Controls.Add(this.lblIMVersion);
            this.pnlFeatureUsage.Controls.Add(this.lblIMFeature);
            this.pnlFeatureUsage.Controls.Add(this.btnIMAdd);
            this.pnlFeatureUsage.Controls.Add(this.lvIMFeatures);
            this.pnlFeatureUsage.Location = new System.Drawing.Point(3, 42);
            this.pnlFeatureUsage.Name = "pnlFeatureUsage";
            this.pnlFeatureUsage.Size = new System.Drawing.Size(562, 345);
            this.pnlFeatureUsage.TabIndex = 2;
            // 
            // dtIMExpiration
            // 
            this.dtIMExpiration.CustomFormat = "MM/dd/yyyy hh:mm:ss tt";
            this.dtIMExpiration.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtIMExpiration.Location = new System.Drawing.Point(90, 78);
            this.dtIMExpiration.Name = "dtIMExpiration";
            this.dtIMExpiration.Size = new System.Drawing.Size(210, 20);
            this.dtIMExpiration.TabIndex = 8;
            this.FNEToolTip.SetToolTip(this.dtIMExpiration, "Specify usage feature expiration date and time");
            // 
            // lblIMExpiration
            // 
            this.lblIMExpiration.AutoSize = true;
            this.lblIMExpiration.Location = new System.Drawing.Point(15, 80);
            this.lblIMExpiration.Name = "lblIMExpiration";
            this.lblIMExpiration.Size = new System.Drawing.Size(56, 13);
            this.lblIMExpiration.TabIndex = 7;
            this.lblIMExpiration.Text = "Expiration:";
            // 
            // btnIMRemove
            // 
            this.btnIMRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIMRemove.BackColor = System.Drawing.Color.Gainsboro;
            this.btnIMRemove.Enabled = false;
            this.btnIMRemove.Location = new System.Drawing.Point(450, 76);
            this.btnIMRemove.Name = "btnIMRemove";
            this.btnIMRemove.Size = new System.Drawing.Size(102, 23);
            this.btnIMRemove.TabIndex = 10;
            this.btnIMRemove.Text = "Remove Selected";
            this.FNEToolTip.SetToolTip(this.btnIMRemove, "Remove selected feature usage data from the information message");
            this.btnIMRemove.UseVisualStyleBackColor = false;
            this.btnIMRemove.Click += new System.EventHandler(this.btnIMRemove_Click);
            // 
            // txtIMVersion
            // 
            this.txtIMVersion.Location = new System.Drawing.Point(90, 30);
            this.txtIMVersion.Name = "txtIMVersion";
            this.txtIMVersion.Size = new System.Drawing.Size(147, 20);
            this.txtIMVersion.TabIndex = 4;
            this.FNEToolTip.SetToolTip(this.txtIMVersion, "Specify usage feature version");
            // 
            // txtIMCount
            // 
            this.txtIMCount.BackColor = System.Drawing.SystemColors.Window;
            this.txtIMCount.Location = new System.Drawing.Point(90, 54);
            this.txtIMCount.Name = "txtIMCount";
            this.txtIMCount.Size = new System.Drawing.Size(87, 20);
            this.txtIMCount.TabIndex = 6;
            this.FNEToolTip.SetToolTip(this.txtIMCount, "Specify usage feature count");
            this.txtIMCount.TextChanged += new System.EventHandler(this.txtIMCount_TextChanged);
            // 
            // txtIMFeature
            // 
            this.txtIMFeature.Location = new System.Drawing.Point(90, 6);
            this.txtIMFeature.Name = "txtIMFeature";
            this.txtIMFeature.Size = new System.Drawing.Size(210, 20);
            this.txtIMFeature.TabIndex = 2;
            this.FNEToolTip.SetToolTip(this.txtIMFeature, "Specify usage feature name");
            // 
            // lblIMCount
            // 
            this.lblIMCount.AutoSize = true;
            this.lblIMCount.Location = new System.Drawing.Point(15, 56);
            this.lblIMCount.Name = "lblIMCount";
            this.lblIMCount.Size = new System.Drawing.Size(41, 13);
            this.lblIMCount.TabIndex = 5;
            this.lblIMCount.Text = "Count: ";
            // 
            // lblIMVersion
            // 
            this.lblIMVersion.AutoSize = true;
            this.lblIMVersion.Location = new System.Drawing.Point(15, 32);
            this.lblIMVersion.Name = "lblIMVersion";
            this.lblIMVersion.Size = new System.Drawing.Size(48, 13);
            this.lblIMVersion.TabIndex = 3;
            this.lblIMVersion.Text = "Version: ";
            // 
            // lblIMFeature
            // 
            this.lblIMFeature.AutoSize = true;
            this.lblIMFeature.Location = new System.Drawing.Point(15, 8);
            this.lblIMFeature.Name = "lblIMFeature";
            this.lblIMFeature.Size = new System.Drawing.Size(46, 13);
            this.lblIMFeature.TabIndex = 1;
            this.lblIMFeature.Text = "Feature:";
            // 
            // btnIMAdd
            // 
            this.btnIMAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIMAdd.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnIMAdd.Location = new System.Drawing.Point(355, 76);
            this.btnIMAdd.Name = "btnIMAdd";
            this.btnIMAdd.Size = new System.Drawing.Size(65, 23);
            this.btnIMAdd.TabIndex = 9;
            this.btnIMAdd.Text = "Add";
            this.FNEToolTip.SetToolTip(this.btnIMAdd, "Add feature usage data to the information message");
            this.btnIMAdd.UseVisualStyleBackColor = false;
            this.btnIMAdd.Click += new System.EventHandler(this.btnIMAdd_Click);
            // 
            // lvIMFeatures
            // 
            this.lvIMFeatures.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvIMFeatures.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colIMFeature,
            this.colIMVersion,
            this.colIMCount,
            this.colIMExpiration});
            this.lvIMFeatures.FullRowSelect = true;
            this.lvIMFeatures.GridLines = true;
            this.lvIMFeatures.Location = new System.Drawing.Point(3, 105);
            this.lvIMFeatures.Name = "lvIMFeatures";
            this.lvIMFeatures.Size = new System.Drawing.Size(553, 240);
            this.lvIMFeatures.TabIndex = 11;
            this.lvIMFeatures.UseCompatibleStateImageBehavior = false;
            this.lvIMFeatures.View = System.Windows.Forms.View.Details;
            this.lvIMFeatures.SelectedIndexChanged += new System.EventHandler(this.lvIMFeatures_SelectedIndexChanged);
            // 
            // colIMFeature
            // 
            this.colIMFeature.Text = "Feature";
            this.colIMFeature.Width = 195;
            // 
            // colIMVersion
            // 
            this.colIMVersion.Text = "Version";
            this.colIMVersion.Width = 95;
            // 
            // colIMCount
            // 
            this.colIMCount.Text = "Count";
            this.colIMCount.Width = 80;
            // 
            // colIMExpiration
            // 
            this.colIMExpiration.Text = "Expiration";
            this.colIMExpiration.Width = 195;
            // 
            // ckbIMAddExisting
            // 
            this.ckbIMAddExisting.AutoSize = true;
            this.ckbIMAddExisting.Location = new System.Drawing.Point(10, 15);
            this.ckbIMAddExisting.Name = "ckbIMAddExisting";
            this.ckbIMAddExisting.Size = new System.Drawing.Size(128, 17);
            this.ckbIMAddExisting.TabIndex = 1;
            this.ckbIMAddExisting.Text = "Use Existing Features";
            this.FNEToolTip.SetToolTip(this.ckbIMAddExisting, "Add existing trusted storage features to information message");
            this.ckbIMAddExisting.UseVisualStyleBackColor = true;
            this.ckbIMAddExisting.CheckedChanged += new System.EventHandler(this.ckbIMAddExisting_CheckedChanged);
            // 
            // cbxServerType
            // 
            this.cbxServerType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxServerType.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.cbxServerType.Location = new System.Drawing.Point(60, 32);
            this.cbxServerType.Name = "cbxServerType";
            this.cbxServerType.Size = new System.Drawing.Size(200, 21);
            this.cbxServerType.TabIndex = 2;
            this.FNEToolTip.SetToolTip(this.cbxServerType, "Select back office or local license server type");
            this.cbxServerType.SelectedIndexChanged += new System.EventHandler(this.cbxServerType_SelectedIndexChanged);
            // 
            // lblServerType
            // 
            this.lblServerType.AutoSize = true;
            this.lblServerType.Location = new System.Drawing.Point(5, 35);
            this.lblServerType.Name = "lblServerType";
            this.lblServerType.Size = new System.Drawing.Size(34, 13);
            this.lblServerType.TabIndex = 1;
            this.lblServerType.Text = "Type:";
            // 
            // lblServerInfo
            // 
            this.lblServerInfo.AutoSize = true;
            this.lblServerInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblServerInfo.Location = new System.Drawing.Point(5, 10);
            this.lblServerInfo.Name = "lblServerInfo";
            this.lblServerInfo.Size = new System.Drawing.Size(111, 13);
            this.lblServerInfo.TabIndex = 0;
            this.lblServerInfo.Text = "Server Information";
            // 
            // lblServerURL
            // 
            this.lblServerURL.AutoSize = true;
            this.lblServerURL.Location = new System.Drawing.Point(5, 72);
            this.lblServerURL.Name = "lblServerURL";
            this.lblServerURL.Size = new System.Drawing.Size(32, 13);
            this.lblServerURL.TabIndex = 5;
            this.lblServerURL.Text = "URL:";
            // 
            // txtServerURL
            // 
            this.txtServerURL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtServerURL.Location = new System.Drawing.Point(60, 69);
            this.txtServerURL.Name = "txtServerURL";
            this.txtServerURL.Size = new System.Drawing.Size(510, 20);
            this.txtServerURL.TabIndex = 6;
            this.FNEToolTip.SetToolTip(this.txtServerURL, "Specify back office or local license server URL");
            this.txtServerURL.TextChanged += new System.EventHandler(this.txtServerURL_TextChanged);
            // 
            // pageShortCodes
            // 
            this.pageShortCodes.Controls.Add(this.tabTemplate);
            this.pageShortCodes.Controls.Add(this.panelTemplates);
            this.pageShortCodes.Location = new System.Drawing.Point(4, 22);
            this.pageShortCodes.Name = "pageShortCodes";
            this.pageShortCodes.Size = new System.Drawing.Size(576, 566);
            this.pageShortCodes.TabIndex = 4;
            this.pageShortCodes.Text = "Short Code Requests";
            this.pageShortCodes.ToolTipText = "Work with short code templates, requests and responses";
            this.pageShortCodes.UseVisualStyleBackColor = true;
            // 
            // tabTemplate
            // 
            this.tabTemplate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabTemplate.Controls.Add(this.pageTemplateFeatures);
            this.tabTemplate.Controls.Add(this.pageTemplateVD);
            this.tabTemplate.Controls.Add(this.pageTemplateRequest);
            this.tabTemplate.Controls.Add(this.pageTemplateResponse);
            this.tabTemplate.Location = new System.Drawing.Point(3, 276);
            this.tabTemplate.Name = "tabTemplate";
            this.tabTemplate.SelectedIndex = 0;
            this.tabTemplate.ShowToolTips = true;
            this.tabTemplate.Size = new System.Drawing.Size(570, 284);
            this.tabTemplate.TabIndex = 2;
            this.tabTemplate.SelectedIndexChanged += new System.EventHandler(this.tabTemplate_SelectedIndexChanged);
            // 
            // pageTemplateFeatures
            // 
            this.pageTemplateFeatures.Controls.Add(this.lvTemplateFeatures);
            this.pageTemplateFeatures.Location = new System.Drawing.Point(4, 22);
            this.pageTemplateFeatures.Name = "pageTemplateFeatures";
            this.pageTemplateFeatures.Padding = new System.Windows.Forms.Padding(3);
            this.pageTemplateFeatures.Size = new System.Drawing.Size(562, 258);
            this.pageTemplateFeatures.TabIndex = 1;
            this.pageTemplateFeatures.Text = "Features";
            this.pageTemplateFeatures.ToolTipText = "Features contained in currently selected license template";
            this.pageTemplateFeatures.UseVisualStyleBackColor = true;
            // 
            // lvTemplateFeatures
            // 
            this.lvTemplateFeatures.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvTemplateFeatures.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colTemplateType,
            this.colTemplateFeature,
            this.colTemplateVersion,
            this.colTemplateCount,
            this.colTemplateExpiry,
            this.colTemplateVS});
            this.lvTemplateFeatures.FullRowSelect = true;
            this.lvTemplateFeatures.GridLines = true;
            this.lvTemplateFeatures.Location = new System.Drawing.Point(3, 3);
            this.lvTemplateFeatures.MultiSelect = false;
            this.lvTemplateFeatures.Name = "lvTemplateFeatures";
            this.lvTemplateFeatures.ShowItemToolTips = true;
            this.lvTemplateFeatures.Size = new System.Drawing.Size(556, 252);
            this.lvTemplateFeatures.TabIndex = 0;
            this.lvTemplateFeatures.UseCompatibleStateImageBehavior = false;
            this.lvTemplateFeatures.View = System.Windows.Forms.View.Details;
            this.lvTemplateFeatures.DoubleClick += new System.EventHandler(this.lvFeature_DoubleClick);
            // 
            // colTemplateType
            // 
            this.colTemplateType.Text = "Type";
            this.colTemplateType.Width = 50;
            // 
            // colTemplateFeature
            // 
            this.colTemplateFeature.Text = "Feature";
            this.colTemplateFeature.Width = 187;
            // 
            // colTemplateVersion
            // 
            this.colTemplateVersion.Text = "Version";
            this.colTemplateVersion.Width = 105;
            // 
            // colTemplateCount
            // 
            this.colTemplateCount.Text = "Count";
            this.colTemplateCount.Width = 80;
            // 
            // colTemplateExpiry
            // 
            this.colTemplateExpiry.Text = "Expiration";
            this.colTemplateExpiry.Width = 135;
            // 
            // colTemplateVS
            // 
            this.colTemplateVS.Text = "Vendor String";
            this.colTemplateVS.Width = 150;
            // 
            // pageTemplateVD
            // 
            this.pageTemplateVD.Controls.Add(this.lvTemplateVD);
            this.pageTemplateVD.Location = new System.Drawing.Point(4, 22);
            this.pageTemplateVD.Name = "pageTemplateVD";
            this.pageTemplateVD.Size = new System.Drawing.Size(562, 258);
            this.pageTemplateVD.TabIndex = 4;
            this.pageTemplateVD.Text = "Vendor Dictionary";
            this.pageTemplateVD.ToolTipText = "Vendor dictionary information for currently selected license template";
            this.pageTemplateVD.UseVisualStyleBackColor = true;
            // 
            // lvTemplateVD
            // 
            this.lvTemplateVD.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colTemplateVDType,
            this.colTemplateVDKey,
            this.colTemplateVDValue});
            this.lvTemplateVD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvTemplateVD.FullRowSelect = true;
            this.lvTemplateVD.GridLines = true;
            this.lvTemplateVD.Location = new System.Drawing.Point(0, 0);
            this.lvTemplateVD.Name = "lvTemplateVD";
            this.lvTemplateVD.Size = new System.Drawing.Size(562, 258);
            this.lvTemplateVD.TabIndex = 9;
            this.lvTemplateVD.UseCompatibleStateImageBehavior = false;
            this.lvTemplateVD.View = System.Windows.Forms.View.Details;
            // 
            // colTemplateVDType
            // 
            this.colTemplateVDType.Text = "Type";
            this.colTemplateVDType.Width = 67;
            // 
            // colTemplateVDKey
            // 
            this.colTemplateVDKey.Text = "Key";
            this.colTemplateVDKey.Width = 235;
            // 
            // colTemplateVDValue
            // 
            this.colTemplateVDValue.Text = "Value";
            this.colTemplateVDValue.Width = 235;
            // 
            // pageTemplateRequest
            // 
            this.pageTemplateRequest.Controls.Add(this.rbSCRequestPDHex);
            this.pageTemplateRequest.Controls.Add(this.rbSCRequestPDChar);
            this.pageTemplateRequest.Controls.Add(this.btnSCRequestPD);
            this.pageTemplateRequest.Controls.Add(this.txtSCRequestPD);
            this.pageTemplateRequest.Controls.Add(this.lblSCRequestPD);
            this.pageTemplateRequest.Controls.Add(this.btnSCRequestFile);
            this.pageTemplateRequest.Controls.Add(this.txtSCRequestFile);
            this.pageTemplateRequest.Controls.Add(this.lblSCRequestFile);
            this.pageTemplateRequest.Controls.Add(this.btnGenerateSCRequest);
            this.pageTemplateRequest.Controls.Add(this.txtEncodingSegSize);
            this.pageTemplateRequest.Controls.Add(this.lblEncodingSegSize);
            this.pageTemplateRequest.Controls.Add(this.txtEncodingChars);
            this.pageTemplateRequest.Controls.Add(this.lblEncodingChars);
            this.pageTemplateRequest.Controls.Add(this.cbxEncodingType);
            this.pageTemplateRequest.Controls.Add(this.lblEncodingType);
            this.pageTemplateRequest.Location = new System.Drawing.Point(4, 22);
            this.pageTemplateRequest.Name = "pageTemplateRequest";
            this.pageTemplateRequest.Size = new System.Drawing.Size(562, 258);
            this.pageTemplateRequest.TabIndex = 5;
            this.pageTemplateRequest.Text = "Request";
            this.pageTemplateRequest.ToolTipText = "Generate a short code request using the currently selected license template";
            this.pageTemplateRequest.UseVisualStyleBackColor = true;
            // 
            // rbSCRequestPDHex
            // 
            this.rbSCRequestPDHex.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rbSCRequestPDHex.AutoSize = true;
            this.rbSCRequestPDHex.Location = new System.Drawing.Point(199, 150);
            this.rbSCRequestPDHex.Name = "rbSCRequestPDHex";
            this.rbSCRequestPDHex.Size = new System.Drawing.Size(86, 17);
            this.rbSCRequestPDHex.TabIndex = 11;
            this.rbSCRequestPDHex.Text = "Hexadecimal";
            this.FNEToolTip.SetToolTip(this.rbSCRequestPDHex, "Short code request publisher data is hexadecimal");
            this.rbSCRequestPDHex.UseVisualStyleBackColor = true;
            this.rbSCRequestPDHex.CheckedChanged += new System.EventHandler(this.rbSCRequestPDHex_CheckedChanged);
            // 
            // rbSCRequestPDChar
            // 
            this.rbSCRequestPDChar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rbSCRequestPDChar.AutoSize = true;
            this.rbSCRequestPDChar.Checked = true;
            this.rbSCRequestPDChar.Location = new System.Drawing.Point(100, 150);
            this.rbSCRequestPDChar.Name = "rbSCRequestPDChar";
            this.rbSCRequestPDChar.Size = new System.Drawing.Size(71, 17);
            this.rbSCRequestPDChar.TabIndex = 10;
            this.rbSCRequestPDChar.TabStop = true;
            this.rbSCRequestPDChar.Text = "Character";
            this.FNEToolTip.SetToolTip(this.rbSCRequestPDChar, "Short code request publisher data is character");
            this.rbSCRequestPDChar.UseVisualStyleBackColor = true;
            this.rbSCRequestPDChar.CheckedChanged += new System.EventHandler(this.errorProvider_reset);
            // 
            // btnSCRequestPD
            // 
            this.btnSCRequestPD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSCRequestPD.ImageIndex = 1;
            this.btnSCRequestPD.ImageList = this.imageList;
            this.btnSCRequestPD.Location = new System.Drawing.Point(518, 82);
            this.btnSCRequestPD.Name = "btnSCRequestPD";
            this.btnSCRequestPD.Size = new System.Drawing.Size(32, 23);
            this.btnSCRequestPD.TabIndex = 9;
            this.FNEToolTip.SetToolTip(this.btnSCRequestPD, "Browse for optional short code request publisher data file");
            this.btnSCRequestPD.UseVisualStyleBackColor = true;
            this.btnSCRequestPD.Click += new System.EventHandler(this.btnSCRequestPD_Click);
            // 
            // txtSCRequestPD
            // 
            this.txtSCRequestPD.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSCRequestPD.Location = new System.Drawing.Point(100, 83);
            this.txtSCRequestPD.MaxLength = 1048576;
            this.txtSCRequestPD.Multiline = true;
            this.txtSCRequestPD.Name = "txtSCRequestPD";
            this.txtSCRequestPD.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtSCRequestPD.Size = new System.Drawing.Size(410, 60);
            this.txtSCRequestPD.TabIndex = 8;
            this.FNEToolTip.SetToolTip(this.txtSCRequestPD, "Optional short code request publisher data");
            this.txtSCRequestPD.TextChanged += new System.EventHandler(this.txtSCRequestPD_TextChanged);
            // 
            // lblSCRequestPD
            // 
            this.lblSCRequestPD.AutoSize = true;
            this.lblSCRequestPD.Location = new System.Drawing.Point(0, 85);
            this.lblSCRequestPD.Name = "lblSCRequestPD";
            this.lblSCRequestPD.Size = new System.Drawing.Size(79, 13);
            this.lblSCRequestPD.TabIndex = 7;
            this.lblSCRequestPD.Text = "Publisher Data:";
            // 
            // btnSCRequestFile
            // 
            this.btnSCRequestFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSCRequestFile.ImageIndex = 1;
            this.btnSCRequestFile.ImageList = this.imageList;
            this.btnSCRequestFile.Location = new System.Drawing.Point(518, 182);
            this.btnSCRequestFile.Name = "btnSCRequestFile";
            this.btnSCRequestFile.Size = new System.Drawing.Size(32, 23);
            this.btnSCRequestFile.TabIndex = 14;
            this.FNEToolTip.SetToolTip(this.btnSCRequestFile, "Browse for optional generated short code request data file location");
            this.btnSCRequestFile.UseVisualStyleBackColor = true;
            this.btnSCRequestFile.Click += new System.EventHandler(this.btnSCRequestFile_Click);
            // 
            // txtSCRequestFile
            // 
            this.txtSCRequestFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSCRequestFile.Location = new System.Drawing.Point(100, 183);
            this.txtSCRequestFile.Name = "txtSCRequestFile";
            this.txtSCRequestFile.Size = new System.Drawing.Size(410, 20);
            this.txtSCRequestFile.TabIndex = 13;
            this.FNEToolTip.SetToolTip(this.txtSCRequestFile, "Optional file in which to store the generated short code request data");
            // 
            // lblSCRequestFile
            // 
            this.lblSCRequestFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblSCRequestFile.AutoSize = true;
            this.lblSCRequestFile.Location = new System.Drawing.Point(0, 185);
            this.lblSCRequestFile.Name = "lblSCRequestFile";
            this.lblSCRequestFile.Size = new System.Drawing.Size(69, 13);
            this.lblSCRequestFile.TabIndex = 12;
            this.lblSCRequestFile.Text = "Request File:";
            // 
            // btnGenerateSCRequest
            // 
            this.btnGenerateSCRequest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnGenerateSCRequest.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnGenerateSCRequest.Enabled = false;
            this.btnGenerateSCRequest.Location = new System.Drawing.Point(195, 220);
            this.btnGenerateSCRequest.Name = "btnGenerateSCRequest";
            this.btnGenerateSCRequest.Size = new System.Drawing.Size(120, 23);
            this.btnGenerateSCRequest.TabIndex = 15;
            this.btnGenerateSCRequest.Text = "Generate Request";
            this.FNEToolTip.SetToolTip(this.btnGenerateSCRequest, "Generate short code request");
            this.btnGenerateSCRequest.UseVisualStyleBackColor = false;
            this.btnGenerateSCRequest.Click += new System.EventHandler(this.btnGenerateSCRequest_Click);
            // 
            // txtEncodingSegSize
            // 
            this.txtEncodingSegSize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEncodingSegSize.Location = new System.Drawing.Point(450, 13);
            this.txtEncodingSegSize.Name = "txtEncodingSegSize";
            this.txtEncodingSegSize.Size = new System.Drawing.Size(100, 20);
            this.txtEncodingSegSize.TabIndex = 4;
            this.txtEncodingSegSize.Text = "0";
            this.FNEToolTip.SetToolTip(this.txtEncodingSegSize, "Specify short code request segment size or 0 for unsegmented request");
            this.txtEncodingSegSize.TextChanged += new System.EventHandler(this.txtEncodingSegSize_TextChanged);
            // 
            // lblEncodingSegSize
            // 
            this.lblEncodingSegSize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEncodingSegSize.AutoSize = true;
            this.lblEncodingSegSize.Location = new System.Drawing.Point(360, 15);
            this.lblEncodingSegSize.Name = "lblEncodingSegSize";
            this.lblEncodingSegSize.Size = new System.Drawing.Size(75, 13);
            this.lblEncodingSegSize.TabIndex = 3;
            this.lblEncodingSegSize.Text = "Segment Size:";
            // 
            // txtEncodingChars
            // 
            this.txtEncodingChars.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEncodingChars.Location = new System.Drawing.Point(100, 48);
            this.txtEncodingChars.Name = "txtEncodingChars";
            this.txtEncodingChars.ReadOnly = true;
            this.txtEncodingChars.Size = new System.Drawing.Size(450, 20);
            this.txtEncodingChars.TabIndex = 6;
            this.FNEToolTip.SetToolTip(this.txtEncodingChars, "Short code request encoding character set");
            this.txtEncodingChars.TextChanged += new System.EventHandler(this.errorProvider_reset);
            // 
            // lblEncodingChars
            // 
            this.lblEncodingChars.AutoSize = true;
            this.lblEncodingChars.Location = new System.Drawing.Point(0, 50);
            this.lblEncodingChars.Name = "lblEncodingChars";
            this.lblEncodingChars.Size = new System.Drawing.Size(85, 13);
            this.lblEncodingChars.TabIndex = 5;
            this.lblEncodingChars.Text = "Encoding Chars:";
            // 
            // cbxEncodingType
            // 
            this.cbxEncodingType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxEncodingType.Location = new System.Drawing.Point(100, 12);
            this.cbxEncodingType.MaxDropDownItems = 10;
            this.cbxEncodingType.Name = "cbxEncodingType";
            this.cbxEncodingType.Size = new System.Drawing.Size(100, 21);
            this.cbxEncodingType.TabIndex = 2;
            this.FNEToolTip.SetToolTip(this.cbxEncodingType, "Specify short code request encoding type");
            this.cbxEncodingType.SelectedIndexChanged += new System.EventHandler(this.cbxEncodingType_SelectedIndexChanged);
            // 
            // lblEncodingType
            // 
            this.lblEncodingType.AutoSize = true;
            this.lblEncodingType.Location = new System.Drawing.Point(0, 15);
            this.lblEncodingType.Name = "lblEncodingType";
            this.lblEncodingType.Size = new System.Drawing.Size(82, 13);
            this.lblEncodingType.TabIndex = 1;
            this.lblEncodingType.Text = "Encoding Type:";
            // 
            // pageTemplateResponse
            // 
            this.pageTemplateResponse.Controls.Add(this.cbxSCResponseFile);
            this.pageTemplateResponse.Controls.Add(this.txtSCResponseID);
            this.pageTemplateResponse.Controls.Add(this.lblSCResponseID);
            this.pageTemplateResponse.Controls.Add(this.btnReadSCResponse);
            this.pageTemplateResponse.Controls.Add(this.btnSCResponseFile);
            this.pageTemplateResponse.Controls.Add(this.txtSCResponsePD);
            this.pageTemplateResponse.Controls.Add(this.lblSCResponsePD);
            this.pageTemplateResponse.Controls.Add(this.btnSCResponsePD);
            this.pageTemplateResponse.Controls.Add(this.lblSCResponseFile);
            this.pageTemplateResponse.Controls.Add(this.btnProcessSCResponse);
            this.pageTemplateResponse.Controls.Add(this.txtDecodingSegSize);
            this.pageTemplateResponse.Controls.Add(this.lblDecodingSegSize);
            this.pageTemplateResponse.Controls.Add(this.txtDecodingChars);
            this.pageTemplateResponse.Controls.Add(this.lblDecodingChars);
            this.pageTemplateResponse.Controls.Add(this.cbxDecodingType);
            this.pageTemplateResponse.Controls.Add(this.lblDecodingType);
            this.pageTemplateResponse.Location = new System.Drawing.Point(4, 22);
            this.pageTemplateResponse.Name = "pageTemplateResponse";
            this.pageTemplateResponse.Size = new System.Drawing.Size(562, 258);
            this.pageTemplateResponse.TabIndex = 6;
            this.pageTemplateResponse.Text = "Response";
            this.pageTemplateResponse.ToolTipText = "Inspect and process a short code response";
            this.pageTemplateResponse.UseVisualStyleBackColor = true;
            // 
            // cbxSCResponseFile
            // 
            this.cbxSCResponseFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxSCResponseFile.FormattingEnabled = true;
            this.cbxSCResponseFile.Location = new System.Drawing.Point(100, 72);
            this.cbxSCResponseFile.MaxDropDownItems = 10;
            this.cbxSCResponseFile.Name = "cbxSCResponseFile";
            this.cbxSCResponseFile.Size = new System.Drawing.Size(410, 21);
            this.cbxSCResponseFile.TabIndex = 8;
            this.FNEToolTip.SetToolTip(this.cbxSCResponseFile, "Optional location of a file containing the short code response data");
            // 
            // txtSCResponseID
            // 
            this.txtSCResponseID.Location = new System.Drawing.Point(100, 128);
            this.txtSCResponseID.Name = "txtSCResponseID";
            this.txtSCResponseID.ReadOnly = true;
            this.txtSCResponseID.Size = new System.Drawing.Size(80, 20);
            this.txtSCResponseID.TabIndex = 12;
            this.FNEToolTip.SetToolTip(this.txtSCResponseID, "Short code response template id");
            // 
            // lblSCResponseID
            // 
            this.lblSCResponseID.AutoSize = true;
            this.lblSCResponseID.Location = new System.Drawing.Point(0, 130);
            this.lblSCResponseID.Name = "lblSCResponseID";
            this.lblSCResponseID.Size = new System.Drawing.Size(68, 13);
            this.lblSCResponseID.TabIndex = 11;
            this.lblSCResponseID.Text = "Template ID:";
            // 
            // btnReadSCResponse
            // 
            this.btnReadSCResponse.BackColor = System.Drawing.Color.Beige;
            this.btnReadSCResponse.Location = new System.Drawing.Point(201, 105);
            this.btnReadSCResponse.Name = "btnReadSCResponse";
            this.btnReadSCResponse.Size = new System.Drawing.Size(120, 23);
            this.btnReadSCResponse.TabIndex = 10;
            this.btnReadSCResponse.Text = "Read Response Data";
            this.FNEToolTip.SetToolTip(this.btnReadSCResponse, "Read short code response data from optionally specified file or from a response d" +
                    "ata dialog");
            this.btnReadSCResponse.UseVisualStyleBackColor = false;
            this.btnReadSCResponse.Click += new System.EventHandler(this.btnReadSCResponse_Click);
            // 
            // btnSCResponseFile
            // 
            this.btnSCResponseFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSCResponseFile.ImageIndex = 1;
            this.btnSCResponseFile.ImageList = this.imageList;
            this.btnSCResponseFile.Location = new System.Drawing.Point(518, 71);
            this.btnSCResponseFile.Name = "btnSCResponseFile";
            this.btnSCResponseFile.Size = new System.Drawing.Size(32, 23);
            this.btnSCResponseFile.TabIndex = 9;
            this.FNEToolTip.SetToolTip(this.btnSCResponseFile, "Browse for file containing the short code response data");
            this.btnSCResponseFile.UseVisualStyleBackColor = true;
            this.btnSCResponseFile.Click += new System.EventHandler(this.btnSCResponseFile_Click);
            // 
            // txtSCResponsePD
            // 
            this.txtSCResponsePD.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSCResponsePD.Location = new System.Drawing.Point(100, 156);
            this.txtSCResponsePD.MaxLength = 1048576;
            this.txtSCResponsePD.Multiline = true;
            this.txtSCResponsePD.Name = "txtSCResponsePD";
            this.txtSCResponsePD.ReadOnly = true;
            this.txtSCResponsePD.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtSCResponsePD.Size = new System.Drawing.Size(410, 60);
            this.txtSCResponsePD.TabIndex = 14;
            this.FNEToolTip.SetToolTip(this.txtSCResponsePD, "Short code response publisher data");
            // 
            // lblSCResponsePD
            // 
            this.lblSCResponsePD.AutoSize = true;
            this.lblSCResponsePD.Location = new System.Drawing.Point(0, 158);
            this.lblSCResponsePD.Name = "lblSCResponsePD";
            this.lblSCResponsePD.Size = new System.Drawing.Size(79, 13);
            this.lblSCResponsePD.TabIndex = 13;
            this.lblSCResponsePD.Text = "Publisher Data:";
            // 
            // btnSCResponsePD
            // 
            this.btnSCResponsePD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSCResponsePD.ImageIndex = 1;
            this.btnSCResponsePD.ImageList = this.imageList;
            this.btnSCResponsePD.Location = new System.Drawing.Point(518, 155);
            this.btnSCResponsePD.Name = "btnSCResponsePD";
            this.btnSCResponsePD.Size = new System.Drawing.Size(32, 23);
            this.btnSCResponsePD.TabIndex = 15;
            this.FNEToolTip.SetToolTip(this.btnSCResponsePD, "Save short code response publisher data to a file");
            this.btnSCResponsePD.UseVisualStyleBackColor = true;
            this.btnSCResponsePD.Click += new System.EventHandler(this.btnSCResponsePD_Click);
            // 
            // lblSCResponseFile
            // 
            this.lblSCResponseFile.AutoSize = true;
            this.lblSCResponseFile.Location = new System.Drawing.Point(0, 75);
            this.lblSCResponseFile.Name = "lblSCResponseFile";
            this.lblSCResponseFile.Size = new System.Drawing.Size(77, 13);
            this.lblSCResponseFile.TabIndex = 7;
            this.lblSCResponseFile.Text = "Response File:";
            // 
            // btnProcessSCResponse
            // 
            this.btnProcessSCResponse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnProcessSCResponse.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnProcessSCResponse.Enabled = false;
            this.btnProcessSCResponse.Location = new System.Drawing.Point(201, 225);
            this.btnProcessSCResponse.Name = "btnProcessSCResponse";
            this.btnProcessSCResponse.Size = new System.Drawing.Size(120, 23);
            this.btnProcessSCResponse.TabIndex = 16;
            this.btnProcessSCResponse.Text = "Process Response";
            this.FNEToolTip.SetToolTip(this.btnProcessSCResponse, "Process short code response into trusted storage");
            this.btnProcessSCResponse.UseVisualStyleBackColor = false;
            this.btnProcessSCResponse.Click += new System.EventHandler(this.btnProcessSCResponse_Click);
            // 
            // txtDecodingSegSize
            // 
            this.txtDecodingSegSize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDecodingSegSize.Location = new System.Drawing.Point(450, 13);
            this.txtDecodingSegSize.Name = "txtDecodingSegSize";
            this.txtDecodingSegSize.Size = new System.Drawing.Size(100, 20);
            this.txtDecodingSegSize.TabIndex = 4;
            this.txtDecodingSegSize.Text = "0";
            this.FNEToolTip.SetToolTip(this.txtDecodingSegSize, "Specify short code response segment size or 0 for unsegmented response");
            this.txtDecodingSegSize.TextChanged += new System.EventHandler(this.txtDecodingSegSize_TextChanged);
            // 
            // lblDecodingSegSize
            // 
            this.lblDecodingSegSize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDecodingSegSize.AutoSize = true;
            this.lblDecodingSegSize.Location = new System.Drawing.Point(360, 15);
            this.lblDecodingSegSize.Name = "lblDecodingSegSize";
            this.lblDecodingSegSize.Size = new System.Drawing.Size(75, 13);
            this.lblDecodingSegSize.TabIndex = 3;
            this.lblDecodingSegSize.Text = "Segment Size:";
            // 
            // txtDecodingChars
            // 
            this.txtDecodingChars.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDecodingChars.Location = new System.Drawing.Point(100, 43);
            this.txtDecodingChars.Name = "txtDecodingChars";
            this.txtDecodingChars.ReadOnly = true;
            this.txtDecodingChars.Size = new System.Drawing.Size(450, 20);
            this.txtDecodingChars.TabIndex = 6;
            this.FNEToolTip.SetToolTip(this.txtDecodingChars, "Short code response decoding character set");
            // 
            // lblDecodingChars
            // 
            this.lblDecodingChars.AutoSize = true;
            this.lblDecodingChars.Location = new System.Drawing.Point(0, 45);
            this.lblDecodingChars.Name = "lblDecodingChars";
            this.lblDecodingChars.Size = new System.Drawing.Size(86, 13);
            this.lblDecodingChars.TabIndex = 5;
            this.lblDecodingChars.Text = "Decoding Chars:";
            // 
            // cbxDecodingType
            // 
            this.cbxDecodingType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxDecodingType.Location = new System.Drawing.Point(100, 12);
            this.cbxDecodingType.MaxDropDownItems = 10;
            this.cbxDecodingType.Name = "cbxDecodingType";
            this.cbxDecodingType.Size = new System.Drawing.Size(100, 21);
            this.cbxDecodingType.TabIndex = 2;
            this.FNEToolTip.SetToolTip(this.cbxDecodingType, "Specify short code response decoding type");
            this.cbxDecodingType.SelectedIndexChanged += new System.EventHandler(this.cbxDecodingType_SelectedIndexChanged);
            // 
            // lblDecodingType
            // 
            this.lblDecodingType.AutoSize = true;
            this.lblDecodingType.Location = new System.Drawing.Point(0, 15);
            this.lblDecodingType.Name = "lblDecodingType";
            this.lblDecodingType.Size = new System.Drawing.Size(83, 13);
            this.lblDecodingType.TabIndex = 1;
            this.lblDecodingType.Text = "Decoding Type:";
            // 
            // panelTemplates
            // 
            this.panelTemplates.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panelTemplates.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelTemplates.Controls.Add(this.btnDeleteTemplateLicenses);
            this.panelTemplates.Controls.Add(this.cbxTemplateFile);
            this.panelTemplates.Controls.Add(this.btnClearTemplates);
            this.panelTemplates.Controls.Add(this.lvTemplates);
            this.panelTemplates.Controls.Add(this.btnLoadTemplateFile);
            this.panelTemplates.Controls.Add(this.lblTemplateLoadComplete);
            this.panelTemplates.Controls.Add(this.btnInspectTemplateFile);
            this.panelTemplates.Controls.Add(this.lblTemplates);
            this.panelTemplates.Controls.Add(this.lblTemplateFile);
            this.panelTemplates.Controls.Add(this.btnBrowseTemplateFile);
            this.panelTemplates.Location = new System.Drawing.Point(3, 3);
            this.panelTemplates.Name = "panelTemplates";
            this.panelTemplates.Size = new System.Drawing.Size(570, 270);
            this.panelTemplates.TabIndex = 1;
            // 
            // btnDeleteTemplateLicenses
            // 
            this.btnDeleteTemplateLicenses.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteTemplateLicenses.BackColor = System.Drawing.Color.PaleTurquoise;
            this.btnDeleteTemplateLicenses.Enabled = false;
            this.btnDeleteTemplateLicenses.Location = new System.Drawing.Point(380, 8);
            this.btnDeleteTemplateLicenses.Name = "btnDeleteTemplateLicenses";
            this.btnDeleteTemplateLicenses.Size = new System.Drawing.Size(185, 23);
            this.btnDeleteTemplateLicenses.TabIndex = 3;
            this.btnDeleteTemplateLicenses.Text = "Delete Selected Template Licenses";
            this.FNEToolTip.SetToolTip(this.btnDeleteTemplateLicenses, "Delete all short code license source license information generated from the selec" +
                    "ted template(s)");
            this.btnDeleteTemplateLicenses.UseVisualStyleBackColor = false;
            this.btnDeleteTemplateLicenses.Click += new System.EventHandler(this.btnDeleteTemplateLicenses_Click);
            // 
            // cbxTemplateFile
            // 
            this.cbxTemplateFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxTemplateFile.FormattingEnabled = true;
            this.cbxTemplateFile.Location = new System.Drawing.Point(87, 189);
            this.cbxTemplateFile.MaxDropDownItems = 10;
            this.cbxTemplateFile.Name = "cbxTemplateFile";
            this.cbxTemplateFile.Size = new System.Drawing.Size(416, 21);
            this.cbxTemplateFile.TabIndex = 6;
            this.FNEToolTip.SetToolTip(this.cbxTemplateFile, "Enter a license template file location");
            this.cbxTemplateFile.TextChanged += new System.EventHandler(this.cbxTemplateFile_TextChanged);
            // 
            // btnClearTemplates
            // 
            this.btnClearTemplates.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClearTemplates.BackColor = System.Drawing.Color.Gainsboro;
            this.btnClearTemplates.Location = new System.Drawing.Point(200, 8);
            this.btnClearTemplates.Name = "btnClearTemplates";
            this.btnClearTemplates.Size = new System.Drawing.Size(153, 23);
            this.btnClearTemplates.TabIndex = 2;
            this.btnClearTemplates.Text = "Clear Templates";
            this.FNEToolTip.SetToolTip(this.btnClearTemplates, "Clear all templates from the template collection");
            this.btnClearTemplates.UseVisualStyleBackColor = false;
            this.btnClearTemplates.Click += new System.EventHandler(this.btnClearTemplates_Click);
            // 
            // lvTemplates
            // 
            this.lvTemplates.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvTemplates.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colTemplateID,
            this.colTemplateExpiration,
            this.colTemplateMachineType,
            this.colFileName});
            this.lvTemplates.FullRowSelect = true;
            this.lvTemplates.GridLines = true;
            this.lvTemplates.HideSelection = false;
            this.lvTemplates.Location = new System.Drawing.Point(3, 38);
            this.lvTemplates.MultiSelect = false;
            this.lvTemplates.Name = "lvTemplates";
            this.lvTemplates.Size = new System.Drawing.Size(564, 147);
            this.lvTemplates.TabIndex = 4;
            this.lvTemplates.UseCompatibleStateImageBehavior = false;
            this.lvTemplates.View = System.Windows.Forms.View.Details;
            this.lvTemplates.SelectedIndexChanged += new System.EventHandler(this.lvTemplates_SelectedIndexChanged);
            // 
            // colTemplateID
            // 
            this.colTemplateID.Text = "ID";
            // 
            // colTemplateExpiration
            // 
            this.colTemplateExpiration.Text = "Expiration";
            this.colTemplateExpiration.Width = 145;
            // 
            // colTemplateMachineType
            // 
            this.colTemplateMachineType.Text = "Machine Type";
            this.colTemplateMachineType.Width = 105;
            // 
            // colFileName
            // 
            this.colFileName.Text = "File Name";
            this.colFileName.Width = 250;
            // 
            // btnLoadTemplateFile
            // 
            this.btnLoadTemplateFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLoadTemplateFile.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnLoadTemplateFile.Location = new System.Drawing.Point(408, 235);
            this.btnLoadTemplateFile.Name = "btnLoadTemplateFile";
            this.btnLoadTemplateFile.Size = new System.Drawing.Size(80, 23);
            this.btnLoadTemplateFile.TabIndex = 10;
            this.btnLoadTemplateFile.Text = "Load File";
            this.FNEToolTip.SetToolTip(this.btnLoadTemplateFile, "Load the specified template file into the current template collection");
            this.btnLoadTemplateFile.UseVisualStyleBackColor = false;
            this.btnLoadTemplateFile.Click += new System.EventHandler(this.btnLoadTemplateFile_Click);
            // 
            // lblTemplateLoadComplete
            // 
            this.lblTemplateLoadComplete.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTemplateLoadComplete.AutoEllipsis = true;
            this.lblTemplateLoadComplete.ForeColor = System.Drawing.Color.Blue;
            this.lblTemplateLoadComplete.Location = new System.Drawing.Point(6, 216);
            this.lblTemplateLoadComplete.Name = "lblTemplateLoadComplete";
            this.lblTemplateLoadComplete.Size = new System.Drawing.Size(540, 13);
            this.lblTemplateLoadComplete.TabIndex = 8;
            this.lblTemplateLoadComplete.Visible = false;
            // 
            // btnInspectTemplateFile
            // 
            this.btnInspectTemplateFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInspectTemplateFile.BackColor = System.Drawing.Color.Beige;
            this.btnInspectTemplateFile.Location = new System.Drawing.Point(302, 235);
            this.btnInspectTemplateFile.Name = "btnInspectTemplateFile";
            this.btnInspectTemplateFile.Size = new System.Drawing.Size(80, 23);
            this.btnInspectTemplateFile.TabIndex = 9;
            this.btnInspectTemplateFile.Text = "Inspect File";
            this.FNEToolTip.SetToolTip(this.btnInspectTemplateFile, "Inspect features contained within a license template file");
            this.btnInspectTemplateFile.UseVisualStyleBackColor = false;
            this.btnInspectTemplateFile.Click += new System.EventHandler(this.btnInspectTemplateFile_Click);
            // 
            // lblTemplates
            // 
            this.lblTemplates.AutoSize = true;
            this.lblTemplates.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTemplates.Location = new System.Drawing.Point(5, 10);
            this.lblTemplates.Name = "lblTemplates";
            this.lblTemplates.Size = new System.Drawing.Size(65, 13);
            this.lblTemplates.TabIndex = 1;
            this.lblTemplates.Text = "Templates";
            this.lblTemplates.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTemplateFile
            // 
            this.lblTemplateFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblTemplateFile.AutoSize = true;
            this.lblTemplateFile.Location = new System.Drawing.Point(0, 192);
            this.lblTemplateFile.Name = "lblTemplateFile";
            this.lblTemplateFile.Size = new System.Drawing.Size(73, 13);
            this.lblTemplateFile.TabIndex = 5;
            this.lblTemplateFile.Text = "Template File:";
            // 
            // btnBrowseTemplateFile
            // 
            this.btnBrowseTemplateFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowseTemplateFile.ImageIndex = 1;
            this.btnBrowseTemplateFile.ImageList = this.imageList;
            this.btnBrowseTemplateFile.Location = new System.Drawing.Point(520, 188);
            this.btnBrowseTemplateFile.Name = "btnBrowseTemplateFile";
            this.btnBrowseTemplateFile.Size = new System.Drawing.Size(32, 23);
            this.btnBrowseTemplateFile.TabIndex = 7;
            this.FNEToolTip.SetToolTip(this.btnBrowseTemplateFile, "Browse for a license template file");
            this.btnBrowseTemplateFile.UseVisualStyleBackColor = true;
            this.btnBrowseTemplateFile.Click += new System.EventHandler(this.btnBrowseTemplateFile_Click);
            // 
            // pageAcqRet
            // 
            this.pageAcqRet.Controls.Add(this.cbxVersion);
            this.pageAcqRet.Controls.Add(this.cbxFeatureName);
            this.pageAcqRet.Controls.Add(this.lblDoSelect);
            this.pageAcqRet.Controls.Add(this.btnReturnAll);
            this.pageAcqRet.Controls.Add(this.btnReturn);
            this.pageAcqRet.Controls.Add(this.btnAcquire);
            this.pageAcqRet.Controls.Add(this.txtCount);
            this.pageAcqRet.Controls.Add(this.lblCount);
            this.pageAcqRet.Controls.Add(this.lblVersion);
            this.pageAcqRet.Controls.Add(this.lblFeature);
            this.pageAcqRet.Controls.Add(this.lblAcquired);
            this.pageAcqRet.Controls.Add(this.lvAcquired);
            this.pageAcqRet.Location = new System.Drawing.Point(4, 22);
            this.pageAcqRet.Name = "pageAcqRet";
            this.pageAcqRet.Size = new System.Drawing.Size(576, 566);
            this.pageAcqRet.TabIndex = 2;
            this.pageAcqRet.Text = "Acquire/Return";
            this.pageAcqRet.ToolTipText = "Acquire and return licenses";
            this.pageAcqRet.UseVisualStyleBackColor = true;
            // 
            // cbxVersion
            // 
            this.cbxVersion.Location = new System.Drawing.Point(70, 42);
            this.cbxVersion.Name = "cbxVersion";
            this.cbxVersion.Size = new System.Drawing.Size(170, 21);
            this.cbxVersion.TabIndex = 4;
            this.FNEToolTip.SetToolTip(this.cbxVersion, "Specify the feature version to acquire");
            this.cbxVersion.TextChanged += new System.EventHandler(this.errorProvider_reset);
            // 
            // cbxFeatureName
            // 
            this.cbxFeatureName.Location = new System.Drawing.Point(70, 12);
            this.cbxFeatureName.Name = "cbxFeatureName";
            this.cbxFeatureName.Size = new System.Drawing.Size(230, 21);
            this.cbxFeatureName.TabIndex = 2;
            this.FNEToolTip.SetToolTip(this.cbxFeatureName, "Specify the feature name to acquire");
            this.cbxFeatureName.TextChanged += new System.EventHandler(this.cbxFeatureName_TextChanged);
            // 
            // lblDoSelect
            // 
            this.lblDoSelect.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDoSelect.AutoSize = true;
            this.lblDoSelect.ForeColor = System.Drawing.Color.Blue;
            this.lblDoSelect.Location = new System.Drawing.Point(15, 538);
            this.lblDoSelect.Name = "lblDoSelect";
            this.lblDoSelect.Size = new System.Drawing.Size(223, 13);
            this.lblDoSelect.TabIndex = 10;
            this.lblDoSelect.Text = "Select one or more acquired licenses to return";
            this.lblDoSelect.Visible = false;
            // 
            // btnReturnAll
            // 
            this.btnReturnAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReturnAll.BackColor = System.Drawing.Color.Gainsboro;
            this.btnReturnAll.Enabled = false;
            this.btnReturnAll.Location = new System.Drawing.Point(492, 536);
            this.btnReturnAll.Name = "btnReturnAll";
            this.btnReturnAll.Size = new System.Drawing.Size(75, 23);
            this.btnReturnAll.TabIndex = 12;
            this.btnReturnAll.Text = "Return All";
            this.FNEToolTip.SetToolTip(this.btnReturnAll, "Return all features which are returnable");
            this.btnReturnAll.UseVisualStyleBackColor = false;
            this.btnReturnAll.Click += new System.EventHandler(this.btnReturnAll_Click);
            // 
            // btnReturn
            // 
            this.btnReturn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReturn.BackColor = System.Drawing.Color.Gainsboro;
            this.btnReturn.Enabled = false;
            this.btnReturn.Location = new System.Drawing.Point(372, 536);
            this.btnReturn.Name = "btnReturn";
            this.btnReturn.Size = new System.Drawing.Size(95, 23);
            this.btnReturn.TabIndex = 11;
            this.btnReturn.Text = "Return Selected";
            this.FNEToolTip.SetToolTip(this.btnReturn, "Return those selected features which are returnable");
            this.btnReturn.UseVisualStyleBackColor = false;
            this.btnReturn.Click += new System.EventHandler(this.btnReturn_Click);
            // 
            // btnAcquire
            // 
            this.btnAcquire.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnAcquire.Location = new System.Drawing.Point(225, 73);
            this.btnAcquire.Name = "btnAcquire";
            this.btnAcquire.Size = new System.Drawing.Size(75, 23);
            this.btnAcquire.TabIndex = 7;
            this.btnAcquire.Text = "Acquire";
            this.FNEToolTip.SetToolTip(this.btnAcquire, "Acquire the specified feature");
            this.btnAcquire.UseVisualStyleBackColor = false;
            this.btnAcquire.Click += new System.EventHandler(this.btnAcquire_Click);
            // 
            // txtCount
            // 
            this.txtCount.BackColor = System.Drawing.SystemColors.Window;
            this.txtCount.Location = new System.Drawing.Point(70, 73);
            this.txtCount.Name = "txtCount";
            this.txtCount.Size = new System.Drawing.Size(107, 20);
            this.txtCount.TabIndex = 6;
            this.FNEToolTip.SetToolTip(this.txtCount, "Specify the feature count to acquire");
            this.txtCount.TextChanged += new System.EventHandler(this.txtCount_TextChanged);
            // 
            // lblCount
            // 
            this.lblCount.AutoSize = true;
            this.lblCount.Location = new System.Drawing.Point(15, 75);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(41, 13);
            this.lblCount.TabIndex = 5;
            this.lblCount.Text = "Count: ";
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Location = new System.Drawing.Point(15, 45);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(48, 13);
            this.lblVersion.TabIndex = 3;
            this.lblVersion.Text = "Version: ";
            // 
            // lblFeature
            // 
            this.lblFeature.AutoSize = true;
            this.lblFeature.Location = new System.Drawing.Point(15, 15);
            this.lblFeature.Name = "lblFeature";
            this.lblFeature.Size = new System.Drawing.Size(46, 13);
            this.lblFeature.TabIndex = 1;
            this.lblFeature.Text = "Feature:";
            // 
            // lblAcquired
            // 
            this.lblAcquired.AutoSize = true;
            this.lblAcquired.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAcquired.Location = new System.Drawing.Point(0, 105);
            this.lblAcquired.Name = "lblAcquired";
            this.lblAcquired.Size = new System.Drawing.Size(115, 13);
            this.lblAcquired.TabIndex = 8;
            this.lblAcquired.Text = "Acquired Licenses:";
            // 
            // lvAcquired
            // 
            this.lvAcquired.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvAcquired.BackColor = System.Drawing.Color.Gainsboro;
            this.lvAcquired.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colAcquiredFeatureType,
            this.colAcquiredFeatureName,
            this.colAcquiredFeatureVersion,
            this.colAcquiredFeatureCount,
            this.colAcquiredFeatureExpiration,
            this.colAcquiredVendorString});
            this.lvAcquired.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lvAcquired.FullRowSelect = true;
            this.lvAcquired.GridLines = true;
            this.lvAcquired.HideSelection = false;
            this.lvAcquired.Location = new System.Drawing.Point(0, 130);
            this.lvAcquired.Name = "lvAcquired";
            this.lvAcquired.ShowItemToolTips = true;
            this.lvAcquired.Size = new System.Drawing.Size(576, 400);
            this.lvAcquired.SmallImageList = this.licenseImageList;
            this.lvAcquired.TabIndex = 9;
            this.lvAcquired.UseCompatibleStateImageBehavior = false;
            this.lvAcquired.View = System.Windows.Forms.View.Details;
            this.lvAcquired.SelectedIndexChanged += new System.EventHandler(this.lvAcquired_SelectedIndexChanged);
            this.lvAcquired.DoubleClick += new System.EventHandler(this.lvLicense_DoubleClick);
            // 
            // colAcquiredFeatureType
            // 
            this.colAcquiredFeatureType.Text = "Type";
            // 
            // colAcquiredFeatureName
            // 
            this.colAcquiredFeatureName.Text = "Feature";
            this.colAcquiredFeatureName.Width = 203;
            // 
            // colAcquiredFeatureVersion
            // 
            this.colAcquiredFeatureVersion.Text = "Version";
            this.colAcquiredFeatureVersion.Width = 94;
            // 
            // colAcquiredFeatureCount
            // 
            this.colAcquiredFeatureCount.Text = "Count";
            this.colAcquiredFeatureCount.Width = 76;
            // 
            // colAcquiredFeatureExpiration
            // 
            this.colAcquiredFeatureExpiration.Text = "Expiration";
            this.colAcquiredFeatureExpiration.Width = 140;
            // 
            // colAcquiredVendorString
            // 
            this.colAcquiredVendorString.Text = "Vendor String";
            this.colAcquiredVendorString.Width = 150;
            // 
            // FNEToolTip
            // 
            this.FNEToolTip.ShowAlways = true;
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(95, 83);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(400, 20);
            this.textBox1.TabIndex = 14;
            this.FNEToolTip.SetToolTip(this.textBox1, "Optional file in which to store the capability response data");
            // 
            // textBox2
            // 
            this.textBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox2.Location = new System.Drawing.Point(95, 48);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(400, 20);
            this.textBox2.TabIndex = 12;
            this.FNEToolTip.SetToolTip(this.textBox2, "Optional file in which to store the capability response data");
            // 
            // textBox3
            // 
            this.textBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox3.Location = new System.Drawing.Point(95, 13);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(400, 20);
            this.textBox3.TabIndex = 10;
            this.FNEToolTip.SetToolTip(this.textBox3, "Optional file in which to store the capability response data");
            // 
            // textBox4
            // 
            this.textBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox4.Location = new System.Drawing.Point(370, 223);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(125, 20);
            this.textBox4.TabIndex = 19;
            this.FNEToolTip.SetToolTip(this.textBox4, "Optional served feature borrow time interval");
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.Location = new System.Drawing.Point(110, 223);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 17;
            this.FNEToolTip.SetToolTip(this.comboBox1, "Served feature borrow time granularity");
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.button1.Location = new System.Drawing.Point(420, 82);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "Generate";
            this.FNEToolTip.SetToolTip(this.button1, "Generate a capability request correlation ID value");
            this.button1.UseVisualStyleBackColor = false;
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.comboBox2.Location = new System.Drawing.Point(95, 13);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(120, 21);
            this.comboBox2.TabIndex = 2;
            this.FNEToolTip.SetToolTip(this.comboBox2, "Select global license server capability request operation type");
            // 
            // textBox5
            // 
            this.textBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox5.Location = new System.Drawing.Point(95, 188);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(400, 20);
            this.textBox5.TabIndex = 15;
            this.FNEToolTip.SetToolTip(this.textBox5, "Optional capability request requestor ID");
            // 
            // textBox6
            // 
            this.textBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox6.Location = new System.Drawing.Point(95, 153);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(400, 20);
            this.textBox6.TabIndex = 13;
            this.FNEToolTip.SetToolTip(this.textBox6, "Optional capability request enterprise ID");
            // 
            // textBox7
            // 
            this.textBox7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox7.Location = new System.Drawing.Point(95, 118);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(400, 20);
            this.textBox7.TabIndex = 11;
            this.FNEToolTip.SetToolTip(this.textBox7, "Optional capability request acquisition ID");
            // 
            // textBox8
            // 
            this.textBox8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox8.Location = new System.Drawing.Point(95, 83);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(315, 20);
            this.textBox8.TabIndex = 8;
            this.FNEToolTip.SetToolTip(this.textBox8, "Capability request correlation ID (required with global license server undo reque" +
                    "st)");
            // 
            // checkBox1
            // 
            this.checkBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(370, 15);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(122, 17);
            this.checkBox1.TabIndex = 4;
            this.checkBox1.Text = "One-Time Activation";
            this.FNEToolTip.SetToolTip(this.checkBox1, "Not typically used except by device manufacturers for device activation");
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(250, 15);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(91, 17);
            this.checkBox2.TabIndex = 3;
            this.checkBox2.Text = "Served Buffer";
            this.FNEToolTip.SetToolTip(this.checkBox2, "Request the capability response be generated as a served buffer response");
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel,
            this.toolStripProgressBar});
            this.statusStrip.Location = new System.Drawing.Point(0, 640);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.ShowItemToolTips = true;
            this.statusStrip.Size = new System.Drawing.Size(584, 22);
            this.statusStrip.SizingGrip = false;
            this.statusStrip.TabIndex = 3;
            this.statusStrip.Text = "statusStrip";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.AutoSize = false;
            this.toolStripStatusLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(569, 17);
            this.toolStripStatusLabel.Spring = true;
            this.toolStripStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripProgressBar
            // 
            this.toolStripProgressBar.Enabled = false;
            this.toolStripProgressBar.Name = "toolStripProgressBar";
            this.toolStripProgressBar.Size = new System.Drawing.Size(150, 16);
            this.toolStripProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.toolStripProgressBar.Visible = false;
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnToolStripOpen,
            this.btnToolStripSave,
            this.btnToolStripSaveAs,
            this.btnToolStripDelete,
            this.grpToolStrip,
            this.btnToolStripAbout,
            this.cbxToolStripConfig});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.ShowItemToolTips = true;
            this.menuStrip.Size = new System.Drawing.Size(584, 42);
            this.menuStrip.TabIndex = 1;
            // 
            // btnToolStripOpen
            // 
            this.btnToolStripOpen.Image = ((System.Drawing.Image)(resources.GetObject("btnToolStripOpen.Image")));
            this.btnToolStripOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnToolStripOpen.Name = "btnToolStripOpen";
            this.btnToolStripOpen.Size = new System.Drawing.Size(43, 35);
            this.btnToolStripOpen.Text = "&Open ";
            this.btnToolStripOpen.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnToolStripOpen.ToolTipText = "Open configuration file";
            this.btnToolStripOpen.Click += new System.EventHandler(this.btnToolStripOpen_Click);
            // 
            // btnToolStripSave
            // 
            this.btnToolStripSave.Enabled = false;
            this.btnToolStripSave.Image = ((System.Drawing.Image)(resources.GetObject("btnToolStripSave.Image")));
            this.btnToolStripSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnToolStripSave.Name = "btnToolStripSave";
            this.btnToolStripSave.Size = new System.Drawing.Size(35, 35);
            this.btnToolStripSave.Text = "&Save";
            this.btnToolStripSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnToolStripSave.ToolTipText = "Save current configuration";
            this.btnToolStripSave.Click += new System.EventHandler(this.btnToolStripSave_Click);
            // 
            // btnToolStripSaveAs
            // 
            this.btnToolStripSaveAs.Image = ((System.Drawing.Image)(resources.GetObject("btnToolStripSaveAs.Image")));
            this.btnToolStripSaveAs.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnToolStripSaveAs.Name = "btnToolStripSaveAs";
            this.btnToolStripSaveAs.Size = new System.Drawing.Size(51, 35);
            this.btnToolStripSaveAs.Text = "S&ave As";
            this.btnToolStripSaveAs.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnToolStripSaveAs.ToolTipText = "Save current configuration under a new name";
            this.btnToolStripSaveAs.Click += new System.EventHandler(this.btnToolStripSaveAs_Click);
            // 
            // btnToolStripDelete
            // 
            this.btnToolStripDelete.Enabled = false;
            this.btnToolStripDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnToolStripDelete.Image")));
            this.btnToolStripDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnToolStripDelete.Name = "btnToolStripDelete";
            this.btnToolStripDelete.Size = new System.Drawing.Size(44, 35);
            this.btnToolStripDelete.Text = "&Delete";
            this.btnToolStripDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnToolStripDelete.ToolTipText = "Delete current configuration";
            this.btnToolStripDelete.Click += new System.EventHandler(this.btnToolStripDelete_Click);
            // 
            // grpToolStrip
            // 
            this.grpToolStrip.Name = "grpToolStrip";
            this.grpToolStrip.Size = new System.Drawing.Size(6, 38);
            // 
            // btnToolStripAbout
            // 
            this.btnToolStripAbout.Image = ((System.Drawing.Image)(resources.GetObject("btnToolStripAbout.Image")));
            this.btnToolStripAbout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnToolStripAbout.Name = "btnToolStripAbout";
            this.btnToolStripAbout.Size = new System.Drawing.Size(44, 35);
            this.btnToolStripAbout.Text = "A&bout";
            this.btnToolStripAbout.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnToolStripAbout.ToolTipText = "About DotNetDemo";
            this.btnToolStripAbout.Click += new System.EventHandler(this.btnToolStripAbout_Click);
            // 
            // cbxToolStripConfig
            // 
            this.cbxToolStripConfig.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.cbxToolStripConfig.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxToolStripConfig.MaxDropDownItems = 20;
            this.cbxToolStripConfig.Name = "cbxToolStripConfig";
            this.cbxToolStripConfig.Size = new System.Drawing.Size(200, 38);
            this.cbxToolStripConfig.ToolTipText = "Current configuration";
            this.cbxToolStripConfig.SelectedIndexChanged += new System.EventHandler(this.cbxToolStripConfig_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 85);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Enterprise ID:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Activation ID:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Correlation ID:";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(280, 225);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Borrow Interval:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 225);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Borrow Granularity:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Operation:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 190);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Requestor ID:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 155);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Enterprise ID:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 120);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Acquisition ID:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 85);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 13);
            this.label10.TabIndex = 7;
            this.label10.Text = "Correlation ID:";
            // 
            // DotNetDemoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(584, 662);
            this.Controls.Add(this.menuStrip);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.tabMain);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(500, 600);
            this.Name = "DotNetDemoForm";
            this.Text = "FlexNet Embedded .NET XT Toolbox 9.0";
            this.Load += new System.EventHandler(this.DotNetDemoForm_Load);
            this.SystemColorsChanged += new System.EventHandler(this.DotNetDemoForm_SystemColorsChanged);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DotNetDemoForm_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DotNetDemo_KeyDown);
            this.tabMain.ResumeLayout(false);
            this.pageSetup.ResumeLayout(false);
            this.pnlHostInfo.ResumeLayout(false);
            this.pnlHostInfo.PerformLayout();
            this.pnlHostId.ResumeLayout(false);
            this.pnlHostId.PerformLayout();
            this.pnlIdentity.ResumeLayout(false);
            this.pnlIdentity.PerformLayout();
            this.pageLicenseSourceCollection.ResumeLayout(false);
            this.pageLicenseSourceCollection.PerformLayout();
            this.tabLicenses.ResumeLayout(false);
            this.pageAllLicenses.ResumeLayout(false);
            this.pageTrustedStorageLicenses.ResumeLayout(false);
            this.pageTrialLicenses.ResumeLayout(false);
            this.pageShortCodeLicenses.ResumeLayout(false);
            this.pageBufferLicenses.ResumeLayout(false);
            this.pageServer.ResumeLayout(false);
            this.pageServer.PerformLayout();
            this.tabServerRequest.ResumeLayout(false);
            this.pageCapabilityRequest.ResumeLayout(false);
            this.pageCapabilityRequest.PerformLayout();
            this.tabCapabilityRequest.ResumeLayout(false);
            this.pageCR_Options.ResumeLayout(false);
            this.pageCR_Options.PerformLayout();
            this.pageCR_LLS.ResumeLayout(false);
            this.pageCR_LLS.PerformLayout();
            this.pageCR_BO.ResumeLayout(false);
            this.pageCR_BO.PerformLayout();
            this.pageCR_VD.ResumeLayout(false);
            this.pageCR_VD.PerformLayout();
            this.pageCR_FS.ResumeLayout(false);
            this.pageCR_FS.PerformLayout();
            this.pageCR_AuxHostIds.ResumeLayout(false);
            this.pageCR_AuxHostIds.PerformLayout();
            this.pageCR_Resp.ResumeLayout(false);
            this.pageInfoMessage.ResumeLayout(false);
            this.pageInfoMessage.PerformLayout();
            this.pnlFeatureUsage.ResumeLayout(false);
            this.pnlFeatureUsage.PerformLayout();
            this.pageShortCodes.ResumeLayout(false);
            this.tabTemplate.ResumeLayout(false);
            this.pageTemplateFeatures.ResumeLayout(false);
            this.pageTemplateVD.ResumeLayout(false);
            this.pageTemplateRequest.ResumeLayout(false);
            this.pageTemplateRequest.PerformLayout();
            this.pageTemplateResponse.ResumeLayout(false);
            this.pageTemplateResponse.PerformLayout();
            this.panelTemplates.ResumeLayout(false);
            this.panelTemplates.PerformLayout();
            this.pageAcqRet.ResumeLayout(false);
            this.pageAcqRet.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.TabPage pageLicenseSourceCollection;
        private System.Windows.Forms.Button btnLicenseFileBrowse;
        private System.Windows.Forms.Label lblLicenseFile;
        private System.Windows.Forms.TabControl tabLicenses;
        private System.Windows.Forms.TabPage pageAllLicenses;
        private System.Windows.Forms.TabPage pageTrustedStorageLicenses;
        private System.Windows.Forms.TabPage pageBufferLicenses;
        private System.Windows.Forms.TabPage pageTrialLicenses;
        private System.Windows.Forms.ListView lvTrialFeatures;
        private System.Windows.Forms.ColumnHeader colTrialFeatureName;
        private System.Windows.Forms.ColumnHeader colTrialFeatureVersion;
        private System.Windows.Forms.ColumnHeader colTrialFeatureCount;
        private System.Windows.Forms.ColumnHeader colTrialFeatureExpiration;
        private System.Windows.Forms.TabPage pageServer;
        private System.Windows.Forms.Label lblServerURL;
        private System.Windows.Forms.TextBox txtServerURL;
        private System.Windows.Forms.Label lblServerInfo;
        private System.Windows.Forms.TabPage pageAcqRet;
        private System.Windows.Forms.Label lblAcquired;
        private System.Windows.Forms.ListView lvAcquired;
        private System.Windows.Forms.ColumnHeader colAcquiredFeatureName;
        private System.Windows.Forms.ColumnHeader colAcquiredFeatureVersion;
        private System.Windows.Forms.ColumnHeader colAcquiredFeatureCount;
        private System.Windows.Forms.ColumnHeader colAcquiredFeatureExpiration;
        private System.Windows.Forms.Button btnReturn;
        private System.Windows.Forms.Button btnAcquire;
        private System.Windows.Forms.TextBox txtCount;
        private System.Windows.Forms.Label lblCount;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Label lblFeature;
        private System.Windows.Forms.ColumnHeader colTrialType;
        private System.Windows.Forms.ListView lvAllFeatures;
        private System.Windows.Forms.ColumnHeader colAllType;
        private System.Windows.Forms.ColumnHeader colAllFeatureName;
        private System.Windows.Forms.ColumnHeader colAllFeatureVersion;
        private System.Windows.Forms.ColumnHeader colAllFeatureCount;
        private System.Windows.Forms.ColumnHeader colAllFeatureExpiration;
        private System.Windows.Forms.ListView lvTrustedStorageFeatures;
        private System.Windows.Forms.ColumnHeader colTSType;
        private System.Windows.Forms.ColumnHeader colTSFeatureName;
        private System.Windows.Forms.ColumnHeader colTSFeatureVersion;
        private System.Windows.Forms.ColumnHeader colTSFeatureCount;
        private System.Windows.Forms.ColumnHeader colTSFeatureExpiration;
        private System.Windows.Forms.ListView lvBufferFeatures;
        private System.Windows.Forms.ColumnHeader colBufferType;
        private System.Windows.Forms.ColumnHeader colBufferFeatureName;
        private System.Windows.Forms.ColumnHeader colBufferFeatureVersion;
        private System.Windows.Forms.ColumnHeader colBufferFeatrureCount;
        private System.Windows.Forms.ColumnHeader colBufferFeatureExpiration;
        private System.Windows.Forms.Label lblLoadComplete;
        private System.Windows.Forms.Button btnInspectLicenseFile;
        private System.Windows.Forms.Button btnLoadLicenseFile;
        private System.Windows.Forms.ToolTip FNEToolTip;
        private System.Windows.Forms.Label lblServerType;
        private System.Windows.Forms.Button btnReturnAll;
        private System.Windows.Forms.CheckBox ckbTrials;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label lblDoSelect;
        private System.Windows.Forms.TabPage pageSetup;
        private System.Windows.Forms.TextBox txtHostId;
        private System.Windows.Forms.Button btnBrowsePath;
        private System.Windows.Forms.Label lblStoragePath;
        private System.Windows.Forms.Label lblHostId;
        private System.Windows.Forms.ComboBox cbxHostIdType;
        private System.Windows.Forms.Button btnRestart;
        private System.Windows.Forms.Button btnSetIdentity;
        private System.Windows.Forms.Label lblSetupNotice;
        private System.Windows.Forms.Button btnBrowseIdentity;
        private System.Windows.Forms.TextBox txtIdentityFile;
        private System.Windows.Forms.Label lblIdentity;
        private System.Windows.Forms.TextBox txtStoragePath;
        private System.Windows.Forms.Button btnSetHostId;
        private System.Windows.Forms.TabControl tabServerRequest;
        private System.Windows.Forms.TabPage pageCapabilityRequest;
        private System.Windows.Forms.TabPage pageInfoMessage;
        private System.Windows.Forms.Button btnCRSend;
        private System.Windows.Forms.TabControl tabCapabilityRequest;
        private System.Windows.Forms.CheckBox ckbProcessResponse;
        private System.Windows.Forms.CheckBox ckbForceResponse;
        private System.Windows.Forms.TabPage pageCR_LLS;
        private System.Windows.Forms.Button btnLLSAdd;
        private System.Windows.Forms.ListView lvLLSFeatures;
        private System.Windows.Forms.ColumnHeader colLLSFeatureName;
        private System.Windows.Forms.ColumnHeader colLLSFeatureVersion;
        private System.Windows.Forms.TabPage pageCR_Resp;
        private System.Windows.Forms.ListView lvCRResponse;
        private System.Windows.Forms.ColumnHeader colRespFeature;
        private System.Windows.Forms.ColumnHeader colRespVersion;
        private System.Windows.Forms.ColumnHeader colRespExpiration;
        private System.Windows.Forms.ColumnHeader colRespCount;
        private System.Windows.Forms.Button btnRespFile;
        private System.Windows.Forms.TextBox txtRespFile;
        private System.Windows.Forms.Label lblRespFile;
        private System.Windows.Forms.TextBox txtLLSVersion;
        private System.Windows.Forms.TextBox txtLLSCount;
        private System.Windows.Forms.TextBox txtLLSFeature;
        private System.Windows.Forms.Label lblLLSCount;
        private System.Windows.Forms.Label lblLLSVersion;
        private System.Windows.Forms.Label lblLLSFeature;
        private System.Windows.Forms.ComboBox cbxServerType;
        private System.Windows.Forms.ColumnHeader colLLSFeatureCount;
        private System.Windows.Forms.TabPage pageCR_BO;
        private System.Windows.Forms.TextBox txtBOCount;
        private System.Windows.Forms.TextBox txtBORights;
        private System.Windows.Forms.Label lblBOCount;
        private System.Windows.Forms.Label lblBORights;
        private System.Windows.Forms.Button btnBOAdd;
        private System.Windows.Forms.ListView lvBORights;
        private System.Windows.Forms.ColumnHeader colBORightsName;
        private System.Windows.Forms.ColumnHeader colBORightsCount;
        private System.Windows.Forms.Panel pnlIdentity;
        private System.Windows.Forms.Button btnLLSRemove;
        private System.Windows.Forms.Label lblSetupIdentity;
        private System.Windows.Forms.Panel pnlHostId;
        private System.Windows.Forms.Label lblSetupHostId;
        private System.Windows.Forms.Button btnBORemove;
        private System.Windows.Forms.Button btnIMSend;
        private System.Windows.Forms.Panel pnlFeatureUsage;
        private System.Windows.Forms.Button btnIMRemove;
        private System.Windows.Forms.TextBox txtIMVersion;
        private System.Windows.Forms.TextBox txtIMCount;
        private System.Windows.Forms.TextBox txtIMFeature;
        private System.Windows.Forms.Label lblIMCount;
        private System.Windows.Forms.Label lblIMVersion;
        private System.Windows.Forms.Label lblIMFeature;
        private System.Windows.Forms.Button btnIMAdd;
        private System.Windows.Forms.ListView lvIMFeatures;
        private System.Windows.Forms.ColumnHeader colIMFeature;
        private System.Windows.Forms.ColumnHeader colIMVersion;
        private System.Windows.Forms.ColumnHeader colIMCount;
        private System.Windows.Forms.CheckBox ckbIMAddExisting;
        private System.Windows.Forms.DateTimePicker dtIMExpiration;
        private System.Windows.Forms.Label lblIMExpiration;
        private System.Windows.Forms.ColumnHeader colIMExpiration;
        private System.Windows.Forms.TabPage pageCR_VD;
        private System.Windows.Forms.ListView lvVendorDictionary;
        private System.Windows.Forms.ColumnHeader colVDType;
        private System.Windows.Forms.ColumnHeader colVDKey;
        private System.Windows.Forms.ColumnHeader colVDValue;
        private System.Windows.Forms.ComboBox cbxVDType;
        private System.Windows.Forms.Button btnVDRemove;
        private System.Windows.Forms.TextBox txtVDKey;
        private System.Windows.Forms.TextBox txtVDValue;
        private System.Windows.Forms.Label lblVDValue;
        private System.Windows.Forms.Label lblVDKey;
        private System.Windows.Forms.Label lblVDType;
        private System.Windows.Forms.Button btnVDAdd;
        private System.Windows.Forms.ColumnHeader colAllVendorString;
        private System.Windows.Forms.ColumnHeader colTSVendorString;
        private System.Windows.Forms.Button btnCRResponseDetails;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.ComboBox cbxHostId;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripButton btnToolStripOpen;
        private System.Windows.Forms.ToolStripButton btnToolStripSave;
        private System.Windows.Forms.ToolStripButton btnToolStripSaveAs;
        private System.Windows.Forms.ToolStripButton btnToolStripDelete;
        private System.Windows.Forms.ToolStripSeparator grpToolStrip;
        private System.Windows.Forms.ToolStripComboBox cbxToolStripConfig;
        private System.Windows.Forms.ComboBox cbxLicenseFile;
        private System.Windows.Forms.ColumnHeader colTrialVendorString;
        private System.Windows.Forms.ColumnHeader colBufferVendorString;
        private System.Windows.Forms.Button btnResetTrials;
        private System.Windows.Forms.Button btnResetTS;
        private System.Windows.Forms.ComboBox cbxVersion;
        private System.Windows.Forms.ComboBox cbxFeatureName;
        private System.Windows.Forms.Button btnCRSave;
        private System.Windows.Forms.Button btnIMSave;
        private System.Windows.Forms.Panel pnlHostInfo;
        private System.Windows.Forms.Label lblSetupHostInfo;
        private System.Windows.Forms.TextBox txtHostType;
        private System.Windows.Forms.Label lblHostName;
        private System.Windows.Forms.TextBox txtHostName;
        private System.Windows.Forms.Label lblHostType;
        private System.Windows.Forms.Button btnContinue;
        private System.Windows.Forms.ToolStripButton btnToolStripAbout;
        private System.Windows.Forms.Label lblHostIDType;
        private System.Windows.Forms.ColumnHeader colAcquiredVendorString;
        private System.Windows.Forms.Button btnResetSC;
        private System.Windows.Forms.CheckBox ckbShortCode;
        private System.Windows.Forms.TabPage pageShortCodeLicenses;
        private System.Windows.Forms.ListView lvShortCodeFeatures;
        private System.Windows.Forms.ColumnHeader colSCType;
        private System.Windows.Forms.ColumnHeader colSCFeature;
        private System.Windows.Forms.ColumnHeader colSCVersion;
        private System.Windows.Forms.ColumnHeader colSCCount;
        private System.Windows.Forms.ColumnHeader colSCExpiration;
        private System.Windows.Forms.ColumnHeader colSCVendorString;
        private System.Windows.Forms.TabPage pageShortCodes;
        private System.Windows.Forms.Panel panelTemplates;
        private System.Windows.Forms.Label lblTemplates;
        private System.Windows.Forms.Button btnBrowseTemplateFile;
        private System.Windows.Forms.Label lblTemplateFile;
        private System.Windows.Forms.Button btnLoadTemplateFile;
        private System.Windows.Forms.Button btnInspectTemplateFile;
        private System.Windows.Forms.Label lblTemplateLoadComplete;
        private System.Windows.Forms.TabControl tabTemplate;
        private System.Windows.Forms.TabPage pageTemplateFeatures;
        private System.Windows.Forms.ListView lvTemplateFeatures;
        private System.Windows.Forms.ColumnHeader colTemplateFeature;
        private System.Windows.Forms.ColumnHeader colTemplateVersion;
        private System.Windows.Forms.ColumnHeader colTemplateCount;
        private System.Windows.Forms.TabPage pageTemplateVD;
        private System.Windows.Forms.ListView lvTemplateVD;
        private System.Windows.Forms.ColumnHeader colTemplateVDType;
        private System.Windows.Forms.ColumnHeader colTemplateVDKey;
        private System.Windows.Forms.ColumnHeader colTemplateVDValue;
        private System.Windows.Forms.ListView lvTemplates;
        private System.Windows.Forms.ColumnHeader colTemplateID;
        private System.Windows.Forms.ColumnHeader colTemplateExpiration;
        private System.Windows.Forms.ColumnHeader colTemplateMachineType;
        private System.Windows.Forms.ColumnHeader colFileName;
        private System.Windows.Forms.Button btnClearTemplates;
        private System.Windows.Forms.TabPage pageTemplateRequest;
        private System.Windows.Forms.TabPage pageTemplateResponse;
        private System.Windows.Forms.ComboBox cbxTemplateFile;
        private System.Windows.Forms.Label lblEncodingType;
        private System.Windows.Forms.TextBox txtEncodingChars;
        private System.Windows.Forms.Label lblEncodingChars;
        private System.Windows.Forms.ComboBox cbxEncodingType;
        private System.Windows.Forms.TextBox txtEncodingSegSize;
        private System.Windows.Forms.Label lblEncodingSegSize;
        private System.Windows.Forms.Button btnGenerateSCRequest;
        private System.Windows.Forms.ColumnHeader colTemplateExpiry;
        private System.Windows.Forms.ColumnHeader colTemplateVS;
        private System.Windows.Forms.Label lblSCRequestFile;
        private System.Windows.Forms.Button btnSCRequestFile;
        private System.Windows.Forms.TextBox txtSCRequestFile;
        private System.Windows.Forms.TextBox txtSCRequestPD;
        private System.Windows.Forms.Label lblSCRequestPD;
        private System.Windows.Forms.Button btnSCRequestPD;
        private System.Windows.Forms.RadioButton rbSCRequestPDHex;
        private System.Windows.Forms.RadioButton rbSCRequestPDChar;
        private System.Windows.Forms.Button btnSCResponseFile;
        private System.Windows.Forms.TextBox txtSCResponsePD;
        private System.Windows.Forms.Label lblSCResponsePD;
        private System.Windows.Forms.Button btnSCResponsePD;
        private System.Windows.Forms.Label lblSCResponseFile;
        private System.Windows.Forms.Button btnProcessSCResponse;
        private System.Windows.Forms.TextBox txtDecodingSegSize;
        private System.Windows.Forms.Label lblDecodingSegSize;
        private System.Windows.Forms.TextBox txtDecodingChars;
        private System.Windows.Forms.Label lblDecodingChars;
        private System.Windows.Forms.ComboBox cbxDecodingType;
        private System.Windows.Forms.Label lblDecodingType;
        private System.Windows.Forms.Button btnReadSCResponse;
        private System.Windows.Forms.TextBox txtSCResponseID;
        private System.Windows.Forms.Label lblSCResponseID;
        private System.Windows.Forms.ComboBox cbxSCResponseFile;
        private System.Windows.Forms.ColumnHeader colRespType;
        private System.Windows.Forms.ColumnHeader colTemplateType;
        private System.Windows.Forms.ColumnHeader colAcquiredFeatureType;
        private System.Windows.Forms.CheckBox ckbHTTPS;
        private System.Windows.Forms.TabPage pageCR_Options;
        private System.Windows.Forms.Label lblCorrelationID;
        private System.Windows.Forms.Label lblRequestorID;
        private System.Windows.Forms.TextBox txtEnterpriseID;
        private System.Windows.Forms.Label lblEnterpriseID;
        private System.Windows.Forms.TextBox txtAcquisitionID;
        private System.Windows.Forms.Label lblAcquisitionID;
        private System.Windows.Forms.TextBox txtCorrelationID;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtRequestorID;
        private System.Windows.Forms.ComboBox cbxOperation;
        private System.Windows.Forms.Label lblOperation;
        private System.Windows.Forms.CheckBox ckbOTA;
        private System.Windows.Forms.TextBox txtBorrowInterval;
        private System.Windows.Forms.Label lblBorrowInterval;
        private System.Windows.Forms.ComboBox cbxBorrowGranularity;
        private System.Windows.Forms.Label lblBorrowGranularity;
        private System.Windows.Forms.ImageList licenseImageList;
        private System.Windows.Forms.CheckBox ckbVMDetectionEnabled;
        private System.Windows.Forms.Button btnServerAdvanced;
        private System.Windows.Forms.Button btnDeleteTemplateLicenses;
        private System.Windows.Forms.Panel pnlTSInstances;
        private System.Windows.Forms.Label lblTrustedStorage;
        private System.Windows.Forms.Button btnTrustedStorage;
        private System.Windows.Forms.ComboBox cbxTSInstance;
        private System.Windows.Forms.Label lblTSInstance;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox ckbIncremental;
        private System.Windows.Forms.TabPage pageCR_FS;
        private System.Windows.Forms.ComboBox cbxFSType;
        private System.Windows.Forms.Button btnFSRemove;
        private System.Windows.Forms.TextBox txtFSKey;
        private System.Windows.Forms.TextBox txtFSValue;
        private System.Windows.Forms.Label lblFSValue;
        private System.Windows.Forms.Label lblFSKey;
        private System.Windows.Forms.Label lblFSType;
        private System.Windows.Forms.Button btnFSAdd;
        private System.Windows.Forms.ListView lvFeatureSelectors;
        private System.Windows.Forms.ColumnHeader colFSType;
        private System.Windows.Forms.ColumnHeader colFSKey;
        private System.Windows.Forms.ColumnHeader colFSValue;
        private System.Windows.Forms.CheckBox ckbLLSPartial;
        private System.Windows.Forms.ColumnHeader colLLSFeaturePartial;
        private System.Windows.Forms.TabPage pageCR_AuxHostIds;
        private System.Windows.Forms.Button btnAuxHostIdRemove;
        private System.Windows.Forms.TextBox txtAuxHostIdValue;
        private System.Windows.Forms.Label lblAuxHostIdValue;
        private System.Windows.Forms.Label lblAuxHostIdType;
        private System.Windows.Forms.Button btnAuxHostIdAdd;
        private System.Windows.Forms.ListView lvAuxHostIds;
        private System.Windows.Forms.ColumnHeader colAuxHostIdType;
        private System.Windows.Forms.ColumnHeader colAuxHostIdValue;
        private System.Windows.Forms.ComboBox cbxAuxHostIdType;
        private System.Windows.Forms.CheckBox ckbRequestAll;
        private System.Windows.Forms.ColumnHeader colRespVendorString;
        private System.Windows.Forms.CheckBox ckbBOPartial;
        private System.Windows.Forms.ColumnHeader colBORightsPartial;



    }
}

