// <copyright file="AdvancedServer.Designer.cs" company="Flexera Software LLC">
//     Copyright (c) 2012-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

namespace DotNetDemo
{
    partial class AdvancedServer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblProxyServer = new System.Windows.Forms.Label();
            this.txtProxyServer = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblProxyPort = new System.Windows.Forms.Label();
            this.txtProxyPort = new System.Windows.Forms.TextBox();
            this.pnlHttpHeaders = new System.Windows.Forms.Panel();
            this.lblAdditionalHttpHeaders = new System.Windows.Forms.Label();
            this.lblHttpHeaderValue = new System.Windows.Forms.Label();
            this.txtHttpHeaderValue = new System.Windows.Forms.TextBox();
            this.lblHttpHeaderType = new System.Windows.Forms.Label();
            this.txtHttpHeaderType = new System.Windows.Forms.TextBox();
            this.btnHttpHeaderAdd = new System.Windows.Forms.Button();
            this.btnHttpHeaderRemove = new System.Windows.Forms.Button();
            this.lvAdditionalHttpHeaders = new System.Windows.Forms.ListView();
            this.colHttpHeaderType = new System.Windows.Forms.ColumnHeader();
            this.colHttpHeaderValue = new System.Windows.Forms.ColumnHeader();
            this.pnlHttpHeaders.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblProxyServer
            // 
            this.lblProxyServer.AutoSize = true;
            this.lblProxyServer.Location = new System.Drawing.Point(10, 10);
            this.lblProxyServer.Name = "lblProxyServer";
            this.lblProxyServer.Size = new System.Drawing.Size(70, 13);
            this.lblProxyServer.TabIndex = 1;
            this.lblProxyServer.Text = "Proxy Server:";
            // 
            // txtProxyServer
            // 
            this.txtProxyServer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtProxyServer.Location = new System.Drawing.Point(90, 8);
            this.txtProxyServer.Name = "txtProxyServer";
            this.txtProxyServer.Size = new System.Drawing.Size(350, 20);
            this.txtProxyServer.TabIndex = 2;
            this.txtProxyServer.TextChanged += new System.EventHandler(this.txtProxyServer_TextChanged);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSave.Location = new System.Drawing.Point(383, 375);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.Color.Gainsboro;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(482, 375);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            // 
            // lblProxyPort
            // 
            this.lblProxyPort.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblProxyPort.AutoSize = true;
            this.lblProxyPort.Location = new System.Drawing.Point(460, 10);
            this.lblProxyPort.Name = "lblProxyPort";
            this.lblProxyPort.Size = new System.Drawing.Size(29, 13);
            this.lblProxyPort.TabIndex = 3;
            this.lblProxyPort.Text = "Port:";
            // 
            // txtProxyPort
            // 
            this.txtProxyPort.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtProxyPort.Location = new System.Drawing.Point(500, 8);
            this.txtProxyPort.Name = "txtProxyPort";
            this.txtProxyPort.Size = new System.Drawing.Size(65, 20);
            this.txtProxyPort.TabIndex = 4;
            this.txtProxyPort.TextChanged += new System.EventHandler(this.txtProxyPort_TextChanged);
            // 
            // pnlHttpHeaders
            // 
            this.pnlHttpHeaders.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlHttpHeaders.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlHttpHeaders.Controls.Add(this.lblAdditionalHttpHeaders);
            this.pnlHttpHeaders.Controls.Add(this.lblHttpHeaderValue);
            this.pnlHttpHeaders.Controls.Add(this.txtHttpHeaderValue);
            this.pnlHttpHeaders.Controls.Add(this.lblHttpHeaderType);
            this.pnlHttpHeaders.Controls.Add(this.txtHttpHeaderType);
            this.pnlHttpHeaders.Controls.Add(this.btnHttpHeaderAdd);
            this.pnlHttpHeaders.Controls.Add(this.btnHttpHeaderRemove);
            this.pnlHttpHeaders.Controls.Add(this.lvAdditionalHttpHeaders);
            this.pnlHttpHeaders.Location = new System.Drawing.Point(10, 40);
            this.pnlHttpHeaders.Name = "pnlHttpHeaders";
            this.pnlHttpHeaders.Size = new System.Drawing.Size(562, 325);
            this.pnlHttpHeaders.TabIndex = 5;
            // 
            // lblAdditionalHttpHeaders
            // 
            this.lblAdditionalHttpHeaders.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAdditionalHttpHeaders.AutoSize = true;
            this.lblAdditionalHttpHeaders.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAdditionalHttpHeaders.Location = new System.Drawing.Point(190, 10);
            this.lblAdditionalHttpHeaders.Name = "lblAdditionalHttpHeaders";
            this.lblAdditionalHttpHeaders.Size = new System.Drawing.Size(187, 13);
            this.lblAdditionalHttpHeaders.TabIndex = 1;
            this.lblAdditionalHttpHeaders.Text = "Additional Custom Http Headers";
            this.lblAdditionalHttpHeaders.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblHttpHeaderValue
            // 
            this.lblHttpHeaderValue.AutoSize = true;
            this.lblHttpHeaderValue.Location = new System.Drawing.Point(10, 72);
            this.lblHttpHeaderValue.Name = "lblHttpHeaderValue";
            this.lblHttpHeaderValue.Size = new System.Drawing.Size(75, 13);
            this.lblHttpHeaderValue.TabIndex = 4;
            this.lblHttpHeaderValue.Text = "Header Value:";
            // 
            // txtHttpHeaderValue
            // 
            this.txtHttpHeaderValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHttpHeaderValue.Location = new System.Drawing.Point(100, 70);
            this.txtHttpHeaderValue.Name = "txtHttpHeaderValue";
            this.txtHttpHeaderValue.Size = new System.Drawing.Size(444, 20);
            this.txtHttpHeaderValue.TabIndex = 5;
            this.txtHttpHeaderValue.TextChanged += new System.EventHandler(this.txtHttpHeaderValue_TextChanged);
            // 
            // lblHttpHeaderType
            // 
            this.lblHttpHeaderType.AutoSize = true;
            this.lblHttpHeaderType.Location = new System.Drawing.Point(10, 46);
            this.lblHttpHeaderType.Name = "lblHttpHeaderType";
            this.lblHttpHeaderType.Size = new System.Drawing.Size(72, 13);
            this.lblHttpHeaderType.TabIndex = 2;
            this.lblHttpHeaderType.Text = "Header Type:";
            // 
            // txtHttpHeaderType
            // 
            this.txtHttpHeaderType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHttpHeaderType.Location = new System.Drawing.Point(100, 44);
            this.txtHttpHeaderType.Name = "txtHttpHeaderType";
            this.txtHttpHeaderType.Size = new System.Drawing.Size(444, 20);
            this.txtHttpHeaderType.TabIndex = 3;
            this.txtHttpHeaderType.TextChanged += new System.EventHandler(this.txtHttpHeaderType_TextChanged);
            // 
            // btnHttpHeaderAdd
            // 
            this.btnHttpHeaderAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnHttpHeaderAdd.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnHttpHeaderAdd.Location = new System.Drawing.Point(329, 95);
            this.btnHttpHeaderAdd.Name = "btnHttpHeaderAdd";
            this.btnHttpHeaderAdd.Size = new System.Drawing.Size(65, 23);
            this.btnHttpHeaderAdd.TabIndex = 6;
            this.btnHttpHeaderAdd.Text = "Add";
            this.btnHttpHeaderAdd.UseVisualStyleBackColor = false;
            this.btnHttpHeaderAdd.Click += new System.EventHandler(this.btnHttpHeaderAdd_Click);
            // 
            // btnHttpHeaderRemove
            // 
            this.btnHttpHeaderRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnHttpHeaderRemove.BackColor = System.Drawing.Color.Gainsboro;
            this.btnHttpHeaderRemove.Enabled = false;
            this.btnHttpHeaderRemove.Location = new System.Drawing.Point(424, 95);
            this.btnHttpHeaderRemove.Name = "btnHttpHeaderRemove";
            this.btnHttpHeaderRemove.Size = new System.Drawing.Size(102, 23);
            this.btnHttpHeaderRemove.TabIndex = 7;
            this.btnHttpHeaderRemove.Text = "Remove Selected";
            this.btnHttpHeaderRemove.UseVisualStyleBackColor = false;
            this.btnHttpHeaderRemove.Click += new System.EventHandler(this.btnHttpHeaderRemove_Click);
            // 
            // lvAdditionalHttpHeaders
            // 
            this.lvAdditionalHttpHeaders.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvAdditionalHttpHeaders.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colHttpHeaderType,
            this.colHttpHeaderValue});
            this.lvAdditionalHttpHeaders.FullRowSelect = true;
            this.lvAdditionalHttpHeaders.GridLines = true;
            this.lvAdditionalHttpHeaders.Location = new System.Drawing.Point(10, 125);
            this.lvAdditionalHttpHeaders.Name = "lvAdditionalHttpHeaders";
            this.lvAdditionalHttpHeaders.Size = new System.Drawing.Size(538, 186);
            this.lvAdditionalHttpHeaders.TabIndex = 8;
            this.lvAdditionalHttpHeaders.UseCompatibleStateImageBehavior = false;
            this.lvAdditionalHttpHeaders.View = System.Windows.Forms.View.Details;
            this.lvAdditionalHttpHeaders.SelectedIndexChanged += new System.EventHandler(this.lvAdditionalHttpHeaders_SelectedIndexChanged);
            // 
            // colHttpHeaderType
            // 
            this.colHttpHeaderType.Text = "Type";
            this.colHttpHeaderType.Width = 180;
            // 
            // colHttpHeaderValue
            // 
            this.colHttpHeaderValue.Text = "Value";
            this.colHttpHeaderValue.Width = 356;
            // 
            // AdvancedServer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 412);
            this.Controls.Add(this.txtProxyPort);
            this.Controls.Add(this.lblProxyPort);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtProxyServer);
            this.Controls.Add(this.lblProxyServer);
            this.Controls.Add(this.pnlHttpHeaders);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AdvancedServer";
            this.Text = "Advanced Server Options";
            this.Shown += new System.EventHandler(this.AdvancedServer_Shown);
            this.SystemColorsChanged += new System.EventHandler(this.AdvancedServer_SystemColorsChanged);
            this.pnlHttpHeaders.ResumeLayout(false);
            this.pnlHttpHeaders.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblProxyServer;
        private System.Windows.Forms.TextBox txtProxyServer;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblProxyPort;
        private System.Windows.Forms.TextBox txtProxyPort;
        private System.Windows.Forms.Panel pnlHttpHeaders;
        private System.Windows.Forms.Button btnHttpHeaderAdd;
        private System.Windows.Forms.Button btnHttpHeaderRemove;
        private System.Windows.Forms.ListView lvAdditionalHttpHeaders;
        private System.Windows.Forms.ColumnHeader colHttpHeaderType;
        private System.Windows.Forms.ColumnHeader colHttpHeaderValue;
        private System.Windows.Forms.TextBox txtHttpHeaderType;
        private System.Windows.Forms.Label lblHttpHeaderValue;
        private System.Windows.Forms.TextBox txtHttpHeaderValue;
        private System.Windows.Forms.Label lblHttpHeaderType;
        private System.Windows.Forms.Label lblAdditionalHttpHeaders;

    }
}