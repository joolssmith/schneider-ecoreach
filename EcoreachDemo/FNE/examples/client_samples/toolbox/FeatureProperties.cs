﻿// <copyright file="FeatureProperties.cs" company="Flexera Software LLC">
//     Copyright (c) 2012-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using FlxDotNetClient;

namespace DotNetDemo
{
    public partial class FeatureProperties : Form
    {
        private const string strUncounted     = "Uncounted";
        private const string strPermanent     = "Permanent";
        private const string strNotApplicable = "N/A";
        private const string strUnspecified   = "Unspecified";

        private Dictionary<Color, List<Color>> lvColors = new Dictionary<Color,List<Color>>();
        private readonly Color[] steelBlues = { Color.SteelBlue, Color.LightSteelBlue };
        private readonly Color[] blues      = { Color.DarkBlue, Color.LightBlue };
        private readonly Color[] blueGreens = { Color.Teal, Color.PaleTurquoise };
        private readonly Color[] greys      = { Color.DimGray, Color.Silver };
        private readonly Color[] goldens    = { Color.DarkGoldenrod, Color.NavajoWhite };

        private IFeature feature;
        private ILicense license;

        public FeatureProperties()
        {
            InitializeComponent();
            lvColors.Add(Color.DimGray,           new List<Color>(greys));
            lvColors.Add(Color.Silver,            new List<Color>(greys));
            lvColors.Add(Color.DarkBlue,          new List<Color>(blues));
            lvColors.Add(Color.LightBlue,         new List<Color>(blues));
            lvColors.Add(Color.Teal,              new List<Color>(blueGreens));
            lvColors.Add(Color.PaleTurquoise,     new List<Color>(blueGreens));
            lvColors.Add(Color.SteelBlue,         new List<Color>(steelBlues));
            lvColors.Add(Color.LightSteelBlue,    new List<Color>(steelBlues));
            lvColors.Add(Color.DarkGoldenrod,     new List<Color>(goldens));
            lvColors.Add(Color.NavajoWhite,       new List<Color>(goldens));
        }

        public IFeature Feature
        {
            set
            {
                feature = value;
                if (feature != null)
                {
                    license = null;
                    txtFPName.Text = feature.Name;
                    txtFPVersion.Text = feature.Version;
                    txtFPCount.Text = feature.IsUncounted ? strUncounted : feature.Count.ToString();
                    ckbFPStale.Visible = false;
                    lblFPFinalExpiration.Visible = txtFPFinalExpiration.Visible = true;
                    txtFPAvailable.Text = feature.IsUncounted ? strUncounted : feature.AvailableAcquisitionCount.ToString();
                    ckbFPMetered.Checked = ckbFPReusable.Visible = feature.IsMetered;
                    txtFPUndoInterval.Enabled = txtFPUndoRemaining.Enabled = (feature.IsMetered && !feature.IsMeteredReusable);
                    if (feature.IsMetered)
                    {
                        ckbFPReusable.Checked = feature.IsMeteredReusable;
                    }
                    txtFPUndoInterval.Text = txtFPUndoInterval.Enabled ? 
                        (feature.MeteredUndoInterval.HasValue ? feature.MeteredUndoInterval.Value.ToString() : strUnspecified) : strNotApplicable;
                    txtFPUndoRemaining.Text = txtFPUndoRemaining.Enabled ?
                        (feature.UndoIntervalRemaining.HasValue ? feature.UndoIntervalRemaining.Value.ToString() : strUnspecified) : strNotApplicable;
                    txtFPIssued.Text = feature.Issued.HasValue ? feature.Issued.ToString() : string.Empty;
                    txtFPStart.Text = feature.StartDate.HasValue ? feature.StartDate.ToString() : string.Empty;
                    txtFPExpiration.Text = feature.Expiration.HasValue ? feature.Expiration.ToString() : strPermanent;
                    txtFPFinalExpiration.Text = feature.FinalExpiration.HasValue ? feature.FinalExpiration.ToString() : strPermanent;
                    Dictionary<HostIdEnum, List<string>> hostIds = feature.HostIds;
                    if (hostIds != null && hostIds.Count > 0)
                    {
                        foreach (KeyValuePair<HostIdEnum, List<string>> kvp in hostIds)
                        {
                            HostIdEnum hostIdType = kvp.Key;
                            foreach (string hostIdValue in kvp.Value)
                            {
                                txtFPHostIds.Text += string.Format("{0}{1}: {2}", 
                                    string.IsNullOrEmpty(txtFPHostIds.Text) ? string.Empty : Environment.NewLine,
                                    DotNetDemoForm.GetEnumDescription(hostIdType), hostIdValue);
                            }
                        }
                    }
                    txtFPVendorString.Text = feature.VendorString ?? string.Empty;
                    txtFPIssuer.Text = feature.Issuer ?? string.Empty;
                    txtFPNotice.Text = feature.Notice ?? string.Empty;
                    txtFPSN.Text = feature.SerialNumber ?? string.Empty;
                    txtFPAcqStatus.Text = (feature.ValidStatusForAcquisition() == ErrorCode.FLXERR_SUCCESS) ?
                        "Valid for acquisition" : DotNetDemoForm.GetEnumDescription(feature.ValidStatusForAcquisition());
                    txtFPSrvStatus.Text = (feature.ValidStatusForServing() == ErrorCode.FLXERR_SUCCESS) ?
                        "Valid for serving" : DotNetDemoForm.GetEnumDescription(feature.ValidStatusForServing());

                    if (feature.UndoIntervalRemaining.HasValue && feature.UndoIntervalRemaining.Value.Ticks > 0)
                    {
                        undoTimer.Tick += new EventHandler(FeatureProperties_TimerPop);
                        undoTimer.Enabled = true;
                    }
                }
            }
        }

        public ILicense License
        {
            set
            {
                license = value;
                if (license != null)
                {
                    feature = null;
                    txtFPName.Text = license.Name;
                    txtFPVersion.Text = license.Version;
                    txtFPCount.Text = license.Count.ToString();
                    lblFPAvailable.Visible = txtFPAvailable.Visible = false;
                    lblFPFinalExpiration.Visible = txtFPFinalExpiration.Visible = false;
                    ckbFPStale.Checked = license.IsStale;
                    if (license.IsMetered)
                    {
                        ckbFPMetered.Checked = true;
                        ckbFPReusable.Checked = license.IsMeteredReusable;
                    }
                    else
                    {
                        ckbFPMetered.Checked = ckbFPReusable.Visible = false;
                        ckbFPStale.Location = new Point(ckbFPMetered.Location.X, ckbFPMetered.Location.Y);
                    }
                    txtFPUndoInterval.Enabled = txtFPUndoRemaining.Enabled = (license.IsMetered && !license.IsMeteredReusable);
                    txtFPUndoInterval.Text = txtFPUndoInterval.Enabled ?
                        (license.MeteredUndoInterval.HasValue ? license.MeteredUndoInterval.Value.ToString() : strUnspecified) : strNotApplicable;
                    txtFPUndoRemaining.Text = txtFPUndoRemaining.Enabled ?
                        (license.UndoIntervalRemaining.HasValue ? license.UndoIntervalRemaining.Value.ToString() : strUnspecified) : strNotApplicable;
                    txtFPIssued.Text = license.Issued.HasValue ? license.Issued.ToString() : string.Empty;
                    txtFPStart.Text = license.StartDate.HasValue ? license.StartDate.ToString() : string.Empty;
                    txtFPExpiration.Text = license.Expiration.HasValue ? license.Expiration.ToString() : strPermanent;

                    // No HostIds on license - move bunch of stuff around.
                    int deltaY = lblFPVendorString.Location.Y - lblFPHostIds.Location.Y;
                    int formDeltaY = deltaY + (lblFPSrvStatus.Location.Y - lblFPSN.Location.Y); 
                    lblFPHostIds.Visible = txtFPHostIds.Visible = false;
                    lblFPIssuer.Location = new Point(lblFPVendorString.Location.X, lblFPVendorString.Location.Y);
                    txtFPIssuer.Location = new Point(txtFPVendorString.Location.X, txtFPVendorString.Location.Y);
                    txtFPIssuer.Size = new Size(txtFPVendorString.Size.Width, txtFPIssuer.Size.Height);
                    txtFPIssuer.Text = license.Issuer ?? string.Empty;
                    lblFPVendorString.Location = new Point(lblFPHostIds.Location.X, lblFPHostIds.Location.Y);
                    txtFPVendorString.Location = new Point(txtFPHostIds.Location.X, txtFPHostIds.Location.Y);
                    txtFPVendorString.Text = license.VendorString ?? string.Empty;
                    lblFPNotice.Location = new Point(lblFPIssuer.Location.X, lblFPNotice.Location.Y - deltaY);
                    txtFPNotice.Location = new Point(txtFPIssuer.Location.X, txtFPNotice.Location.Y - deltaY);
                    txtFPNotice.Size = new Size(txtFPIssuer.Size.Width, txtFPNotice.Size.Height);
                    txtFPNotice.Text = license.Notice ?? string.Empty;
                    lblFPSN.Location = new Point(lblFPIssuer.Location.X, lblFPSN.Location.Y - deltaY);
                    txtFPSN.Location = new Point(txtFPIssuer.Location.X, txtFPSN.Location.Y - deltaY);
                    txtFPSN.Size = new Size(txtFPIssuer.Size.Width, txtFPSN.Size.Height);
                    txtFPSN.Text = license.SerialNumber ?? string.Empty;

                    // These don't exist for licenses either
                    lblFPAcqStatus.Visible = txtFPAcqStatus.Visible = false;
                    lblFPSrvStatus.Visible = txtFPSrvStatus.Visible = false;

                    // Adjust whole form size
                    this.MinimumSize = new Size(this.Size.Width, this.Size.Height - formDeltaY);
                    this.Size = new Size(this.Size.Width, this.Size.Height - formDeltaY);


                    if (license.UndoIntervalRemaining.HasValue && license.UndoIntervalRemaining.Value.Ticks > 0)
                    {
                        undoTimer.Tick += new EventHandler(FeatureProperties_TimerPop);
                        undoTimer.Enabled = true;
                    }
                }
            }
        }

        public Color TextBackColor
        {
            set
            {
                txtFPName.BackColor = value;
                FeatureProperties_SystemColorsChanged(this, new EventArgs());
            }
        }

        private void FeatureProperties_SystemColorsChanged(object sender, System.EventArgs e)
        {
            if (lvColors.ContainsKey(txtFPName.BackColor))
            {
                txtFPName.BackColor = lvColors[txtFPName.BackColor][SystemInformation.HighContrast ? 0 : 1];
            }
            Color backColor = txtFPName.BackColor;
            txtFPVersion.BackColor = txtFPCount.BackColor = backColor;
            txtFPAvailable.BackColor = txtFPUndoInterval.BackColor = txtFPUndoRemaining.BackColor = backColor;
            txtFPIssued.BackColor = txtFPStart.BackColor = txtFPExpiration.BackColor = txtFPFinalExpiration.BackColor = backColor;
            txtFPHostIds.BackColor = txtFPVendorString.BackColor = backColor;
            txtFPIssuer.BackColor = txtFPNotice.BackColor = txtFPSN.BackColor = backColor;
            txtFPAcqStatus.BackColor = txtFPSrvStatus.BackColor = backColor;

            ckbFPStale.ForeColor = (!ckbFPStale.Checked ? System.Drawing.Color.FromKnownColor(KnownColor.ControlText) :
                (SystemInformation.HighContrast ? Color.LightPink : Color.Red));
        }

        private void FeatureProperties_TimerPop(object sender, System.EventArgs e)
        {
            if (feature != null)
            {
                txtFPUndoRemaining.Text = txtFPUndoRemaining.Enabled ?
                    (feature.UndoIntervalRemaining.HasValue ? feature.UndoIntervalRemaining.Value.ToString() : strUnspecified) : strNotApplicable;
                undoTimer.Enabled = feature.UndoIntervalRemaining.HasValue && feature.UndoIntervalRemaining.Value.Ticks > 0;
            }
            else if (license != null)
            {
                txtFPUndoRemaining.Text = txtFPUndoRemaining.Enabled ?
                    (license.UndoIntervalRemaining.HasValue ? license.UndoIntervalRemaining.Value.ToString() : strUnspecified) : strNotApplicable;
                undoTimer.Enabled = license.UndoIntervalRemaining.HasValue && license.UndoIntervalRemaining.Value.Ticks > 0;

            }
            else
            {
                undoTimer.Enabled = false;
            }
        }

    }
}
