﻿// <copyright file="ResponseDetails.cs" company="Flexera Software LLC">
//     Copyright (c) 2012-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using FlxDotNetClient;

namespace DotNetDemo
{
    public partial class ResponseDetails : Form
    {
        private const string strUncounted = "Uncounted";
        private const string strPerpetual = "Perpetual";
        private const string strPreview = "Preview";
        private const string strTrustedStorage = "Trusted Storage";
        private const string strCapResp = " Capability Response Details";
        private ICapabilityResponse response;

        public ResponseDetails()
        {
            InitializeComponent();
            ResponseDetails_SystemColorsChanged(this, new EventArgs());
        }

        public ICapabilityResponse Response
        {
            get { return response; }
            set
            {
                response = value;
                if (response != null)
                {
                    tabResponseDetails.TabPages.Clear();

                    this.Text = (response.IsPreview ? strPreview : strTrustedStorage) + strCapResp;
                    txtServerName.Text = response.ServerName;
                    txtServerURI.Text = response.ServerURI;
                    txtBackupServerURI.Text = response.BackupServerURI;
                    txtRenewInterval.Text = response.RenewInterval.ToString();
                    txtCorrelationID.Text = response.CorrelationId;
                    if (response.UUID != null)
                    {
                        try
                        {
                            Guid guid = new Guid(response.UUID);
                            txtUUID.Text = guid.ToString();
                        }
                        catch 
                        {
                            StringBuilder sb = new StringBuilder((response.UUID.Length + 1) * 2);
                            sb.Append("0x");
                            foreach (byte b in response.UUID)
                            {
                                sb.AppendFormat("{0:x2}", b);
                            }
                            txtUUID.Text = sb.ToString();
                        }
                    }
                    txtServerID.Text = DotNetDemoForm.GetEnumDescription((Enum)response.ServerId.Key) +
                                       ": " + response.ServerId.Value;
                    if (response.ServedTime.HasValue)
                    {
                        txtServedTime.Text = response.ServedTime.Value.ToString();
                    }
                    txtVMType.Text = response.VirtualMachineType.ToString();
                    ckbConfirmRequestNeeded.Checked = response.ConfirmationRequestNeeded;
                    ckbCloneSuspect.Checked = response.CloneSuspect;
                    Dictionary<HostIdEnum, List<string>> hostIds = response.HostIds;
                    if (hostIds != null && hostIds.Count > 0)
                    {
                        tabResponseDetails.TabPages.Add(pageRDHostIdInfo);
                        lvRDHostIds.BeginUpdate();
                        foreach (KeyValuePair<HostIdEnum, List<string>> kvp in hostIds)
                        {
                            HostIdEnum hostIdType = kvp.Key;
                            foreach (string hostIdValue in kvp.Value)
                            {
                                lvRDHostIds.Items.Add(new ListViewItem(new string[]
                                    { DotNetDemoForm.GetEnumDescription(hostIdType), hostIdValue }));
                            }
                        }
                        lvRDHostIds.EndUpdate();
                    }
                    if (response.ServerInstance != LicenseServerInstance.Unknown)
                    {
                        txtServerInstance.Text = response.ServerInstance.ToString();
                    }
                    if (response.VirtualMachineInfo != null && response.VirtualMachineInfo.Count > 0)
                    {
                        tabResponseDetails.TabPages.Add(pageRDVMInfo);
                        FillKeyValue(lvRDVMInfo, response.VirtualMachineInfo);
                    }
                    if (response.VendorDictionary != null && response.VendorDictionary.Count > 0)
                    {
                        tabResponseDetails.TabPages.Add(pageRDVDInfo);
                        FillKeyValue(lvRDVDInfo, response.VendorDictionary);
                    }
                    if (response.Status != null && response.Status.Count > 0)
                    {
                        tabResponseDetails.TabPages.Add(pageRDStatusInfo);
                        foreach (IResponseStatus statusItem in response.Status)
                        {
                            lvRDStatusInfo.Items.Add(new ListViewItem(new string[]{
                                statusItem.TypeDescription,
                                string.Format("0x{0:x8}", (int)statusItem.Code),
                                statusItem.CodeDescription, 
                                statusItem.Details}));
                        }
                    }
                    if (tabResponseDetails.TabPages.Count == 0)
                    {
                        tabResponseDetails.Visible = false;
                        this.MinimumSize = new Size(this.MinimumSize.Width, 210);
                        this.Height = 210;
                    }
                }
            }
        }

        private void FillKeyValue(ListView lv, ReadOnlyDictionary dictionary)
        {
            string itemType;
            lv.BeginUpdate();
            foreach (KeyValuePair<string, object> kvp in dictionary)
            {
                if (kvp.Value is string)
                {
                    itemType = "String";
                }
                else if (kvp.Value is int)
                {
                    itemType = "Integer";
                }
                else
                {
                    itemType = kvp.Value.GetType().ToString();
                }
                lv.Items.Add(new ListViewItem(new string[] { itemType, kvp.Key.ToString(), kvp.Value.ToString() }));
            }
            lv.EndUpdate();
         }

        private void ResponseDetails_SystemColorsChanged(object sender, EventArgs e)
        {
            lvRDHostIds.BackColor = SystemInformation.HighContrast ? Color.Teal : Color.PaleTurquoise;
            lvRDVMInfo.BackColor = SystemInformation.HighContrast ? Color.SteelBlue : Color.LightSteelBlue;
            lvRDVDInfo.BackColor = SystemInformation.HighContrast ? Color.Gray : Color.Gainsboro;
            lvRDStatusInfo.BackColor = SystemInformation.HighContrast ? Color.DimGray : Color.Silver;
        }

    }
}
