// <copyright file="FeatureView.Designer.cs" company="Flexera Software LLC">
//     Copyright (c) 2012-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

namespace DotNetDemo
{
    partial class FeatureView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvFeatureView = new System.Windows.Forms.ListView();
            this.colViewType = new System.Windows.Forms.ColumnHeader();
            this.colViewFeatureName = new System.Windows.Forms.ColumnHeader();
            this.colViewFeatureVersion = new System.Windows.Forms.ColumnHeader();
            this.colViewFeatureCount = new System.Windows.Forms.ColumnHeader();
            this.colViewFeatureExpiration = new System.Windows.Forms.ColumnHeader();
            this.colViewFeatureValid = new System.Windows.Forms.ColumnHeader();
            this.colViewFeatureVendorString = new System.Windows.Forms.ColumnHeader();
            this.lblSourceType = new System.Windows.Forms.Label();
            this.colViewFeatureMaxCount = new System.Windows.Forms.ColumnHeader();
            this.SuspendLayout();
            // 
            // lvFeatureView
            // 
            this.lvFeatureView.AllowColumnReorder = true;
            this.lvFeatureView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvFeatureView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colViewType,
            this.colViewFeatureName,
            this.colViewFeatureVersion,
            this.colViewFeatureCount,
            this.colViewFeatureMaxCount,
            this.colViewFeatureExpiration,
            this.colViewFeatureValid,
            this.colViewFeatureVendorString});
            this.lvFeatureView.FullRowSelect = true;
            this.lvFeatureView.GridLines = true;
            this.lvFeatureView.Location = new System.Drawing.Point(0, 30);
            this.lvFeatureView.MultiSelect = false;
            this.lvFeatureView.Name = "lvFeatureView";
            this.lvFeatureView.ShowGroups = false;
            this.lvFeatureView.ShowItemToolTips = true;
            this.lvFeatureView.Size = new System.Drawing.Size(584, 332);
            this.lvFeatureView.TabIndex = 2;
            this.lvFeatureView.UseCompatibleStateImageBehavior = false;
            this.lvFeatureView.View = System.Windows.Forms.View.Details;
            this.lvFeatureView.DoubleClick += new System.EventHandler(this.lvFeatureView_DoubleClick);
            // 
            // colViewType
            // 
            this.colViewType.Text = "Type";
            this.colViewType.Width = 45;
            // 
            // colViewFeatureName
            // 
            this.colViewFeatureName.Text = "Feature";
            this.colViewFeatureName.Width = 120;
            // 
            // colViewFeatureVersion
            // 
            this.colViewFeatureVersion.Text = "Version";
            this.colViewFeatureVersion.Width = 47;
            // 
            // colViewFeatureCount
            // 
            this.colViewFeatureCount.Text = "Count";
            this.colViewFeatureCount.Width = 66;
            // 
            // colViewFeatureExpiration
            // 
            this.colViewFeatureExpiration.Text = "Expiration";
            this.colViewFeatureExpiration.Width = 130;
            // 
            // colViewFeatureValid
            // 
            this.colViewFeatureValid.Text = "Valid";
            this.colViewFeatureValid.Width = 172;
            // 
            // colViewFeatureVendorString
            // 
            this.colViewFeatureVendorString.Text = "Vendor String";
            this.colViewFeatureVendorString.Width = 150;
            // 
            // lblSourceType
            // 
            this.lblSourceType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSourceType.Location = new System.Drawing.Point(0, 8);
            this.lblSourceType.Name = "lblSourceType";
            this.lblSourceType.Size = new System.Drawing.Size(584, 13);
            this.lblSourceType.TabIndex = 1;
            this.lblSourceType.Text = "Source Type: ";
            // 
            // colViewFeatureMaxCount
            // 
            this.colViewFeatureMaxCount.Text = "Max Count";
            this.colViewFeatureMaxCount.Width = 66;
            // 
            // FeatureView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 362);
            this.Controls.Add(this.lblSourceType);
            this.Controls.Add(this.lvFeatureView);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FeatureView";
            this.Load += new System.EventHandler(this.FeatureView_Load);
            this.SystemColorsChanged += new System.EventHandler(this.FeatureView_SystemColorsChanged);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lvFeatureView;
        private System.Windows.Forms.ColumnHeader colViewType;
        private System.Windows.Forms.ColumnHeader colViewFeatureName;
        private System.Windows.Forms.ColumnHeader colViewFeatureVersion;
        private System.Windows.Forms.ColumnHeader colViewFeatureCount;
        private System.Windows.Forms.ColumnHeader colViewFeatureExpiration;
        private System.Windows.Forms.ColumnHeader colViewFeatureValid;
        private System.Windows.Forms.ColumnHeader colViewFeatureVendorString;
        private System.Windows.Forms.Label lblSourceType;
        private System.Windows.Forms.ColumnHeader colViewFeatureMaxCount;
    }
}