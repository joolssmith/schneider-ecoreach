// <copyright file="ShortCodeResponse.Designer.cs" company="Flexera Software LLC">
//     Copyright (c) 2012-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

namespace DotNetDemo
{
    partial class ShortCodeResponse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSCResponse = new System.Windows.Forms.Label();
            this.txtSCResponse = new System.Windows.Forms.TextBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.lblSCSegment = new System.Windows.Forms.Label();
            this.txtSCSegment = new System.Windows.Forms.TextBox();
            this.btnSCSegmentAdd = new System.Windows.Forms.Button();
            this.lblSCResponseComplete = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblSCResponse
            // 
            this.lblSCResponse.AutoSize = true;
            this.lblSCResponse.Location = new System.Drawing.Point(10, 120);
            this.lblSCResponse.Name = "lblSCResponse";
            this.lblSCResponse.Size = new System.Drawing.Size(114, 13);
            this.lblSCResponse.TabIndex = 4;
            this.lblSCResponse.Text = "Short Code Response:";
            // 
            // txtSCResponse
            // 
            this.txtSCResponse.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSCResponse.Location = new System.Drawing.Point(150, 108);
            this.txtSCResponse.MaxLength = 1048576;
            this.txtSCResponse.Multiline = true;
            this.txtSCResponse.Name = "txtSCResponse";
            this.txtSCResponse.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtSCResponse.Size = new System.Drawing.Size(617, 210);
            this.txtSCResponse.TabIndex = 5;
            this.txtSCResponse.TextChanged += new System.EventHandler(this.errorprovider_reset);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.BackColor = System.Drawing.Color.Gainsboro;
            this.btnOK.Location = new System.Drawing.Point(682, 325);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 7;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = false;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblSCSegment
            // 
            this.lblSCSegment.AutoSize = true;
            this.lblSCSegment.Location = new System.Drawing.Point(10, 10);
            this.lblSCSegment.Name = "lblSCSegment";
            this.lblSCSegment.Size = new System.Drawing.Size(103, 13);
            this.lblSCSegment.TabIndex = 1;
            this.lblSCSegment.Text = "Response Segment:";
            // 
            // txtSCSegment
            // 
            this.txtSCSegment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSCSegment.Location = new System.Drawing.Point(150, 8);
            this.txtSCSegment.MaxLength = 1048576;
            this.txtSCSegment.Multiline = true;
            this.txtSCSegment.Name = "txtSCSegment";
            this.txtSCSegment.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtSCSegment.Size = new System.Drawing.Size(617, 60);
            this.txtSCSegment.TabIndex = 2;
            this.txtSCSegment.TextChanged += new System.EventHandler(this.errorprovider_reset);
            // 
            // btnSCSegmentAdd
            // 
            this.btnSCSegmentAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSCSegmentAdd.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnSCSegmentAdd.Location = new System.Drawing.Point(682, 74);
            this.btnSCSegmentAdd.Name = "btnSCSegmentAdd";
            this.btnSCSegmentAdd.Size = new System.Drawing.Size(75, 23);
            this.btnSCSegmentAdd.TabIndex = 3;
            this.btnSCSegmentAdd.Text = "Add";
            this.btnSCSegmentAdd.UseVisualStyleBackColor = false;
            this.btnSCSegmentAdd.Click += new System.EventHandler(this.btnSCSegmentAdd_Click);
            // 
            // lblSCResponseComplete
            // 
            this.lblSCResponseComplete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSCResponseComplete.AutoSize = true;
            this.lblSCResponseComplete.ForeColor = System.Drawing.Color.Blue;
            this.lblSCResponseComplete.Location = new System.Drawing.Point(550, 330);
            this.lblSCResponseComplete.Name = "lblSCResponseComplete";
            this.lblSCResponseComplete.Size = new System.Drawing.Size(101, 13);
            this.lblSCResponseComplete.TabIndex = 6;
            this.lblSCResponseComplete.Text = "Response complete";
            this.lblSCResponseComplete.Visible = false;
            // 
            // ShortCodeResponse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 362);
            this.Controls.Add(this.lblSCResponseComplete);
            this.Controls.Add(this.btnSCSegmentAdd);
            this.Controls.Add(this.txtSCSegment);
            this.Controls.Add(this.lblSCSegment);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.txtSCResponse);
            this.Controls.Add(this.lblSCResponse);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(300, 300);
            this.Name = "ShortCodeResponse";
            this.Text = "Enter Short Code Response";
            this.Load += new System.EventHandler(this.ShortCodeResponse_Load);
            this.SystemColorsChanged += new System.EventHandler(this.ShortCodeResponse_SystemColorsChanged);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSCResponse;
        private System.Windows.Forms.TextBox txtSCResponse;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label lblSCSegment;
        private System.Windows.Forms.TextBox txtSCSegment;
        private System.Windows.Forms.Button btnSCSegmentAdd;
        private System.Windows.Forms.Label lblSCResponseComplete;

    }
}