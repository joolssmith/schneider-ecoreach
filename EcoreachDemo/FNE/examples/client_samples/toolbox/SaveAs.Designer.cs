// <copyright file="SaveAs.Designer.cs" company="Flexera Software LLC">
//     Copyright (c) 2012-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

namespace DotNetDemo
{
    partial class SaveAs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblConfigTitle = new System.Windows.Forms.Label();
            this.lblConfigFile = new System.Windows.Forms.Label();
            this.txtConfigTitle = new System.Windows.Forms.TextBox();
            this.txtConfigFile = new System.Windows.Forms.TextBox();
            this.lblConfigName = new System.Windows.Forms.Label();
            this.txtConfigName = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnBrowseConfig = new System.Windows.Forms.Button();
            this.configColorDialog = new System.Windows.Forms.ColorDialog();
            this.btnConfigColor = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblConfigTitle
            // 
            this.lblConfigTitle.AutoSize = true;
            this.lblConfigTitle.Location = new System.Drawing.Point(10, 10);
            this.lblConfigTitle.Name = "lblConfigTitle";
            this.lblConfigTitle.Size = new System.Drawing.Size(95, 13);
            this.lblConfigTitle.TabIndex = 1;
            this.lblConfigTitle.Text = "Configuration Title:";
            // 
            // lblConfigFile
            // 
            this.lblConfigFile.AutoSize = true;
            this.lblConfigFile.Location = new System.Drawing.Point(10, 45);
            this.lblConfigFile.Name = "lblConfigFile";
            this.lblConfigFile.Size = new System.Drawing.Size(91, 13);
            this.lblConfigFile.TabIndex = 3;
            this.lblConfigFile.Text = "Configuration File:";
            // 
            // txtConfigTitle
            // 
            this.txtConfigTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtConfigTitle.Location = new System.Drawing.Point(140, 8);
            this.txtConfigTitle.Name = "txtConfigTitle";
            this.txtConfigTitle.Size = new System.Drawing.Size(370, 20);
            this.txtConfigTitle.TabIndex = 2;
            this.txtConfigTitle.TextChanged += new System.EventHandler(this.errorprovider_reset);
            // 
            // txtConfigFile
            // 
            this.txtConfigFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtConfigFile.Location = new System.Drawing.Point(140, 43);
            this.txtConfigFile.Name = "txtConfigFile";
            this.txtConfigFile.Size = new System.Drawing.Size(370, 20);
            this.txtConfigFile.TabIndex = 4;
            this.txtConfigFile.TextChanged += new System.EventHandler(this.errorprovider_reset);
            // 
            // lblConfigName
            // 
            this.lblConfigName.AutoSize = true;
            this.lblConfigName.Location = new System.Drawing.Point(10, 80);
            this.lblConfigName.Name = "lblConfigName";
            this.lblConfigName.Size = new System.Drawing.Size(103, 13);
            this.lblConfigName.TabIndex = 6;
            this.lblConfigName.Text = "Configuration Name:";
            // 
            // txtConfigName
            // 
            this.txtConfigName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtConfigName.Location = new System.Drawing.Point(140, 78);
            this.txtConfigName.Name = "txtConfigName";
            this.txtConfigName.Size = new System.Drawing.Size(200, 20);
            this.txtConfigName.TabIndex = 7;
            this.txtConfigName.TextChanged += new System.EventHandler(this.errorprovider_reset);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSave.Location = new System.Drawing.Point(383, 115);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 12;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.Color.Gainsboro;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(482, 115);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 13;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            // 
            // btnBrowseConfig
            // 
            this.btnBrowseConfig.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowseConfig.Location = new System.Drawing.Point(525, 42);
            this.btnBrowseConfig.Name = "btnBrowseConfig";
            this.btnBrowseConfig.Size = new System.Drawing.Size(32, 23);
            this.btnBrowseConfig.TabIndex = 5;
            this.btnBrowseConfig.UseVisualStyleBackColor = true;
            this.btnBrowseConfig.Click += new System.EventHandler(this.btnBrowseConfig_Click);
            // 
            // btnConfigColor
            // 
            this.btnConfigColor.Location = new System.Drawing.Point(360, 77);
            this.btnConfigColor.Name = "btnConfigColor";
            this.btnConfigColor.Size = new System.Drawing.Size(150, 23);
            this.btnConfigColor.TabIndex = 14;
            this.btnConfigColor.Text = "Configuration Color";
            this.btnConfigColor.UseVisualStyleBackColor = true;
            this.btnConfigColor.Click += new System.EventHandler(this.btnConfigColor_Click);
            // 
            // SaveAs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 152);
            this.Controls.Add(this.btnConfigColor);
            this.Controls.Add(this.btnBrowseConfig);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtConfigName);
            this.Controls.Add(this.lblConfigName);
            this.Controls.Add(this.txtConfigFile);
            this.Controls.Add(this.txtConfigTitle);
            this.Controls.Add(this.lblConfigFile);
            this.Controls.Add(this.lblConfigTitle);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SaveAs";
            this.Text = "Save Current Configuration As";
            this.Shown += new System.EventHandler(this.SaveAs_Shown);
            this.SystemColorsChanged += new System.EventHandler(this.SaveAs_SystemColorsChanged);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblConfigTitle;
        private System.Windows.Forms.Label lblConfigFile;
        private System.Windows.Forms.TextBox txtConfigTitle;
        private System.Windows.Forms.TextBox txtConfigFile;
        private System.Windows.Forms.Label lblConfigName;
        private System.Windows.Forms.TextBox txtConfigName;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnBrowseConfig;
        private System.Windows.Forms.ColorDialog configColorDialog;
        private System.Windows.Forms.Button btnConfigColor;

    }
}