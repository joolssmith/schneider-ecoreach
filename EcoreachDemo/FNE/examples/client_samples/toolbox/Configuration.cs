﻿// <copyright file="Configuration.cs" company="Flexera Software LLC">
//     Copyright (c) 2012-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace DotNetDemo
{
    [Serializable]
    [XmlRoot("DotNetDemo")]
    public class Configuration : ICloneable
    {
        public string Name { get; set; }
        string formTitle = "FlexNet Embedded .NET XT Toolbox 9.0";
        int    configColor  = SystemColors.Window.ToArgb();
        public string IdentityData { get; set; }
        public string TrustedStoragePath { get; set; }
        bool   vmDetectionEnabled = true;
        public string HostIdType { get; set; }
        public string HostId { get; set; }
        string hostType = "FLX_CLIENT";
        public string HostName { get; set; }
        List<string> licenseFiles = new List<string>();
        string serverType = DotNetDemoForm.strBackOffice;
        ServerInfo testBOServerInfo = new ServerInfo(DotNetDemoForm.strBackOffice, false, "localhost", "8080", "request" );
        ServerInfo fnoServerInfo = new ServerInfo(DotNetDemoForm.strFNO, false, "localhost", "8888", "flexnet/deviceservices" );
        ServerInfo localServerInfo = new ServerInfo(DotNetDemoForm.strLocal, false, "localhost", "7070", "fne/bin/capability");
        ServerInfo cloudServerInfo = new ServerInfo(DotNetDemoForm.strCloud, true, "demouat.compliance.flexnetoperations.com", "443", "instances/{Instance ID}/request");
        public string ProxyServer { get; set; }
        public string ProxyPort { get; set; }
        XmlSerializableDictionary<string, string> httpHeaders;
        string operation = DotNetDemoForm.strRequest;
        string serverInstance = DotNetDemoForm.strDefault;
        public string CorrelationID { get; set; }
        public string AcquisitionID { get; set; }
        public string EnterpriseID { get; set; }
        public string RequestorID { get; set; }
        public string BorrowGranularity { get; set; }
        public string BorrowInterval { get; set; }
        // public bool ServedBuffer = { get; set; }
        public bool OneTimeActivation { get; set; }
        public bool Incremental { get; set; }
        public bool RequestAll { get; set; }
        // public bool UseCRExistingFeatures { get; set; }
        bool forceResponse = true;
        bool processResponse = true;
        public string ResponseFile { get; set; }
        List<RightsInfo> rights = new List<RightsInfo>();
        List<FeatureInfo> features = new List<FeatureInfo>();
        List<FeatureSelector> featureSelectors = new List<FeatureSelector>();
        List<AuxiliaryHostId> auxiliaryHostIds = new List<AuxiliaryHostId>();
        public bool UseIMExistingFeatures { get; set; }
        List<UsageInfo> usage = new List<UsageInfo>();
        List<string>  templateFiles = new List<string>();
        public string ScRequestEncoding { get; set; }
        public uint   ScRequestSegmentSize { get; set; }
        public bool   ScRequestPDHex { get; set; }
        public string ScRequestPD { get; set; }
        public string ScResponseDecoding { get; set; }
        public uint   ScResponseSegmentSize { get; set; }
        List<string>  scResponseFiles = new List<string>();

        public Configuration()
        {
        }

        [XmlElement("Title")]
        public string FormTitle
        {
            get { return formTitle; }
            set { formTitle = value; }
        }

        [XmlIgnore]
        public Color Color
        {
            get
            {
                return Color.FromArgb(configColor);
            }
            set
            {
                configColor = value.ToArgb();
            }
        }

        public int ConfigColor
        {
            get { return configColor; }
            set { configColor = value; }
        }

        public bool VMDetectionEnabled
        {
            get { return vmDetectionEnabled; }
            set { vmDetectionEnabled = value; }
        }

        public string HostType
        {
            get { return hostType; }
            set { hostType = value; }
        }

        public List<String> LicenseFiles
        {
            get { return licenseFiles; }
            set { licenseFiles = value; }
        }

        public string ServerType
        {
            get { return serverType; }
            set { serverType = value; }
        }

         public ServerInfo TestBOServerInfo
         {
            get { return testBOServerInfo; }
            set { testBOServerInfo = value; }
         }

        public ServerInfo FnoServerInfo
        {
            get { return fnoServerInfo; }
            set { fnoServerInfo = value; }
        }

        public ServerInfo CloudServerInfo
        {
            get { return cloudServerInfo; }
            set { cloudServerInfo = value; }
        }

        public ServerInfo LocalServerInfo
        {
            get { return localServerInfo; }
            set { localServerInfo = value; }
        }

        public XmlSerializableDictionary<string, string> HttpHeaders
        {
            get { return httpHeaders; }
            set { httpHeaders = value; }
        }

        public string Operation
        {
            get { return operation; }
            set { operation = value; }
        }

        public string ServerInstance
        {
            get { return serverInstance; }
            set { serverInstance = value; }
        }

        public bool ForceResponse
        {
            get { return forceResponse; }
            set { forceResponse = value; }
        }

        public bool ProcessResponse
        {
            get { return processResponse; }
            set {processResponse = value; }
        }

        public List<RightsInfo> Rights
        {
            get { return rights; }
            set { rights = value; }
        }

        public List<FeatureInfo> Features
        {
            get { return features; }
            set { features = value; }
        }

        public List<FeatureSelector> FeatureSelectors
        {
            get { return featureSelectors; }
            set { featureSelectors = value; }
        }

        public List<AuxiliaryHostId> AuxiliaryHostIds
        {
            get { return auxiliaryHostIds; }
            set { auxiliaryHostIds = value; }
        }

        public List<UsageInfo> Usage
        {
            get { return usage; }
            set { usage = value; }
        }

        public List<String> TemplateFiles
        {
            get { return templateFiles; }
            set { templateFiles = value; }
        }

        public List<String> ScResponseFiles
        {
            get { return scResponseFiles; }
            set { scResponseFiles = value; }
        }

        public object Clone()
        {
            Configuration newObject = new Configuration();
            newObject.Name = this.Name;
            newObject.formTitle = this.formTitle;
            newObject.configColor = this.configColor;
            newObject.IdentityData = this.IdentityData;
            newObject.TrustedStoragePath = this.TrustedStoragePath;
            newObject.vmDetectionEnabled = this.vmDetectionEnabled;
            newObject.HostIdType = this.HostIdType;
            newObject.HostId = this.HostId;
            newObject.hostType = this.hostType;
            newObject.HostName = this.HostName;
            newObject.licenseFiles = new List<string>(this.licenseFiles);
            newObject.serverType = this.serverType;
            if (testBOServerInfo != null)
            {
                newObject.testBOServerInfo = (ServerInfo)this.testBOServerInfo.Clone();
            }
            if (fnoServerInfo != null)
            {
                newObject.fnoServerInfo = (ServerInfo)this.fnoServerInfo.Clone();
            }
            if (localServerInfo != null)
            {
                newObject.localServerInfo = (ServerInfo)this.localServerInfo.Clone();
            }
            if (cloudServerInfo != null)
            {
                newObject.cloudServerInfo = (ServerInfo)this.cloudServerInfo.Clone();
            }
            newObject.OneTimeActivation = this.OneTimeActivation;
            newObject.Incremental = this.Incremental;
            newObject.RequestAll = this.RequestAll;
            newObject.ProxyServer = this.ProxyServer;
            newObject.ProxyPort = this.ProxyPort;
            newObject.httpHeaders = this.httpHeaders == null ? null : new XmlSerializableDictionary<string, string>(this.httpHeaders);
            newObject.operation = this.operation;
            newObject.serverInstance = this.serverInstance;
            newObject.CorrelationID = this.CorrelationID;
            newObject.AcquisitionID = this.AcquisitionID;
            newObject.EnterpriseID = this.EnterpriseID;
            newObject.RequestorID = this.RequestorID;
            newObject.BorrowGranularity = this.BorrowGranularity;
            newObject.BorrowInterval = this.BorrowInterval;
            newObject.forceResponse = this.forceResponse;
            newObject.processResponse = this.processResponse;
            newObject.ResponseFile = this.ResponseFile;
            newObject.rights = new List<RightsInfo>(this.rights);
            newObject.features = new List<FeatureInfo>(this.features);
            newObject.featureSelectors = new List<FeatureSelector>(this.featureSelectors);
            newObject.auxiliaryHostIds = new List<AuxiliaryHostId>(this.auxiliaryHostIds);
            newObject.UseIMExistingFeatures = this.UseIMExistingFeatures;
            newObject.usage = new List<UsageInfo>(this.usage);
            newObject.templateFiles = new List<string>(this.templateFiles);
            newObject.ScRequestEncoding = this.ScRequestEncoding;
            newObject.ScRequestSegmentSize = this.ScRequestSegmentSize;
            newObject.ScRequestPDHex = this.ScRequestPDHex;
            newObject.ScRequestPD = this.ScRequestPD;
            newObject.ScResponseDecoding = this.ScResponseDecoding;
            newObject.ScResponseSegmentSize = this.ScResponseSegmentSize;
            newObject.ScResponseFiles = new List<string>(this.scResponseFiles);

            return newObject;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Configuration return false.
            Configuration config = obj as Configuration;
            if (config == null)
            {
                return false;
            }

            return this.Equals(config);
        }

        public bool Equals(Configuration config)
        {
            // If parameter is null return false.
            if (config == null)
            {
                return false;
            }

            // Return true if all the configuration fields match:
            return (Name == config.Name &&
                    formTitle == config.formTitle &&
                    configColor == config.configColor &&
                    IdentityData == config.IdentityData &&
                    TrustedStoragePath == config.TrustedStoragePath &&
                    vmDetectionEnabled == config.vmDetectionEnabled &&
                    HostIdType == config.HostIdType &&
                    HostId == config.HostId &&
                    hostType == config.hostType &&
                    HostName == config.HostName &&
                    SequenceEqual<string>.Compare(licenseFiles, config.licenseFiles) &&
                    serverType == config.serverType &&
                    testBOServerInfo == config.testBOServerInfo &&
                    fnoServerInfo == config.fnoServerInfo &&
                    localServerInfo == config.localServerInfo &&
                    cloudServerInfo == config.cloudServerInfo &&
                    OneTimeActivation == config.OneTimeActivation &&
                    Incremental == config.Incremental &&
                    RequestAll == config.RequestAll &&
                    ProxyServer == config.ProxyServer &&
                    ProxyPort == config.ProxyPort &&
                    DictionaryEqual<string, string>.Compare(httpHeaders, config.httpHeaders) && 
                    operation == config.operation &&
                    serverInstance == config.serverInstance &&
                    CorrelationID == config.CorrelationID &&
                    AcquisitionID == config.AcquisitionID &&
                    EnterpriseID == config.EnterpriseID &&
                    RequestorID == config.RequestorID &&
                    BorrowGranularity == config.BorrowGranularity &&
                    BorrowInterval == config.BorrowInterval &&
                    forceResponse == config.forceResponse &&
                    ResponseFile == config.ResponseFile &&
                    SequenceEqual<RightsInfo>.Compare(rights, config.rights) &&
                    SequenceEqual<FeatureInfo>.Compare(features, config.features) &&
                    SequenceEqual<FeatureSelector>.Compare(featureSelectors, config.featureSelectors) &&
                    SequenceEqual<AuxiliaryHostId>.Compare(auxiliaryHostIds, config.auxiliaryHostIds) &&
                    UseIMExistingFeatures == config.UseIMExistingFeatures &&
                    SequenceEqual<UsageInfo>.Compare(usage, config.usage) &&
                    SequenceEqual<string>.Compare(templateFiles, config.templateFiles) &&
                    ScRequestEncoding == config.ScRequestEncoding &&
                    ScRequestSegmentSize == config.ScRequestSegmentSize &&
                    ScRequestPDHex == config.ScRequestPDHex &&
                    ScRequestPD == config.ScRequestPD &&
                    ScResponseDecoding == config.ScResponseDecoding &&
                    ScResponseSegmentSize == config.ScResponseSegmentSize &&
                    SequenceEqual<string>.Compare(scResponseFiles, config.scResponseFiles));
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        public static bool operator == (Configuration config1, Configuration config2)
        {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(config1, config2))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)config1 == null) || ((object)config2 == null))
            {
                return false;
            }

            return config1.Equals(config2);
        }

        public static bool operator != (Configuration config1, Configuration config2)
        {
            return !(config1 == config2);
        }


        static public void SerializeToXML(List<Configuration> configurations, string file)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<Configuration>));
            using (TextWriter textWriter = new StreamWriter(file))
            {
                serializer.Serialize(textWriter, configurations);
                textWriter.Close();
            }
        }

        static public List<Configuration> DeserializeFromXML(string file)
        {
            XmlSerializer deserializer = new XmlSerializer(typeof(List<Configuration>));
            List<Configuration> configurations = null;
            using (TextReader textReader = new StreamReader(file))
            {
                configurations = (List<Configuration>)deserializer.Deserialize(textReader);
                textReader.Close();
            }
            return configurations;
        }

    }

    #region Server Information class

    [Serializable]
    [XmlRoot("ServerInfo")]
    public class ServerInfo : ICloneable
    {
        public bool UseHTTPS   { get; set; }
        public string Type     { get; set; }
        public string Host     { get; set; }
        public string Port     { get; set; }
        public string Endpoint { get; set; }

        public ServerInfo()
        {
        }

        public ServerInfo(string type, bool useHTTPS, string host, string port, string endpoint)
        {
            this.UseHTTPS = useHTTPS;
            this.Type     = type;
            this.Host     = host;
            this.Port     = port;
            this.Endpoint = endpoint;
        }

        public object Clone()
        {
            return new ServerInfo(this.Type, this.UseHTTPS, this.Host, this.Port, this.Endpoint);
        }

        public override string ToString()
        {
            return string.Format("ServerInfo: {0}|{1}|{2}|{3}|{4}", Type ?? string.Empty,
                                                                    UseHTTPS ? "https" : "http",
                                                                    Host ?? string.Empty,
                                                                    Port ?? string.Empty,
                                                                    Endpoint ?? string.Empty);
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to ServerInfo return false.
            ServerInfo info = obj as ServerInfo;
            if (info == null)
            {
                return false;
            }

            return this.Equals(info);
        }

        public bool Equals(ServerInfo info)
        {
            // If parameter is null return false.
            if (info  == null)
            {
                return false;
            }

            // Return true if all the server info fields match:
            return (Type     == info.Type       &&
                    UseHTTPS == info.UseHTTPS   &&
                    Host     == info.Host       &&
                    Port     == info.Port       &&
                    Endpoint == info.Endpoint);
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public static bool operator == (ServerInfo info1, ServerInfo info2)
        {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(info1, info2))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)info1 == null) || ((object)info2 == null))
            {
                return false;
            }

            return info1.Equals(info2);
        }

        public static bool operator != (ServerInfo info1, ServerInfo info2)
        {
            return !(info1 == info2);
        }
    }

    #endregion

    #region Serializable Dictionary class

    /// <summary>
    /// Xml serializable dictionary object.
    /// </summary>
    /// <typeparam name="TKey">Key type</typeparam>
    /// <typeparam name="TValue">Object type</typeparam>
    [SerializableAttribute]
    public class XmlSerializableDictionary<TKey, TValue> : Dictionary<TKey, TValue>, IXmlSerializable
    {
        #region Constructors.

        /// <summary>
        /// Initializes a new instance of the <see cref="XmlSerializableDictionary&lt;TKey, TValue&gt;"/> class.
        /// </summary>
        public XmlSerializableDictionary()
            : base()
        {
            // default constructor.
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="XmlSerializableDictionary&lt;TKey, TValue&gt;"/> class.
        /// </summary>
        /// <param name="info">
        /// A <see cref="T:System.Collections.Generic.IDictionary"></see> object containing the initial information
        /// used to populate this class.
        /// </param>
        public XmlSerializableDictionary(IDictionary<TKey, TValue> dictionary)
            : base(dictionary)
        {
        }
 
        /// <summary>
        /// Initializes a new instance of the <see cref="XmlSerializableDictionary&lt;TKey, TValue&gt;"/> class.
        /// </summary>
        /// <param name="info">
        /// A <see cref="T:System.Runtime.Serialization.SerializationInfo"></see> object containing the information
        /// required to serialize the <see cref="T:System.Collections.Generic.Dictionary`2"></see>.
        /// </param>
        /// <param name="context">
        /// A <see cref="T:System.Runtime.Serialization.StreamingContext"></see> structure containing the source
        /// and destination of the serialized stream associated with the
        /// <see  cref="T:System.Collections.Generic.Dictionary`2"></see>.
        /// </param>
        protected XmlSerializableDictionary(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        #endregion
  
        #region Constants.

        private const string ITEM = "item";
        private const string KEY = "key";
        private const string VALUE = "value";

        #endregion
 
        #region IXmlSerializable Members

        /// <summary>
        /// This property is reserved, apply the <see cref="T:System.Xml.Serialization.XmlSchemaProviderAttribute"></see>
        /// to the class instead.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Xml.Schema.XmlSchema"></see> that describes the XML representation of the object that is
        /// produced by the <see cref="M:System.Xml.Serialization.IXmlSerializable.WriteXml(System.Xml.XmlWriter)"></see> method
        /// and consumed by the <see cref="M:System.Xml.Serialization.IXmlSerializable.ReadXml(System.Xml.XmlReader)"></see> method.
        /// </returns>
        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }
 
        /// <summary>
        /// Generates an object from its XML representation.
        /// </summary>
        /// <param name="reader">
        /// The <see cref="T:System.Xml.XmlReader"></see> stream from which the object is deserialized.
        /// </param>
        public void ReadXml(System.Xml.XmlReader reader)
        {
            XmlSerializer keySerializer = new XmlSerializer(typeof(TKey));
            XmlSerializer valueSerializer = new XmlSerializer(typeof(TValue));
 
            bool wasEmpty = reader.IsEmptyElement;
            reader.Read();
 
            if (wasEmpty)
                return;
            while (reader.NodeType != System.Xml.XmlNodeType.EndElement)
            {
                reader.ReadStartElement(ITEM);
                reader.ReadStartElement(KEY);
                TKey key = (TKey)keySerializer.Deserialize(reader);
                reader.ReadEndElement();
                reader.ReadStartElement(VALUE);
                TValue value = (TValue)valueSerializer.Deserialize(reader);
                reader.ReadEndElement();
                this.Add(key, value);
                reader.ReadEndElement();
                reader.MoveToContent();
            }
            reader.ReadEndElement();
        }
 
        /// <summary>
        /// Converts an object into its XML representation.
        /// </summary>
        /// <param name="writer">
        /// The <see cref="T:System.Xml.XmlWriter"></see> stream to which the object is serialized.
        /// </param>
        public void WriteXml(System.Xml.XmlWriter writer)
        {
            XmlSerializer keySerializer = new XmlSerializer(typeof(TKey));
            XmlSerializer valueSerializer = new XmlSerializer(typeof(TValue));
 
            foreach (TKey key in this.Keys)
            {
                writer.WriteStartElement(ITEM);
                writer.WriteStartElement(KEY);
 
                keySerializer.Serialize(writer, key);
                writer.WriteEndElement();
 
                writer.WriteStartElement(VALUE);
                TValue value = this[key];
                valueSerializer.Serialize(writer, value);
                writer.WriteEndElement();
 
                writer.WriteEndElement();
            }
        }

        #endregion
    }
 
    #endregion

    #region Back Office rights information class

    [Serializable]
    [XmlRoot("RightsInfo")]
    public class RightsInfo : ICloneable
    {
        public string Id      { get; set; }
        public string Count   { get; set; }
        public bool   Partial { get; set; }

        public RightsInfo()
        {
        }

        public RightsInfo(string id, string count)
        {
            this.Id = id;
            this.Count = count;
        }

        public RightsInfo(string id, string count, bool partial)
        {
            this.Id = id;
            this.Count = count;
            this.Partial = partial;
        }

        public object Clone()
        {
            return new RightsInfo(this.Id, this.Count, this.Partial);
        }

        public override string ToString()
        {
            return string.Format("RightsInfo: {0}|{1}|{2}",
                Id ?? string.Empty, Count ?? string.Empty,
                Partial == true ? "Partial fulfillment allowed" : "No partial fulfillment");
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to RightsInfo return false.
            RightsInfo info = obj as RightsInfo;
            if (info == null)
            {
                return false;
            }

            return this.Equals(info);
        }

        public bool Equals(RightsInfo info)
        {
            // If parameter is null return false.
            if (info == null)
            {
                return false;
            }

            // Return true if all the rights info fields match:
            return (Id == info.Id && Count == info.Count && Partial == info.Partial);
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public static bool operator == (RightsInfo info1, RightsInfo info2)
        {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(info1, info2))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)info1 == null) || ((object)info2 == null))
            {
                return false;
            }

            return info1.Equals(info2);
        }

        public static bool operator != (RightsInfo info1, RightsInfo info2)
        {
            return !(info1 == info2);
        }
    }

    #endregion

    #region Capability Request feature information

    [Serializable]
    [XmlRoot("FeatureInfo")]
    public class FeatureInfo : ICloneable
    {
        public string Name    { get; set; }
        public string Version { get; set; }
        public string Count   { get; set; }
        public bool   Partial { get; set; }

        public FeatureInfo()
        {
        }

        public FeatureInfo(string name, string version, string count)
        {
            this.Name = name;
            this.Version = version;
            this.Count = count;
        }

        public FeatureInfo(string name, string version, string count, bool partial)
        {
            this.Name = name;
            this.Version = version;
            this.Count = count;
            this.Partial = partial;
        }

        public object Clone()
        {
            return new FeatureInfo(this.Name, this.Version, this.Count, this.Partial);
        }

        public override string ToString()
        {
            return string.Format("FeatureInfo: {0}|{1}|{2}|{3}",
                Name ?? string.Empty, Version ?? string.Empty, Count ?? string.Empty,
                Partial == true ? "Partial fulfillment allowed" : "No partial fulfillment");
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to FeatureInfo return false.
            FeatureInfo info = obj as FeatureInfo;
            if (info == null)
            {
                return false;
            }

            return this.Equals(info);
        }

        public bool Equals(FeatureInfo info)
        {
            // If parameter is null return false.
            if (info == null)
            {
                return false;
            }

            // Return true if all the feature info fields match:
            return (Name == info.Name &&
                    Version == info.Version &&
                    Count == info.Count &&
                    Partial == info.Partial);
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public static bool operator == (FeatureInfo info1, FeatureInfo info2)
        {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(info1, info2))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)info1 == null) || ((object)info2 == null))
            {
                return false;
            }

            return info1.Equals(info2);
        }

        public static bool operator != (FeatureInfo info1, FeatureInfo info2)
        {
            return !(info1 == info2);
        }
    }

    #endregion

    #region Capability Request feature selector information

    [Serializable]
    [XmlRoot("FeatureSelector")]
    public class FeatureSelector : ICloneable
    {
        public string Type  { get; set; }
        public string Key   { get; set; }
        public string Value { get; set; }

        public FeatureSelector()
        {
        }

        public FeatureSelector(string type, string key, string value)
        {
            this.Type = type;
            this.Key = key;
            this.Value = value;
        }

        public object Clone()
        {
            return new FeatureInfo(this.Type, this.Key, this.Value);
        }

        public override string ToString()
        {
            return string.Format("FeatureSelector: {0}|{1}|{2}",
                Type ?? string.Empty, Key ?? string.Empty, Value ?? string.Empty);
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to FeatureSelector return false.
            FeatureSelector info = obj as FeatureSelector;
            if (info == null)
            {
                return false;
            }

            return this.Equals(info);
        }

        public bool Equals(FeatureSelector fs)
        {
            // If parameter is null return false.
            if (fs == null)
            {
                return false;
            }

            // Return true if all the feature selector fields match:
            return (Type  == fs.Type &&
                    Key   == fs.Key  &&
                    Value == fs.Value);
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public static bool operator == (FeatureSelector fs1, FeatureSelector fs2)
        {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(fs1, fs2))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)fs1 == null) || ((object)fs2 == null))
            {
                return false;
            }

            return fs1.Equals(fs2);
        }

        public static bool operator != (FeatureSelector fs1, FeatureSelector fs2)
        {
            return !(fs1 == fs2);
        }
    }

    #endregion
    
    #region Capability Request feature selector information

    [Serializable]
    [XmlRoot("AuxiliaryHostId")]
    public class AuxiliaryHostId : ICloneable
    {
        public string Type  { get; set; }
        public string Value { get; set; }

        public AuxiliaryHostId()
        {
        }

        public AuxiliaryHostId(string type, string value)
        {
            this.Type = type;
            this.Value = value;
        }

        public object Clone()
        {
            return new AuxiliaryHostId(this.Type, this.Value);
        }

        public override string ToString()
        {
            return string.Format("AuxiliaryHostId: {0}|{1}",
                Type ?? string.Empty, Value ?? string.Empty);
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to FeatureSelector return false.
            AuxiliaryHostId info = obj as AuxiliaryHostId;
            if (info == null)
            {
                return false;
            }

            return this.Equals(info);
        }

        public bool Equals(AuxiliaryHostId auxHostId)
        {
            // If parameter is null return false.
            if (auxHostId == null)
            {
                return false;
            }

            // Return true if all the auxiliary host id fields match:
            return (Type == auxHostId.Type && Value == auxHostId.Value);
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public static bool operator == (AuxiliaryHostId auxHostId1, AuxiliaryHostId auxHostId2)
        {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(auxHostId1, auxHostId2))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)auxHostId1 == null) || ((object)auxHostId2 == null))
            {
                return false;
            }

            return auxHostId1.Equals(auxHostId2);
        }

        public static bool operator != (AuxiliaryHostId auxHostId1, AuxiliaryHostId auxHostId2)
        {
            return !(auxHostId1 == auxHostId2);
        }
    }

    #endregion

    #region Information Message usage information

    [Serializable]
    [XmlRoot("UsageInfo")]
    public class UsageInfo : ICloneable
    {
        public string    Name       { get; set; }
        public string    Version    { get; set; }
        public string    Count      { get; set; }
        public DateTime? Expiration { get; set; }

        public UsageInfo()
        {
        }

        public UsageInfo(string name, string version, string count, DateTime? expiration)
        {
            this.Name = name;
            this.Version = version;
            this.Count = count;
            this.Expiration = expiration;
        }

        public object Clone()
        {
            return new UsageInfo(this.Name, this.Version, this.Count, this.Expiration);
        }

        public override string ToString()
        {
            return string.Format("UsageInfo: {0}|{1}|{2}|{3}", Name ?? string.Empty, Version ?? string.Empty, Count ?? string.Empty,
                Expiration.HasValue ? Expiration.Value.ToString() : string.Empty);
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to UsageInfo return false.
            UsageInfo info = obj as UsageInfo;
            if (info == null)
            {
                return false;
            }

            return this.Equals(info);
        }

        public bool Equals(UsageInfo info)
        {
            // If parameter is null return false.
            if (info == null)
            {
                return false;
            }

            // Return true if all the server info fields match:
            return (Name    == info.Name    &&
                    Version == info.Version &&
                    Count    == info.Count  &&
                    Expiration == info.Expiration);
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public static bool operator == (UsageInfo info1, UsageInfo info2)
        {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(info1, info2))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)info1 == null) || ((object)info2 == null))
            {
                return false;
            }

            return info1.Equals(info2);
        }

        public static bool operator != (UsageInfo info1, UsageInfo info2)
        {
            return !(info1 == info2);
        }
    }

    #endregion

    #region Helper classes

    public static class SequenceEqual<T>
    {
        public static bool Compare(List<T> list1, List<T> list2)
        {
            if (list1 == null && list2 == null)
            {
                return true;
            }

            if (list1 == null || list2 == null)
            {
                return false;
            }

            if (list1.Count != list2.Count)
            {
                return false;
            }

            for (int i = 0; i < list1.Count; i++)
            {
                bool found = false;
                foreach (T entry in list2)
                {
                    if (list1[i].Equals(entry))
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    return false;
                }
            }
            return true;
        }
    }

    public static class DictionaryEqual<TKey, TValue>
    {
        public static bool Compare(Dictionary<TKey, TValue> dict1, Dictionary<TKey, TValue> dict2)
        {
            if (dict1 == null && dict2 == null)
            {
                return true;
            }

            if (dict1 == null || dict2 == null)
            {
                return false;
            }

            if (dict1.Count != dict2.Count)
            {
                return false;
            }

            foreach (KeyValuePair<TKey, TValue> kvp in dict1)
            {
                if (!dict2.ContainsKey(kvp.Key) || !dict1[kvp.Key].Equals(dict2[kvp.Key]))
                {
                    return false;
                }
            }
            return true;
        }
    }

    #endregion

}
