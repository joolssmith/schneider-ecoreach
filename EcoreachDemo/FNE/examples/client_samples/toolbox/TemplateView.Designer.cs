// <copyright file="TemplateView.Designer.cs" company="Flexera Software LLC">
//     Copyright (c) 2012-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

namespace DotNetDemo
{
    partial class TemplateView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabTemplateDetails = new System.Windows.Forms.TabControl();
            this.pageTemplateFeatures = new System.Windows.Forms.TabPage();
            this.lvTemplateFeatures = new System.Windows.Forms.ListView();
            this.colTemplateFeatureIndex = new System.Windows.Forms.ColumnHeader();
            this.colTemplateFeature = new System.Windows.Forms.ColumnHeader();
            this.colTemplateFeatureVersion = new System.Windows.Forms.ColumnHeader();
            this.colTemplateFeatureCount = new System.Windows.Forms.ColumnHeader();
            this.colTemplateFeatureExpiration = new System.Windows.Forms.ColumnHeader();
            this.colTemplateFeatureValid = new System.Windows.Forms.ColumnHeader();
            this.colTemplateFeatureVendorString = new System.Windows.Forms.ColumnHeader();
            this.pageTemplateVD = new System.Windows.Forms.TabPage();
            this.lvTemplateVD = new System.Windows.Forms.ListView();
            this.colTemplateVDType = new System.Windows.Forms.ColumnHeader();
            this.colTemplateVDKey = new System.Windows.Forms.ColumnHeader();
            this.colTemplateVDValue = new System.Windows.Forms.ColumnHeader();
            this.txtTemplateID = new System.Windows.Forms.TextBox();
            this.txtTemplateExpiration = new System.Windows.Forms.TextBox();
            this.lblTemplateID = new System.Windows.Forms.Label();
            this.lblTemplateMachineType = new System.Windows.Forms.Label();
            this.lblTemplateExpiration = new System.Windows.Forms.Label();
            this.txtTemplateMachineType = new System.Windows.Forms.TextBox();
            this.tabTemplateDetails.SuspendLayout();
            this.pageTemplateFeatures.SuspendLayout();
            this.pageTemplateVD.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabTemplateDetails
            // 
            this.tabTemplateDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabTemplateDetails.Controls.Add(this.pageTemplateFeatures);
            this.tabTemplateDetails.Controls.Add(this.pageTemplateVD);
            this.tabTemplateDetails.Location = new System.Drawing.Point(0, 90);
            this.tabTemplateDetails.Margin = new System.Windows.Forms.Padding(0);
            this.tabTemplateDetails.Name = "tabTemplateDetails";
            this.tabTemplateDetails.SelectedIndex = 0;
            this.tabTemplateDetails.Size = new System.Drawing.Size(586, 376);
            this.tabTemplateDetails.TabIndex = 7;
            // 
            // pageTemplateFeatures
            // 
            this.pageTemplateFeatures.Controls.Add(this.lvTemplateFeatures);
            this.pageTemplateFeatures.Location = new System.Drawing.Point(4, 22);
            this.pageTemplateFeatures.Margin = new System.Windows.Forms.Padding(0);
            this.pageTemplateFeatures.Name = "pageTemplateFeatures";
            this.pageTemplateFeatures.Size = new System.Drawing.Size(578, 350);
            this.pageTemplateFeatures.TabIndex = 1;
            this.pageTemplateFeatures.Text = "Features";
            this.pageTemplateFeatures.UseVisualStyleBackColor = true;
            // 
            // lvTemplateFeatures
            // 
            this.lvTemplateFeatures.BackColor = System.Drawing.Color.PaleTurquoise;
            this.lvTemplateFeatures.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colTemplateFeatureIndex,
            this.colTemplateFeature,
            this.colTemplateFeatureVersion,
            this.colTemplateFeatureCount,
            this.colTemplateFeatureExpiration,
            this.colTemplateFeatureValid,
            this.colTemplateFeatureVendorString});
            this.lvTemplateFeatures.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvTemplateFeatures.FullRowSelect = true;
            this.lvTemplateFeatures.GridLines = true;
            this.lvTemplateFeatures.Location = new System.Drawing.Point(0, 0);
            this.lvTemplateFeatures.Margin = new System.Windows.Forms.Padding(0);
            this.lvTemplateFeatures.MultiSelect = false;
            this.lvTemplateFeatures.Name = "lvTemplateFeatures";
            this.lvTemplateFeatures.ShowItemToolTips = true;
            this.lvTemplateFeatures.Size = new System.Drawing.Size(578, 350);
            this.lvTemplateFeatures.TabIndex = 0;
            this.lvTemplateFeatures.UseCompatibleStateImageBehavior = false;
            this.lvTemplateFeatures.View = System.Windows.Forms.View.Details;
            this.lvTemplateFeatures.DoubleClick += new System.EventHandler(this.lvTemplateFeatures_DoubleClick);
            // 
            // colTemplateFeatureIndex
            // 
            this.colTemplateFeatureIndex.Text = "Index";
            this.colTemplateFeatureIndex.Width = 38;
            // 
            // colTemplateFeature
            // 
            this.colTemplateFeature.Text = "Feature";
            this.colTemplateFeature.Width = 125;
            // 
            // colTemplateFeatureVersion
            // 
            this.colTemplateFeatureVersion.Text = "Version";
            this.colTemplateFeatureVersion.Width = 47;
            // 
            // colTemplateFeatureCount
            // 
            this.colTemplateFeatureCount.Text = "Count";
            this.colTemplateFeatureCount.Width = 66;
            // 
            // colTemplateFeatureExpiration
            // 
            this.colTemplateFeatureExpiration.Text = "Expiration";
            this.colTemplateFeatureExpiration.Width = 134;
            // 
            // colTemplateFeatureValid
            // 
            this.colTemplateFeatureValid.Text = "Valid";
            this.colTemplateFeatureValid.Width = 164;
            // 
            // colTemplateFeatureVendorString
            // 
            this.colTemplateFeatureVendorString.Text = "Vendor String";
            this.colTemplateFeatureVendorString.Width = 150;
            // 
            // pageTemplateVD
            // 
            this.pageTemplateVD.Controls.Add(this.lvTemplateVD);
            this.pageTemplateVD.Location = new System.Drawing.Point(4, 22);
            this.pageTemplateVD.Margin = new System.Windows.Forms.Padding(0);
            this.pageTemplateVD.Name = "pageTemplateVD";
            this.pageTemplateVD.Size = new System.Drawing.Size(578, 350);
            this.pageTemplateVD.TabIndex = 3;
            this.pageTemplateVD.Text = "Vendor Dictionary";
            this.pageTemplateVD.UseVisualStyleBackColor = true;
            // 
            // lvTemplateVD
            // 
            this.lvTemplateVD.BackColor = System.Drawing.Color.Gainsboro;
            this.lvTemplateVD.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colTemplateVDType,
            this.colTemplateVDKey,
            this.colTemplateVDValue});
            this.lvTemplateVD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvTemplateVD.GridLines = true;
            this.lvTemplateVD.Location = new System.Drawing.Point(0, 0);
            this.lvTemplateVD.Margin = new System.Windows.Forms.Padding(0);
            this.lvTemplateVD.Name = "lvTemplateVD";
            this.lvTemplateVD.Size = new System.Drawing.Size(578, 350);
            this.lvTemplateVD.TabIndex = 1;
            this.lvTemplateVD.UseCompatibleStateImageBehavior = false;
            this.lvTemplateVD.View = System.Windows.Forms.View.Details;
            // 
            // colTemplateVDType
            // 
            this.colTemplateVDType.Text = "Type";
            this.colTemplateVDType.Width = 100;
            // 
            // colTemplateVDKey
            // 
            this.colTemplateVDKey.Text = "Key";
            this.colTemplateVDKey.Width = 200;
            // 
            // colTemplateVDValue
            // 
            this.colTemplateVDValue.Text = "Value";
            this.colTemplateVDValue.Width = 274;
            // 
            // txtTemplateID
            // 
            this.txtTemplateID.Location = new System.Drawing.Point(140, 8);
            this.txtTemplateID.Name = "txtTemplateID";
            this.txtTemplateID.ReadOnly = true;
            this.txtTemplateID.Size = new System.Drawing.Size(80, 20);
            this.txtTemplateID.TabIndex = 2;
            // 
            // txtTemplateExpiration
            // 
            this.txtTemplateExpiration.Location = new System.Drawing.Point(140, 35);
            this.txtTemplateExpiration.Name = "txtTemplateExpiration";
            this.txtTemplateExpiration.ReadOnly = true;
            this.txtTemplateExpiration.Size = new System.Drawing.Size(140, 20);
            this.txtTemplateExpiration.TabIndex = 4;
            // 
            // lblTemplateID
            // 
            this.lblTemplateID.AutoSize = true;
            this.lblTemplateID.Location = new System.Drawing.Point(10, 10);
            this.lblTemplateID.Name = "lblTemplateID";
            this.lblTemplateID.Size = new System.Drawing.Size(21, 13);
            this.lblTemplateID.TabIndex = 1;
            this.lblTemplateID.Text = "ID:";
            // 
            // lblTemplateMachineType
            // 
            this.lblTemplateMachineType.AutoSize = true;
            this.lblTemplateMachineType.Location = new System.Drawing.Point(10, 60);
            this.lblTemplateMachineType.Name = "lblTemplateMachineType";
            this.lblTemplateMachineType.Size = new System.Drawing.Size(78, 13);
            this.lblTemplateMachineType.TabIndex = 5;
            this.lblTemplateMachineType.Text = "Machine Type:";
            // 
            // lblTemplateExpiration
            // 
            this.lblTemplateExpiration.AutoSize = true;
            this.lblTemplateExpiration.Location = new System.Drawing.Point(10, 35);
            this.lblTemplateExpiration.Name = "lblTemplateExpiration";
            this.lblTemplateExpiration.Size = new System.Drawing.Size(56, 13);
            this.lblTemplateExpiration.TabIndex = 3;
            this.lblTemplateExpiration.Text = "Expiration:";
            // 
            // txtTemplateMachineType
            // 
            this.txtTemplateMachineType.Location = new System.Drawing.Point(140, 60);
            this.txtTemplateMachineType.Name = "txtTemplateMachineType";
            this.txtTemplateMachineType.ReadOnly = true;
            this.txtTemplateMachineType.Size = new System.Drawing.Size(140, 20);
            this.txtTemplateMachineType.TabIndex = 6;
            // 
            // TemplateView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 462);
            this.Controls.Add(this.txtTemplateMachineType);
            this.Controls.Add(this.tabTemplateDetails);
            this.Controls.Add(this.txtTemplateID);
            this.Controls.Add(this.txtTemplateExpiration);
            this.Controls.Add(this.lblTemplateID);
            this.Controls.Add(this.lblTemplateMachineType);
            this.Controls.Add(this.lblTemplateExpiration);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(300, 300);
            this.Name = "TemplateView";
            this.SystemColorsChanged += new System.EventHandler(this.TemplateView_SystemColorsChanged);
            this.tabTemplateDetails.ResumeLayout(false);
            this.pageTemplateFeatures.ResumeLayout(false);
            this.pageTemplateVD.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabTemplateDetails;
        private System.Windows.Forms.TabPage pageTemplateFeatures;
        private System.Windows.Forms.ListView lvTemplateFeatures;
        private System.Windows.Forms.ColumnHeader colTemplateFeatureIndex;
        private System.Windows.Forms.ColumnHeader colTemplateFeature;
        private System.Windows.Forms.ColumnHeader colTemplateFeatureVersion;
        private System.Windows.Forms.TabPage pageTemplateVD;
        private System.Windows.Forms.ListView lvTemplateVD;
        private System.Windows.Forms.ColumnHeader colTemplateVDType;
        private System.Windows.Forms.ColumnHeader colTemplateVDKey;
        private System.Windows.Forms.ColumnHeader colTemplateVDValue;
        private System.Windows.Forms.TextBox txtTemplateID;
        private System.Windows.Forms.TextBox txtTemplateExpiration;
        private System.Windows.Forms.Label lblTemplateID;
        private System.Windows.Forms.Label lblTemplateMachineType;
        private System.Windows.Forms.Label lblTemplateExpiration;
        private System.Windows.Forms.TextBox txtTemplateMachineType;
        private System.Windows.Forms.ColumnHeader colTemplateFeatureCount;
        private System.Windows.Forms.ColumnHeader colTemplateFeatureExpiration;
        private System.Windows.Forms.ColumnHeader colTemplateFeatureValid;
        private System.Windows.Forms.ColumnHeader colTemplateFeatureVendorString;

    }
}