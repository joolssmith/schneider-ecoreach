// <copyright file="ServerInstanceSelect.designer.cs" company="Flexera Software LLC">
//     Copyright (c) 2012-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

namespace DotNetDemo
{
    partial class ServerInstanceSelect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cklbTSServerInstance = new System.Windows.Forms.CheckedListBox();
            this.btnTSInstanceCancel = new System.Windows.Forms.Button();
            this.btnTSInstanceOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cklbTSServerInstance
            // 
            this.cklbTSServerInstance.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cklbTSServerInstance.CheckOnClick = true;
            this.cklbTSServerInstance.FormattingEnabled = true;
            this.cklbTSServerInstance.Location = new System.Drawing.Point(10, 10);
            this.cklbTSServerInstance.Name = "cklbTSServerInstance";
            this.cklbTSServerInstance.Size = new System.Drawing.Size(225, 154);
            this.cklbTSServerInstance.TabIndex = 0;
            this.cklbTSServerInstance.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.cklbTSServerInstance_ItemCheck);
            // 
            // btnTSInstanceCancel
            // 
            this.btnTSInstanceCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnTSInstanceCancel.BackColor = System.Drawing.Color.Gainsboro;
            this.btnTSInstanceCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnTSInstanceCancel.Location = new System.Drawing.Point(94, 180);
            this.btnTSInstanceCancel.Name = "btnTSInstanceCancel";
            this.btnTSInstanceCancel.Size = new System.Drawing.Size(75, 23);
            this.btnTSInstanceCancel.TabIndex = 2;
            this.btnTSInstanceCancel.Text = "Cancel";
            this.btnTSInstanceCancel.UseVisualStyleBackColor = false;
            // 
            // btnTSInstanceOK
            // 
            this.btnTSInstanceOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnTSInstanceOK.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnTSInstanceOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnTSInstanceOK.Location = new System.Drawing.Point(10, 180);
            this.btnTSInstanceOK.Name = "btnTSInstanceOK";
            this.btnTSInstanceOK.Size = new System.Drawing.Size(75, 23);
            this.btnTSInstanceOK.TabIndex = 1;
            this.btnTSInstanceOK.Text = "OK";
            this.btnTSInstanceOK.UseVisualStyleBackColor = false;
            // 
            // ServerInstanceSelect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(244, 212);
            this.Controls.Add(this.btnTSInstanceCancel);
            this.Controls.Add(this.btnTSInstanceOK);
            this.Controls.Add(this.cklbTSServerInstance);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(195, 140);
            this.Name = "ServerInstanceSelect";
            this.Text = "Target Server Instance Location";
            this.SystemColorsChanged += new System.EventHandler(this.ServerInstanceSelect_SystemColorsChanged);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckedListBox cklbTSServerInstance;
        private System.Windows.Forms.Button btnTSInstanceCancel;
        private System.Windows.Forms.Button btnTSInstanceOK;


    }
}