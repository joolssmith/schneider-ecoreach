// <copyright file="ResponseDetails.Designer.cs" company="Flexera Software LLC">
//     Copyright (c) 2012-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

namespace DotNetDemo
{
    partial class ResponseDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblServerName = new System.Windows.Forms.Label();
            this.lblServerURI = new System.Windows.Forms.Label();
            this.lblBackupServerURI = new System.Windows.Forms.Label();
            this.txtServerName = new System.Windows.Forms.TextBox();
            this.txtBackupServerURI = new System.Windows.Forms.TextBox();
            this.txtServerURI = new System.Windows.Forms.TextBox();
            this.lblRenewInterval = new System.Windows.Forms.Label();
            this.txtRenewInterval = new System.Windows.Forms.TextBox();
            this.lblUUID = new System.Windows.Forms.Label();
            this.txtUUID = new System.Windows.Forms.TextBox();
            this.lblVMType = new System.Windows.Forms.Label();
            this.txtVMType = new System.Windows.Forms.TextBox();
            this.tabResponseDetails = new System.Windows.Forms.TabControl();
            this.pageRDHostIdInfo = new System.Windows.Forms.TabPage();
            this.lvRDHostIds = new System.Windows.Forms.ListView();
            this.colRDHostIdType = new System.Windows.Forms.ColumnHeader();
            this.colRDHostIdValue = new System.Windows.Forms.ColumnHeader();
            this.pageRDVMInfo = new System.Windows.Forms.TabPage();
            this.lvRDVMInfo = new System.Windows.Forms.ListView();
            this.colRDVMInfoType = new System.Windows.Forms.ColumnHeader();
            this.colRDVMInfoKey = new System.Windows.Forms.ColumnHeader();
            this.colRDVMInfoValue = new System.Windows.Forms.ColumnHeader();
            this.pageRDVDInfo = new System.Windows.Forms.TabPage();
            this.lvRDVDInfo = new System.Windows.Forms.ListView();
            this.colRDVDType = new System.Windows.Forms.ColumnHeader();
            this.colRDVDKey = new System.Windows.Forms.ColumnHeader();
            this.colRDVDValue = new System.Windows.Forms.ColumnHeader();
            this.pageRDStatusInfo = new System.Windows.Forms.TabPage();
            this.lvRDStatusInfo = new System.Windows.Forms.ListView();
            this.colRDStatusCategory = new System.Windows.Forms.ColumnHeader();
            this.colRDStatusCode = new System.Windows.Forms.ColumnHeader();
            this.colRDStatusCodeDesc = new System.Windows.Forms.ColumnHeader();
            this.colRDStatusDetails = new System.Windows.Forms.ColumnHeader();
            this.lblCorrelationID = new System.Windows.Forms.Label();
            this.txtCorrelationID = new System.Windows.Forms.TextBox();
            this.lblServerID = new System.Windows.Forms.Label();
            this.txtServerID = new System.Windows.Forms.TextBox();
            this.lblServedTime = new System.Windows.Forms.Label();
            this.txtServedTime = new System.Windows.Forms.TextBox();
            this.lblServerInstance = new System.Windows.Forms.Label();
            this.txtServerInstance = new System.Windows.Forms.TextBox();
            this.ckbConfirmRequestNeeded = new System.Windows.Forms.CheckBox();
            this.ckbCloneSuspect = new System.Windows.Forms.CheckBox();
            this.tabResponseDetails.SuspendLayout();
            this.pageRDHostIdInfo.SuspendLayout();
            this.pageRDVMInfo.SuspendLayout();
            this.pageRDVDInfo.SuspendLayout();
            this.pageRDStatusInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblServerName
            // 
            this.lblServerName.AutoSize = true;
            this.lblServerName.Location = new System.Drawing.Point(10, 135);
            this.lblServerName.Name = "lblServerName";
            this.lblServerName.Size = new System.Drawing.Size(72, 13);
            this.lblServerName.TabIndex = 13;
            this.lblServerName.Text = "Server Name:";
            // 
            // lblServerURI
            // 
            this.lblServerURI.AutoSize = true;
            this.lblServerURI.Location = new System.Drawing.Point(10, 160);
            this.lblServerURI.Name = "lblServerURI";
            this.lblServerURI.Size = new System.Drawing.Size(63, 13);
            this.lblServerURI.TabIndex = 15;
            this.lblServerURI.Text = "Server URI:";
            // 
            // lblBackupServerURI
            // 
            this.lblBackupServerURI.AutoSize = true;
            this.lblBackupServerURI.Location = new System.Drawing.Point(10, 185);
            this.lblBackupServerURI.Name = "lblBackupServerURI";
            this.lblBackupServerURI.Size = new System.Drawing.Size(103, 13);
            this.lblBackupServerURI.TabIndex = 17;
            this.lblBackupServerURI.Text = "Backup Server URI:";
            // 
            // txtServerName
            // 
            this.txtServerName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtServerName.Location = new System.Drawing.Point(140, 133);
            this.txtServerName.Name = "txtServerName";
            this.txtServerName.ReadOnly = true;
            this.txtServerName.Size = new System.Drawing.Size(420, 20);
            this.txtServerName.TabIndex = 14;
            // 
            // txtBackupServerURI
            // 
            this.txtBackupServerURI.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBackupServerURI.Location = new System.Drawing.Point(140, 183);
            this.txtBackupServerURI.Name = "txtBackupServerURI";
            this.txtBackupServerURI.ReadOnly = true;
            this.txtBackupServerURI.Size = new System.Drawing.Size(420, 20);
            this.txtBackupServerURI.TabIndex = 18;
            // 
            // txtServerURI
            // 
            this.txtServerURI.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtServerURI.Location = new System.Drawing.Point(140, 158);
            this.txtServerURI.Name = "txtServerURI";
            this.txtServerURI.ReadOnly = true;
            this.txtServerURI.Size = new System.Drawing.Size(420, 20);
            this.txtServerURI.TabIndex = 16;
            // 
            // lblRenewInterval
            // 
            this.lblRenewInterval.AutoSize = true;
            this.lblRenewInterval.Location = new System.Drawing.Point(10, 210);
            this.lblRenewInterval.Name = "lblRenewInterval";
            this.lblRenewInterval.Size = new System.Drawing.Size(82, 13);
            this.lblRenewInterval.TabIndex = 19;
            this.lblRenewInterval.Text = "Renew Interval:";
            // 
            // txtRenewInterval
            // 
            this.txtRenewInterval.Location = new System.Drawing.Point(140, 208);
            this.txtRenewInterval.Name = "txtRenewInterval";
            this.txtRenewInterval.ReadOnly = true;
            this.txtRenewInterval.Size = new System.Drawing.Size(200, 20);
            this.txtRenewInterval.TabIndex = 20;
            // 
            // lblUUID
            // 
            this.lblUUID.AutoSize = true;
            this.lblUUID.Location = new System.Drawing.Point(10, 85);
            this.lblUUID.Name = "lblUUID";
            this.lblUUID.Size = new System.Drawing.Size(37, 13);
            this.lblUUID.TabIndex = 9;
            this.lblUUID.Text = "UUID:";
            // 
            // txtUUID
            // 
            this.txtUUID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUUID.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUUID.Location = new System.Drawing.Point(140, 83);
            this.txtUUID.Name = "txtUUID";
            this.txtUUID.ReadOnly = true;
            this.txtUUID.Size = new System.Drawing.Size(420, 22);
            this.txtUUID.TabIndex = 10;
            // 
            // lblVMType
            // 
            this.lblVMType.AutoSize = true;
            this.lblVMType.Location = new System.Drawing.Point(270, 60);
            this.lblVMType.Name = "lblVMType";
            this.lblVMType.Size = new System.Drawing.Size(78, 13);
            this.lblVMType.TabIndex = 7;
            this.lblVMType.Text = "Machine Type:";
            // 
            // txtVMType
            // 
            this.txtVMType.Location = new System.Drawing.Point(360, 58);
            this.txtVMType.Name = "txtVMType";
            this.txtVMType.ReadOnly = true;
            this.txtVMType.Size = new System.Drawing.Size(200, 20);
            this.txtVMType.TabIndex = 8;
            // 
            // tabResponseDetails
            // 
            this.tabResponseDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabResponseDetails.Controls.Add(this.pageRDHostIdInfo);
            this.tabResponseDetails.Controls.Add(this.pageRDVMInfo);
            this.tabResponseDetails.Controls.Add(this.pageRDVDInfo);
            this.tabResponseDetails.Controls.Add(this.pageRDStatusInfo);
            this.tabResponseDetails.Location = new System.Drawing.Point(10, 260);
            this.tabResponseDetails.Name = "tabResponseDetails";
            this.tabResponseDetails.SelectedIndex = 0;
            this.tabResponseDetails.Size = new System.Drawing.Size(565, 300);
            this.tabResponseDetails.TabIndex = 25;
            // 
            // pageRDHostIdInfo
            // 
            this.pageRDHostIdInfo.Controls.Add(this.lvRDHostIds);
            this.pageRDHostIdInfo.Location = new System.Drawing.Point(4, 22);
            this.pageRDHostIdInfo.Name = "pageRDHostIdInfo";
            this.pageRDHostIdInfo.Size = new System.Drawing.Size(557, 274);
            this.pageRDHostIdInfo.TabIndex = 4;
            this.pageRDHostIdInfo.Text = "Host Ids";
            this.pageRDHostIdInfo.UseVisualStyleBackColor = true;
            // 
            // lvRDHostIds
            // 
            this.lvRDHostIds.BackColor = System.Drawing.Color.PaleTurquoise;
            this.lvRDHostIds.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colRDHostIdType,
            this.colRDHostIdValue});
            this.lvRDHostIds.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvRDHostIds.GridLines = true;
            this.lvRDHostIds.Location = new System.Drawing.Point(0, 0);
            this.lvRDHostIds.Name = "lvRDHostIds";
            this.lvRDHostIds.Size = new System.Drawing.Size(557, 274);
            this.lvRDHostIds.TabIndex = 1;
            this.lvRDHostIds.UseCompatibleStateImageBehavior = false;
            this.lvRDHostIds.View = System.Windows.Forms.View.Details;
            // 
            // colRDHostIdType
            // 
            this.colRDHostIdType.Text = "ID Type";
            this.colRDHostIdType.Width = 220;
            // 
            // colRDHostIdValue
            // 
            this.colRDHostIdValue.Text = "ID Value";
            this.colRDHostIdValue.Width = 383;
            // 
            // pageRDVMInfo
            // 
            this.pageRDVMInfo.Controls.Add(this.lvRDVMInfo);
            this.pageRDVMInfo.Location = new System.Drawing.Point(4, 22);
            this.pageRDVMInfo.Name = "pageRDVMInfo";
            this.pageRDVMInfo.Padding = new System.Windows.Forms.Padding(3);
            this.pageRDVMInfo.Size = new System.Drawing.Size(557, 244);
            this.pageRDVMInfo.TabIndex = 1;
            this.pageRDVMInfo.Text = "Virtual Machine Info";
            this.pageRDVMInfo.UseVisualStyleBackColor = true;
            // 
            // lvRDVMInfo
            // 
            this.lvRDVMInfo.BackColor = System.Drawing.Color.LightSteelBlue;
            this.lvRDVMInfo.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colRDVMInfoType,
            this.colRDVMInfoKey,
            this.colRDVMInfoValue});
            this.lvRDVMInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvRDVMInfo.FullRowSelect = true;
            this.lvRDVMInfo.GridLines = true;
            this.lvRDVMInfo.Location = new System.Drawing.Point(3, 3);
            this.lvRDVMInfo.Name = "lvRDVMInfo";
            this.lvRDVMInfo.Size = new System.Drawing.Size(551, 238);
            this.lvRDVMInfo.TabIndex = 2;
            this.lvRDVMInfo.UseCompatibleStateImageBehavior = false;
            this.lvRDVMInfo.View = System.Windows.Forms.View.Details;
            // 
            // colRDVMInfoType
            // 
            this.colRDVMInfoType.Text = "Type";
            this.colRDVMInfoType.Width = 100;
            // 
            // colRDVMInfoKey
            // 
            this.colRDVMInfoKey.Text = "Key";
            this.colRDVMInfoKey.Width = 200;
            // 
            // colRDVMInfoValue
            // 
            this.colRDVMInfoValue.Text = "Value";
            this.colRDVMInfoValue.Width = 253;
            // 
            // pageRDVDInfo
            // 
            this.pageRDVDInfo.Controls.Add(this.lvRDVDInfo);
            this.pageRDVDInfo.Location = new System.Drawing.Point(4, 22);
            this.pageRDVDInfo.Name = "pageRDVDInfo";
            this.pageRDVDInfo.Size = new System.Drawing.Size(557, 244);
            this.pageRDVDInfo.TabIndex = 3;
            this.pageRDVDInfo.Text = "Vendor Dictionary";
            this.pageRDVDInfo.UseVisualStyleBackColor = true;
            // 
            // lvRDVDInfo
            // 
            this.lvRDVDInfo.BackColor = System.Drawing.Color.Gainsboro;
            this.lvRDVDInfo.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colRDVDType,
            this.colRDVDKey,
            this.colRDVDValue});
            this.lvRDVDInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvRDVDInfo.GridLines = true;
            this.lvRDVDInfo.Location = new System.Drawing.Point(0, 0);
            this.lvRDVDInfo.Name = "lvRDVDInfo";
            this.lvRDVDInfo.Size = new System.Drawing.Size(557, 244);
            this.lvRDVDInfo.TabIndex = 3;
            this.lvRDVDInfo.UseCompatibleStateImageBehavior = false;
            this.lvRDVDInfo.View = System.Windows.Forms.View.Details;
            // 
            // colRDVDType
            // 
            this.colRDVDType.Text = "Type";
            this.colRDVDType.Width = 100;
            // 
            // colRDVDKey
            // 
            this.colRDVDKey.Text = "Key";
            this.colRDVDKey.Width = 200;
            // 
            // colRDVDValue
            // 
            this.colRDVDValue.Text = "Value";
            this.colRDVDValue.Width = 253;
            // 
            // pageRDStatusInfo
            // 
            this.pageRDStatusInfo.Controls.Add(this.lvRDStatusInfo);
            this.pageRDStatusInfo.Location = new System.Drawing.Point(4, 22);
            this.pageRDStatusInfo.Name = "pageRDStatusInfo";
            this.pageRDStatusInfo.Size = new System.Drawing.Size(557, 244);
            this.pageRDStatusInfo.TabIndex = 2;
            this.pageRDStatusInfo.Text = "Status Info";
            this.pageRDStatusInfo.UseVisualStyleBackColor = true;
            // 
            // lvRDStatusInfo
            // 
            this.lvRDStatusInfo.BackColor = System.Drawing.Color.Silver;
            this.lvRDStatusInfo.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colRDStatusCategory,
            this.colRDStatusCode,
            this.colRDStatusCodeDesc,
            this.colRDStatusDetails});
            this.lvRDStatusInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvRDStatusInfo.GridLines = true;
            this.lvRDStatusInfo.Location = new System.Drawing.Point(0, 0);
            this.lvRDStatusInfo.Name = "lvRDStatusInfo";
            this.lvRDStatusInfo.Size = new System.Drawing.Size(557, 244);
            this.lvRDStatusInfo.TabIndex = 4;
            this.lvRDStatusInfo.UseCompatibleStateImageBehavior = false;
            this.lvRDStatusInfo.View = System.Windows.Forms.View.Details;
            // 
            // colRDStatusCategory
            // 
            this.colRDStatusCategory.Text = "Category";
            this.colRDStatusCategory.Width = 100;
            // 
            // colRDStatusCode
            // 
            this.colRDStatusCode.Text = "Code";
            this.colRDStatusCode.Width = 70;
            // 
            // colRDStatusCodeDesc
            // 
            this.colRDStatusCodeDesc.Text = "Code Description";
            this.colRDStatusCodeDesc.Width = 180;
            // 
            // colRDStatusDetails
            // 
            this.colRDStatusDetails.Text = "Details";
            this.colRDStatusDetails.Width = 203;
            // 
            // lblCorrelationID
            // 
            this.lblCorrelationID.AutoSize = true;
            this.lblCorrelationID.Location = new System.Drawing.Point(10, 110);
            this.lblCorrelationID.Name = "lblCorrelationID";
            this.lblCorrelationID.Size = new System.Drawing.Size(74, 13);
            this.lblCorrelationID.TabIndex = 11;
            this.lblCorrelationID.Text = "Correlation ID:";
            // 
            // txtCorrelationID
            // 
            this.txtCorrelationID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCorrelationID.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCorrelationID.Location = new System.Drawing.Point(140, 108);
            this.txtCorrelationID.Name = "txtCorrelationID";
            this.txtCorrelationID.ReadOnly = true;
            this.txtCorrelationID.Size = new System.Drawing.Size(420, 22);
            this.txtCorrelationID.TabIndex = 12;
            // 
            // lblServerID
            // 
            this.lblServerID.AutoSize = true;
            this.lblServerID.Location = new System.Drawing.Point(10, 10);
            this.lblServerID.Name = "lblServerID";
            this.lblServerID.Size = new System.Drawing.Size(55, 13);
            this.lblServerID.TabIndex = 1;
            this.lblServerID.Text = "Server ID:";
            // 
            // txtServerID
            // 
            this.txtServerID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtServerID.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtServerID.Location = new System.Drawing.Point(140, 8);
            this.txtServerID.Name = "txtServerID";
            this.txtServerID.ReadOnly = true;
            this.txtServerID.Size = new System.Drawing.Size(420, 22);
            this.txtServerID.TabIndex = 2;
            // 
            // lblServedTime
            // 
            this.lblServedTime.AutoSize = true;
            this.lblServedTime.Location = new System.Drawing.Point(10, 35);
            this.lblServedTime.Name = "lblServedTime";
            this.lblServedTime.Size = new System.Drawing.Size(70, 13);
            this.lblServedTime.TabIndex = 3;
            this.lblServedTime.Text = "Served Time:";
            // 
            // txtServedTime
            // 
            this.txtServedTime.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtServedTime.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtServedTime.Location = new System.Drawing.Point(140, 33);
            this.txtServedTime.Name = "txtServedTime";
            this.txtServedTime.ReadOnly = true;
            this.txtServedTime.Size = new System.Drawing.Size(420, 22);
            this.txtServedTime.TabIndex = 4;
            // 
            // lblServerInstance
            // 
            this.lblServerInstance.AutoSize = true;
            this.lblServerInstance.Location = new System.Drawing.Point(10, 60);
            this.lblServerInstance.Name = "lblServerInstance";
            this.lblServerInstance.Size = new System.Drawing.Size(85, 13);
            this.lblServerInstance.TabIndex = 5;
            this.lblServerInstance.Text = "Server Instance:";
            // 
            // txtServerInstance
            // 
            this.txtServerInstance.Location = new System.Drawing.Point(140, 58);
            this.txtServerInstance.Name = "txtServerInstance";
            this.txtServerInstance.ReadOnly = true;
            this.txtServerInstance.Size = new System.Drawing.Size(120, 20);
            this.txtServerInstance.TabIndex = 6;
            // 
            // ckbConfirmRequestNeeded
            // 
            this.ckbConfirmRequestNeeded.AutoSize = true;
            this.ckbConfirmRequestNeeded.Location = new System.Drawing.Point(10, 235);
            this.ckbConfirmRequestNeeded.Name = "ckbConfirmRequestNeeded";
            this.ckbConfirmRequestNeeded.Size = new System.Drawing.Size(168, 17);
            this.ckbConfirmRequestNeeded.TabIndex = 21;
            this.ckbConfirmRequestNeeded.Text = "Confirmation Request Needed";
            this.ckbConfirmRequestNeeded.UseVisualStyleBackColor = true;
            // 
            // ckbCloneSuspect
            // 
            this.ckbCloneSuspect.AutoSize = true;
            this.ckbCloneSuspect.Location = new System.Drawing.Point(200, 235);
            this.ckbCloneSuspect.Name = "ckbCloneSuspect";
            this.ckbCloneSuspect.Size = new System.Drawing.Size(95, 17);
            this.ckbCloneSuspect.TabIndex = 22;
            this.ckbCloneSuspect.Text = "Clone Suspect";
            this.ckbCloneSuspect.UseVisualStyleBackColor = true;
            // 
            // ResponseDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 562);
            this.Controls.Add(this.ckbCloneSuspect);
            this.Controls.Add(this.ckbConfirmRequestNeeded);
            this.Controls.Add(this.txtServerInstance);
            this.Controls.Add(this.lblServerInstance);
            this.Controls.Add(this.txtServedTime);
            this.Controls.Add(this.lblServerID);
            this.Controls.Add(this.txtCorrelationID);
            this.Controls.Add(this.lblCorrelationID);
            this.Controls.Add(this.tabResponseDetails);
            this.Controls.Add(this.lblUUID);
            this.Controls.Add(this.txtRenewInterval);
            this.Controls.Add(this.lblServerURI);
            this.Controls.Add(this.lblServerName);
            this.Controls.Add(this.lblVMType);
            this.Controls.Add(this.txtServerID);
            this.Controls.Add(this.txtServerName);
            this.Controls.Add(this.lblServedTime);
            this.Controls.Add(this.txtVMType);
            this.Controls.Add(this.txtUUID);
            this.Controls.Add(this.lblRenewInterval);
            this.Controls.Add(this.lblBackupServerURI);
            this.Controls.Add(this.txtServerURI);
            this.Controls.Add(this.txtBackupServerURI);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(380, 450);
            this.Name = "ResponseDetails";
            this.Text = "Capability Response Details";
            this.SystemColorsChanged += new System.EventHandler(this.ResponseDetails_SystemColorsChanged);
            this.tabResponseDetails.ResumeLayout(false);
            this.pageRDHostIdInfo.ResumeLayout(false);
            this.pageRDVMInfo.ResumeLayout(false);
            this.pageRDVDInfo.ResumeLayout(false);
            this.pageRDStatusInfo.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblServerName;
        private System.Windows.Forms.Label lblServerURI;
        private System.Windows.Forms.Label lblBackupServerURI;
        private System.Windows.Forms.TextBox txtServerName;
        private System.Windows.Forms.TextBox txtBackupServerURI;
        private System.Windows.Forms.TextBox txtServerURI;
        private System.Windows.Forms.Label lblRenewInterval;
        private System.Windows.Forms.TextBox txtRenewInterval;
        private System.Windows.Forms.Label lblUUID;
        private System.Windows.Forms.TextBox txtUUID;
        private System.Windows.Forms.Label lblVMType;
        private System.Windows.Forms.TextBox txtVMType;
        private System.Windows.Forms.TabControl tabResponseDetails;
        private System.Windows.Forms.TabPage pageRDVMInfo;
        private System.Windows.Forms.ListView lvRDVMInfo;
        private System.Windows.Forms.ColumnHeader colRDVMInfoType;
        private System.Windows.Forms.ColumnHeader colRDVMInfoKey;
        private System.Windows.Forms.ColumnHeader colRDVMInfoValue;
        private System.Windows.Forms.TabPage pageRDVDInfo;
        private System.Windows.Forms.ListView lvRDVDInfo;
        private System.Windows.Forms.ColumnHeader colRDVDType;
        private System.Windows.Forms.ColumnHeader colRDVDKey;
        private System.Windows.Forms.TabPage pageRDStatusInfo;
        private System.Windows.Forms.ListView lvRDStatusInfo;
        private System.Windows.Forms.ColumnHeader colRDStatusCategory;
        private System.Windows.Forms.ColumnHeader colRDStatusCode;
        private System.Windows.Forms.ColumnHeader colRDStatusCodeDesc;
        private System.Windows.Forms.ColumnHeader colRDStatusDetails;
        private System.Windows.Forms.ColumnHeader colRDVDValue;
        private System.Windows.Forms.TabPage pageRDHostIdInfo;
        private System.Windows.Forms.ListView lvRDHostIds;
        private System.Windows.Forms.ColumnHeader colRDHostIdType;
        private System.Windows.Forms.ColumnHeader colRDHostIdValue;
        private System.Windows.Forms.Label lblCorrelationID;
        private System.Windows.Forms.TextBox txtCorrelationID;
        private System.Windows.Forms.Label lblServerID;
        private System.Windows.Forms.TextBox txtServerID;
        private System.Windows.Forms.Label lblServedTime;
        private System.Windows.Forms.TextBox txtServedTime;
        private System.Windows.Forms.Label lblServerInstance;
        private System.Windows.Forms.TextBox txtServerInstance;
        private System.Windows.Forms.CheckBox ckbConfirmRequestNeeded;
        private System.Windows.Forms.CheckBox ckbCloneSuspect;

    }
}