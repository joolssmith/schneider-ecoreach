﻿// <copyright file="SaveAs.cs" company="Flexera Software LLC">
//     Copyright (c) 2012-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using FlxDotNetClient;

namespace DotNetDemo
{
    public partial class AdvancedServer : Form
    {
        private const string strNoHeaderType = "No HTTP header type supplied.";
        private const string strNoHeaderValue = "No HTTP header value supplied.";

        XmlSerializableDictionary<string, string> headers = new XmlSerializableDictionary<string, string>();
        ErrorProvider errorProvider;

        public AdvancedServer()
        {
            InitializeComponent();
            lblProxyPort.Enabled = txtProxyPort.Enabled = false;
            AdvancedServer_SystemColorsChanged(this, new EventArgs());
            errorProvider = new ErrorProvider(this);
            errorProvider.BlinkRate = 500;
        }

        public string ProxyServer
        {
            get
            {
                return (txtProxyServer.Text == null || String.IsNullOrEmpty(txtProxyServer.Text.Trim())) ? null : txtProxyServer.Text.Trim();
            }
            set
            {
                txtProxyServer.Text = value;
                lblProxyPort.Enabled = txtProxyPort.Enabled = txtProxyServer.Text != null && !String.IsNullOrEmpty(txtProxyServer.Text.Trim());
            }
        }

        public string ProxyPort
        {
            get
            { 
                return (!txtProxyPort.Enabled || txtProxyPort.Text == null || String.IsNullOrEmpty(txtProxyPort.Text.Trim())) ? null : txtProxyPort.Text.Trim();
            }
            set
            {
                txtProxyPort.Text = value;
            }
        }

        public XmlSerializableDictionary<string, string> HttpHeaders
        {
            get
            {
                return headers.Count == 0 ? null : headers;
            }
            set
            {
                headers = (value == null ? new XmlSerializableDictionary<string, string>() : new XmlSerializableDictionary<string, string>(value));
                lvAdditionalHttpHeaders.BeginUpdate();
                lvAdditionalHttpHeaders.Items.Clear();
                if (headers.Count > 0)
                {
                    foreach (KeyValuePair<string, string> kvp in headers)
                    {
                        lvAdditionalHttpHeaders.Items.Add(new ListViewItem(new string[] { kvp.Key, kvp.Value } ));
                    }
                }
                lvAdditionalHttpHeaders.EndUpdate();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            errorProvider.Clear();
        }

        private void AdvancedServer_Shown(object sender, EventArgs e)
        {
            if (txtProxyServer.CanFocus)
            {
                txtProxyServer.Focus();
            }
        }

        private void AdvancedServer_SystemColorsChanged(object sender, EventArgs e)
        {
            btnSave.BackColor = btnHttpHeaderAdd.BackColor = SystemInformation.HighContrast ? Color.SteelBlue : Color.LightSteelBlue;
            btnCancel.BackColor = btnHttpHeaderRemove.BackColor = SystemInformation.HighContrast ? Color.Gray : Color.Gainsboro;
        }

        private void btnHttpHeaderAdd_Click(object sender, EventArgs e)
        {
            errorProvider.Clear();
            if (txtHttpHeaderType.Text == null || String.IsNullOrEmpty(txtHttpHeaderType.Text.Trim()))
            {
                errorProvider.SetIconAlignment(txtHttpHeaderType, ErrorIconAlignment.MiddleRight);
                errorProvider.SetError(txtHttpHeaderType, strNoHeaderType);
            }
            else if (txtHttpHeaderValue.Text == null || String.IsNullOrEmpty(txtHttpHeaderValue.Text.Trim()))
            {
                errorProvider.SetIconAlignment(txtHttpHeaderValue, ErrorIconAlignment.MiddleRight);
                errorProvider.SetError(txtHttpHeaderValue, strNoHeaderValue);
            }
            else
            {
                string type = txtHttpHeaderType.Text.Trim();
                string value = txtHttpHeaderValue.Text.Trim();
                if (headers.ContainsKey(type))
                {
                    headers[type] = value;
                    foreach (ListViewItem item in lvAdditionalHttpHeaders.Items)
                    {
                        if (item.SubItems[0].Text.Equals(type, StringComparison.OrdinalIgnoreCase))
                        {
                            item.SubItems[1].Text = value;
                            break;
                        }
                    }
                }
                else
                {
                    headers.Add(type, value);
                    lvAdditionalHttpHeaders.Items.Add(new ListViewItem(new string[] { type, value }));
                }
            }
            CheckPoxyPort();
        }

        private void lvAdditionalHttpHeaders_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnHttpHeaderRemove.Enabled = lvAdditionalHttpHeaders.SelectedIndices.Count > 0;
        }

        private void btnHttpHeaderRemove_Click(object sender, EventArgs e)
        {
            lvAdditionalHttpHeaders.BeginUpdate();
            foreach (ListViewItem item in lvAdditionalHttpHeaders.SelectedItems)
            {
                headers.Remove(item.SubItems[0].Text.Trim());
                lvAdditionalHttpHeaders.Items.Remove(item);
            }
            lvAdditionalHttpHeaders.EndUpdate();
        }

        private void txtHttpHeaderType_TextChanged(object sender, EventArgs e)
        {
            errorProvider.Clear();
            CheckPoxyPort();
        }

        private void txtHttpHeaderValue_TextChanged(object sender, EventArgs e)
        {
            errorProvider.Clear();
            CheckPoxyPort();
        }

        private void txtProxyServer_TextChanged(object sender, EventArgs e)
        {
            errorProvider.Clear();
            lblProxyPort.Enabled = txtProxyPort.Enabled = (txtProxyServer.Text != null && !String.IsNullOrEmpty(txtProxyServer.Text.Trim()));
            CheckPoxyPort();
        }

        private void txtProxyPort_TextChanged(object sender, EventArgs e)
        {
            errorProvider.Clear();
            CheckPoxyPort();
        }

        private void CheckPoxyPort()
        {
            if (txtProxyPort.Enabled)
            {
                if (txtProxyPort.Text == null || String.IsNullOrEmpty(txtProxyPort.Text))
                {
                    btnSave.Enabled = true;
                }
                else
                {
                    ulong port;
                    string strPort = DotNetDemoForm.GetConfigString(txtProxyPort.Text);
                    if (strPort == null || !DotNetDemoForm.GetCount(strPort, out port) || port == 0)
                    {
                        errorProvider.SetIconAlignment(txtProxyPort, ErrorIconAlignment.MiddleRight);
                        errorProvider.SetError(txtProxyPort, DotNetDemoForm.strInvalidPort);
                        btnSave.Enabled = false;
                    }
                    else
                    {
                        btnSave.Enabled = true;
                    }
                }
            }
        }
 
    }
}
