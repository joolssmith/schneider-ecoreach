﻿// <copyright file="SaveAs.cs" company="Flexera Software LLC">
//     Copyright (c) 2012-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using FlxDotNetClient;

namespace DotNetDemo
{
    public partial class SaveAs : Form
    {
        private const string strNeedConfigFile = "Configuration file name is required";
        private const string strNeedConfigName = "Configuration name is required";
        private const string strSaveAsTitle = "Save To New Or Existing Configuration File";
        private Configuration config;
        ErrorProvider errorProvider;

        private Color color = SystemColors.Window;

        public SaveAs()
        {
            InitializeComponent();
            SaveAs_SystemColorsChanged(this, new EventArgs());

            btnConfigColor.BackColor = color;

            IntPtr Hicon = Properties.Resources.document_save_as_3.GetHicon();
            this.Icon = Icon.FromHandle(Hicon);
            btnBrowseConfig.Image = Properties.Resources.document_open_2.ToBitmap();
            errorProvider = new ErrorProvider(this);
            errorProvider.BlinkRate = 500;
        }

        public Configuration Config
        {
            set
            {
                config = value;
                if (config != null)
                {
                    txtConfigTitle.Text = config.FormTitle;
                }
            }
        }

        public string ConfigTitle
        {
            get { return txtConfigTitle.Text.Trim(); }
        }

        public string ConfigFile
        {
            get
            {
                string configFile = txtConfigFile.Text.Trim();
                if (!configFile.EndsWith(DotNetDemoForm.strXMLSuffix, StringComparison.OrdinalIgnoreCase))
                {
                    configFile = configFile + DotNetDemoForm.strXMLSuffix;
                }
                return configFile;
            }
            set { txtConfigFile.Text = value;  }
        }

        public string ConfigName
        {
            get { return txtConfigName.Text.Trim(); }
        }

        public Color ConfigColor
        {
            get { return color; }
            set
            {
                color = btnConfigColor.BackColor = value;
            }
        }

        private void btnBrowseConfig_Click(object sender, EventArgs e)
        {
            errorProvider.Clear();
            using (OpenFileDialog dialog = new OpenFileDialog())
            {
                dialog.AddExtension = false;
                dialog.Filter = DotNetDemoForm.strXMLFilter;
                dialog.Title = strSaveAsTitle;
                dialog.CheckFileExists = false;
                dialog.CheckPathExists = true;
                dialog.InitialDirectory = Application.StartupPath;
                dialog.RestoreDirectory = true;
                DialogResult result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    txtConfigFile.Text = dialog.FileName;
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            errorProvider.Clear();
            if (string.IsNullOrEmpty(txtConfigFile.Text) || string.IsNullOrEmpty(txtConfigFile.Text.Trim()))
            {
                errorProvider.SetIconAlignment(txtConfigFile, ErrorIconAlignment.MiddleLeft);
                errorProvider.SetError(txtConfigFile, strNeedConfigFile);
                DialogResult = DialogResult.None;
            }
            else if (string.IsNullOrEmpty(txtConfigName.Text) || string.IsNullOrEmpty(txtConfigName.Text.Trim()))
            {
                errorProvider.SetIconAlignment(txtConfigName, ErrorIconAlignment.MiddleLeft);
                errorProvider.SetError(txtConfigName, strNeedConfigName);
                DialogResult = DialogResult.None;
            }
         }

        private void errorprovider_reset(object sender, EventArgs e)
        {
            errorProvider.Clear();
        }

        private void SaveAs_Shown(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtConfigFile.Text) & txtConfigFile.CanFocus)
            {
                txtConfigFile.Focus();
            }
            else if (txtConfigName.CanFocus)
            {
                txtConfigName.Focus();
            }
        }

        private void btnConfigColor_Click(object sender, EventArgs e)
        {
            configColorDialog.AllowFullOpen = true;
            configColorDialog.SolidColorOnly = false;
            configColorDialog.AnyColor = true;
            configColorDialog.Color = btnConfigColor.BackColor = color;
            DialogResult result = configColorDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                ConfigColor = configColorDialog.Color;
            }
            return;
        }

        private void SaveAs_SystemColorsChanged(object sender, EventArgs e)
        {
            btnSave.BackColor = SystemInformation.HighContrast ? Color.SteelBlue : Color.LightSteelBlue;
            btnCancel.BackColor = SystemInformation.HighContrast ? Color.Gray : Color.Gainsboro;
        }
 
    }
}
