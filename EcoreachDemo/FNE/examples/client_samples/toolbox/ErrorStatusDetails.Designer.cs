// <copyright file="ErrorStatusDetails.Designer.cs" company="Flexera Software LLC">
//     Copyright (c) 2012-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

namespace DotNetDemo
{
    partial class ErrorStatusDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtErrorStatus = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtErrorStatus
            // 
            this.txtErrorStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtErrorStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtErrorStatus.Location = new System.Drawing.Point(0, 0);
            this.txtErrorStatus.Multiline = true;
            this.txtErrorStatus.Name = "txtErrorStatus";
            this.txtErrorStatus.ReadOnly = true;
            this.txtErrorStatus.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtErrorStatus.Size = new System.Drawing.Size(584, 162);
            this.txtErrorStatus.TabIndex = 0;
            this.txtErrorStatus.WordWrap = false;
            // 
            // ErrorStatusDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 162);
            this.Controls.Add(this.txtErrorStatus);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ErrorStatusDetails";
            this.Text = "Error Status Details";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtErrorStatus;


    }
}
