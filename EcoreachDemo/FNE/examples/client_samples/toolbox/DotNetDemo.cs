﻿// <copyright file="DotNetDemo.cs" company="Flexera Software LLC">
//     Copyright (c) 2012-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

using System;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace DotNetDemo
{
    static class DotNetDemo
    {
        static string backOne = Path.DirectorySeparatorChar + "..";

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.AssemblyResolve += new ResolveEventHandler(LoadFromLibDirectory);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new DotNetDemoForm(args));
        }

        static Assembly LoadFromLibDirectory(object sender, ResolveEventArgs args)
        {
            string libFolderPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + backOne + backOne + backOne + Path.DirectorySeparatorChar + "lib";
            string assemblyName = new AssemblyName(args.Name).Name + ".dll";
            string assemblyPath = Path.Combine(libFolderPath, assemblyName);
            if (Directory.Exists(libFolderPath) && File.Exists(assemblyPath))
            {
                return Assembly.LoadFrom(assemblyPath);
            }
            else if (!assemblyName.StartsWith("{") && !assemblyName.StartsWith("mscorlib") && !assemblyName.EndsWith("&.dll"))
            {
                string userMsg = string.Format("Unable to locate {0}. DotNetDemo terminating.", assemblyName);
                MessageBox.Show(userMsg, "DotNetDemo Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(0);
                return null;
            }
            else
            {
                return null;
            }
        }
    }
}
