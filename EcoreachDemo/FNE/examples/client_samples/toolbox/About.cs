﻿// <copyright file="About.cs" company="Flexera Software LLC">
//     Copyright (c) 2012-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

using System.Drawing;
using System.Windows.Forms;
using FlxDotNetClient;

namespace DotNetDemo
{
    public partial class About : Form
    {
        private const string strUncounted = "Uncounted";
        private const string strPerpetual = "Perpetual";
        private const int iVSHeaderIndex = 6;

        public About()
        {
            InitializeComponent();
        }

        public Bitmap Image
        {
            set
            {
                picAbout.Image = value;
            }
        }

        public string Caption
        {
            set
            {
                this.Text = value;
            }
        }

        public string Info
        {
            set
            {
                txtAbout.Text = value;
            }
        }

        private void About_Shown(object sender, System.EventArgs e)
        {
            if (txtAbout.Focused && btnAboutOK.CanFocus)
            {
                // Remove annoying text highlight.
                btnAboutOK.Focus();
            }
        }

    }
}
