﻿// <copyright file="ServerInstanceSelect.cs" company="Flexera Software LLC">
//     Copyright (c) 2016-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using FlxDotNetClient;

namespace DotNetDemo
{
    public partial class ServerInstanceSelect : Form
    {
        int lastCheckedIndex = -1;
        int defaultIndex = -1;

        public ServerInstanceSelect()
        {
            InitializeComponent();
            Array instanceArray = Enum.GetValues(typeof(LicenseServerInstance));
            int prevValue = -1;
            int itemIndex = 0;
            foreach (LicenseServerInstance serverInstance in instanceArray)
            {
                if (serverInstance != LicenseServerInstance.Unknown && prevValue != (int)serverInstance)
                {
                    cklbTSServerInstance.Items.Add(serverInstance, serverInstance == LicenseServerInstance.Default);
                    if (serverInstance == LicenseServerInstance.Default)
                    {
                        lastCheckedIndex = itemIndex;
                        defaultIndex = itemIndex;
                    }
                    itemIndex++;
                }
                prevValue = (int)serverInstance;
            }

            int sizeAdjustment = (int)LicenseServerInstance.Max - 10;
            if (sizeAdjustment != 0)
            {   // Form sized for max of 10 trusted storage license server istances. Adjust dynamically.
                this.Size = new Size(this.Size.Width, this.Size.Height + sizeAdjustment * 14);
            }
            ServerInstanceSelect_SystemColorsChanged(this, new EventArgs());
        }

        private void cklbTSServerInstance_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (e.NewValue == CheckState.Checked)
            {
                if (lastCheckedIndex >= 0 && e.Index != lastCheckedIndex)
                {
                    cklbTSServerInstance.ItemCheck -= new ItemCheckEventHandler(cklbTSServerInstance_ItemCheck);
                    cklbTSServerInstance.SetItemCheckState(lastCheckedIndex, CheckState.Unchecked);
                    cklbTSServerInstance.ItemCheck += new ItemCheckEventHandler(cklbTSServerInstance_ItemCheck);
                }
                lastCheckedIndex = e.Index;
            }
            else if (e.NewValue == CheckState.Unchecked)
            {
                if (e.Index == defaultIndex)
                {
                    e.NewValue = CheckState.Checked;
                }
                else
                {
                    cklbTSServerInstance.ItemCheck -= new ItemCheckEventHandler(cklbTSServerInstance_ItemCheck);
                    cklbTSServerInstance.SetItemCheckState(defaultIndex, CheckState.Checked);
                    cklbTSServerInstance.ItemCheck += new ItemCheckEventHandler(cklbTSServerInstance_ItemCheck);
                    lastCheckedIndex = defaultIndex;
                }
            }
        }

        public LicenseServerInstance SelectedServerInstance
        {
            get
            {
                for (int i = 0; i < cklbTSServerInstance.Items.Count; i++ )
                {
                    if (cklbTSServerInstance.GetItemChecked(i))
                    {
                        return (LicenseServerInstance)cklbTSServerInstance.Items[i];
                    }
                }
                return LicenseServerInstance.Default;
            }
        }

        private void ServerInstanceSelect_SystemColorsChanged(object sender, EventArgs e)
        {
            btnTSInstanceOK.BackColor = SystemInformation.HighContrast ? Color.SteelBlue : Color.LightSteelBlue;
            btnTSInstanceCancel.BackColor = SystemInformation.HighContrast ? Color.Gray : Color.Gainsboro;
        }
 
    }
}
