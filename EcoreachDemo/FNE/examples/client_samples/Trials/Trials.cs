// <copyright file="Trials.cs" company="Flexera Software LLC">
//     Copyright (c) 2011-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

using System;
using System.IO;
using System.Text;
using FlxDotNetClient;
using DemoUtilities;
using IdentityData;


/****************************************************************************
    Trials.cs

    This example program allows you to:
    1. Process the trial file of your choice.
    2. Acquire licenses from the trial.
*****************************************************************************/


namespace Trials
{
    public static class Trials
    {
        private static readonly string emptyIdentity =
@"License-enabled code requires client identity data,
which you create with pubidutil and printbin -CS.
See the User Guide for more information.";

        private static readonly string defaultFile = "Using default trial license file {0}";

        private static readonly string attemptingAcquire = "Attempting to acquire license for feature '{0}', version '{1}'";
        private static readonly string licenseAcquired = "License acquisition for feature '{0}', version '{1}' successful";
        private static readonly string attemptingReturn = "Attempting to return license for feature '{0}', version '{1}'";
        private static readonly string licenseReturned = "License for feature '{0}', version '{1}' successfully returned";

        private static readonly string surveyFeature = "survey";
        private static readonly string highresFeature = "highres";
        private static readonly string lowresFeature = "lowres";
        private static readonly string downloadFeature = "download";
        private static readonly string uploadFeature = "upload";
        private static readonly string updatesFeature = "updates";
        private static readonly string specialFeature = "special";

        private static readonly string version = "1.0";

        private static string inputFile = @".\trial.bin";
        private static ILicensing licensing;

        private static bool ValidateCommandLineArgs(string[] args)
        {
            bool validCommand = false;
            if (args.Length == 0)
            {
                Util.DisplayInfoMessage(String.Format(defaultFile, inputFile));
                validCommand = true;
            }
            else if (args.Length == 1)
            {
                string argument = args[0].ToLowerInvariant();
                if (argument != "-h" && argument != "-help")
                {
                    validCommand = true;
                    inputFile = args[0];
                }
            }
            return validCommand;
        }

        public static void Main(string[] args)
        {
            if (!ValidateCommandLineArgs(args))
            {
                Usage(Path.GetFileName(Environment.GetCommandLineArgs()[0]));
                return;
            }

            if (IdentityClient.IdentityData == null || IdentityClient.IdentityData.Length == 0)
            {
                Console.WriteLine(emptyIdentity);
                return;
            }

            try
            {
                Util.DisplayInfoMessage(String.Format("Trial data file is {0}", inputFile));

                string strPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + Path.DirectorySeparatorChar;
			    // Initialize ILicensing interface with identity data using the Windows common document 
                // respository as the trusted storage location and the hard-coded string hostid "1234567890".
                using (licensing = LicensingFactory.GetLicensing(IdentityClient.IdentityData, strPath, "1234567890"))
                {
                    Util.DisplayInfoMessage(String.Format("Number of features loaded from trials: {0}", CheckNumberOfFeatures()));

                    // determine if trial has been loaded 
                    DateTime expirationDate;
                    if (licensing.LicenseManager.TrialIsLoaded(inputFile, out expirationDate))
                    {
                        if (expirationDate.CompareTo(DateTime.Now) > 0)
                        {
                            Util.DisplayInfoMessage(String.Format("Trial has already been loaded and will expire on {0}", expirationDate));
                        }
                        else
                        {
                            Util.DisplayInfoMessage("Trial has already been loaded and has expired");
                        }
                    }
                    // process trial into trials license source
                    ProcessTrial(inputFile);
                    Util.DisplayInfoMessage(String.Format("Number of features loaded from trials: {0}", CheckNumberOfFeatures()));

                    // Acquire and return 1 "survey" license
                    AcquireReturn(surveyFeature);

                    // Acquire and return 1 "highres" license
                    AcquireReturn(highresFeature);

                    // Acquire and return 1 "lowres" license
                    AcquireReturn(lowresFeature);

                    // Acquire and return 1 "download" license
                    AcquireReturn(downloadFeature);

                    // Acquire and return 1 "upload" license
                    AcquireReturn(uploadFeature);

                    // Acquire and return 1 "updates" license
                    AcquireReturn(updatesFeature);

                    // Acquire and return 1 "special" license
                    AcquireReturn(specialFeature);
                }
            }
            catch (Exception exc)
            {
                HandleException(exc);
            }
        }

        private static int CheckNumberOfFeatures()
        {
            // obtain the count of features in the trial license collection
            IFeatureCollection featureCollection = licensing.LicenseManager.GetFeatureCollection(LicenseSourceOption.Trials);
            return featureCollection.Count;
        }

        private static bool ProcessTrial(string trialFile)
        {
            Util.DisplayInfoMessage("Processing trial licensing data");
            try
            {
                licensing.LicenseManager.AddTrialLicenseSource(trialFile);
            }
            catch (PublicLicensingException licensingException)
            {
                switch (licensingException.ErrorCode)
                {
                    case ErrorCode.FLXERR_TRIAL_ALREADY_LOADED:
                    case ErrorCode.FLXERR_TRIAL_EXPIRED:
                        HandleException(licensingException);
                        return false;
                    default:
                        throw licensingException;
                }

            }
            return true;
        }

        private static void AcquireReturn(string requestedFeature)
        {
            string currentFeature = requestedFeature;
            string currentVersion = version;
            Util.DisplayInfoMessage(String.Empty);
            Util.DisplayInfoMessage(String.Format(attemptingAcquire, requestedFeature, version));
            try
            {
                // attempt to obtain version 1.0 (or greater) of the requested feature
                ILicense acquiredLicense = licensing.LicenseManager.Acquire(requestedFeature, version);
                try
                {
                    currentFeature = acquiredLicense.Name;
                    currentVersion = acquiredLicense.Version;
                    Util.DisplayInfoMessage(String.Format(licenseAcquired, currentFeature, currentVersion));
                    //// application logic here
                }
                finally
                {
                    Util.DisplayInfoMessage(String.Format(attemptingReturn, currentFeature, currentVersion));
                    // return license. note: it is also possible to use a "using" statement to implicitly return the license
                    acquiredLicense.ReturnLicense();
                    Util.DisplayInfoMessage(String.Format(licenseReturned, currentFeature, currentVersion));
                }
            }
            catch (Exception exc)
            {
                HandleException(exc);
            }
        }

        private static void HandleException(Exception exc)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(String.Format("{0} encountered: ", exc.GetType()));
            FlxException flxException = exc as FlxException;
            if (flxException != null)
            {
                builder.Append(flxException.ToString());
            }
            else
            {
                builder.Append(exc.Message);
            }
            Util.DisplayErrorMessage(builder.ToString());
        }

        private static void Usage(string applicationName)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(String.Empty);
            builder.AppendLine(String.Format("{0} [binary_trial_file]", applicationName));
            builder.AppendLine("Activates a limited-duration trial based on a binary trial file.");
            builder.AppendLine(String.Empty);
            builder.AppendLine(String.Format("If unset, default binary_trial_file is {0}.", inputFile));

            Util.DisplayMessage(builder.ToString(), "USAGE");
        }
    }
}