/****************************************************************************
  Copyright (c) 2008-2017 Flexera Software LLC.
  All Rights Reserved.
  This software has been provided pursuant to a License Agreement
  containing restrictions on its use.  This software contains
  valuable trade secrets and proprietary information of
  Flexera Software LLC and is protected by law.
  It may not be copied or distributed in any form or medium, disclosed
  to third parties, reverse engineered or used in any manner not
  provided for in said License Agreement except with the prior
  written authorization from Flexera Software LLC.
*****************************************************************************/
using System.Windows.Forms;

namespace Calc
{
	static class License
	{
		private static readonly int expected_licensed_value = 42;

		public static void ValidateCommandLineArgs(string[] args)
		{
		}

		//Returns 42
		public static int Get_expected_licensed_value()
		{
			return expected_licensed_value;
		}

		//Returns 42 if licensed
		public static int Acquire()
		{
			int licensed = (int)DialogResult.Yes;
			int expected = Get_expected_licensed_value();
			int dialogResult = ((int)MessageBox.Show(
				"Are we Licensed?",
				"Spoof License Check Code",
				MessageBoxButtons.YesNo));
			licensed -= dialogResult;
			licensed += expected;

			return licensed;
		}
	}
}
