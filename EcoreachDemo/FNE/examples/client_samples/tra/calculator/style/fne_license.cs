/****************************************************************************
  Copyright (c) 2008-2017 Flexera Software LLC.
  All Rights Reserved.
  This software has been provided pursuant to a License Agreement
  containing restrictions on its use.  This software contains
  valuable trade secrets and proprietary information of
  Flexera Software LLC and is protected by law.
  It may not be copied or distributed in any form or medium, disclosed
  to third parties, reverse engineered or used in any manner not
  provided for in said License Agreement except with the prior
  written authorization from Flexera Software LLC.
*****************************************************************************/
using System;
using System.IO;
using System.Text;
using DemoUtilities;
using FlxDotNetClient;
using IdentityData;

namespace Calc
{
	static class License
	{
		private static readonly int expected_licensed_value = 42;

		private static string inputFile = @".\license.bin";
		private static readonly string emptyIdentity =
@"License-enabled code requires client identity data,
which you create with pubidutil and printbin -CS.
See the User Guide for more information.";
		private static readonly string highresFeature = "highres";
		private static readonly string version = "1.0";
		private static readonly string hostId = "1234567890";

		private static ILicensing licensing = null;

		public static void ValidateCommandLineArgs(string[] args)
		{
			bool validCommand = false;
			if (args.Length == 0)
			{
				Util.DisplayInfoMessage(String.Format("Using default license file {0}", inputFile));
				validCommand = true;
			}
			else
			{
				string argument = args[0].ToLowerInvariant();
				if (argument != "-h" && argument != "-help")
				{
					validCommand = true;
					inputFile = args[0];
				}
			}
			if (!validCommand)
			{
				Usage(Path.GetFileName(Environment.GetCommandLineArgs()[0]));
				throw new ArgumentException("invalid argument", "args");
			}
		}

		//Returns 42
		public static int Get_expected_licensed_value()
		{
			return expected_licensed_value;
		}

		//Returns 42 if licensed
		public static int Acquire()
		{
			int licensed = 0;
			if (IdentityClient.IdentityData == null || IdentityClient.IdentityData.Length == 0)
			{
				Console.WriteLine(emptyIdentity);
				throw new ArgumentException(emptyIdentity, "IdentityClient.IdentityData");
			}
			try
			{
				byte[] buffer = Util.ReadData(inputFile);
				if (buffer != null)
				{
					// Initialize ILicensing interface with identity data using memory-based trusted
					// storage and the hard-coded string hostid "1234567890".
					using (licensing = LicensingFactory.GetLicensing(
							  IdentityClient.IdentityData,
							  null,
							  hostId))
					{
						// add a Buffer license source
						licensing.LicenseManager.AddBufferLicenseSource(buffer, "BasicClientLicenseSource");

						// Acquire and return 1 "highres" license
						return AcquireReturn(highresFeature, version);
					}
				}
				else
				{
					// issue encountered accessing input file
				}
			}
			catch (DllNotFoundException)
			{
				// Not a typical error. Make sure the native FlxCore library is available
				Util.DisplayErrorMessage(String.Format("Failed to load FlxCore{0}.dll. Check the PATH specification.",
					IntPtr.Size == 4 ? String.Empty : "64"));
			}
			return licensed;
		}

		private static void Usage(string applicationName)
		{
			StringBuilder builder = new StringBuilder();
			builder.AppendLine(String.Empty);
			builder.AppendLine(String.Format("{0} [binary_license_file]", applicationName));
			builder.AppendLine("Attempts to acquire 'highres' features from a signed");
			builder.AppendLine("binary license file.");
			builder.AppendLine(String.Empty);
			builder.AppendLine(String.Format("If unset, default binary_license_file is {0}.", inputFile));

			Util.DisplayMessage(builder.ToString(), "USAGE");
		}

		private static int AcquireReturn(string requestedFeature, string requestedVersion)
		{
			int licensed = 0;
			try
			{
				// acquire the license
				ILicense acquiredLicense = licensing.LicenseManager.Acquire(requestedFeature, requestedVersion);
				try
				{
					Util.DisplayInfoMessage(String.Format("License acquisition for feature '{0}' version '{1}' successful", requestedFeature, acquiredLicense.Version));
					licensed += expected_licensed_value;
				}
				finally
				{
					// return license. note: it is also possible to use a "using" statement to implicitly return the license
					acquiredLicense.ReturnLicense();
				}
			}
			catch (Exception exc)
			{
				HandleException(exc);
			}
			return licensed;
		}

		private static void HandleException(Exception exc)
		{
			StringBuilder builder = new StringBuilder();
			builder.AppendLine(String.Format("{0} encountered: ", exc.GetType()));
			FlxException flxException = exc as FlxException;
			if (flxException != null)
			{
				builder.Append(flxException.ToString());
			}
			else
			{
				builder.Append(exc.Message);
			}
			Util.DisplayErrorMessage(builder.ToString());
		}
	}
}
