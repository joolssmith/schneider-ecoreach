/****************************************************************************
  Copyright (c) 2008-2017 Flexera Software LLC.
  All Rights Reserved.
  This software has been provided pursuant to a License Agreement
  containing restrictions on its use.  This software contains
  valuable trade secrets and proprietary information of
  Flexera Software LLC and is protected by law.
  It may not be copied or distributed in any form or medium, disclosed
  to third parties, reverse engineered or used in any manner not
  provided for in said License Agreement except with the prior
  written authorization from Flexera Software LLC.
*****************************************************************************/
using System;
using System.IO;
using System.Text;
using DemoUtilities;
using FlxDotNetClient;
using IdentityData;

using Com.Flexnet.Tra;

namespace Calc
{
	static class License
	{
		private static ILicensing licensing = null;

		public static void ValidateCommandLineArgs(calc_tra tra, string[] args)
		{
			bool validCommand = false;
			if (tra.get_value(calc_tra.TRA_VARIABLE_test_value_ALIAS_1) != 1)
			{
				System.Windows.Forms.MessageBox.Show("Tamper Detected. Do you have a debugger attached?", "Calculator");
				while (true)
				{
					Environment.Exit(1);
				}
			}
			if (args.Length == 0)
			{
				Util.DisplayInfoMessage(String.Format(
					"Using default license file {0}",
					tra.get_string(calc_tra.TRA_STRING_INPUT_FILE_ALIAS_1)));
				validCommand = true;
			}
			else
			{
				string argument = args[0].ToLowerInvariant();
				if (argument != "-h" && argument != "-help")
				{
					validCommand = true;
					tra.set_string(calc_tra.TRA_STRING_INPUT_FILE_ALIAS_2,args[0]);
				}
			}
			if (!validCommand)
			{
				Usage(tra,Path.GetFileName(Environment.GetCommandLineArgs()[0]));
				throw new ArgumentException("invalid argument", "args");
			}
		}

		//Returns 42 if no tamper - else 1
		public static int Get_expected_licensed_value(calc_tra tra)
		{
			return 1 - tra.get_value(calc_tra.TRA_VARIABLE_skewed_licensed_value_ALIAS_1);
		}

		//Returns 42 if licensed and no tamper
		public static calc_tdt_TDT Acquire(calc_tra tra)
		{
			calc_tdt_TDT licensed = new calc_tdt_TDT(tra, calc_tra.TRA_VARIABLE_ZERO_ALIAS_1);
			if (IdentityClient.IdentityData == null || IdentityClient.IdentityData.Length == 0)
			{
				Console.WriteLine(tra.get_string(calc_tra.TRA_STRING_EMPTY_IDENTITY_ALIAS_1));
				throw new ArgumentException(
					tra.get_string(calc_tra.TRA_STRING_EMPTY_IDENTITY_ALIAS_1),
					tra.get_string(calc_tra.TRA_STRING_IdentityClient_IdentityData_ALIAS_1));
			}
			try
			{
				byte[] buffer = Util.ReadData(
					tra.get_string(calc_tra.TRA_STRING_INPUT_FILE_ALIAS_3));
				if (buffer != null)
				{
					// Initialize ILicensing interface with identity data using memory-based trusted
					// storage and the hard-coded string hostid "1234567890".
					using (licensing = LicensingFactory.GetLicensing(
							IdentityClient.IdentityData,
							null,
							tra.get_string(calc_tra.TRA_STRING_HOST_ID_ALIAS_1)))
					{
						// add a Buffer license source
						licensing.LicenseManager.AddBufferLicenseSource(
							buffer,
							tra.get_string(calc_tra.TRA_STRING_SOURCE_NAME_ALIAS_1));

						// Acquire and return 1 "highres" license
						return AcquireReturn(
							tra,
							tra.get_string(calc_tra.TRA_STRING_HIGHRES_FEATURE_ALIAS_1),
							tra.get_string(calc_tra.TRA_STRING_VERSION_ALIAS_1));
					}
				}
				else
				{
					// issue encountered accessing input file
				}
			}
			catch (DllNotFoundException)
			{
				// Not a typical error. Make sure the native FlxCore library is available
				Util.DisplayErrorMessage(String.Format(
					tra.get_string(calc_tra.TRA_STRING_FAILED_TO_LOAD_FLXCORE_FORMAT_ALIAS_1),
					IntPtr.Size == 4 ? String.Empty : tra.get_string(calc_tra.TRA_STRING_SIXTY_FOUR_ALIAS_1)));
			}
			return licensed;
		}

		private static void Usage(calc_tra tra,string applicationName)
		{
			StringBuilder builder = new StringBuilder();
			builder.AppendLine(String.Empty);
			builder.AppendLine(String.Format("{0} [binary_license_file]", applicationName));
			builder.AppendLine("Attempts to acquire 'highres' features from a signed");
			builder.AppendLine("binary license file.");
			builder.AppendLine(String.Empty);
			builder.AppendLine(String.Format("If unset, default binary_license_file is {0}.",
				tra.get_string(calc_tra.TRA_STRING_INPUT_FILE_ALIAS_4)));

			Util.DisplayMessage(builder.ToString(), "USAGE");
		}

		private static calc_tdt_TDT AcquireReturn(
			calc_tra tra,
			string requestedFeature,
			string requestedVersion)
		{
			calc_tdt_TDT licensed = new calc_tdt_TDT(tra, calc_tra.TRA_VARIABLE_ZERO_ALIAS_2);
			try
			{
				// acquire the license
				ILicense acquiredLicense = licensing.LicenseManager.Acquire(requestedFeature, requestedVersion);
				try
				{
					calc_tdt_TDT expected = new calc_tdt_TDT(
						tra,
						calc_tra.TRA_VARIABLE_expected_licensed_value_ALIAS_3);
					Util.DisplayInfoMessage(String.Format(
						tra.get_string(calc_tra.TRA_STRING_ACQUISITION_SUCCESSFUL_FORMAT_ALIAS_1),
						requestedFeature,
						acquiredLicense.Version));
					licensed += expected;
				}
				finally
				{
					// return license. note: it is also possible to use a "using" statement to implicitly return the license
					acquiredLicense.ReturnLicense();
				}
			}
			catch (Exception exc)
			{
				HandleException(exc);
			}
			return licensed;
		}

		private static void HandleException(Exception exc)
		{
			StringBuilder builder = new StringBuilder();
			builder.AppendLine(String.Format("{0} encountered: ", exc.GetType()));
			FlxException flxException = exc as FlxException;
			if (flxException != null)
			{
				builder.Append(flxException.ToString());
			}
			else
			{
				builder.Append(exc.Message);
			}
			Util.DisplayErrorMessage(builder.ToString());
		}
	}
}
