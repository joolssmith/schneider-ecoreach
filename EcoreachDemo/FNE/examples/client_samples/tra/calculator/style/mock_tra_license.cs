/****************************************************************************
  Copyright (c) 2008-2017 Flexera Software LLC.
  All Rights Reserved.
  This software has been provided pursuant to a License Agreement
  containing restrictions on its use.  This software contains
  valuable trade secrets and proprietary information of
  Flexera Software LLC and is protected by law.
  It may not be copied or distributed in any form or medium, disclosed
  to third parties, reverse engineered or used in any manner not
  provided for in said License Agreement except with the prior
  written authorization from Flexera Software LLC.
*****************************************************************************/
using System.Windows.Forms;
using Com.Flexnet.Tra;

namespace Calc
{
	static class License
	{
		public static void ValidateCommandLineArgs(calc_tra tra, string[] args)
		{
		}

		//Returns 42 if no tamper - else 1
		public static int Get_expected_licensed_value(calc_tra tra)
		{
			return 1 - tra.get_value(calc_tra.TRA_VARIABLE_skewed_licensed_value_ALIAS_1);
		}

		//Returns 42 if licensed and no tamper
		public static calc_tdt_TDT Acquire(calc_tra tra)
		{
			calc_tdt_TDT licensed = new calc_tdt_TDT(
				tra,
				calc_tra.TRA_VARIABLE_DialogResult_Yes_ALIAS_1);
			calc_tdt_TDT expected = new calc_tdt_TDT(
				tra,
				calc_tra.TRA_VARIABLE_expected_licensed_value_ALIAS_3);
			calc_tdt_TDT dialogResult = new calc_tdt_TDT(tra);
			dialogResult.call((int)MessageBox.Show(
				tra.get_string(calc_tra.TRA_STRING_Are_we_Licensed_ALIAS_1),
				tra.get_string(calc_tra.TRA_STRING_Spoof_License_Check_Code_ALIAS_1),
				MessageBoxButtons.YesNo));
			licensed -= dialogResult;
			licensed += expected;

			return licensed;
		}
	}
}
