﻿/****************************************************************************
  Copyright (c) 2008-2017 Flexera Software LLC.
  All Rights Reserved.
  This software has been provided pursuant to a License Agreement
  containing restrictions on its use.  This software contains
  valuable trade secrets and proprietary information of
  Flexera Software LLC and is protected by law.
  It may not be copied or distributed in any form or medium, disclosed
  to third parties, reverse engineered or used in any manner not
  provided for in said License Agreement except with the prior
  written authorization from Flexera Software LLC.
*****************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Com.Flexnet.Tra;
using System.Diagnostics;

namespace Calc
{
	public partial class Calculator : Form
	{
		bool bool_state = false;
		double text_value = 0;
		private calc_tra tra;
		public calc_tra Tra { get { return tra; } }
		private calc_tdt_TDT licensed;
		public calc_tdt_TDT zero;
		public calc_tdt_TDT one;
		private calc_tdt_TDT operatorPlus;
		private calc_tdt_TDT operatorMinus;
		private calc_tdt_TDT operatorMultiply;
		private calc_tdt_TDT operatorDivide;

		public Calculator(string[] args)
		{
			tra = new calc_tra();
			if (tra != null)
			{
				operatorPlus = new calc_tdt_TDT(tra, calc_tra.TRA_VARIABLE_PLUS_ALIAS_1);
				operatorMinus = new calc_tdt_TDT(tra, calc_tra.TRA_VARIABLE_MINUS_ALIAS_1);
				operatorMultiply = new calc_tdt_TDT(tra, calc_tra.TRA_VARIABLE_MULTIPLY_ALIAS_1);
				operatorDivide = new calc_tdt_TDT(tra, calc_tra.TRA_VARIABLE_DIVIDE_ALIAS_1);
				zero = new calc_tdt_TDT(tra, calc_tra.TRA_VARIABLE_ZERO_ALIAS_1);
				one = new calc_tdt_TDT(tra, calc_tra.TRA_VARIABLE_test_value_ALIAS_1);
				License.ValidateCommandLineArgs(tra, args);
			}
			else
			{
				throw new InvalidOperationException("Failed to initialize");
			}
			InitializeComponent();
		}

		private void Calculator_Load(object sender, EventArgs e)
		{
		}

		private void Calculator_Show(object sender, EventArgs e)
		{
			licensed = License.Acquire(tra);
		}

		private void Number_clicked(object sender, EventArgs e)
		{
			if (Display.Text == "0" || bool_state) {
				Display.Clear();
			}
			bool_state = false;
			Button bu = (Button)sender;
			Display.Text = Display.Text + bu.Text;
		}

		private void Operator_click(object sender, EventArgs e)
		{
			Button bu = (Button)sender;
			tra.set_string(calc_tra.TRA_STRING_operator_value_ALIAS_3, bu.Text);
			text_value = double.Parse(Display.Text);
			bool_state = true;
			Display.Text = text_value + "" + tra.get_string(calc_tra.TRA_STRING_operator_value_ALIAS_1);

		}

		private void Equal_click(object sender, EventArgs e)
		{
			if (zero != one )
			{
				calc_tdt_TDT operatorValue = new calc_tdt_TDT(tra);
				String operatorString = tra.get_string(calc_tra.TRA_STRING_operator_value_ALIAS_2);
				operatorValue.call(Convert.ToInt32(operatorString[0]));
				if (operatorValue == operatorPlus) {
//					tra.call (calc_tra.TRA_FUNCTION_TEST_ALIAS_1,this);
					Display.Text = (text_value + double.Parse(Display.Text)).ToString();
				}
				else if (operatorValue == operatorMinus) {
					Display.Text = (text_value - double.Parse(Display.Text)).ToString();
				}
				else if (operatorValue == operatorMultiply) {
					Display.Text = (text_value * double.Parse(Display.Text)).ToString();
				}
				else if (operatorValue == operatorDivide) {
					tra.iff ( calc_tra.TRA_SNIF_IS_LICENSED_ALIAS_1, this );
				}
				else
				{
					int val1 = operatorValue.call();
					int val2 = operatorDivide.call();
					Debug.Assert( false, "Unexpected value" );
				}
			}
		}

		//Returns 42 if licensed
		private calc_tdt_TDT IsLicensed()
		{
			return licensed;
		}

		//Expect 0 if licensed and not tampered
		public static int test_licensed (params object[] in_p) {
			try{
				Calculator instance = (Calculator)in_p[0];
				return License.Get_expected_licensed_value(instance.tra) - instance.IsLicensed().call();
			} catch (InvalidCastException) {
				Debug.WriteLine("No conversion to a Calculator exists for the value.");
			}
			return -1;
		}

		public static int no_license (params object[] in_p) {
			Calculator instance = (Calculator)in_p[0];
			instance.Display.Text = instance.tra.get_string(calc_tra.TRA_STRING_NO_DIV_LIC_ALIAS_1);
			System.Threading.Thread.Sleep(1000);
			return 0;
		}

		public static int on_licensed(params object[] in_p) {
			Calculator instance = (Calculator)in_p[0];
			if (instance.zero != instance.one )
			{
				instance.Display.Text = (instance.text_value / double.Parse(instance.Display.Text)).ToString();
			}
			return 0;
		}

	}
}
