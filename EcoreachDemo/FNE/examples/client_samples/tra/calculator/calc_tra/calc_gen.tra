[[--
****************************************************************************
  Copyright (c) 2008-2017 Flexera Software LLC.
  All Rights Reserved.
  This software has been provided pursuant to a License Agreement
  containing restrictions on its use.  This software contains
  valuable trade secrets and proprietary information of
  Flexera Software LLC and is protected by law.
  It may not be copied or distributed in any form or medium, disclosed
  to third parties, reverse engineered or used in any manner not
  provided for in said License Agreement except with the prior
  written authorization from Flexera Software LLC.
*****************************************************************************
--]]
--------------------------------------calc_gen.tra----------------------------------------
--The purpose of calc_gen.tra is to generate the TRA CS code in the pre-build phase.
--It is also used in the post_build phase when the injected TRA code is added to the
--assembly prior to signing the application.
------------------------------------------------------------------------------------------
local code = assert( get_byte_array() )
assert( load_byte_array( code, "tra-gen" ) )

declare.strings.PLUS = { "+" }
declare.strings.MINUS = { "-" }
declare.strings.MULTIPLY = { "*" }
declare.strings.DIVIDE = { "/" }
declare.strings.EQUALS = { "=" }
declare.strings.operator_value = { "", ["aliases"] = 3 }
declare.strings.HOME_CALL = { "NOT CALLED" }
declare.strings.NO_DIV_LIC = { "No div Lic" }
--Used in FNE
declare.strings.INPUT_FILE = { ".\\license.bin", ["aliases"] = 4 }
declare.strings.HOST_ID = { "1234567890" }
declare.strings.VERSION = { "1.0" }
declare.strings.HIGHRES_FEATURE = { "highres" }
declare.strings.SOURCE_NAME = { "CalcLicenseSource" }
declare.strings.EMPTY_IDENTITY = { [["License-enabled code requires client identity data,
which you create with pubidutil and printbin -CS.
See the User Guide for more information."]] }
declare.strings.IdentityClient_IdentityData = { "IdentityClient.IdentityData" }
declare.strings.FAILED_TO_LOAD_FLXCORE_FORMAT = { "Failed to load FlxCore{0}.dll. Check the PATH specification." }
declare.strings.SIXTY_FOUR = { "64" }
declare.strings.ACQUISITION_SUCCESSFUL_FORMAT = { "License acquisition for feature '{0}' version '{1}' successful" }
---Used in mock
declare.strings.Are_we_Licensed = { "Are we Licensed?" }
declare.strings.Spoof_License_Check_Code = { "Spoof License Check Code" }
--
declare.variables.test_value = { 1 }
declare.variables.DialogResult_Yes = { 6 } --Hard code enum DialogResult value
declare.variables.ZERO = { 0, ["aliases"] = 2 }
declare.variables.expected_licensed_value = { 42, ["aliases"] = 3 }
declare.variables.skewed_licensed_value = { -41 }

declare.variables.MULTIPLY = { 42 }
declare.variables.PLUS = { 43 }
declare.variables.MINUS = { 45 }
declare.variables.DIVIDE = { 47 }

--declare.tra_functions.TEST = { "do_test"}

--options.callhome = "l_obf_callhome"

declare.snif.IS_LICENSED = { ["predicate"] = "test_licensed",["on_false"] = "no_license",["on_true"] = "on_licensed", ["actions"] = {
["checksigned"] = { "FlxLicensingClient", "FlxClientCommon" },
--["checksum"] = "FlxLicensingClient",
} }

--options.script = "script"

options.register = {
	[ "test_licensed" ] = "Calc.Calculator.test_licensed",
	[ "no_license" ] = "Calc.Calculator.no_license",
	[ "on_licensed" ] = "Calc.Calculator.on_licensed" ,
}
options.verify_modules = {
	[ "csharp" ] = {
		[ "FlxLicensingClient" ] = {
			[ "lazy" ] = true,
			[ "checksum" ] = true,
			[ "checksigned" ] = "flex",
		},
		[ "FlxClientCommon" ] = {
			[ "lazy" ] = true,
			[ "checksum" ] = true,
			[ "checksigned" ] = "flex",
		},
	}
}
options.TDT = { ["typename"] = {"TDT"} }

options.certificate = {
	["cs_test"] = {
		["strongnamepublickeytoken"] = "92b66d145d70df52",
--[[
		[ "signer" ] = "My SPC",
		[ "issuer" ] = "My CA",
		[ "serialnumber" ] = "0f 11 73 c7 5b 61 50 8d 40 9f aa 94 9b af 80 db ",
--]]
		[ "default" ] = true
	},
	["flex"] = {
		["strongnamepublickeytoken"] = "bd2a668b6e12bf71",
		[ "signer" ] = "Flexera Software LLC",
		[ "issuer" ] = "Symantec Class 3 SHA256 Code Signing CA",
		[ "serialnumber" ] = "45 8a 21 ba ab 49 ca 09 52 dc da 5b 6c cd e2 2a ",
	},
}
--
options.name = "calc"
--
options.header = "calc with tra c# example (c) FlexeraSoftware"
--
options.debug = {}
--options.debug.trace = true
--options.debug.stats = true
--options.debug.allow_failure = false
--options.debug.allow_debugger = true
--options.debug.vshost = true
----options.debug.visual_studio = false

return tra_generate( options, declare )
