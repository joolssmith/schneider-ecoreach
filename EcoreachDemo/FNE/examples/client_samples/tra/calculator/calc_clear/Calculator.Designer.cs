/****************************************************************************
  Copyright (c) 2008-2017 Flexera Software LLC.
  All Rights Reserved.
  This software has been provided pursuant to a License Agreement
  containing restrictions on its use.  This software contains
  valuable trade secrets and proprietary information of
  Flexera Software LLC and is protected by law.
  It may not be copied or distributed in any form or medium, disclosed
  to third parties, reverse engineered or used in any manner not
  provided for in said License Agreement except with the prior
  written authorization from Flexera Software LLC.
*****************************************************************************/
namespace Calc
{
	partial class Calculator
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.button5 = new System.Windows.Forms.Button();
			this.button6 = new System.Windows.Forms.Button();
			this.button7 = new System.Windows.Forms.Button();
			this.button8 = new System.Windows.Forms.Button();
			this.button9 = new System.Windows.Forms.Button();
			this.button_divide = new System.Windows.Forms.Button();
			this.button_multiply = new System.Windows.Forms.Button();
			this.button_equals = new System.Windows.Forms.Button();
			this.button_plus = new System.Windows.Forms.Button();
			this.button0 = new System.Windows.Forms.Button();
			this.button_minus = new System.Windows.Forms.Button();
			this.Display = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			//
			// button1
			//
			this.button1.Location = new System.Drawing.Point(12, 107);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(39, 33);
			this.button1.TabIndex = 2;
			this.button1.Text = "1";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.Number_clicked);
			//
			// button2
			//
			this.button2.Location = new System.Drawing.Point(69, 107);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(39, 33);
			this.button2.TabIndex = 3;
			this.button2.Text = "2";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.Number_clicked);
			//
			// button4
			//
			this.button4.Location = new System.Drawing.Point(12, 161);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(39, 33);
			this.button4.TabIndex = 5;
			this.button4.Text = "4";
			this.button4.UseVisualStyleBackColor = true;
			this.button4.Click += new System.EventHandler(this.Number_clicked);
			//
			// button3
			//
			this.button3.Location = new System.Drawing.Point(122, 107);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(39, 33);
			this.button3.TabIndex = 4;
			this.button3.Text = "3";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.Number_clicked);
			//
			// button5
			//
			this.button5.Location = new System.Drawing.Point(69, 161);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(39, 33);
			this.button5.TabIndex = 6;
			this.button5.Text = "5";
			this.button5.UseVisualStyleBackColor = true;
			this.button5.Click += new System.EventHandler(this.Number_clicked);
			//
			// button6
			//
			this.button6.Location = new System.Drawing.Point(122, 161);
			this.button6.Name = "button6";
			this.button6.Size = new System.Drawing.Size(39, 33);
			this.button6.TabIndex = 7;
			this.button6.Text = "6";
			this.button6.UseVisualStyleBackColor = true;
			this.button6.Click += new System.EventHandler(this.Number_clicked);
			//
			// button7
			//
			this.button7.Location = new System.Drawing.Point(12, 213);
			this.button7.Name = "button7";
			this.button7.Size = new System.Drawing.Size(39, 33);
			this.button7.TabIndex = 8;
			this.button7.Text = "7";
			this.button7.UseVisualStyleBackColor = true;
			this.button7.Click += new System.EventHandler(this.Number_clicked);
			//
			// button8
			//
			this.button8.Location = new System.Drawing.Point(69, 213);
			this.button8.Name = "button8";
			this.button8.Size = new System.Drawing.Size(39, 33);
			this.button8.TabIndex = 9;
			this.button8.Text = "8";
			this.button8.UseVisualStyleBackColor = true;
			this.button8.Click += new System.EventHandler(this.Number_clicked);
			//
			// button9
			//
			this.button9.Location = new System.Drawing.Point(122, 213);
			this.button9.Name = "button9";
			this.button9.Size = new System.Drawing.Size(39, 33);
			this.button9.TabIndex = 10;
			this.button9.Text = "9";
			this.button9.UseVisualStyleBackColor = true;
			this.button9.Click += new System.EventHandler(this.Number_clicked);
			//
			// button_divide
			//
			this.button_divide.Location = new System.Drawing.Point(179, 107);
			this.button_divide.Name = "button_divide";
			this.button_divide.Size = new System.Drawing.Size(39, 33);
			this.button_divide.TabIndex = 11;
			this.button_divide.Text = "/";
			this.button_divide.UseVisualStyleBackColor = true;
			this.button_divide.Click += new System.EventHandler(this.Operator_click);
			//
			// button_multiply
			//
			this.button_multiply.Location = new System.Drawing.Point(179, 159);
			this.button_multiply.Name = "button_multiply";
			this.button_multiply.Size = new System.Drawing.Size(39, 33);
			this.button_multiply.TabIndex = 11;
			this.button_multiply.Text = "*";
			this.button_multiply.UseVisualStyleBackColor = true;
			this.button_multiply.Click += new System.EventHandler(this.Operator_click);
			//
			// button_equals
			//
			this.button_equals.Location = new System.Drawing.Point(179, 213);
			this.button_equals.Name = "button_equals";
			this.button_equals.Size = new System.Drawing.Size(39, 88);
			this.button_equals.TabIndex = 1;
			this.button_equals.Text = "=";
			this.button_equals.UseVisualStyleBackColor = true;
			this.button_equals.Click += new System.EventHandler(this.Equal_click);
			//
			// button_plus
			//
			this.button_plus.Location = new System.Drawing.Point(122, 268);
			this.button_plus.Name = "button_plus";
			this.button_plus.Size = new System.Drawing.Size(39, 33);
			this.button_plus.TabIndex = 14;
			this.button_plus.Text = "+";
			this.button_plus.UseVisualStyleBackColor = true;
			this.button_plus.Click += new System.EventHandler(this.Operator_click);
			//
			// button0
			//
			this.button0.Location = new System.Drawing.Point(69, 268);
			this.button0.Name = "button0";
			this.button0.Size = new System.Drawing.Size(39, 33);
			this.button0.TabIndex = 12;
			this.button0.Text = "0";
			this.button0.UseVisualStyleBackColor = true;
			this.button0.Click += new System.EventHandler(this.Number_clicked);
			//
			// button_minus
			//
			this.button_minus.Location = new System.Drawing.Point(12, 268);
			this.button_minus.Name = "button_minus";
			this.button_minus.Size = new System.Drawing.Size(39, 33);
			this.button_minus.TabIndex = 12;
			this.button_minus.Text = "-";
			this.button_minus.UseVisualStyleBackColor = true;
			this.button_minus.Click += new System.EventHandler(this.Operator_click);
			//
			// Display
			//
			this.Display.Font = new System.Drawing.Font("Microsoft Sans Serif", 32F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Display.Location = new System.Drawing.Point(13, 42);
			this.Display.Name = "Display";
			this.Display.Size = new System.Drawing.Size(205, 56);
			this.Display.TabIndex = 0;
			//
			// Calculator
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(241, 320);
			this.Controls.Add(this.Display);
			this.Controls.Add(this.button_equals);
			this.Controls.Add(this.button_plus);
			this.Controls.Add(this.button0);
			this.Controls.Add(this.button_minus);
			this.Controls.Add(this.button_multiply);
			this.Controls.Add(this.button_divide);
			this.Controls.Add(this.button9);
			this.Controls.Add(this.button8);
			this.Controls.Add(this.button7);
			this.Controls.Add(this.button6);
			this.Controls.Add(this.button5);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.button4);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Name = "Calculator";
			this.Text = "Calculator";
			this.Load += new System.EventHandler(this.Calculator_Load);
			this.Shown += new System.EventHandler(this.Calculator_Show);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.Button button6;
		private System.Windows.Forms.Button button7;
		private System.Windows.Forms.Button button8;
		private System.Windows.Forms.Button button9;
		private System.Windows.Forms.Button button_divide;
		private System.Windows.Forms.Button button_multiply;
		private System.Windows.Forms.Button button_equals;
		private System.Windows.Forms.Button button_plus;
		private System.Windows.Forms.Button button0;
		private System.Windows.Forms.Button button_minus;
		private System.Windows.Forms.TextBox Display;
	}
}

