﻿/****************************************************************************
  Copyright (c) 2008-2017 Flexera Software LLC.
  All Rights Reserved.
  This software has been provided pursuant to a License Agreement
  containing restrictions on its use.  This software contains
  valuable trade secrets and proprietary information of
  Flexera Software LLC and is protected by law.
  It may not be copied or distributed in any form or medium, disclosed
  to third parties, reverse engineered or used in any manner not
  provided for in said License Agreement except with the prior
  written authorization from Flexera Software LLC.
*****************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Calc
{
	public partial class Calculator : Form
	{
		bool bool_state = false;
		string operator_value = "";
		double text_value = 0;
		public int licensed = 0;

		public Calculator(string[] args)
		{
			License.ValidateCommandLineArgs(args);
			InitializeComponent();
		}

		private void Calculator_Load(object sender, EventArgs e)
		{
		}

		private void Calculator_Show(object sender, EventArgs e)
		{
			licensed = License.Acquire();
		}

		private void Number_clicked(object sender, EventArgs e)
		{
			if (Display.Text == "0" || bool_state) {
				Display.Clear();
			}
			bool_state = false;
			Button bu = (Button)sender;
			Display.Text = Display.Text + bu.Text;
		}

		private void Operator_click(object sender, EventArgs e)
		{
			Button bu = (Button)sender;
			operator_value = bu.Text;
			text_value = double.Parse(Display.Text);
			bool_state = true;
			Display.Text = text_value + "" + operator_value;

		}

		private void Equal_click(object sender, EventArgs e)
		{
			switch (operator_value)
			{
				case "+":
					Display.Text = (text_value + double.Parse(Display.Text)).ToString();
					break;

				case "-":
					Display.Text = (text_value - double.Parse(Display.Text)).ToString();
					break;

				case "*":
					Display.Text = (text_value * double.Parse(Display.Text)).ToString();
					break;

				case "/":
					if (IsLicensed() == License.Get_expected_licensed_value())
					{
						Display.Text = (text_value / double.Parse(Display.Text)).ToString();
					}
					else
					{
						Display.Text = "No div Lic";
					}
					break;
			}
		}

		//Returns 42 if licensed
		private int IsLicensed()
		{
			return licensed;
		}
	}
}
