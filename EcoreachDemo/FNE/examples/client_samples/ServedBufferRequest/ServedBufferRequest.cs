// <copyright file="ServedBufferRequest.cs" company="Flexera Software LLC">
//     Copyright (c) 2013-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using DemoUtilities;
using FlxDotNetClient;
using IdentityData;

/****************************************************************************
    ServedBufferRequest.cs

    This example program allows you to:
    1. Send a capability request with the served-buffer property enabled,
       over http to the server and process the response, optionally saving
       served buffer response data to a file.
    2. Save a served buffer capability request into a file, for offline or
       indirect processing.
    3. Process one or two served buffer responses contained in files
       conveyed offline or indirectly.
    4. Given two served buffer response files report a stale response, if 
       one exists.
*****************************************************************************/

namespace ServedBufferRequest
{
    public static class ServedBufferRequest
    {
        private static readonly string emptyIdentity =
@"License-enabled code requires client identity data,
which you create with pubidutil and printbin -CS.
See the User Guide for more information.";

        private static readonly string processingServedBufferResponse = "Processing served buffer response";
        private static readonly string acquiringLicense = "Acquiring license";
        private static readonly string attemptingAcquire = "Attempting to acquire license for feature '{0}' version '{1}'";
        private static readonly string licenseAcquired = "License acquisition for feature '{0}' version '{1}' successful";
        private static readonly string attemptingReturn = "Attempting to return license for feature '{0}' version '{1}'";
        private static readonly string licenseReturned = "License for feature '{0}' version '{1}' successfully returned";
        private static readonly string surveyFeature = "survey";
        private static readonly string version = "1.0";

        private static ILicensing licensing;
        private static RequestType requestType;
        private static string fileName = String.Empty;
        private static string fileName1 = String.Empty;
        private static string saveFile = String.Empty;
        private static string serverUrl = String.Empty;

        private static string currentFeature = String.Empty;

        private enum RequestType
        {
            generateCapabilityRequest,
            processServedBufferResponse,
            sendCapabilityRequest,
            reportStale
        }

        private static bool ValidateCommandLineArgs(string[] args)
        {
            bool multiCommand = false;
            bool invalidSpec = false;
            string command = String.Empty;
            string option = String.Empty;

            if (args.Length == 0)
            {
                return false;
            }

            for (int ii = 0; !invalidSpec && ii < args.Length; ii++)
            {
                option = args[ii].ToLowerInvariant();
                switch (option)
                {
                    case "-h":
                    case "-help":
                        return false;
                    case "-generate":
                        multiCommand = !String.IsNullOrEmpty(command);
                        invalidSpec = (multiCommand || ++ii >= args.Length || String.IsNullOrEmpty((fileName = args[ii])));
                        if (!invalidSpec)
                        {
                            requestType = RequestType.generateCapabilityRequest;
                            command = option;
                        }
                        break;
                    case "-process":
                        multiCommand = !String.IsNullOrEmpty(command);
                        invalidSpec = (multiCommand || ++ii >= args.Length || String.IsNullOrEmpty((fileName = args[ii])));
                        if (!invalidSpec)
                        {
                            requestType = RequestType.processServedBufferResponse;
                            command = option;
                            if ((ii + 1) < args.Length && !args[ii + 1].StartsWith("-"))
                            {
                                fileName1 = args[++ii];
                            }
                        }
                        break;
                    case "-server":
                        multiCommand = !String.IsNullOrEmpty(command);
                        invalidSpec = (multiCommand || ii + 1 >= args.Length );
                        if (!invalidSpec)
                        {
                            requestType = RequestType.sendCapabilityRequest;
                            command = option;
                            serverUrl = args[++ii];
                        }
                        break;
                    case "-reportstale":
                        multiCommand = !String.IsNullOrEmpty(command);
                        invalidSpec = (multiCommand || ii + 2 >= args.Length || 
                                       String.IsNullOrEmpty((fileName = args[++ii])) ||
                                       String.IsNullOrEmpty((fileName1 = args[++ii])));
                        if (!invalidSpec)
                        {
                            requestType = RequestType.reportStale;
                            command = option;
                        }
                        break;
                    case "-save":
                        invalidSpec = (++ii >= args.Length || String.IsNullOrEmpty(args[ii]));
                        if (!invalidSpec)
                        {
                            saveFile = args[ii];
                        }
                        break;
                    default:
                        Util.DisplayErrorMessage(String.Format("unknown option: {0}", option));
                        return false;
                }
            }
            if (multiCommand)
            {
                Util.DisplayErrorMessage(String.Format("multiple operations specified: {0}, {1}", command, option));
                command = String.Empty;
            }
            else if (invalidSpec)
            {
                Util.DisplayErrorMessage(String.Format("invalid specification for option: {0}", option));
                command = String.Empty;
            }
            return !String.IsNullOrEmpty(command);
        }

        public static void Main(string[] args)
        {
            if (!ValidateCommandLineArgs(args))
            {
                Usage(Path.GetFileName(Environment.GetCommandLineArgs()[0]));
                return;
            }

            if (IdentityClient.IdentityData == null || IdentityClient.IdentityData.Length == 0)
            {
                Console.WriteLine(emptyIdentity);
                return;
            }

            try
            {
                string strPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + Path.DirectorySeparatorChar;
			    // Initialize ILicensing interface with identity data using the Windows common document 
                // respository as the trusted storage location and the hard-coded string hostid "1234567890".
                using (licensing = LicensingFactory.GetLicensing(
                          IdentityClient.IdentityData,
                          strPath,
                          "1234567890"))
                {
                    // The optional host name is typically set by a user as a friendly name for the host.  
                    // The host name is not used for license enforcement.                  
                    licensing.LicenseManager.HostName = "Sample Device";
                    // The host type is typically a name set by the implementer, and is not modifiable by the user.
                    // While optional, the host type may be used in certain scenarios by some back-office systems such as FlexNet Operations.
                    licensing.LicenseManager.HostType = "Sample Device Type";
                    switch (requestType)
                    {
                        case RequestType.generateCapabilityRequest:
                            GenerateCapabilityRequest();
                            break;
                        case RequestType.processServedBufferResponse:
                            ProcessServedBufferResponse(fileName);
                            if (!String.IsNullOrEmpty(fileName1))
                            {
                                ProcessServedBufferResponse(fileName1);
                            }
                            AcquireReturn(surveyFeature, version);
                            break;
                        case RequestType.sendCapabilityRequest:
                            SendCapabilityRequest();
                            break;
                        case RequestType.reportStale:
                            ReportStale();
                            break;
                    }
                }
            }
            catch (Exception exc)
            {
                HandleException(exc);
            }
        }

        private static void GenerateCapabilityRequest()
        {
            // saving the capablity request data tto a file
            Util.DisplayInfoMessage("Creating the capability request");
            ICapabilityRequestData capabilityRequestData = licensing.LicenseManager.CreateCapabilityRequest(GenerateRequestOptions());
            if (Util.WriteData(fileName, capabilityRequestData.ToArray()))
            {
                Util.DisplayInfoMessage(String.Format("Served buffer capability request data written to: {0}", fileName));
            }
        }

        private static void ProcessServedBufferResponse(string name)
        {
            // read and process the served buffer capability response data from the provided file
            Util.DisplayInfoMessage(String.Format("Reading served buffer response data from: {0}", name));
            byte[] binCapResponse = Util.ReadData(name);
            if (binCapResponse == null || binCapResponse.Length == 0)
            {
                Util.DisplayInfoMessage(String.Format("File {0} is empty", name));
            }
            else
            {
                ProcessServedBufferResponse(binCapResponse);
            }
        }

        private static void SendCapabilityRequest()
        {
            Util.DisplayInfoMessage("Creating the served buffer capability request");

            // create the served buffer capability request
            ICapabilityRequestData capabilityRequestData = licensing.LicenseManager.CreateCapabilityRequest(GenerateRequestOptions());
            Util.DisplayInfoMessage(String.Format("Sending the served buffer capability request to: {0}", serverUrl));
            byte[] binCapResponse = null;

            // send the capability request to the server and receive the server response
            CommFactory.Create(serverUrl).SendBinaryMessage(capabilityRequestData.ToArray(), out binCapResponse);
            if (binCapResponse != null && binCapResponse.Length > 0)
            {
                Util.DisplayInfoMessage("Served buffer response received");
                if (!String.IsNullOrEmpty(saveFile))
                {
                    Util.DisplayInfoMessage(String.Format("Saving served buffer response to: {0}", saveFile));
                    File.WriteAllBytes(saveFile, binCapResponse);
                }
            }
            ProcessServedBufferResponse(binCapResponse);
            AcquireReturn(surveyFeature, version);
        }

        private static void ProcessServedBufferResponse(byte[] binCapResponse)
        {
            bool responseIsStale = false;
            Util.DisplayInfoMessage("Processing served buffer response");
            if (licensing.LicenseManager.ServedBufferLicenseSourceExists(binCapResponse, out responseIsStale) &&
                responseIsStale)
            {
                // served buffer response is out of date and will throw a ResponseStaleException if processed
                Util.DisplayInfoMessage("Served buffer response data is stale and will not be processed");
            }
            else
            {
                ICapabilityResponse response = licensing.LicenseManager.AddServedBufferLicenseSource(binCapResponse);
                Util.DisplayInfoMessage("Served buffer response processed");
                ShowServedBufferResponseDetails(response);
                ShowServedBufferFeatures(response.ServerId);
            }
        }

        private static void ShowServedBufferResponseDetails(ICapabilityResponse response)
        {
            Util.DisplayInfoMessage("Obtaining served buffer response details");

            // get machine type from response */
            switch (response.VirtualMachineType)
            {
                case MachineTypeEnum.FLX_MACHINE_TYPE_PHYSICAL:
                    Util.DisplayInfoMessage("Machine type: PHYSICAL");
                    break;
                case MachineTypeEnum.FLX_MACHINE_TYPE_VIRTUAL:
                    Util.DisplayInfoMessage("Machine type: VIRTUAL");
                    // get virtual machine dictionary from response
                    ShowDictionary(response.VirtualMachineInfo, "virtual machine");
                    break;
                case MachineTypeEnum.FLX_MACHINE_TYPE_UNKNOWN:
                default:
                    Util.DisplayInfoMessage("Machine type: UNKNOWN");
                    break;
            }

            // get vendor dictionary from response
            ShowDictionary(response.VendorDictionary, "vendor");

            // get status information
            Util.DisplayInfoMessage(String.Format("Served buffer response contains {0} status item{1}", 
                response.Status.Count, response.Status.Count == 1 ? String.Empty : "s"));
            if (response.Status.Count > 0)
            {
                foreach (IResponseStatus statusItem in response.Status)
                {
                    Util.DisplayInfoMessage(String.Format("Status - category: {0}, code: {1}{2}, details: {3}",
                        statusItem.TypeDescription, (int)statusItem.Code,
                        String.IsNullOrEmpty(statusItem.CodeDescription) ? String.Empty : " (" + statusItem.CodeDescription + ")",
                        statusItem.Details));
                }
            }
        }

        private static void ShowDictionary(ReadOnlyDictionary dictionary, string dictionaryType)
        {
            Util.DisplayInfoMessage(String.Format("Served buffer response contains {0} {1} dictionary item{2}",
                dictionary.Count, dictionaryType, dictionary.Count == 1 ? String.Empty : "s"));
            string itemType;
            foreach (KeyValuePair<string, object> item in dictionary)
            {
                if (item.Value is string)
                {
                    itemType = "string";
                }
                else if (item.Value is int)
                {
                    itemType = "integer";
                }
                else
                {
                    itemType = "unknown";
                }
                Util.DisplayInfoMessage(String.Format("{0} dictionary {1} item type: {2}={3}", 
                                        dictionaryType.Substring(0, 1).ToUpperInvariant() + dictionaryType.Substring(1),                                        
                                        itemType, item.Key, item.Value));
            }
        }

        private static void ShowServedBufferFeatures(KeyValuePair<HostIdEnum, string> serverId)
        {
            // display the features found in the served buffer 
            IFeatureCollection collection = licensing.LicenseManager.GetFeatureCollection(serverId);
            StringBuilder builder = new StringBuilder();
            builder.Append(String.Format("Features loaded from served buffer response: {0}", collection.Count));
            /*
            foreach (IFeature feature in collection)
            {
                builder.AppendLine(String.Empty);
                builder.Append(feature.ToString());
            }
            */ 
            Util.DisplayInfoMessage(builder.ToString());
        }

        private static ICapabilityRequestOptions GenerateRequestOptions()
        {
            // create the capability request options object
            ICapabilityRequestOptions options = licensing.LicenseManager.CreateCapabilityRequestOptions();

            // force capability response from server even if nothing has changed
            options.ForceResponse = true;

            // set the served buffer flag
            options.ServedBuffer = true;

            /*---------------------------------------------------------------------------*/
            /* Other optional information can be added to the request.  For example,     */
            /* Before sending the request to a back-office server such as FlexNet        */
            /* Operations, you can add one or more optional rights IDs and counts to the */
            /* capability request using FlxCapabilityRequestAddRightsId, as in:          */
            /*                                                                           */
            /* options.AddRightsId("ACT-TEST", 1);                                       */
            /*                                                                           */
            /* Similarly, before sending a request to a local license server, you can    */
            /* add one or more desired features and counts to the request, or instruct   */
            /* the local server to include vendor dictionary data in its response.       */
            /*                                                                           */
            /* options.AddDesiredFeature(new FeatureData("survey", version, 1));         */
            /* options.AddDesiredFeature(new FeatureData("highres", version, 1));        */
            /*---------------------------------------------------------------------------*/

            /*---------------------------------------------------------------------------*/
            /* Moreover, when FNE code is acting as a proxy, the hostid and type on      */
            /* behalf of which the request is being generated should be explicitly       */
            /* set.  In direct communications between a client and server, this is not   */
            /* necessary:                                                                */
            /*                                                                           */
            /* licensing.LicenseManager.SetHostId(HostIdEnum.FLX_HOSTID_TYPE_ETHERNET,   */
            /*                                    "<value>");                            */
            /*---------------------------------------------------------------------------*/

            return options;
        }

        private static void AcquireReturn(string requestedFeature, string requestedVersion)
        {
            // acquire license
            string currentFeature = requestedFeature;
            Util.DisplayInfoMessage(String.Format(attemptingAcquire, requestedFeature, requestedVersion));
            try
            {
                ILicense acquiredLicense = licensing.LicenseManager.Acquire(requestedFeature, requestedVersion);
                try
                {
                    currentFeature = acquiredLicense.Name;
                    Util.DisplayInfoMessage(String.Format(licenseAcquired, currentFeature, requestedVersion));
                    //// application logic here
                }
                finally
                {
                    // return license 
                    Util.DisplayInfoMessage(String.Format(attemptingReturn, currentFeature, requestedVersion));
                    acquiredLicense.ReturnLicense();
                    Util.DisplayInfoMessage(String.Format(licenseReturned, currentFeature, requestedVersion));
                }
            }
            catch (Exception exc)
            {
                HandleException(exc);
            }
        }

        private static void ReportStale()
        {
            if (fileName.Equals(fileName1))
            {
                Util.DisplayErrorMessage("-reportstale option file names cannot be the same");
                return;
            }
            Util.DisplayInfoMessage(String.Format("Checking files {0}, {1} for a stale served buffer response", fileName, fileName1));
            ICapabilityResponse crFile = licensing.LicenseManager.GetResponseDetails(File.ReadAllBytes(fileName));
            MessageTypeEnum crType = crFile.Type;
            KeyValuePair<HostIdEnum, string> serverId = crFile.ServerId;
            DateTime? servedTime = crFile.ServedTime;
            ICapabilityResponse crFile1 = licensing.LicenseManager.GetResponseDetails(File.ReadAllBytes(fileName1));
            MessageTypeEnum crType1 = crFile1.Type;
            KeyValuePair<HostIdEnum, string> serverId1 = crFile1.ServerId;
            DateTime? servedTime1 = crFile1.ServedTime;
            if (crType  != MessageTypeEnum.FLX_MESSAGE_TYPE_BUFFER_CAPABILITY_RESPONSE ||
                crType1 != MessageTypeEnum.FLX_MESSAGE_TYPE_BUFFER_CAPABILITY_RESPONSE)
            {
                if (crType != MessageTypeEnum.FLX_MESSAGE_TYPE_BUFFER_CAPABILITY_RESPONSE)
                {
                    Util.DisplayErrorMessage(String.Format("File '{0}' is not a served buffer response file", fileName));
                }
                if (crType1 != MessageTypeEnum.FLX_MESSAGE_TYPE_BUFFER_CAPABILITY_RESPONSE)
                {
                    Util.DisplayErrorMessage(String.Format("File '{0}' is not a served buffer response file", fileName1));
                }
                return;
            }
            if (serverId.Key != serverId1.Key || !string.Equals(serverId.Value, serverId1.Value))
            {
                Util.DisplayInfoMessage("Files are from different servers");
            }
            else if (!servedTime.HasValue || !servedTime1.HasValue)
            {
                Util.DisplayInfoMessage(String.Format("No served time found in file: {0}", !servedTime.HasValue ? fileName : fileName1));
            }
            else 
            {
                Util.DisplayInfoMessage(String.Format("Stale file: {0}", servedTime1.Value >= servedTime.Value ? fileName : fileName1));
            }
        }

        private static void HandleException(Exception exc)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(String.Format("{0} encountered: ", exc.GetType()));
            PublicLicensingException flxException = exc as PublicLicensingException;
            if (flxException != null)
            {
                switch (flxException.ErrorCode)
                {
                    case ErrorCode.FLXERR_RESPONSE_STALE:
                    case ErrorCode.FLXERR_RESPONSE_EXPIRED:
                    case ErrorCode.FLXERR_CAPABILITY_RESPONSE_DATA_MISSING:
                    case ErrorCode.FLXERR_CAPABILITY_RESPONSE_TYPE_INVALID:
                        builder.Append(String.Format("{0}: {1}", processingServedBufferResponse, flxException));
                        break;
                    case ErrorCode.FLXERR_FEATURE_NOT_FOUND:
                        builder.Append(String.Format("{0} {1}: {2}", acquiringLicense, currentFeature, flxException));
                        break;
                    default:
                        builder.Append(flxException.ToString());
                        break;
                }
            }
            else
            {
                builder.Append(exc.Message);
            }
            Util.DisplayErrorMessage(builder.ToString());
        }

        private static void Usage(string applicationName)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(String.Empty);
            builder.AppendLine(String.Format("{0} [-generate outputfile]", applicationName));
            builder.AppendLine(String.Format("{0} [-reportstale inputfile inputfile1]", applicationName));
            builder.AppendLine(String.Format("{0} [-process inputfile [inputfile1]]", applicationName));
            builder.AppendLine(String.Format("{0} [-server url [-save outputfile]]", applicationName));
            builder.AppendLine(String.Empty);
            builder.AppendLine("where:");
            builder.AppendLine("-generate Generates served buffer capability request into a file.");
		    builder.AppendLine("-reportstale  If 'inputfile' and 'inputfile1' originate from the same");
            builder.AppendLine("          server a determination will be made as to which file is older.");
            builder.AppendLine("-process  Processes served buffer response(s) from file(s).");
            builder.AppendLine("-server   Sends served buffer capability request to a server and processes");
            builder.AppendLine("          the served buffer response.");
            builder.AppendLine("          For the test back-office server, use");
            builder.AppendLine("          http://hostname:8080/request.");
            builder.AppendLine("          For FlexNet Operations, use");
            builder.AppendLine("          http://hostname:8888/flexnet/deviceservices.");
            builder.AppendLine("          For FlexNet Embedded License Server, use");
            builder.AppendLine("          http://hostname:7070/fne/bin/capability.");
            builder.AppendLine("          For Cloud License Server, use");
            builder.AppendLine("          https://<tenant>.compliance.flexnetoperations.com/instances/<instance-id>/request.");
            builder.AppendLine("          For FNO Cloud, use");
            builder.AppendLine("          https://<tenant>.compliance.flexnetoperations.com/deviceservices.");
            builder.AppendLine("-save     Saved served buffer response file name.");

            Util.DisplayMessage(builder.ToString(), "USAGE");
        }
    }
}