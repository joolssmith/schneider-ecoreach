// <copyright file="FileUpload.cs" company="Flexera Software LLC">
//     Copyright (c) 2015-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Security;
using System.Text;
using DemoUtilities;
using FlxDotNetClient;
using IdentityData;

/****************************************************************************
    FileUpload.cs

    This example program allows you to register a product and upload a file
    to a content server. The content server may either be specified in the
    example execution arguments or configured in the notification server. 
*****************************************************************************/


namespace FileUpload
{
    public static class FileUpload
    {
        private static readonly string emptyIdentity =
@"Update-enabled code requires client identity data,
which you create with pubidutil and printbin -CS.
See the User Guide for more information.";

        private static readonly List<string> validPlatforms = new List<string>{ "win32", "win64" };
        private static readonly string dashLine = "--------------------------------------------------------------------";
        private static readonly uint progressBarWidth = 50;

        // Product test information
        private static string myProduct = "{77777777-7777-7777-7777-777777777777}";
        private static string myVersion = "1.0";
        private static string myLanguage = "1033";
        private static string myPlatform = null;     // Use default system platform
        private static string myStoragePath = null;  // Use default storage path

        private static string localFile = "";
        private static string fileType = "";
        private static string uploadServer = "";
        private static string destPath = "";
        private static string destFileName = "";

        private static string domainName = "";
        private static string userName = "";
        private static string password = null;

        private static bool quick = false;

        private static IConnect connectClient;
        private static IProduct product;
        private static string   serverUrl = String.Empty;
        private static long     maxRate = 0;

        // Set this to some value between 0 and 100 to demonstrate pause of file upload
        // tasks at the specified percentage. Note that the file content server must 
        // support resumption of a partially uploaded file for this to function correctly.
        private static uint     userDefinedPauseAtPercentage = 0;

        // When set to true a second file will is added to the list of files to be uploaded.
        // The second file is the same file as the first file and the file overwrite policy
        // is set to always overwrite the first file. By default a second file upload task
        // is not added to the task manager object.
        private static bool     addSecondFileUpload = false;

        private static bool createProductComm  = false;
        private static bool createUploadComm = false;

        private static NetworkCredential uploadCredentials = null;


        private class ProgessUserData
        {
            public TaskState lastTaskState;
            public uint lastPercentComplete;

            public ProgessUserData()
            {
                lastTaskState = TaskState.INITIAL;
            }
        };

        private static bool ValidateCommandLineArgs(string[] args)
        {
            bool validCommand = true;
            bool invalidSpec = false;
            string option = String.Empty;

            if (args.Length == 0)
            {
                Util.DisplayErrorMessage("Missing required notification server specification");
                return false;
            }

            for (int ii = 0; !invalidSpec && ii < args.Length; ii++)
            {
                option = args[ii].ToLowerInvariant();
                switch (option)
                {
                    case "-h":
                    case "-help":
                        break;
                    case "-server":
                        if ((invalidSpec = (++ii >= args.Length)))
                        {
                            Util.DisplayErrorMessage(String.Format("Invalid notification server specified: {0}", "missing value" ));
                        }
                        else
                        {
                            serverUrl = args[ii];
                        }
                        break;
                    case "-productid":
                        if ((invalidSpec = (++ii >= args.Length)))
                        {
                            Util.DisplayErrorMessage(String.Format("Invalid product ID specified: {0}", "missing value" ));
                        }
                        else
                        {
                            try
                            {
                                Guid guid = new Guid(args[ii].Trim());
                                myProduct = guid.ToString("B");
                            }
                            catch
                            {
                                Util.DisplayErrorMessage(String.Format("Invalid product ID specified: {0}", "'" + args[ii] + "'"));
                                invalidSpec = true;
                            }
                        }
                        break;
                    case "-productver":
                        if ((invalidSpec = (++ii >= args.Length)))
                        {
                            Util.DisplayErrorMessage(String.Format("Invalid product version specified: {0}", "missing value"));
                        }
                        else
                        {
                            myVersion = args[ii];
                        }
                        break;
                    case "-productplat":
                        if (++ii >= args.Length || !validPlatforms.Contains(args[ii].ToLowerInvariant()))
                        {
                            Util.DisplayErrorMessage(String.Format("Invalid product platform specified: {0}", ii >= args.Length ? "missing value" : "'" + args[ii] + "'"));
                            invalidSpec = true;
                        }
                        else
                        {
                            myPlatform = args[ii];
                        }
                        break;
                    case "-productlang":
                        uint uiTemp;
                        if (++ii >= args.Length || args[ii].Length != 4 || !uint.TryParse(args[ii], out uiTemp))
                        {
                            Util.DisplayErrorMessage(String.Format("Invalid product language specified: {0}", ii >= args.Length ? "missing value" : "'" + args[ii] + "'"));
                            invalidSpec = true;
                        }
                        else
                        {
                            myLanguage = args[ii];
                        }
                        break;
                    case "-localfile":
                        if (++ii >= args.Length || String.IsNullOrEmpty(args[ii]))
                        {
                            Util.DisplayErrorMessage(String.Format("Invalid local file specified: {0}", ii >= args.Length ? "missing value" : "'" + args[ii] + "'"));
                            invalidSpec = true;
                        }
                        else
                        {
                            localFile = args[ii];
                        }
                        break;
                    case "-filetype":
                        if (++ii >= args.Length)
                        {
                            Util.DisplayErrorMessage("Invalid file type specified: missing value");
                            invalidSpec = true;
                        }
                        else
                        {
                            fileType = args[ii];
                        }
                        break;
                    case "-uploadserver":
                        if (++ii >= args.Length)
                        {
                            Util.DisplayErrorMessage(String.Format("Invalid upload server specified: {0}", ii >= args.Length ? "missing value" : "'" + args[ii] + "'"));
                            invalidSpec = true;
                        }
                        else
                        {
                            uploadServer = args[ii];
                        }
                        break;
                    case "-destpath":
                        if (++ii >= args.Length)
                        {
                            Util.DisplayErrorMessage("Invalid destination path specified: missing value");
                            invalidSpec = true;
                        }
                        else
                        {
                            destPath = args[ii];
                        }
                        break;
                    case "-destfilename":
                        if (++ii >= args.Length)
                        {
                            Util.DisplayErrorMessage("Invalid destination file name specified: missing value");
                            invalidSpec = true;
                        }
                        else
                        {
                            destFileName = args[ii];
                        }
                        break;
                    case "-domain":
                        if (++ii >= args.Length)
                        {
                            Util.DisplayErrorMessage("Invalid domain name specified: missing value");
                            invalidSpec = true;
                        }
                        else
                        {
                            domainName = args[ii];
                        }
                        break;
                    case "-user":
                        if (++ii >= args.Length)
                        {
                            Util.DisplayErrorMessage("Invalid user name specified: missing value");
                            invalidSpec = true;
                        }
                        else
                        {
                            userName = args[ii];
                        }
                        break;
                    case "-password":
                        if (++ii >= args.Length)
                        {
                            Util.DisplayErrorMessage("Invalid password specified: missing value");
                            invalidSpec = true;
                        }
                        else
                        {
                            password = args[ii];
                        }
                        break;
                    case "-maxrate":
                        long lTemp;
                        if (++ii >= args.Length || !long.TryParse(args[ii], out lTemp))
                        {
                            Util.DisplayErrorMessage(String.Format("Invalid maxrate value specified: {0}", ii >= args.Length ? "missing value" : "'" + args[ii] + "'"));
                            invalidSpec = true;
                        }
                        else
                        {
                            maxRate = lTemp;
                            createUploadComm = true;
                        }
                        break;
                    case "-quick":
                        quick = true;
                        break;
                    case "-commproduct":
                        createProductComm = true;
                        break;
                    case "-commupload":
                        createUploadComm = true;
                        break;
                    default:
                        Util.DisplayErrorMessage(String.Format("Unknown option: {0}", "'" + args[ii] + "'"));
                        invalidSpec = true;
                        break;
                }
            }
            if (String.IsNullOrEmpty(serverUrl))
            {
                Util.DisplayErrorMessage("Missing required notification server specification");
                validCommand = false;
            }
            else if (String.IsNullOrEmpty(localFile))
            {
                Util.DisplayErrorMessage("Missing required local file specification");
                validCommand = false;
            }
            return (validCommand && !invalidSpec);
        }

        public static void Main(string[] args)
        {
            if (!ValidateCommandLineArgs(args))
            {
                Usage(Path.GetFileName(Environment.GetCommandLineArgs()[0]));
                return;
            }

            if (!File.Exists(localFile))
            {
                Util.DisplayErrorMessage("Specified local file does not exist: " + localFile);
                return;
            }

            if (IdentityClient.IdentityData == null || IdentityClient.IdentityData.Length == 0)
            {
                Console.WriteLine(emptyIdentity);
                return;
            }

            ServicePointManager
                .ServerCertificateValidationCallback +=
                (sender, cert, chain, sslPolicyErrors) => true;

            try
            {
                // Use default storage path
                using (connectClient = ConnectFactory.GetConnect(
                          IdentityClient.IdentityData,
                          myStoragePath))
                {
                    RegisterProduct();
                    product.NotificationServer = serverUrl;
                    if (createProductComm)
                    {
                        IComm productComm = CommFactory.Create();
                        // Adjust Notification Server IComm settings here.
                        product.Comm = productComm;
                    }
                    if (!String.IsNullOrEmpty(userName) || !String.IsNullOrEmpty(password))
                    {
                        uploadCredentials = new NetworkCredential(userName, password, domainName);
                    }
                    if (createUploadComm)
                    {
                        using (IComm uploadComm = CommFactory.Create())
                        {
                            uploadComm.MaxTransferRate = maxRate;
                            uploadComm.ReadTimeout = 5;
                            // Adjust upload server IComm settings here.
                            if (quick)
                            {
                                DoQuickUpload(uploadComm);
                            }
                            else
                            {
                                DoUpload(uploadComm);
                            }
                        }

                    }
                    else
                    {
                        if (quick)
                        {
                            DoQuickUpload(null);
                        }
                        else
                        {
                            DoUpload(null);
                        }
                    }
                    Util.DisplayMessage("FileUpload application complete", "COMPLETE");
                }
            }
            catch (Exception exc)
            {
                HandleException(exc);
            }
        }

        private static void RegisterProduct()
        {
            try
            {
                product = connectClient.GetProduct(myProduct);
                if (product != null)
                {
                    Util.DisplayMessage(dashLine);
                    Util.DisplayMessage("Existing registered product");
                }
                else
                {
                    product = connectClient.RegisterProduct(myProduct, myVersion, myLanguage, myPlatform);
                    Util.DisplayMessage(dashLine);
                    Util.DisplayMessage("Newly registered product");
                }   
            }
            catch 
            {
                Util.DisplayErrorMessage("Error encountered attempting to obtain/register product information");
                throw;
            }
            Util.DisplayMessage(String.Format("  Product code:      {0}", product.Code));
            Util.DisplayMessage(String.Format("  Product version:   {0}", product.Version));
            Util.DisplayMessage(String.Format("  Product language:  {0}", product.Language));
            Util.DisplayMessage(String.Format("  Product platform:  {0}", product.Platform));
            Util.DisplayMessage(dashLine);
        }

        private static void DoQuickUpload(IComm ulComm)
        {
            TaskState taskState;
            IFileUploadTask uploadTask = product.CreateFileUploadTask(localFile, uploadServer, ulComm);
            if (uploadCredentials != null)
            {
                uploadTask.Credentials = uploadCredentials;
            }
            uploadTask.UserData = new ProgessUserData();
            uploadTask.StatusChanged += new TaskStatusDelegate(upload_StatusChanged);
            try
            {
                taskState = uploadTask.Execute();
                if (taskState == TaskState.PAUSED)
                {
                    Console.WriteLine("Upload successfully paused. Automatically resuming...");
                    taskState = uploadTask.Execute();
                }
            }
            catch (Exception exc)
            {
                HandleException(exc);
            }
            uploadTask.StatusChanged -= new TaskStatusDelegate(upload_StatusChanged);
        }

        private static void DoUpload(IComm ulComm)
        {
            using (ITaskManager taskManager = TaskFactory.CreateTaskManager())
            {
                IFileUploadTask uploadTask1 = product.CreateFileUploadTask(localFile, uploadServer, ulComm);
                uploadTask1.Type = fileType;
                uploadTask1.AddCustomAttribute("Key1", "Value1");
                uploadTask1.AddCustomAttribute("Key2", 1);
                if (!String.IsNullOrEmpty(destPath))
                {
                    uploadTask1.CustomServerFilePath = destPath;
                }
                if (!String.IsNullOrEmpty(destFileName))
                {
                    uploadTask1.CustomServerFileName = destFileName;
                }
                if (uploadCredentials != null)
                {
                    uploadTask1.Credentials = uploadCredentials;
                }
                uploadTask1.UserData = new ProgessUserData();
                taskManager.AddTask(uploadTask1);
                if (addSecondFileUpload)
                {
                    IFileUploadTask uploadTask2 = product.CreateFileUploadTask(localFile, uploadServer, ulComm);
                    uploadTask2.Type = fileType;
                    uploadTask2.AddCustomAttribute("Key1", "Value2");
                    uploadTask2.AddCustomAttribute("Key2", 2);
                    if (!String.IsNullOrEmpty(destPath))
                    {
                        // If writing to the same (custom) folder set the overwrite policy to overwrite always
                        // for this file since the previous file is the same in this test scenario.
                        uploadTask2.OverwritePolicy = UploadOverwritePolicy.ALWAYS;
                        uploadTask2.CustomServerFilePath = destPath;
                    }
                    if (!String.IsNullOrEmpty(destFileName))
                    {
                        uploadTask2.CustomServerFileName = destFileName;
                    }
                    if (uploadCredentials != null)
                    {
                        uploadTask2.Credentials = uploadCredentials;
                    }
                    uploadTask2.UserData = new ProgessUserData();
                    taskManager.AddTask(uploadTask2);
                }
                taskManager.StatusChanged += new TaskStatusDelegate(upload_StatusChanged);
                try
                {
                    int completedTasks = taskManager.Execute();
                    if (completedTasks < taskManager.TaskCount && taskManager.TaskList[completedTasks].State == TaskState.PAUSED)
                    {
                        Console.WriteLine("Upload successfully paused. Automatically resuming...");
                        completedTasks = taskManager.Resume();
                    }
                    if (completedTasks < taskManager.TaskCount)
                    {
                        ITask failingTask = taskManager.TaskList[completedTasks];
                    }
                }
                catch (Exception exc)
                {
                    HandleException(exc);
                }
                taskManager.StatusChanged -= new TaskStatusDelegate(upload_StatusChanged);
                DisplayFileUploads(taskManager.TaskList);
            }
        }

        private static void DisplayFileUploads(List<ITask> tasks)
        {
            Console.WriteLine();
            for (int i = 0; i < tasks.Count; i++)
            {
                IFileUploadTask upload = tasks[i] as IFileUploadTask;
                if (upload != null)
                {
                    Console.WriteLine("Upload #" + (i + 1));
                    Console.WriteLine("  Client Location:       " + upload.LocalFileName);
                    if (!String.IsNullOrEmpty(upload.Type))
                    {
                        Console.WriteLine("  File Type:             " + upload.Type);
                    }
                    Console.WriteLine("  Client Upload Server:  " + (String.IsNullOrEmpty(upload.Server) ? "None set" : upload.Server));
                    Console.WriteLine("  Server Upload Server:  " + (String.IsNullOrEmpty(upload.NotificationServerUploadServer) ? "None set" : upload.NotificationServerUploadServer));
                    Console.WriteLine("  Upload Location:       " + (String.IsNullOrEmpty(upload.ServerLocation) ? "Not uploaded" : upload.ServerLocation));
                    Console.WriteLine("  Task state:            " + upload.State.ToString());
                    Console.WriteLine();
                }
            }
        }

        private static void upload_StatusChanged(TaskStatusEventArgs args)
        {
            IFileUploadTask currentTask = args.Task as IFileUploadTask;
            switch (args.State)
            {
                case TaskState.STARTED:
                    Console.WriteLine();
                    Console.WriteLine("Upload started for: " + ((currentTask != null) ? currentTask.LocalFileName : "Unknown"));
                    break;
                case TaskState.COMPLETE:
                    ReportPercent(args.Current, args.Total, args.Task.UserData as ProgessUserData);
                    Console.WriteLine(Environment.NewLine + "Upload complete");
                    break;
                case TaskState.FAILED:
                    Console.WriteLine(Environment.NewLine + "Upload failed");
                    if (args.Task.TaskException != null)
                    {
                        HandleException(args.Task.TaskException);
                    }
                    break;
                case TaskState.FAILED_RETRY:
                    Console.WriteLine(Environment.NewLine + "Upload failed - retryable error");
                    if (args.Task.TaskException != null)
                    {
                        HandleException(args.Task.TaskException);
                    }
                    break;
                case TaskState.EXECUTING:
                    if (args.Total > 0)
                    {
                        args.TaskAction = ReportPercent(args.Current, args.Total, args.Task.UserData as ProgessUserData);
                        if (args.TaskAction == TaskAction.CANCEL)
                        {
                            Console.WriteLine(Environment.NewLine + "Upload cancel requested");
                        }
                        else if (args.TaskAction == TaskAction.PAUSE)
                        {
                            Console.WriteLine(Environment.NewLine + "Upload pause requested");
                        }
                    }
                    break;
            }
        }

        private static TaskAction ReportPercent(long currentSize, long totalSize, ProgessUserData userData)
        {
            uint completionPercent = (uint)((currentSize * 100)/totalSize);
            // Display notable progress
            if (completionPercent > userData.lastPercentComplete && completionPercent <= 100)
            {
                string progress = "[";
                uint progressBarPos = (uint)((progressBarWidth * currentSize) / totalSize);
                for (uint i = 0; i < progressBarWidth; ++i)
                {
                    progress += (i < progressBarPos ? "=" : (i == progressBarPos ? ">" : " "));
                }
                progress = String.Format("{0}] {1} percent complete{2}", progress, completionPercent, completionPercent >= 100 ? Environment.NewLine : "\r");
                Console.Write(progress);
                // Update percent complete value in the user data.
                userData.lastPercentComplete = completionPercent;
                if (userDefinedPauseAtPercentage > 0 && completionPercent >= userDefinedPauseAtPercentage)
                {
                    userDefinedPauseAtPercentage = 0;
                    return TaskAction.PAUSE;
                }
            }
            return TaskAction.CONTINUE;
        }
 
        private static void HandleException(Exception exc)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(String.Format("{0} encountered: ", exc.GetType()));
            PublicConnectException flxException = exc as PublicConnectException;
            if (flxException != null)
            {
                builder.Append(flxException.ToString());
            }
            else
            {
                builder.Append(exc.Message);
            }
            Util.DisplayErrorMessage(builder.ToString());
        }

        private static void Usage(string applicationName)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(String.Empty);
            builder.AppendLine(String.Format("{0} -server [-productid] [-productver] [-productlang] [-productplat]", applicationName));
            builder.AppendLine(String.Format("{0} [-localfile] [-filetype] [-uploadserver] [-destpath] -[destfilename]", applicationName));
            builder.AppendLine(String.Format("{0} [-quick] [-comm] [-maxrate] [-domain] [-user] [-password]", applicationName));
            builder.AppendLine(String.Empty);
            builder.AppendLine("where:");
            builder.AppendLine("-server       Notification server.");
            builder.AppendLine("-productid    Product id. Default:");
            builder.AppendLine("              {77777777-7777-7777-7777-777777777777}");
            builder.AppendLine("-productver   Product version. Default: '1.0'.");
            builder.AppendLine("-productlang  Product language. Default: 1033.");
            builder.AppendLine("-productplat  Product platform. Options are [WIN32|WIN64]. Default is ");
            builder.AppendLine("              connect client machine platform.");
            builder.AppendLine("-localfile    The location of the file to be uploaded on the client file system.");
            builder.AppendLine("-filetype     The type associated with the file to be uploaded.");
            builder.AppendLine("-uploadserver The server to which the local file is to be uploaded. If not specified");
            builder.AppendLine("              the target file upload server is supplied by the FlexNet Connect Server.");
            builder.AppendLine("-destpath     Custom target file system path on the upload server.");
            builder.AppendLine("-destfilename Custom target file name on the upload server.");
            builder.AppendLine("-quick        Quick single file upload based on -filename, -filetype and -uploadserver.");
            builder.AppendLine("-commproduct  Create an IComm interface for communications with the product notification server.");
            builder.AppendLine("-commupload   Create an IComm interface for file upload content delivery.");
            builder.AppendLine("-maxrate      Maximum transfer rate to allow for uploads in bytes per second. Automatically");
            builder.AppendLine("              implies '-commupload'.");
            builder.AppendLine("-domain       File upload credentials domain value if required.");
            builder.AppendLine("-user         File upload credentials user name if required.");
            builder.AppendLine("-password     File upload credentials password if required.");

            Util.DisplayMessage(builder.ToString(), "USAGE");
        }
    }
}