// <copyright file="Notification.cs" company="Flexera Software LLC">
//     Copyright (c) 2014-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using DemoUtilities;
using FlxDotNetClient;
using IdentityData;

/****************************************************************************
    Notification.cs

    This example program allows you to:
    1. Retrieve a notification collection from the notification server for
       the specified product id and optionally download files associated 
       with the returned update notifications.
    2. Unregiser a product.
*****************************************************************************/


namespace Notification
{
    public static class Notification
    {
        private static readonly string emptyIdentity =
@"Update-enabled code requires client identity data,
which you create with pubidutil and printbin -CS.
See the User Guide for more information.";

        private static readonly List<string> validPlatforms = new List<string>{ "win32", "win64" };
        private static readonly string dashLine = "--------------------------------------------------------------------";
        private static readonly uint progressBarWidth = 50;

        // Product test information
        private static string myProduct = "{77777777-7777-7777-7777-777777777777}";
        private static string myVersion = "1.0";
        private static string myLanguage = "1033";
        private static string myPlatform = null;     // Use default system platform
        private static string myStoragePath = null;  // Use default storage path

        private static IConnect connectClient;
        private static IProduct product;
        private static string   serverUrl = String.Empty;
        private static long     maxRate = 0;
        private static uint     userDefinedPauseAtPercentage = 0;

        private static bool unregisterProduct  = false;
        private static bool getNotifications   = false;
        private static bool downloadUpdates    = false;
        private static bool createProductComm  = false;
        private static bool createDownloadComm = false;

        private const string strTitle    = "  Title:                          {0}\n";
        private const string strDlUrl    = "  Download URL:                   {0}\n";
        private const string strCode     = "  Product Code:                   {0}\n";
        private const string strVersion  = "  Product Version:                {0}\n";
        private const string strId       = "  Notification ID:                {0}\n";
        private const string strName     = "  Product Name:                   {0}\n";
        private const string strSize     = "  Download Size:                  {0}\n";
        private const string strCmdLine  = "  Execute Command Line:           {0}\n";
        private const string strLocalDir = "  Target Local Directory:         {0}\n";
        private const string strInfoUrl  = "  Information URL:                {0}\n";
        private const string strDesc     = "  Description:                    {0}\n";
        private const string strCategory = "  Category:                       {0}\n";
        private const string strMedia    = "  Media Type:                     {0}\n";
        private const string strInstruc  = "  Install Instructions:           {0}\n";
        private const string strAuthQ    = "  Authentication Question:        {0}\n";
        private const string strAuthFail = "  Authentication Fail Message:    {0}\n";
        private const string strAuthIUrl = "  Authentication Information URL: {0}\n";
        private const string strCOutUrl  = "  Callout URL:                    {0}\n";
        private const string strSecType  = "  Security Type:                  {0}\n";
        private const string strPubKey   = "  Public Key:                     {0}\n";
        private const string strAvailDt  = "  Availability Date:              {0}\n";
        private const string strExpDt    = "  Expiration Date:                {0}\n";
        private const string strSecUrl   = "  Secondary URL:                  {0}\n";
        private const string strDlType  = "  Download Type:                  {0}\n";
        private const string strDispName = "  Display Name:                   {0}\n";
        private const string strElevReqd = "  Elevation Required:             {0}\n";
        private const string strCoLogo   = "  Company Logo:                   {0}\n";
        private const string strProdLogo = "  Product Logo:                   {0}\n";

        private static List<String> updatePropertyDescriptions = new List<String>
            {strTitle, strDlUrl, strCode, strVersion, strId, strName, strSize, strCmdLine, strLocalDir, strInfoUrl, strDesc,
             strCategory, strMedia, strInstruc, strAuthQ, strAuthFail, strAuthIUrl, strCOutUrl, strSecType,
             strPubKey, strAvailDt, strExpDt, strSecUrl, strDlType, strDispName, strElevReqd, strCoLogo, strProdLogo};

        private static List<String> messagePropertyDescriptions = new List<String>
            {strTitle, strCode, strVersion, strId, strName, strInfoUrl, strDesc, strCategory, strAvailDt, strExpDt,
             strDispName, strCoLogo, strProdLogo};


        private class ProgessUserData
        {
            public NotificationUpdateStage lastUpdateStage;
            public uint lastPercentComplete;
            public bool executionStarted;

            public ProgessUserData()
            {
                lastUpdateStage = NotificationUpdateStage.IDLE;
            }
        };

        private static bool ValidateCommandLineArgs(string[] args)
        {
            bool validCommand = false;
            bool invalidSpec = false;
            string option = String.Empty;

            if (args.Length == 0)
            {
                Util.DisplayErrorMessage("Missing required notification server specification");
                return false;
            }

            for (int ii = 0; !invalidSpec && ii < args.Length; ii++)
            {
                option = args[ii].ToLowerInvariant();
                switch (option)
                {
                    case "-h":
                    case "-help":
                        break;
                    case "-unregister":
                        unregisterProduct = true;
                        validCommand = true;
                        break;
                    case "-server":
                        if ((invalidSpec = (++ii >= args.Length)))
                        {
                            Util.DisplayErrorMessage(String.Format("Invalid notification server specified: {0}", "missing value" ));
                        }
                        else
                        {
                            serverUrl = args[ii];
                            getNotifications = true;
                            validCommand = true;
                        }
                        break;
                    case "-productid":
                        if ((invalidSpec = (++ii >= args.Length)))
                        {
                            Util.DisplayErrorMessage(String.Format("Invalid product ID specified: {0}", "missing value" ));
                        }
                        else
                        {
                            try
                            {
                                Guid guid = new Guid(args[ii].Trim());
                                myProduct = guid.ToString("B");
                            }
                            catch
                            {
                                Util.DisplayErrorMessage(String.Format("Invalid product ID specified: {0}", "'" + args[ii] + "'"));
                                invalidSpec = true;
                            }
                        }
                        break;
                    case "-productver":
                        if ((invalidSpec = (++ii >= args.Length)))
                        {
                            Util.DisplayErrorMessage(String.Format("Invalid product version specified: {0}", "missing value"));
                        }
                        else
                        {
                            myVersion = args[ii];
                        }
                        break;
                    case "-productplat":
                        if (++ii >= args.Length || !validPlatforms.Contains(args[ii].ToLowerInvariant()))
                        {
                            Util.DisplayErrorMessage(String.Format("Invalid product platform specified: {0}", ii >= args.Length ? "missing value" : "'" + args[ii] + "'"));
                            invalidSpec = true;
                        }
                        else
                        {
                            myPlatform = args[ii];
                        }
                        break;
                    case "-productlang":
                        uint uiTemp;
                        if (++ii >= args.Length || args[ii].Length != 4 || !uint.TryParse(args[ii], out uiTemp))
                        {
                            Util.DisplayErrorMessage(String.Format("Invalid product language specified: {0}", ii >= args.Length ? "missing value" : "'" + args[ii] + "'"));
                            invalidSpec = true;
                        }
                        else
                        {
                            myLanguage = args[ii];
                        }
                        break;
                    case "-maxrate":
                        long lTemp;
                        if (++ii >= args.Length || !long.TryParse(args[ii], out lTemp))
                        {
                            Util.DisplayErrorMessage(String.Format("Invalid maxrate value specified: {0}", ii >= args.Length ? "missing value" : "'" + args[ii] + "'"));
                            invalidSpec = true;
                        }
                        else
                        {
                            maxRate = lTemp;
                            createDownloadComm = true;
                        }
                        break; 
                    case "-download":
                        downloadUpdates = true;
                        break;
                    case "-commproduct":
                        createProductComm = true;
                        break;
                    case "-commdownload":
                        createDownloadComm = true;
                        break;
                    default:
                        Util.DisplayErrorMessage(String.Format("Unknown option: {0}", "'" + args[ii] + "'"));
                        invalidSpec = true;
                        break;
                }
            }
            if (!validCommand)
            {
                Util.DisplayErrorMessage("Missing required notification server specification");
            }
            return (validCommand && !invalidSpec);
        }

        public static void Main(string[] args)
        {
            if (!ValidateCommandLineArgs(args))
            {
                Usage(Path.GetFileName(Environment.GetCommandLineArgs()[0]));
                return;
            }

            if (IdentityClient.IdentityData == null || IdentityClient.IdentityData.Length == 0)
            {
                Console.WriteLine(emptyIdentity);
                return;
            }

            try
            {
                // Use default storage path
                using (connectClient = ConnectFactory.GetConnect(
                          IdentityClient.IdentityData,
                          myStoragePath))
                {
                    if (getNotifications)
                    {
                        RegisterProduct();
                        if (createProductComm)
                        {
                            using (IComm comm = CommFactory.Create())
                            {
                                product.Comm = comm;
                                // Set notification server communications options such as 
                                // proxy server or client credentials here.
                                GetNotifications();
                            }
                        }
                        else
                        {
                            GetNotifications();
                        }
                        if (downloadUpdates)
                        {
                            if (createDownloadComm)
                            {
                                using (IComm dlComm = CommFactory.Create())
                                {
                                    // Set content delivery server communications options such as 
                                    // proxy server or client credentials here.
                                    dlComm.MaxTransferRate = maxRate;
                                    DownloadUpdates(dlComm);
                                }
                            }
                            else
                            {
                                DownloadUpdates(null);
                            }
                        }
                    }
                    if (unregisterProduct)
                    {
                        UnregisterProduct();
                    }
                    Util.DisplayMessage("Notification application complete", "SUCCESS");
                }
            }
            catch (Exception exc)
            {
                HandleException(exc);
            }
        }

        private static void RegisterProduct()
        {
            try
            {
                product = connectClient.GetProduct(myProduct);
                if (product != null)
                {
                    Util.DisplayMessage(dashLine);
                    Util.DisplayMessage("Existing registered product");
                }
                else
                {
                    product = connectClient.RegisterProduct(myProduct, myVersion, myLanguage, myPlatform);
                    Util.DisplayMessage(dashLine);
                    Util.DisplayMessage("Newly registered product");
                }   
            }
            catch 
            {
                Util.DisplayErrorMessage("Error encountered attempting to obtain/register product information");
                throw;
            }
            Util.DisplayMessage(String.Format("  Product code:      {0}", product.Code));
            Util.DisplayMessage(String.Format("  Product version:   {0}", product.Version));
            Util.DisplayMessage(String.Format("  Product language:  {0}", product.Language));
            Util.DisplayMessage(String.Format("  Product platform:  {0}", product.Platform));
            Util.DisplayMessage(dashLine);
        }

        private static void UnregisterProduct()
        {
            if (product == null)
            {
                try
                {

                    product = connectClient.GetProduct(myProduct);
                }
                catch
                {
                    Util.DisplayErrorMessage("Product not registered");
                    throw;
                }
            }
            try
            {
                connectClient.UnregisterProduct(product);
                Util.DisplayMessage("Product successfully unregistered");
            }
            catch
            {
                Util.DisplayErrorMessage("Product unregistration failed");
                throw;
            }
        }

        private static void GetNotifications()
        {
            try
            {
                product.NotificationServer = serverUrl;
                List<INotification> notifications = product.NotificationCollectionCreate(true);
                uint i = 1;
                int ii = notifications.Count;
                Util.DisplayMessage(String.Format("{0} notification items returned for requested product.", ii == 0 ? "No" : ii.ToString()), ii == 0 ? "INFO" : null);
                foreach (INotification notification in notifications)
                {
                    StringBuilder builder = new StringBuilder();
                    builder.AppendLine("");
                    builder.AppendLine(String.Format("Notification item {0} of {1} attributes:", i, ii));
                    if (notification is INotificationUpdate)
                    {
                        INotificationUpdate update = notification as INotificationUpdate;
                        builder.AppendLine("  Type:                           Update");
                        List<String> updateProperties = new List<String>
                            {update.Title, update.DownloadURL, update.ProductCode, update.ProductVersion, update.ID, update.ProductName,
                             update.DownloadSize.ToString(), update.CommandLine, update.LocalDirectory, update.AdditionalInfoURL,
                             update.Description, update.Category, update.PackageType.ToString(), update.AdditionalInstallInstructions,
                             update.AuthenticationQuestion, update.AuthenticationFailureMessage, update.AuthenticationInformationURL,
                             update.CalloutURL, update.SecurityType, update.PublicKey, update.AvailabilityDate, update.ExpirationDate,
                             update.SecondaryURL, update.DownloadType.ToString(), update.ProductDisplayName, update.ElevationRequired.ToString(),
                             update.CompanyLogo, update.ProductLogo};
                        for (int j = 0; j < updateProperties.Count; j++)
                        {
                            if (!String.IsNullOrEmpty(updateProperties[j]))
                            {
                                builder.AppendFormat(updatePropertyDescriptions[j], updateProperties[j]);
                            }
                        }
                    }
                    else
                    {
                        if (notification is INotificationMessage)
                        {
                            builder.AppendLine("  Type:                           Message");   
                        }
                        else
                        {
                            builder.AppendLine("  Type:                           Unknown");   
                        }
                        List<String> msgProperties = new List<String>
                            {notification.Title, notification.ProductCode, notification.ProductVersion, notification.ID,
                             notification.ProductName, notification.AdditionalInfoURL, notification.Description, notification.Category, 
                             notification.AvailabilityDate, notification.ExpirationDate, notification.ProductDisplayName,
                             notification.CompanyLogo, notification.ProductLogo};
                        for (int j = 0; j < msgProperties.Count; j++)
                        {
                            if (!String.IsNullOrEmpty(msgProperties[j]))
                            {
                                builder.AppendFormat(messagePropertyDescriptions[j], msgProperties[j]);
                            }
                        }

                    }
                    Util.DisplayMessage(builder.ToString());
                    i++;
                }
            }
            catch
            {
                Util.DisplayErrorMessage("Error encountered creating product notification collection");
                throw;
            }
        }

        private static void DownloadUpdates(IComm dlComm)
        {
            string downloadLocation = null;
            string downloadFilename = null;
            List<INotification> notifications = product.NotificationCollectionCreate(false);
            for (int i = 0; i < notifications.Count; i++)
            {
                if (notifications[i] is INotificationUpdate)
                {
                    INotificationUpdate updateNotification = notifications[i] as INotificationUpdate;
                    try
                    {
                        updateNotification.UserData = new ProgessUserData();
                        Util.DisplayMessage(String.Format("Downloading notification #{0}", i + 1));
                        updateNotification.DownloadAndExecute(downloadLocation, downloadFilename, MyStatusDelegate, dlComm);
                        if (updateNotification.DownloadPaused)
                        {
                            Util.DisplayMessage("The download was successfully paused. Automatically resuming...", "Download paused");
                            try
                            {
                                updateNotification.ResumeDownload(MyStatusDelegate, dlComm);
                            }
                            catch (Exception exc)
                            {
                                Util.DisplayErrorMessage(String.Format("{0}Unable to resume download or execute notification item #{1}", Environment.NewLine, i + 1));
                                HandleException(exc);
                            }
                        }
                        if (!String.IsNullOrEmpty(updateNotification.DataLocation))
                        {
                            Util.DisplayMessage(String.Format("Download successful to: {0}", updateNotification.DataLocation));
                        }
                    }
                    catch (Exception exc)
                    {
                        Util.DisplayErrorMessage(String.Format("{0}Unable to download or execute notification item #{1}", Environment.NewLine, i + 1)); 
                        HandleException(exc);
                    }
                    finally
                    {
                        updateNotification.UserData = null;
                    }
                }
            }
        }

        private static NotificationUpdateStatus MyStatusDelegate(NotificationUpdateStage stage, long currentSize, long totalSize, INotificationUpdate update, Exception exc)
        {
            if (((ProgessUserData)update.UserData).lastUpdateStage != stage)
            {
                switch (stage)
                {
                    case NotificationUpdateStage.DOWNLOAD_START:
                        Util.DisplayMessage("Download started"); break;
                    case NotificationUpdateStage.DOWNLOADING:
                        if (totalSize > 0)
                        {
                            return ReportPercent(currentSize, totalSize, update);
                        }
                        break;
                    case NotificationUpdateStage.DOWNLOAD_END:
                        Util.DisplayMessage("Download ended"); break;
                    case NotificationUpdateStage.DOWNLOAD_FAIL:
                        Util.DisplayMessage(((ProgessUserData)update.UserData).executionStarted ? "Execution failed" : "Download failed"); break;
                    case NotificationUpdateStage.EXECUTION_START:
                        Util.DisplayMessage("Execution started");
                        ((ProgessUserData)update.UserData).executionStarted = true;
                        break;
                    case NotificationUpdateStage.EXECUTION_END:
                        Util.DisplayMessage("Execution ended"); break;
                }
                ((ProgessUserData)update.UserData).lastUpdateStage = stage;
            }
            return NotificationUpdateStatus.CONTINUE;
        }

        private static NotificationUpdateStatus ReportPercent(long currentSize, long totalSize, INotificationUpdate update)
        {
            uint completionPercent = (uint)((currentSize * 100)/totalSize);
            // Display notable progress
            if (completionPercent > ((ProgessUserData)update.UserData).lastPercentComplete)
            {
                string progress = "[";
                uint progressBarPos = (uint)((progressBarWidth * currentSize) / totalSize);
                for (uint i = 0; i < progressBarWidth; ++i)
                {
                    progress += (i < progressBarPos ? "=" : (i == progressBarPos ? ">" : " "));
                }
                progress = String.Format("{0}] {1} percent complete{2}", progress, completionPercent, completionPercent >= 100 ? Environment.NewLine : "\r");
                Console.Write(progress);
                // Update percent complete value in the user data.
                ((ProgessUserData)update.UserData).lastPercentComplete = completionPercent;
                if (userDefinedPauseAtPercentage > 0 && completionPercent >= userDefinedPauseAtPercentage)
                {
                    userDefinedPauseAtPercentage = 0;
                    return NotificationUpdateStatus.PAUSE;
                }
            }
            return NotificationUpdateStatus.CONTINUE;
        }

        private static void HandleException(Exception exc)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(String.Format("{0} encountered: ", exc.GetType()));
            PublicConnectException flxException = exc as PublicConnectException;
            if (flxException != null)
            {
                builder.Append(flxException.ToString());
            }
            else
            {
                builder.Append(exc.Message);
            }
            Util.DisplayErrorMessage(builder.ToString());
        }

        private static void Usage(string applicationName)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(String.Empty);
            builder.AppendLine(String.Format("{0} -server [-download] [-productid] [-productver] [-productlang]", applicationName));
            builder.AppendLine(String.Format("{0} [-productplat] [-unregister] [-commproduct] [-commdownload]", applicationName));
            builder.AppendLine(String.Format("{0} [-maxrate]", applicationName));
            builder.AppendLine(String.Empty);
            builder.AppendLine("where:");
	        builder.AppendLine("-server       Notification server.");
            builder.AppendLine("-download     Download and execute files associated with update notifications.");
            builder.AppendLine("-productid    Product id. Default:");
            builder.AppendLine("              {77777777-7777-7777-7777-777777777777}");
            builder.AppendLine("-productver   Product version. Default: '1.0'.");
            builder.AppendLine("-productlang  Product language. Default: 1033.");
            builder.AppendLine("-productplat  Product platform. Options are [WIN32|WIN64]. Default is ");
            builder.AppendLine("              connect client machine platform.");
            builder.AppendLine("-unregister   Unregister the specified product.");
            builder.AppendLine("-commproduct  Create an IComm interface for communications with the product notificaiton server.");
            builder.AppendLine("-commdownload Create an IComm inerface for update content delivery.");
            builder.AppendLine("-maxrate      Maximum transfer rate to allow for downloads in bytes per second. Automatically");
            builder.AppendLine("              implies '-commdownload'.");

            Util.DisplayMessage(builder.ToString(), "USAGE");
        }
    }
}