// <copyright file="Instrumentation.cs" company="Flexera Software LLC">
//     Copyright (c) 2014-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using DemoUtilities;
using FlxDotNetClient;
using IdentityData;

/****************************************************************************
    Instrumentation.cs

    This example program allows you to log one regular instrumentation event
    and one dictionary instrumentation event for a product. It will then
    upload all available events.
*****************************************************************************/


namespace Instrumentation
{
    public static class Instrumentation
    {
        private static readonly string emptyIdentity =
@"Update-enabled code requires client identity data,
which you create with pubidutil and printbin -CS.
See the User Guide for more information.";

        private static readonly List<string> validPlatforms = new List<string> { "win32", "win64" };
        private const string dashLine = "--------------------------------------------------------------------";
        private const uint   eventLogMaxSize = 0;                // Maximum instrumentation log size in bytes, 0 is no limit.
        private const uint   eventsToLog = 2;                    // Number of events to log in the -test example.
        private const ulong  maxUploadBytes = 0x10000;           // Number of bytes from the instrumentation log to upload, 0 indicates entire log.
        private const uint   maxUploadBufferSize = 0x100000;     // Maximum size buffer to allocate if a single event is larger than median buffer.


        // Canned instrumentation test information
        private const string testGroup = "Test Group";
        private const string testAction1 = "Test Action1A";
        private const string testValue1 = "Test Value1A";
        private const string testAction2 = "Test Action2A";
        private const string testValue2 = "Test Value2A";

        private const string testDictName1 = "DictionaryName1A";
        private static readonly byte[] testDictValue1 = new byte[] { 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa };
        private const string testDictName2 = "DictionaryName2A";
        private static readonly string testDictValue2 = "test value";

        // Product test information
        private static string myProduct = "{77777777-7777-7777-7777-777777777777}";
        private static string myVersion = "1.0";
        private static string myLanguage = "1033";
        private static string myPlatform = null;     // Use default system platform

        private static string myInstanceName = null;
        private static string myInstGroup = "InstrumentCmdLineGroup";
        private static string myInstAction = "InstrumentCmdLineAction";
        private static string myInstValue = null;
        private static string myDictName = "InstrumentCmdLineDictName";
        private static string myDictValue = null;

        private static IConnect connectClient;
        private static IProduct product;
        private static string   serverUrl = String.Empty;
        private static string   logFile = String.Empty;
        private static ulong    maxEventUploadValue = 0;


        private static bool purgeLogs          = false;
        private static bool instrumentTest     = false;
        private static bool instrumentUpload   = false;
        private static bool logUpload          = false;
        private static bool eventUpload        = false;
        private static bool unregisterProduct  = false;
        private static bool createProductComm  = false;


        private enum InstrumentationAction
        {
            EVENT_LOGGED,
            UPLOAD,
            PURGED,
            EVENT_CREATED, 
            DICTIONARY_ITEM_CREATED, 
            LIST_INSTANCES
        }

        private static bool ValidateCommandLineArgs(string[] args)
        {
            bool validCommand = false;
            bool invalidSpec = false;
            string option = String.Empty;

            if (args.Length == 0)
            {
                return false;
            }

            for (int ii = 0; !invalidSpec && ii < args.Length; ii++)
            {
                option = args[ii].ToLowerInvariant();
                switch (option)
                {
                    case "-h":
                    case "-help":
                        return false;
                    case "-unregister":
                        unregisterProduct = true;
                        break;
                    case "-server":
                        if (!(invalidSpec = (++ii >= args.Length)))
                        {
                            serverUrl = args[ii];
                        }
                        break;
                    case "-productid":
                        if (!(invalidSpec = (++ii >= args.Length)))
                        {
                            try
                            {
                                Guid guid = new Guid(args[ii].Trim());
                                myProduct = guid.ToString("B");
                            }
                            catch
                            {
                                Util.DisplayErrorMessage(String.Format("Invalid product ID specified: {0}", args[ii]));
                                invalidSpec = true;
                            }
                        }
                        break;
                    case "-productver":
                        if (!(invalidSpec = (++ii >= args.Length)))
                        {
                            myVersion = args[ii];
                        }
                        break;
                    case "-productplat":
                        if (!(invalidSpec = (++ii >= args.Length)))
                        {
                            if (!validPlatforms.Contains(args[ii].ToLowerInvariant()))
                            {
                                Util.DisplayErrorMessage(String.Format("Invalid product platform specified: {0}", args[ii]));
                                invalidSpec = true;
                            }
                            else
                            {
                                myPlatform = args[ii];
                            }
                        }
                        break;
                    case "-productlang":
                        if (!(invalidSpec = (++ii >= args.Length)))
                        {
                            uint temp;
                            myLanguage = args[ii];
                            if (myLanguage.Length != 4 || !uint.TryParse(args[ii], out temp))
                            {
                                Util.DisplayErrorMessage(String.Format("Invalid product language specified: {0}", myLanguage));
                                invalidSpec = true;
                            }
                        }
                        break;
                    case "-group":
                        if (!(invalidSpec = (++ii >= args.Length)))
                        {
                            myInstGroup = args[ii];
                        }
                        break;
                    case "-action":
                        if (!(invalidSpec = (++ii >= args.Length)))
                        {
                            myInstAction = args[ii];
                        }
                        break;
                    case "-value":
                        if (!(invalidSpec = (++ii >= args.Length)))
                        {
                            myInstValue = args[ii];
                        }
                        break;
                    case "-dictionaryname":
                        if (!(invalidSpec = (++ii >= args.Length)))
                        {
                            myDictName = args[ii];
                        }
                        break;
                    case "-dictionaryvalue":
                        if (!(invalidSpec = (++ii >= args.Length)))
                        {
                            myDictValue = args[ii];
                        }
                        break;
                    case "-purge":
                        purgeLogs = true;
                        break;
                    case "-upload":
                        instrumentUpload = true;
                        break;
                    case "-eventupload":
                        eventUpload = true;
                        break;
                    case "-logupload":
                        if (!(invalidSpec = (++ii >= args.Length)))
                        {
                            logFile = args[11];
                            if (!File.Exists(logFile))
                            {
                                Util.DisplayErrorMessage(String.Format("Invalid logupload file name specified: {0}", args[ii]));
                                invalidSpec = true;
                            }
                            else
                            {
                                logUpload = true;
                            }
                        }
                        break;
                    case "-maxeventupload":
                        if (!(invalidSpec = (++ii >= args.Length)))
                        {
                            uint temp;
                            if (!uint.TryParse(args[ii], out temp))
                            {
                                Util.DisplayErrorMessage(String.Format("Invalid maxeventupload value specified: {0}", args[ii]));
                                invalidSpec = true;
                            }
                            else
                            {
                                maxEventUploadValue = temp;
                            }
                        }
                        break;
                    case "-test":
                        instrumentTest = true;
                        break;
                    case "-productcomm":
                        createProductComm = true;
                        break;
                    default:
                        Util.DisplayErrorMessage(String.Format("unknown option: {0}", args[ii]));
                        break;
                }
            }
            if (!invalidSpec)
            {
                if ((logUpload || instrumentUpload || instrumentTest) && String.IsNullOrEmpty(serverUrl))
                {
                    invalidSpec = true;
                    Util.DisplayErrorMessage("Missing '-server [ServerName]'");
                }
                else if (purgeLogs || logUpload || instrumentTest || instrumentUpload ||
                         !String.IsNullOrEmpty(myInstValue) || !String.IsNullOrEmpty(myDictValue))
                {
                    validCommand = true;
                }
            }

            return (validCommand && !invalidSpec);
        }

        public static void Main(string[] args)
        {
            if (!ValidateCommandLineArgs(args))
            {
                Usage(Path.GetFileName(Environment.GetCommandLineArgs()[0]));
                return;
            }

            if (IdentityClient.IdentityData == null || IdentityClient.IdentityData.Length == 0)
            {
                Console.WriteLine(emptyIdentity);
                return;
            }

            try
            {
                // Use default storage path
                using (connectClient = ConnectFactory.GetConnect(
                          IdentityClient.IdentityData,
                          null))
                {
                    RegisterProduct();
                    if (createProductComm)
                    {
                        using (IComm comm = CommFactory.Create())
                        {
                            product.Comm = comm;
                            // Set notification server communications options such as 
                            // proxy server or client credentials here.
                            DoInstrumentation();
                        }
                    }
                    else
                    {
                        DoInstrumentation();
                    }
                    if (unregisterProduct)
                    {
                        UnregisterProduct();
                    }
                }
            }
            catch (Exception exc)
            {
                HandleException(exc);
            }
        }

        private static void RegisterProduct()
        {
            try
            {
                product = connectClient.GetProduct(myProduct);
                if (product != null)
                {
                    Util.DisplayInfoMessage(dashLine);
                    Util.DisplayInfoMessage("Existing registered product");
                }
                else
                {
                    product = connectClient.RegisterProduct(myProduct, myVersion, myLanguage, myPlatform);
                    Util.DisplayInfoMessage(dashLine);
                    Util.DisplayInfoMessage("Newly registered product");
                }   
            }
            catch 
            {
                Util.DisplayErrorMessage("Error encountered attempting to obtain/register product information");
                throw;
            }
            Util.DisplayInfoMessage(String.Format("  Product code:      {0}", product.Code));
            Util.DisplayInfoMessage(String.Format("  Product version:   {0}", product.Version));
            Util.DisplayInfoMessage(String.Format("  Product language:  {0}", product.Language));
            Util.DisplayInfoMessage(String.Format("  Product platform:  {0}", product.Platform));
            Util.DisplayInfoMessage(dashLine);
        }

        private static void UnregisterProduct()
        {
            if (product == null)
            {
                try
                {

                    product = connectClient.GetProduct(myProduct);
                }
                catch
                {
                    Util.DisplayErrorMessage("Product not registered");
                    throw;
                }
            }
            try
            {
                connectClient.UnregisterProduct(product);
                Util.DisplayInfoMessage("Product successfully unregistered");
            }
            catch
            {
                Util.DisplayErrorMessage("Product unregistration failed");
                throw;
            }
        }

        private static void DoInstrumentation()
        {
            if (!String.IsNullOrEmpty(serverUrl))
            {
                product.NotificationServer = serverUrl;
            }

            if (purgeLogs)
            {   // remove all events for a product without uploading them 
                product.DeleteInstrumentationLog(String.Empty);
                DisplayInstrumentationInfo(InstrumentationAction.PURGED, null, null, null, null, 0);
                return;
            }

            if (logUpload)
            {
                byte[] logData = File.ReadAllBytes(logFile);
                IInstrumentationLog instLog = product.GetInstrumentationLog(myInstanceName);
                IInstrumentationEvent instEvent = product.CreateInstrumentationEvent("Log", "start", "debug");
                DisplayInstrumentationInfo(InstrumentationAction.EVENT_CREATED, "Log", "start", "debug", null, 0);
                instEvent.AddDictionaryItem("Details", logData);
                DisplayInstrumentationInfo(InstrumentationAction.DICTIONARY_ITEM_CREATED, "Log", "start", "debug", "Details", 0);
                instLog.Write(instEvent);
                DisplayInstrumentationInfo(InstrumentationAction.EVENT_LOGGED, null, null, null, null, 0);
                instEvent = product.CreateInstrumentationEvent("Log", "stop", "debug");
                DisplayInstrumentationInfo(InstrumentationAction.EVENT_CREATED, "Log", "stop", "debug", null, 0);
                instLog.Write(instEvent);
                DisplayInstrumentationInfo(InstrumentationAction.EVENT_LOGGED, null, null, null, null, 0);
                // Do the upload
                instLog.Upload(maxEventUploadValue, maxUploadBufferSize);
                DisplayInstrumentationInfo(InstrumentationAction.UPLOAD, null, null, null, null, instLog.LastUploadEntryCount);
                return;
            }

            if (myInstValue != null || myDictValue != null)
            {
                IInstrumentationLog instLog = product.GetInstrumentationLog(myInstanceName);
                product.MaxLogSize = eventLogMaxSize;
                IInstrumentationEvent instEvent = product.CreateInstrumentationEvent(myInstGroup, myInstAction, myInstValue);
                DisplayInstrumentationInfo(InstrumentationAction.EVENT_CREATED, myInstGroup, myInstAction, myInstValue, null, 0);
                if (myDictValue != null)
                {
                    instEvent.AddDictionaryItem(myDictName, myDictValue);
                    DisplayInstrumentationInfo(InstrumentationAction.DICTIONARY_ITEM_CREATED, myInstGroup, myInstAction, myInstValue, myDictName, 0);
                }
                instLog.Write(instEvent);
                DisplayInstrumentationInfo(InstrumentationAction.EVENT_LOGGED, null, null, null, null, 0);
            }

            if (instrumentUpload)
            {
                IInstrumentationLog instLog = product.GetInstrumentationLog(myInstanceName);
                // Do the upload
                instLog.Upload(maxEventUploadValue, maxUploadBufferSize);
                DisplayInstrumentationInfo(InstrumentationAction.UPLOAD, null, null, null, null, instLog.LastUploadEntryCount);
            }

            // run sample tests
            if (instrumentTest)
            {
                Dictionary<string, IInstrumentationLog> logs = product.InstrumentationLogs;
                foreach (KeyValuePair<string, IInstrumentationLog> kvp in logs)
                {
                    DisplayInstrumentationInfo(InstrumentationAction.LIST_INSTANCES, null, null, kvp.Key ?? "NULL", null, 0);
                }
                IInstrumentationLog instLog = product.GetInstrumentationLog(myInstanceName);
                IInstrumentationEvent instEvent = null;
                product.MaxLogSize = eventLogMaxSize;
                for (uint i = 0; i < eventsToLog / 2; i++)
                {
                    instEvent = product.CreateInstrumentationEvent(testGroup, testAction1, testValue1);
                    DisplayInstrumentationInfo(InstrumentationAction.EVENT_CREATED, testGroup, testAction1, testValue1, null, 0);
                    instEvent.AddDictionaryItem(testDictName1, testDictValue1);
                    DisplayInstrumentationInfo(InstrumentationAction.DICTIONARY_ITEM_CREATED, testGroup, testAction1, testValue1, testDictName1, 0);
                    instEvent.AddDictionaryItem(testDictName2, testDictValue2);
                    DisplayInstrumentationInfo(InstrumentationAction.DICTIONARY_ITEM_CREATED, testGroup, testAction1, testValue1, testDictName2, 0);
                    instLog.Write(instEvent);
                    DisplayInstrumentationInfo(InstrumentationAction.EVENT_LOGGED, null, null, null, null, 0);

                    instEvent = product.CreateInstrumentationEvent(testGroup, testAction2, testValue2);
                    if (eventUpload)
                    {
                        instEvent.Upload();
                        DisplayInstrumentationInfo(InstrumentationAction.UPLOAD, null, null, null, null, 1);
                    }
                    else
                    {
                        instLog.Write(instEvent);
                        DisplayInstrumentationInfo(InstrumentationAction.EVENT_LOGGED, null, null, null, null, 0);
                    }
                }
                instLog.Upload(maxUploadBytes, maxUploadBufferSize);
                DisplayInstrumentationInfo(InstrumentationAction.UPLOAD, null, null, null, null, instLog.LastUploadEntryCount);
                if (instLog.EntryCount > 0)
                {
                    instLog.Upload(0, maxUploadBufferSize);
                    DisplayInstrumentationInfo(InstrumentationAction.UPLOAD, null, null, null, null, instLog.LastUploadEntryCount);
                }
            }
        }

        private static void DisplayInstrumentationInfo(InstrumentationAction displayType, string group, string action, string value, string dictName, uint num)
        {
            switch (displayType)
            {
                case InstrumentationAction.EVENT_LOGGED:
                    Util.DisplayInfoMessage("Successfully logged event.");
                    break;
                case InstrumentationAction.UPLOAD:
                    Util.DisplayInfoMessage(String.Format("Successfully uploaded {0} event(s).", num));
                    break;
                case InstrumentationAction.PURGED:
                    Util.DisplayInfoMessage("Successfully purged Instrumentation log.");
                    break;
                case InstrumentationAction.EVENT_CREATED:
                    Util.DisplayInfoMessage(String.Format("Successfully added instrumentaton event." +
                        Environment.NewLine + "    Group: {0}, Action: {1}, Value: {2}.",
                        group, action, value));
                    break;
                case InstrumentationAction.DICTIONARY_ITEM_CREATED:
                    Util.DisplayInfoMessage(String.Format("Successfully added instrumentaton event dictionary." +
                        Environment.NewLine + "    Group: {0}, Action: {1}, Value: {2}, Dictionary: {3}.",
                        group, action, value, dictName));
                    break;
                case InstrumentationAction.LIST_INSTANCES:
                    Util.DisplayInfoMessage(String.Format("Instrumentation instance: {0}.", value));
                    break;
            }
        }

        private static void HandleException(Exception exc)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(String.Format("{0} encountered: ", exc.GetType()));
            PublicConnectException flxException = exc as PublicConnectException;
            if (flxException != null)
            {
                builder.Append(flxException.ToString());
            }
            else
            {
                builder.Append(exc.Message);
            }
            Util.DisplayErrorMessage(builder.ToString());
        }

        private static void Usage(string applicationName)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(String.Empty);
            builder.AppendLine(String.Format("{0} [-purge] [-server] [-productid] [-productver] [-productlang] [-productplat]", applicationName));
            builder.AppendLine(String.Format("{0} [-group] [-action] [-value] [-dictionaryname] [-dictionaryvalue]", applicationName));
            builder.AppendLine(String.Format("{0} [-eventupload] [-upload] [-logupload] [-maxeventupload]", applicationName));
            builder.AppendLine(String.Format("{0} [-test] [-unregister] [-productcomm]", applicationName));

            builder.AppendLine(String.Empty);
            builder.AppendLine("where:");
            builder.AppendLine("-purge           Purge all events for a product without uploading.");
	        builder.AppendLine("-server          Notification server.");
            builder.AppendLine("-productid       Product id. Default:");
            builder.AppendLine("                 {77777777-7777-7777-7777-777777777777}");
            builder.AppendLine("-productver      Product version. Default: '1.0'.");
            builder.AppendLine("-productlang     Product language. Default: 1033.");
            builder.AppendLine("-productplat     Product platform. Options are [WIN32|WIN64]. Default is ");
            builder.AppendLine("                 connect client machine platform.");
            builder.AppendLine("-group           A test group name to use for an instrumentaion event.");
            builder.AppendLine("                 Default is 'InstrumentationCmdLineGroup'.");
            builder.AppendLine("-action          A test action to use for an instrumentation event.");
            builder.AppendLine("                 Default is 'InstrumentationCmdLineAction'.");
            builder.AppendLine("-value           A test value to use for an instrumentation event. If not");
            builder.AppendLine("                 specifed the -group and -action command line options");
            builder.AppendLine("                 are ignored.");
            builder.AppendLine("-dictionaryname  A test name to use for an instrumentaion event dictionary");
            builder.AppendLine("                 item. Default is 'InstrumentCmdLineDictName'.");
            builder.AppendLine("-dictionaryvalue A test value to use for an instrumentaion event dictionary");
            builder.AppendLine("                 item. If not specified the '-dictionaryname' command line");
            builder.AppendLine("                 option is ignored.");
            builder.AppendLine("-eventupload     Upload instumentation events individually.");
            builder.AppendLine("-upload          Upload product instrumentation log data to the notification server.");
            builder.AppendLine("-logupload       Create and log instrumentation events with an associated dictionary");
            builder.AppendLine("                 item containing the specified application log file data. Upload ");
            builder.AppendLine("                 product instrumentation log data to the notification server.");
            builder.AppendLine("-maxeventupload  Maximum number of bytes of event data to upload at once.");
            builder.AppendLine("-test            Perform sample instrumentation test function.");
            builder.AppendLine("-unregister      Unregister the specified product.");
            builder.AppendLine("-productcomm     Create an IComm interface for communications with the product notification server.");

            Util.DisplayMessage(builder.ToString(), "USAGE");
        }
    }
}