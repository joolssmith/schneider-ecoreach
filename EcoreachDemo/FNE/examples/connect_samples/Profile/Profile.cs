// <copyright file="Profile.cs" company="Flexera Software LLC">
//     Copyright (c) 2014-2017 Flexera Software LLC.
//     All Rights Reserved.
//     This software has been provided pursuant to a License Agreement
//     containing restrictions on its use.  This software contains
//     valuable trade secrets and proprietary information of
//     Flexera Software LLC and is protected by law.
//     It may not be copied or distributed in any form or medium, disclosed
//     to third parties, reverse engineered or used in any manner not
//     provided for in said License Agreement except with the prior
//     written authorization from Flexera Software LLC.
// </copyright>

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using DemoUtilities;
using FlxDotNetClient;
using IdentityData;

/****************************************************************************
    Profile.cs

    This example program allows you to upload a product profile.
*****************************************************************************/


namespace Profile
{
    public static class Profile
    {
        private static readonly string emptyIdentity =
@"Update-enabled code requires client identity data,
which you create with pubidutil and printbin -CS.
See the User Guide for more information.";

        private static readonly List<string> validPlatforms = new List<string>{ "win32", "win64" };
        private static readonly string dashLine = "--------------------------------------------------------------------";

        // Canned profile test information
        private static readonly string profileGroupA = "GroupA";
        private static readonly string profileGroupB = "GroupB";
        private static readonly string profileName1 = "TestName1";
        private static readonly string profileName2 = "TestName2";
        private static readonly string profileValue1 = "1995";
        private static readonly string profileValue2 = "2012";

        // Product test information
        private static string myProduct = "{77777777-7777-7777-7777-777777777777}";
        private static string myVersion = "1.0";
        private static string myLanguage = "1033";
        private static string myPlatform = null;     // Use default system platform

        private static string myProfileGroup = "ProfileCmdLineGroup";
        private static string myProfileName = "ProfileCmdLineName";
        private static string myProfileValue = null;

        private static IConnect connectClient;
        private static IProduct product;
        private static string   serverUrl = String.Empty;

        private static bool profileTest        = false;
        private static bool demographics       = false;
        private static bool unregisterProduct  = false;
        private static bool createProductComm  = false;

        private enum ProfileAction
        {
            SET,
            GET,
            REMOVE_VALUE,
            REMOVE_GROUP, 
            UPLOAD, 
            DEMOGRAPHICS
        }

        private static bool ValidateCommandLineArgs(string[] args)
        {
            bool validCommand = false;
            bool invalidSpec = false;
            string option = String.Empty;

            if (args.Length == 0)
            {
                return false;
            }

            for (int ii = 0; !invalidSpec && ii < args.Length; ii++)
            {
                option = args[ii].ToLowerInvariant();
                switch (option)
                {
                    case "-h":
                    case "-help":
                        return false;
                    case "-unregister":
                        unregisterProduct = true;
                        break;
                    case "-server":
                        if (!(invalidSpec = (++ii >= args.Length)))
                        {
                            serverUrl = args[ii];
                        }
                        break;
                    case "-productid":
                        if (!(invalidSpec = (++ii >= args.Length)))
                        {
                            try
                            {
                                Guid guid = new Guid(args[ii].Trim());
                                myProduct = guid.ToString("B");
                            }
                            catch
                            {
                                Util.DisplayErrorMessage(String.Format("Invalid product ID specified: {0}", args[ii]));
                                invalidSpec = true;
                            }
                        }
                        break;
                    case "-productver":
                        if (!(invalidSpec = (++ii >= args.Length)))
                        {
                            myVersion = args[ii];
                        }
                        break;
                    case "-productplat":
                        if (!(invalidSpec = (++ii >= args.Length)))
                        {
                            if (!validPlatforms.Contains(args[ii].ToLowerInvariant()))
                            {
                                Util.DisplayErrorMessage(String.Format("Invalid product platform specified: {0}", args[ii]));
                                invalidSpec = true;
                            }
                            else
                            {
                                myPlatform = args[ii];
                            }
                        }
                        break;
                    case "-productlang":
                        if (!(invalidSpec = (++ii >= args.Length)))
                        {
                            uint temp;
                            myLanguage = args[ii];
                            if (myLanguage.Length != 4 || !uint.TryParse(args[ii], out temp))
                            {
                                Util.DisplayErrorMessage(String.Format("Invalid product language specified: {0}", myLanguage));
                                invalidSpec = true;
                            }
                        }
                        break;
                    case "-group":
                        if (!(invalidSpec = (++ii >= args.Length)))
                        {
                            myProfileGroup = args[ii];
                        }
                        break;
                    case "-name":
                        if (!(invalidSpec = (++ii >= args.Length)))
                        {
                            myProfileName = args[ii];
                        }
                        break;
                    case "-value":
                        if (!(invalidSpec = (++ii >= args.Length)))
                        {
                            myProfileValue = args[ii];
                        }
                        break; 
                    case "-test":
                        profileTest = true;
                        break;
                    case "-demographic":
                        demographics = true;
                        break;
                    case "-productcomm":
                        createProductComm = true;
                        break;
                    default:
                        Util.DisplayErrorMessage(String.Format("unknown option: {0}", args[ii]));
                        break;
                }
            }
            if (!invalidSpec)
            {
                if ((profileTest || demographics || !String.IsNullOrEmpty(myProfileValue)) && String.IsNullOrEmpty(serverUrl))
                {
                    invalidSpec = true;
                    Util.DisplayErrorMessage("Missing '-server [ServerName]'");
                }
                else if (unregisterProduct || profileTest || demographics || !String.IsNullOrEmpty(myProfileValue))
                {
                    validCommand = true;
                }
            }

            return (validCommand && !invalidSpec);
        }

        public static void Main(string[] args)
        {
            if (!ValidateCommandLineArgs(args))
            {
                Usage(Path.GetFileName(Environment.GetCommandLineArgs()[0]));
                return;
            }

            if (IdentityClient.IdentityData == null || IdentityClient.IdentityData.Length == 0)
            {
                Console.WriteLine(emptyIdentity);
                return;
            }

            try
            {
                // Use default storage path
                using (connectClient = ConnectFactory.GetConnect(
                          IdentityClient.IdentityData,
                          null))
                {
                    RegisterProduct();
                    if (createProductComm)
                    {
                        using (IComm comm = CommFactory.Create())
                        {
                            product.Comm = comm;
                            // Set notification server communications options such as 
                            // proxy server or client credentials here.
                            WorkWithProfile();
                        }
                    }
                    else
                    {
                        WorkWithProfile();
                    }
                    if (unregisterProduct)
                    {
                        UnregisterProduct();
                    }
                }
            }
            catch (Exception exc)
            {
                HandleException(exc);
            }
        }

        private static void RegisterProduct()
        {
            try
            {
                product = connectClient.GetProduct(myProduct);
                if (product != null)
                {
                    Util.DisplayInfoMessage(dashLine);
                    Util.DisplayInfoMessage("Existing registered product");
                }
                else
                {
                    product = connectClient.RegisterProduct(myProduct, myVersion, myLanguage, myPlatform);
                    Util.DisplayInfoMessage(dashLine);
                    Util.DisplayInfoMessage("Newly registered product");
                }   
            }
            catch 
            {
                Util.DisplayErrorMessage("Error encountered attempting to obtain/register product information");
                throw;
            }
            Util.DisplayInfoMessage(String.Format("  Product code:      {0}", product.Code));
            Util.DisplayInfoMessage(String.Format("  Product version:   {0}", product.Version));
            Util.DisplayInfoMessage(String.Format("  Product language:  {0}", product.Language));
            Util.DisplayInfoMessage(String.Format("  Product platform:  {0}", product.Platform));
            Util.DisplayInfoMessage(dashLine);
        }

        private static void UnregisterProduct()
        {
            if (product == null)
            {
                try
                {

                    product = connectClient.GetProduct(myProduct);
                }
                catch
                {
                    Util.DisplayErrorMessage("Product not registered");
                    throw;
                }
            }
            try
            {
                connectClient.UnregisterProduct(product);
                Util.DisplayInfoMessage("Product successfully unregistered");
            }
            catch
            {
                Util.DisplayErrorMessage("Product unregistration failed");
                throw;
            }
        }

        private static void WorkWithProfile()
        {
            try
            {
                // log and upload command line profile entries
                if (myProfileValue != null)
                {
                    product.NotificationServer = serverUrl;
                    product.Profile.SetItem(myProfileGroup, myProfileName, myProfileValue);
                    DisplayProfileInfo(ProfileAction.SET, myProfileGroup, myProfileName, myProfileValue);
                    product.Profile.Upload(false);
                    DisplayProfileInfo(ProfileAction.UPLOAD, null, null, null);
                }

                // test profile for the product
                if (profileTest)
                {
                    string strValue;
                    product.NotificationServer = serverUrl;
                    product.Profile.SetItem(profileGroupA, profileName1, profileValue1);
                    DisplayProfileInfo(ProfileAction.SET, profileGroupA, profileName1, profileValue1);
                    product.Profile.SetItem(profileGroupA, profileName2, profileValue2);
                    DisplayProfileInfo(ProfileAction.SET, profileGroupA, profileName2, profileValue2);
                    product.Profile.SetItem(profileGroupB, profileName1, profileValue1);
                    DisplayProfileInfo(ProfileAction.SET, profileGroupB, profileName1, profileValue1);
                    product.Profile.SetItem(profileGroupB, profileName2, profileValue2);
                    DisplayProfileInfo(ProfileAction.SET, profileGroupB, profileName2, profileValue2);
                    strValue = product.Profile.GetItem(profileGroupA, profileName1);
                    DisplayProfileInfo(ProfileAction.GET, profileGroupA, profileName1, strValue);
                    strValue = product.Profile.GetItem(profileGroupB, profileName2);
                    DisplayProfileInfo(ProfileAction.GET, profileGroupB, profileName2, strValue);
                    product.Profile.RemoveItem(profileGroupA, profileName1);
                    DisplayProfileInfo(ProfileAction.REMOVE_VALUE, profileGroupA, profileName1, null);
                    product.Profile.RemoveGroup(profileGroupB);
                    DisplayProfileInfo(ProfileAction.REMOVE_GROUP, profileGroupB, null, null);
                    product.Profile.Upload(false);
                    DisplayProfileInfo(ProfileAction.UPLOAD, null, null, null);
                }

                // demographics test
                if (demographics)
                {
                    product.Profile.ServerDemographicRequest();
                    DisplayProfileInfo(ProfileAction.DEMOGRAPHICS, null, null, null);

                }
            }
            catch
            {
                Util.DisplayErrorMessage("Error encountered creating product notification collection");
                throw;
            }
        }

        private static void DisplayProfileInfo(ProfileAction action, string group, string name, string value)
        {
            switch (action)
            {
                case ProfileAction.SET:
                    Util.DisplayInfoMessage(String.Format("Successfully set profile item. Group: {0}, Name: {1}, Value: {2}.",
                        group, name, value));
                    break;
                case ProfileAction.GET:
                    Util.DisplayInfoMessage(String.Format("Successfully retrieved profile item. Group: {0}, Name: {1}, Value: {2}.",
                        group, name, value));
                    break;
                case ProfileAction.REMOVE_VALUE:
                    Util.DisplayInfoMessage(String.Format("Successfully removed profile item.  Group: {0}, Name: {1}.",
                        group, name));
                    break;
                case ProfileAction.REMOVE_GROUP:
                    Util.DisplayInfoMessage(String.Format("Successfully removed profile Group: {0}.",
                        group));
                    break;
                case ProfileAction.UPLOAD:
                    Util.DisplayInfoMessage("Profile successfully uploaded.");
                    break;
                case ProfileAction.DEMOGRAPHICS:
                    Util.DisplayInfoMessage("Demographic request/response from server successfully processed.");
                    break;
            }
        }

        private static void HandleException(Exception exc)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(String.Format("{0} encountered: ", exc.GetType()));
            PublicConnectException flxException = exc as PublicConnectException;
            if (flxException != null)
            {
                builder.Append(flxException.ToString());
            }
            else
            {
                builder.Append(exc.Message);
            }
            Util.DisplayErrorMessage(builder.ToString());
        }

        private static void Usage(string applicationName)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(String.Empty);
            builder.AppendLine(String.Format("{0} -server [-productid] [-productver] [-productlang]", applicationName));
            builder.AppendLine(String.Format("{0} [-productplat] [-group] [-name] [-value] [-test]", applicationName));
            builder.AppendLine(String.Format("{0} [-demographic] [-unregister] [-productcomm]", applicationName));

            builder.AppendLine(String.Empty);
            builder.AppendLine("where:");
	        builder.AppendLine("-server       Notification server.");
            builder.AppendLine("-productid    Product id. Default:");
            builder.AppendLine("              {77777777-7777-7777-7777-777777777777}");
            builder.AppendLine("-productver   Product version. Default: '1.0'.");
            builder.AppendLine("-productlang  Product language. Default: 1033.");
            builder.AppendLine("-productplat  Product platform. Options are [WIN32|WIN64]. Default is ");
            builder.AppendLine("              connect client machine platform.");
            builder.AppendLine("-group        A test group to add to the profile and upload.");
            builder.AppendLine("              Default is 'ProfileCmdLineGroup'.");
            builder.AppendLine("-name         A test name to add to the profile and upload.");
            builder.AppendLine("              Default is 'ProfileCmdLineName'.");
            builder.AppendLine("-value        A test value to add to the profile and upload. If not");
            builder.AppendLine("              specifed the -group and -name command line options");
            builder.AppendLine("              are ignored.");
            builder.AppendLine("-test         Perform sample profile test function.");
            builder.AppendLine("-demographic  Perform demographic request test with notification server.");
            builder.AppendLine("-unregister   Unregister the specified product.");
            builder.AppendLine("-productcomm  Create an IComm interface for communications with the product notification server.");

            Util.DisplayMessage(builder.ToString(), "USAGE");
        }
    }
}