Readme for FlexNet Embedded .Net XT Kit - mono configuration 
-----------------------------------------------------------------

The files ( FlxLicensingClient.dll.config and/or FlxConnectClient.dll.config ) provided
under "monoconfig" directory are specific to mono runtime.

These files contain the dll mapping for the Flxcore and FlxConnect libraries.
Dll mappings are used to map windows library names (.dll) to linux/unix library names (usually .so).
(For more information: http://www.mono-project.com/docs/advanced/pinvoke/dllmap/)

You are required to keep these files beside your executable if you are running your application using mono.
Alternatively, you can edit the global mono configuration e.g. /etc/mono/config or /usr/etc/mono/config to include 
dll mapping mentioned in the given config files.

Please make sure that FlxCore and FlxConnect shared objects (.so) can be found by system loader.
i.e Install the libraries in standard locations( /usr/lib etc ) or set the LD_LIBRARY_PATH environment
variable to an absolute path of the direcory which includes these shared objects.