
@echo off

del L*.bin

dir L*.txt

call ..\FNE\bin\tools\licensefileutil.bat -id NovaIdentityBackOfficeSHA2.bin LV850002-12345616024L001LV800001.txt LV850002-12345616024L001LV800001.bin -unsignedFeatures

call ..\FNE\bin\tools\licensefileutil.bat -id NovaIdentityBackOfficeSHA2.bin LV850003-12345616024L001LV800001.txt LV850003-12345616024L001LV800001.bin -unsignedFeatures

call ..\FNE\bin\tools\licensefileutil.bat -id NovaIdentityBackOfficeSHA2.bin LV850004-12345616024L001LV800001.txt LV850004-12345616024L001LV800001.bin -unsignedFeatures

call ..\FNE\bin\tools\licensefileutil.bat -id NovaIdentityBackOfficeSHA2.bin LV850005-12345616024L001LV800001.txt LV850005-12345616024L001LV800001.bin -unsignedFeatures

call ..\FNE\bin\tools\printbin.bat -full LV850002-12345616024L001LV800001.bin
call ..\FNE\bin\tools\printbin.bat -full LV850003-12345616024L001LV800001.bin
call ..\FNE\bin\tools\printbin.bat -full LV850004-12345616024L001LV800001.bin
call ..\FNE\bin\tools\printbin.bat -full LV850005-12345616024L001LV800001.bin
