﻿using FlxDotNetClient;
using System;
using System.Collections.Generic;
using System.IO;

namespace Ecoreach_Demo
{
    /// <summary>
    /// Example FNE client class able to process binary licens files
    /// </summary>
    class FneClient
    {
        public byte[] Identity { get; set; } //!< The FNE (Nova) Identitiy

        public string Hostid { set; get; }  //!< The FNE Hostid - this must be the same as the Trip Unit

        public string StoragePath { set; get; } //!< FNE Trusted Storage path - should be null as no persistens is intended

        public ILicensing Licensing{ set; private get; } //!< Main FNE licensing object
        /// <summary>
        /// Constructor
        /// </summary>
        public FneClient()
        {
            this.StoragePath = null;
            this.Hostid = null;
            this.Identity = null;
            this.Licensing = null;
        }

        /// <summary>
        /// Initalize the client and retrieve the licensing object
        /// </summary>
        public void initalize()
        {
            if (Identity == null || Identity.Length <= 0)
            {
                throw new InvalidOperationException("The identity is not valid");
            }

            Licensing = LicensingFactory.GetLicensing(Identity, StoragePath, Hostid); 
        }

        /// <summary>
        /// Terminate and cleanup
        /// </summary>
        public void terminate()
        {
            try
            {
                Licensing.Dispose();
            }
            catch(Exception)
            {

            }
        }

        /// <summary>
        /// Add license files into the license manager
        /// </summary>
        /// <param name="filename"></param>
        public void addLicenseFile(string filename)
        {
            Console.WriteLine("adding license file {0}...", filename);
            Licensing.LicenseManager.AddBufferLicenseSource(filename, Path.GetFileName(filename));
        }

        /// <summary>
        /// Display which features are avaialable
        /// </summary>
        public void displayAvailableLicenses()
        {
            foreach (var features in Licensing.LicenseManager.GetFeatureCollection())
            {
                foreach(var feature in features.Value)
                {
                    Console.Write("{0} feature {1}-{2} count {3}/{4} exp:{5}\n",
                        features.Key.ToString(),
                        feature.Name,
                        feature.Version,
                        feature.Count,
                        feature.AvailableAcquisitionCount,
                        feature.IsPerpetual ? "perperual" : feature.Expiration.Value.ToString("yyyy-MM-dd"));
                }
            }
        }

        public void displayAvailableLicenses(String filename)
        {
            Console.WriteLine("available license in file {0}...", filename);
            var featureCollection = Licensing.LicenseManager.GetFeatureCollectionFromFile(LicenseSourceType.FLX_LICENSE_SOURCE_BUFFER, filename);

            var it = featureCollection.GetEnumerator();

            while (it.MoveNext())
            {
                Console.Write("\tfeature {0}-{1} count {2}/{3} exp:{4}\n",
                    it.Current.Name,
                    it.Current.Version,
                    it.Current.Count,
                    it.Current.AvailableAcquisitionCount,
                    it.Current.IsPerpetual ? "perperual" : it.Current.Expiration.Value.ToString("yyyy-MM-dd"));
            }
        }

        /// <summary>
        /// Test whether a feature is avaialable in the licens collection
        /// </summary>
        /// <param name="featureName"></param>
        /// <param name="featureVersion"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public bool testFeature(string featureName, string featureVersion, int count = 1)
        {
            var license = Licensing.LicenseManager.Acquire(featureName, featureVersion, count);
            if (license != null)
            {
                // return licenes to FNE
                license.ReturnLicense();

                return true;
            }
            else
            {
                return false;
            }
        }
    }
    class Program
    {
        static String prompt(String prompt)
        {
            String text = "";

            while (text == "")
            {
                Console.Write("{0} : ", prompt);
                text = Console.ReadLine();
            }
            return text;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Demo starting...");
            FneClient client = new FneClient();
            try
            {
                var hostId = prompt("Please enter the Nova hostid");

                var directory = prompt("Please enter the license file directory");

                // You need to map between features in the licenses and digital modules - this is on way
                // make a list of sku -> feature mappings
                // Item1 = sku
                // Item2 = feature name
                // Item3 = feature version
                var mapping = new List<Tuple<string, string, string>>()
                {
                    new Tuple<string, string, string>("LV850002", "EnergyPerPhase", "1.0"),
                    new Tuple<string, string, string>("LV850003", "WaveformCaptureOnTripEvent", "1.0"),
                    new Tuple<string, string, string>("LV850004", "PowerRestorationAssistant", "1.0"),
                    new Tuple<string, string, string>("LV850005", "MasterpactOperationAssistant", "1.0")
                };

                // set the identity...
                // this can be downloaded from FNO-C
                client.Identity = IdentityData.IdentityClient.IdentityData;

                //set the the hostid
                client.Hostid = hostId;

                Console.WriteLine();
                Console.WriteLine("client initializing ...");
                client.initalize();

                Console.WriteLine();
                Console.WriteLine("viewing license files...");
                foreach (var file in Directory.EnumerateFiles(directory, "*.bin", SearchOption.TopDirectoryOnly))
                {
                    client.displayAvailableLicenses(file);
                }

                // add in the licenses as 
                Console.WriteLine();
                Console.WriteLine("adding license files to client...");
                foreach (var file in Directory.EnumerateFiles(directory, "*.bin", SearchOption.TopDirectoryOnly))
                {
                    client.addLicenseFile(file);
                }
                
                // display all available features
                Console.WriteLine();
                Console.WriteLine("The following features have been loaded...");
                client.displayAvailableLicenses();

                // acquire features
                Console.WriteLine();
                Console.WriteLine("The following digital modules are permissioned...");
                foreach(var item in mapping)
                {
                    if (client.testFeature(item.Item2, item.Item3))
                    {
                        Console.WriteLine("feature {0} {1} for sku {2} is present", item.Item2, item.Item3, item.Item1);
                    }
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("Exception caught {0}", e.Message);
            }
            finally
            {
                Console.WriteLine();
                Console.WriteLine("client terminaing ...");
                client.terminate();
            }
        }
    }
}
